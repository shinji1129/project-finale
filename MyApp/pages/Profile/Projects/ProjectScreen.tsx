import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions, TextInput, Alert, Image, Modal } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../../redux/store';
import { config } from '../../../config';
import BlackWhiteLinearGradient from '../../../components/BlackWhiteLinearGradient';
import InvitationGradient from '../../../components/InvitationGradient';
import { useNavigation } from '@react-navigation/native';
import { currentProjectStatus, setProjectStatusThunk } from '../../../redux/project/thunk';
import { useForm, Controller } from 'react-hook-form';
import { addCommentThunk, isCommentThunk } from '../../../redux/request/comment/thunk';
import CommentBox from '../../../components/commentbox';
import { Artworks } from '../../../redux/profile/artwork/state';
import ImageViewer from 'react-native-image-zoom-viewer';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { addRatingThunk } from '../../../redux/request/rating/thunk';


const FONT_SIZE = Dimensions.get('window').width * 0.038;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


const ProjectScreen: React.FC = ({ route }: any) => {
    const dispatch = useDispatch();
    const token = useSelector((state: IRootState) => state.auth.token);
    // ProjectStatus
    const projectStatus = useSelector((status: IRootState) => status.project.status);
    const applicantsList = useSelector((status: IRootState) => status.project.applicantList);
    const invitedList = useSelector((status: IRootState) => status.project.invitedList);

    const [isCommented, setIsCommented] = useState(false);
    const isCommentBefore = useSelector((state: IRootState) => state.comment.isComment)
    const isBothComment = useSelector((state: IRootState) => state.comment.isBothComment)

    //Image
    const [requestImages, setRequestImages] = useState<Artworks[]>([])
    const [modalVisible, setModalVisible] = useState(false);
    const [indexOfImage, setIndexOfImage] = useState(0);


    //Rating
    const [rating, setRating] = useState(0);

    const userId = useSelector((state: IRootState) => state.auth.user?.id)


    const navigation = useNavigation()

    const [expectBudget, setExpectBudget] = useState('');

    const { request_id, requester, title, tag, time_start, duration, venue, budget_min, budget_max, freelancer_id, details, freelancer, confirmed_budget, expiry_date, status } = route.params;


    const fetchRequestImages = async (request_id: number) => {
        const res = await fetch(`${config.BACKEND_URL}/request/images/${request_id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        })
        const requestImgObj = await res.json();
        setRequestImages(requestImgObj);
    }

    const statusConverter = (status: number | null) => {
        if (status === null) {
            return
        }
        switch (status) {
            case 1:
                return "Pending"
            case 2:
                return "Pending"
            case 3:
                return "Assigned"
            case 4:
                return "Awaiting confirmation"
            case 5:
                return "Completed"
            case 6:
                return "Commented"
            default:
                return ""
        }
    }

    useEffect(() => {
        if (!token || !userId) { return }
        dispatch(isCommentThunk(token, userId, request_id))
    }, [isCommented])

    useEffect(() => {
        if (token) {
            dispatch(currentProjectStatus(token, request_id))

        }
        if (userId && token) {
            dispatch(isCommentThunk(token, userId, request_id))
        }
        fetchRequestImages(request_id)
    }, [projectStatus])

    const applicantStatus = useSelector((state: IRootState) => state.applicant.status)
    const invitedStatus = useSelector((state: IRootState) => state.invited.status);

    useEffect(() => {
        if (token) {
            dispatch(currentProjectStatus(token, request_id))
        }
    }, [applicantStatus, invitedStatus])

    useEffect(() => {
        const loadData = navigation.addListener('focus', () => {
            if (token) {
                dispatch(currentProjectStatus(token, request_id))
            }
        })

        return loadData
    }, [navigation])

    const { handleSubmit, register, errors, control } = useForm();
    const onSubmit = (values: Record<string, any>) => {
        if (!token) { return }
        if (!userId) { return }
        if (rating === 0) {
            return Alert.alert(
                "Rating",
                "Rating is required",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        return (
            dispatch(
                addCommentThunk(
                    token,
                    request_id,
                    values.comment
                ),
            ),
            setIsCommented(true),
            dispatch(
                addRatingThunk(
                    token,
                    request_id,
                    rating
                )
            ),
            console.log('comment', values.comment),
            console.log('rating', rating)
        );
    };

    const onCancel = () => {
        return console.log('cancel is pressed');
    };


    let images: Array<{ url: string, props: any }> = [];
    requestImages.map((requestImage, idx) => {
        images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
    })

    useEffect(() => {
        let images: Array<{ url: string, props: any }> = [];
        requestImages.map((requestImage, idx) => {
            images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
        })
        console.log('request Image inside useEffect:', requestImages)
    }, [requestImages])

    const confirmationHandler = async () => {
        if (token) {
            dispatch(setProjectStatusThunk(token, request_id))
        }
    }

    return (
        <SafeAreaView >
            <BlackWhiteLinearGradient>
                <ScrollView style={{ height: 'auto', width: WINDOW_WIDTH }}>
                    <View style={styles.container}>
                        <View>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                {projectStatus === 5 && isCommented === false && isCommentBefore === false && isBothComment === false && (
                                    <>
                                        <View style={styles.invitation}>
                                            <InvitationGradient>
                                                <View>
                                                    <Text style={styles.text}>Comment:</Text>
                                                    <Controller
                                                        control={control}
                                                        render={({ onChange, onBlur, value }) => (
                                                            <TextInput
                                                                multiline={true}
                                                                autoCapitalize="none"
                                                                autoCorrect={false}
                                                                style={styles.input}
                                                                onBlur={onBlur}
                                                                onChangeText={(value) => onChange(value)}
                                                                value={value}
                                                            />
                                                        )}
                                                        name="comment"
                                                        rules={{ required: true }}
                                                        defaultValue=""
                                                    />
                                                    {errors.comment && (
                                                        <Text style={styles.errorMessage}>This field is required.</Text>
                                                    )}
                                                    <Text style={styles.text}>Rating:</Text>
                                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                        {rating === 0 ?
                                                            <TouchableOpacity onPress={() => setRating(1)}>
                                                                <AntDesign
                                                                    style={styles.emptyStarIcon}
                                                                    name={"staro"}
                                                                    size={24}
                                                                    color={"#000000"}>
                                                                </AntDesign>
                                                            </TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => setRating(1)}>
                                                                <AntDesign
                                                                    style={styles.ratingIcon}
                                                                    name={"star"}
                                                                    size={24}
                                                                    color={"#ffff00"}>
                                                                </AntDesign>
                                                            </TouchableOpacity>
                                                        }
                                                        {rating <= 1 ?
                                                            <TouchableOpacity onPress={() => setRating(2)}>
                                                                <AntDesign
                                                                    style={styles.emptyStarIcon}
                                                                    name={"staro"}
                                                                    size={24}
                                                                    color={"#000000"}>
                                                                </AntDesign>
                                                            </TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => setRating(2)}>
                                                                <AntDesign
                                                                    style={styles.ratingIcon}
                                                                    name={"star"}
                                                                    size={24}
                                                                    color={"#ffff00"}>
                                                                </AntDesign>
                                                            </TouchableOpacity>
                                                        }
                                                        {rating <= 2 ?
                                                            <TouchableOpacity onPress={() => setRating(3)}>
                                                                <AntDesign
                                                                    style={styles.emptyStarIcon}
                                                                    name={"staro"}
                                                                    size={24}
                                                                    color={"#000000"}>
                                                                </AntDesign>
                                                            </TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => setRating(3)}>
                                                                <AntDesign
                                                                    style={styles.ratingIcon}
                                                                    name={"star"}
                                                                    size={24}
                                                                    color={"#ffff00"}>
                                                                </AntDesign>
                                                            </TouchableOpacity>
                                                        }
                                                        {rating <= 3 ?
                                                            <TouchableOpacity onPress={() => setRating(4)}>
                                                                <AntDesign
                                                                    style={styles.emptyStarIcon}
                                                                    name={"staro"}
                                                                    size={24}
                                                                    color={"#000000"}>
                                                                </AntDesign>
                                                            </TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => setRating(4)}>
                                                                <AntDesign
                                                                    style={styles.ratingIcon}
                                                                    name={"star"}
                                                                    size={24}
                                                                    color={"#ffff00"}>
                                                                </AntDesign>
                                                            </TouchableOpacity>
                                                        }
                                                        {rating <= 4 ?
                                                            <TouchableOpacity onPress={() => setRating(5)}>
                                                                <AntDesign
                                                                    style={styles.emptyStarIcon}
                                                                    name={"staro"}
                                                                    size={24}
                                                                    color={"#000000"}>
                                                                </AntDesign>
                                                            </TouchableOpacity> :
                                                            <TouchableOpacity onPress={() => setRating(5)}>
                                                                <AntDesign
                                                                    style={styles.ratingIcon}
                                                                    name={"star"}
                                                                    size={24}
                                                                    color={"#ffff00"}>
                                                                </AntDesign>
                                                            </TouchableOpacity>
                                                        }
                                                    </View>
                                                    <View style={styles.rows}>
                                                        <TouchableOpacity style={styles.cancel} onPress={onCancel}>
                                                            <Text style={styles.buttonText}>Cancel</Text>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity
                                                            style={styles.submit}
                                                            onPress={handleSubmit(onSubmit)}>
                                                            <Text style={styles.buttonText}>confirm</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </InvitationGradient>
                                        </View>
                                    </>
                                )}
                                {projectStatus === 3 && (
                                    <>
                                        <View style={styles.rows}>
                                            <View style={styles.invitation}>
                                                <InvitationGradient>
                                                    <View >
                                                        <Text style={styles.text}>You could send Confirmation to {requester.charAt(0).toUpperCase() + requester.slice(1)} once the job is finished and the payment is settled. </Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-evenly', alignItems: 'center', marginTop: 10, }}>

                                                        <TouchableOpacity style={styles.accept} onPress={confirmationHandler}>
                                                            <Text style={styles.buttonText}> send Confirmation </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </InvitationGradient>
                                            </View>
                                        </View>
                                    </>
                                )}
                            </View>
                            <View style={{ padding: 15 }}>
                                <Text style={styles.titleText}>{title}</Text>
                                {requester.length >= 1 && <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.text}>Job posted by:</Text>
                                    <Text style={styles.requester}>{requester}</Text>
                                </View>}

                                <Text style={{ fontFamily: 'Varela Round', paddingLeft: 5, marginTop: 8 }}>Require:</Text>
                                {tag && tag.length >= 1 && <View style={styles.tagBox}>{tag.map((string: string, idx: number) => {
                                    return (
                                        <View key={`tag_${idx}`} style={styles.tag}>
                                            <Text style={styles.tagText} key={`tag_${idx}`}>{string.charAt(0).toUpperCase() + string.slice(1)}</Text>
                                        </View>
                                    )
                                })}
                                </View>
                                }
                            </View>
                        </View>
                        <View style={styles.border}></View>
                        <View style={{ padding: 15 }}>

                            <View style={styles.middleHalf}>
                                {(budget_min && status < 3) &&
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={styles.text}>Budget:</Text>
                                        <Text style={styles.moneyText}>HKD${budget_min} ~ {budget_max}</Text>
                                    </View>
                                }
                                {status >= 3 && (
                                    <Text style={styles.text}>Confirmed Budget: <Text style={styles.moneyText}>HKD${confirmed_budget}</Text></Text>
                                )}
                                {time_start !== new Date(0).toDateString() && time_start.length >= 1 && <Text style={styles.text}>Date: {time_start}</Text>}
                                {duration && <Text style={styles.text}>To be completed in: {duration} days </Text>}
                                {venue && <Text style={styles.text}>Venue: {venue} </Text>}

                                {expiry_date && <Text style={styles.text}>Expiry date: {expiry_date} </Text>}
                                <Text style={styles.text}>Status: {statusConverter(projectStatus)} </Text>
                                {projectStatus !== null && projectStatus > 2 && <Text style={styles.text}>Assigned to: {freelancer} </Text>}
                            </View>
                            <Text style={styles.text}>Details {details} </Text>
                            <Text style={styles.text}>Reference Image: </Text>
                            <View style={{
                                marginTop: 10,
                                paddingHorizontal: 5,
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                maxHeight: 440,
                                overflow: 'scroll',
                            }}>
                                {requestImages.length >= 1 &&
                                    <>
                                        {requestImages.map((requestImage, index) => (
                                            <TouchableOpacity key={`requestImage_${index}`} onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                                                <Image
                                                    style={{
                                                        height: 100,
                                                        width: 100,
                                                        marginRight: 10,
                                                        marginBottom: 10,
                                                    }}
                                                    source={{
                                                        uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`,
                                                    }}
                                                />
                                            </TouchableOpacity>
                                        ))}
                                    </>}
                            </View>
                        </View>
                        <Modal visible={modalVisible} transparent={true}>
                            <View style={{ backgroundColor: 'black', height: 'auto', position: 'absolute', top: 50, right: 15, zIndex: 999 }}>
                            </View>
                            <ImageViewer
                                imageUrls={images}
                                menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                                onChange={(index) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                                index={indexOfImage}
                                enableSwipeDown={true}
                                enablePreload={true}
                                onSwipeDown={() => {
                                    setModalVisible(false)
                                }} />
                        </Modal>
                        {projectStatus !== null && projectStatus <= 2 && applicantsList.length >= 1 && (
                            <View style={styles.rows}>
                                <TouchableOpacity style={styles.applicantList} onPress={() => {
                                    navigation.navigate('ApplicantList', {
                                        applicantsList: applicantsList,
                                        requestID: request_id
                                    })

                                }}>
                                    <Text style={styles.buttonText}>freelancer applied: {applicantsList.length} </Text>
                                </TouchableOpacity>
                            </View>
                        )}
                        {projectStatus !== null && projectStatus <= 2 &&
                            <View style={styles.rows}>
                                <TouchableOpacity style={styles.invitedList} onPress={() => {
                                    navigation.navigate('InvitedList', {
                                        invitedList: invitedList,
                                        requestID: request_id
                                    })
                                }}>
                                    <Text style={styles.buttonText}> freelancer invited: {invitedList.length} </Text>
                                </TouchableOpacity>
                            </View>}
                        {isBothComment === true && <CommentBox requestId={request_id} />}
                        <View style={styles.pageEndButtons}>
                        </View>
                    </View>
                </ScrollView>
            </BlackWhiteLinearGradient>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: WINDOW_HEIGHT,
        justifyContent: 'flex-start',
        alignContent: 'center',
        flexWrap: "wrap",
        flexDirection: "column",
    },
    requestBoard: {
        width: Dimensions.get('window').width * 0.9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginVertical: 10,
        backgroundColor: '#C4FFF9', // color for jobs
        borderRadius: 7,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.4,
        shadowRadius: 6,

    },
    titleText: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.45,
        textTransform: 'capitalize',
        borderRadius: 5,
        padding: 5,
        width: WINDOW_WIDTH * 0.9,
    },
    text: {
        maxWidth: '95%',
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE,
        borderRadius: 5,
        padding: 5,
        color: '#090909',
    },
    moneyText: {
        fontFamily: 'Varela Round',
        fontWeight: "900",
        fontSize: FONT_SIZE * 1.2,
        borderRadius: 5,
        padding: 5,
        color: '#d06',
    },
    requester: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.40,
        textTransform: 'capitalize',
    },
    tagBox: {
        marginTop: 15,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    border: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: WINDOW_WIDTH * 0.9,
        borderBottomColor: '#7775',
        borderBottomWidth: 1,
        paddingBottom: 18,
        justifyContent: 'flex-start',
    },
    middleHalf: {
        marginTop: 15,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: 1
    },
    pageEndButtons: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30

    },
    rows: {
        marginTop: WINDOW_HEIGHT * 0.015,
        minWidth: WINDOW_WIDTH * 0.5,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    buttonText: {
        paddingHorizontal: 5,
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.9,
    },
    chatBox: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    errorMessage: {
        fontFamily: 'Varela Round',
        color: 'red',
    },
    applicantList: {
        backgroundColor: '#rgb(0,184,155)',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },

    invitedList: {
        backgroundColor: '#548EF6',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },

    invitation: {
        maxWidth: WINDOW_WIDTH * 0.9,
        marginVertical: 15,
        borderRadius: 6,
        shadowColor: '#0009',
        shadowOffset: { width: 3, height: 7 },
        shadowOpacity: 1,
        shadowRadius: 8
    },
    accept: {
        width: '100%',
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    decline: {
        width: '100%',
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    bookmark: {
        top: '-12%',
        height: 43,
        color: '#7BFEFF',
        textShadowColor: '#0007',
        shadowOpacity: 0.4,
        textShadowRadius: 6,
        textShadowOffset: { width: 0, height: 8 },
        zIndex: 1,
    },

    input: {
        padding: 5,
        marginTop: 10,
        minHeight: FONT_SIZE + 20,
        width: WINDOW_WIDTH * 0.8,
        borderColor: '#888',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
    },

    apply: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },

    submit: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: Dimensions.get('window').width * 0.3,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },

    cancel: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: Dimensions.get('window').width * 0.3,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        backgroundColor: '#BF4848',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },

    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
    },
    
    emptyStarIcon: {
        color: '#ccc'
    },
});

export default ProjectScreen;