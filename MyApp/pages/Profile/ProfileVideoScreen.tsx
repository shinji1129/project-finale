import React from 'react';
import { View, Text, SafeAreaView, Dimensions, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import VideoPlayer from 'react-native-video-player';


const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


const ProfileVideoScreen: React.FC = () => {
  const video = useSelector((state: IRootState) => state.video.video);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
        <View style={{
          maxWidth: WINDOW_WIDTH,
          height: WINDOW_HEIGHT * 0.9,
          overflow: 'hidden',
        }}>
          {video.length !== 0 && (
            <>
              {video.map((videoObject, index) => (
                <View key={`video_${index}`} style={{ marginVertical: 15 }}>
                  <Text style={styles.title}>{videoObject.description && videoObject.description.length > 1 ? videoObject.description : 'No Description'}</Text>
                  <VideoPlayer
                    video={{
                      uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${videoObject.filename}`
                    }}
                    videoWidth={1600}
                    videoHeight={900}
                    thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
                  />
                </View>
              ))}
            </>
          )}
          {video.length == 0 && (
            <Text style={{ color: 'grey' }}>
              User did not upload any artworks.
            </Text>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default ProfileVideoScreen;


const styles = StyleSheet.create({
  ArtworkBox: {
    width: WINDOW_WIDTH * 0.9,
    height: 'auto',
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,

    marginTop: 16,
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ddd',
    borderWidth: 1,
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  artworkTextContainer: {
    width: WINDOW_WIDTH,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewArtworkText: {
    width: '100%',
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: '#8885',
    fontFamily: 'Varela Round',
    color: '#fff',
    position: 'absolute',
    fontSize: 20,
    bottom: 150,
    textShadowRadius: 3,
    textShadowColor: '#000',
    textShadowOffset: { width: 0, height: 0 },
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
  },
  title: {
    paddingVertical: 5,
    marginLeft: 12,
    marginVertical: 8,
    fontFamily: 'Syncopate',
    fontSize: 16,
    color: '#444',
  },
})