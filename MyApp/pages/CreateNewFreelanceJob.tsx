import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions, ScrollView, Platform, Button, Image, Alert } from 'react-native';
import { TouchableOpacity, TextInput, FlatList } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from 'react-hook-form';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ImagePicker from 'react-native-image-crop-picker';
import { newProjectThunk } from '../redux/request/newRequest/thunk'
import Icon from 'react-native-vector-icons/AntDesign';
import { config } from '../config';
import { SearchBar } from 'react-native-elements';
import LinearGradientCreateJob from '../components/LinearGradientCreateJob';
import SettingScreenLinearGradient from '../components/SettingScreenLinearGradient';
import { KeyboardAwareFlatList } from '@codler/react-native-keyboard-aware-scroll-view';
import { newProjectProcessing, newProjectFinishProcessing } from '../redux/request/newRequest/actions';


const FONT_SIZE = Dimensions.get('window').width * 0.04;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const BUTTON_WIDTH = Dimensions.get('window').width * 0.3;
const BUTTON_HEIGHT = FONT_SIZE * 2.5;
const INPUT_BOX_WIDTH = Dimensions.get('window').width * 0.75;

const NewProjectScreen: React.FC = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const token = useSelector((state: IRootState) => state.auth.token);
    const { handleSubmit, errors, control, reset } = useForm();
    const [projectPicArr, setProjectPicArr] = useState([]);
    const [uploadProjectPic, setUploadProjectPic] = useState([{
        path: '',
        size: 0,
        width: 0,
        height: 0,
        mime: '',
    }]);
    const [isProjectPic, setIsProjectPic] = useState(false);
    const [category, setCategory] = useState(0);
    const [expiryDate, setExpiryDate] = useState('');
    const [timeStart, setTimeStart] = useState('');
    const [query, setQuery] = useState('');
    const [data, setData] = useState([]);
    const [tags, setTags] = useState([]);
    const [tagArr, setTagArr] = useState<string[]>([])
    const [tagIDArr, setTagIDArr] = useState<number[]>([])

    const updateQuery = async (input: string) => {
        setTags(data.slice());
        setQuery(input);
    }

    interface Tag {
        id: number,
        tag: string
    }

    const filterTags = (tag: Tag) => {
        let search = query.toLowerCase()
        if (tag['tag'].startsWith(search)) {
            return formatTags(tag);
        } else {
            //@ts-ignore
            tags.splice(tags.indexOf(tag), 1);
            return null;
        }
    }

    const formatTags = (tag: Tag) => {
        let tagName = tag['tag'].charAt(0).toUpperCase() + tag['tag'].slice(1);
        return tagName;
    }

    const fetchTag = async () => {
        try {
            const res = await fetch(`${config.BACKEND_URL}/request/tags`);
            const json = await res.json();
            setData(json);
            setTags(json.slice());
        } catch (err) {
            console.log(err);
        } 
    };



    const renderItem = ({ item }: any) => {
        if (filterTags(item) !== null) {
            return (
                <TouchableOpacity
                    onPress={() => {
                        const tempArr = tagArr.slice();
                        //@ts-ignore
                        tempArr.push(item.tag)
                        const newArr = tempArr.reduce((unique, item) => {
                            return unique.includes(item) ? unique : [...unique, item]
                        }, [] as string[])
                        setTagArr(newArr)

                        const tmpIDArr = tagIDArr.slice();
                        //@ts-ignore
                        tmpIDArr.push(item.id)
                        const newIDArr = tmpIDArr.reduce((unique, item) => {
                            return unique.includes(item) ? unique : [...unique, item]
                        }, [] as number[])
                        setTagIDArr(newIDArr)
                        setQuery('');
                    }}>
                    {query !== '' &&
                        <>
                            <View style={styles.searchTagContainer}>
                                <Text style={styles.flatList}>{filterTags(item)}</Text>
                            </View>
                        </>
                    }
                </TouchableOpacity>

            )

        } else {
            return <Text style={{ display: 'none' }}>{null}</Text>
        }
    }

    useEffect(() => {
        const loadData = navigation.addListener('focus', () => {
            dispatch(newProjectProcessing())
            fetchTag();
            // hard code setTimeout for now
            setTimeout(() => {
                dispatch(newProjectFinishProcessing());
            }, 1000)
        })
        return loadData;
    }, [navigation])


    const onSubmit = (values: Record<string, any>) => {
        if (!token) {
            return
        }
        // Checking
        if (values.budget_min && values.budget_max) {
            if (isNaN(parseInt(values.budget_min)) || isNaN(parseInt(values.budget_min))) {
                Alert.alert("Invalid input",
                    "Ranges only accept number",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
                return;
            }

            if (parseInt(values.budget_min) > parseInt(values.budget_max)) {
                Alert.alert("Invalid input",
                    "Please input valid range",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
                return;
            }
        }

        if (category === 0 || category === null) {
            Alert.alert("Invalid input",
                "Please select category",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
            return;
        }

        if (tagIDArr === []) {
            Alert.alert("Invalid input",
                "Please input Tag",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
            return;
        }

        return (
            dispatch(
                newProjectThunk(
                    token,
                    {
                        title: values.title,
                        tags: tagIDArr,
                        category: category,
                        duration: parseInt(values.duration),
                        expiryDate: expiryDate,
                        details: values.details,
                        venue: values.venue,
                        company_name: values.company,
                        min_budget: parseInt(values.budget_min),
                        max_budget: parseInt(values.budget_max),
                        time_start: timeStart,
                        images: uploadProjectPic,
                    },
                ),
            ), (
                setTagArr([]),
                setTagIDArr([]),
                setQuery(''),
                setCategory(0),
                reset()
            ),
            Alert.alert(" Project submitted",
                "",
                [
                    { text: "OK", onPress: () => navigation.navigate('Profile_Tab') }
                ],
                { cancelable: false }
            )
        );
    };


    const CategoryDropdown = () => {
        const categoryArr = [
            { label: 'Design', value: 1 },
            { label: 'Programming', value: 2 },
            { label: 'Make-up', value: 3 },
            { label: 'Translation', value: 4 },
            { label: 'Creative Writing', value: 5 },
            { label: 'Music', value: 6 },
            { label: 'Photography', value: 7 },
            { label: 'Video Related', value: 8 },
        ]

        return (
            <RNPickerSelect
                placeholder={{
                    label: 'Select a category'
                }}
                style={{
                    inputIOS: {
                        borderColor: '#aaa',
                        justifyContent: 'center',
                        alignItems: 'center',
                        textAlign: 'center',
                        fontFamily: 'Syncopate',
                        borderWidth: 1,
                        borderRadius: 6,
                        height: FONT_SIZE + 20,
                        width: INPUT_BOX_WIDTH,
                        fontSize: FONT_SIZE
                    }
                }}
                onValueChange={(value) => {
                    setCategory(value)
                }} items={categoryArr} value={category}
            />
        )
    }

    const ExpiryDateDropdown = () => {
        const date = new Date()
        const dateArr = []

        for (let i = 0; i < 7; i++) {
            const temp = new Date(date)
            temp.setDate(date.getDate() + 1 * i)
            dateArr.push({ label: temp.toDateString(), value: temp.toDateString() })
        }
        return (
            <RNPickerSelect
                placeholder={{
                    label: 'Select date'
                }}
                style={{
                    inputIOS: {
                        marginTop: 8,
                        textAlign: 'center',
                        fontFamily: 'Syncopate',
                        borderColor: '#aaa',
                        borderWidth: 1,
                        width: INPUT_BOX_WIDTH,
                        height: FONT_SIZE + 20,
                        borderRadius: 6,
                        fontSize: FONT_SIZE

                    }
                }}
                onValueChange={(value) => {
                    setExpiryDate(value)
                }} items={dateArr} value={expiryDate} />
        )
    }

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date: any) => {
        setTimeStart(date.toDateString())
        hideDatePicker();
    };

    return (

        <SafeAreaView style={styles.SafeAreaView}>
            <View style={styles.header}>
                <Text style={styles.headerText}>Create new freelance job</Text>
            </View>
            <LinearGradientCreateJob>
                <View style={styles.newProject}>
                    {<KeyboardAwareFlatList
                        extraHeight={100}
                        enableResetScrollToCoords={false}
                        style={{ height: Dimensions.get('window').height }} data={tags} keyExtractor={(item: any) => item.tag.toString()}
                        extraData={query}
                        renderItem={renderItem}
                        showsVerticalScrollIndicator={false}
                        ListHeaderComponent={
                            <>
                                <View style={{ width: WINDOW_WIDTH, alignItems: 'center', justifyContent: 'flex-start' }}>
                                    <View style={styles.divContainer}>
                                        <SettingScreenLinearGradient>
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Category </Text>
                                                    <View>
                                                        <CategoryDropdown />
                                                    </View>
                                                </View>

                                                <View style={{ margin: 10 }}></View>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Tags </Text>

                                                    {tagArr !== [] &&
                                                        <>
                                                            <View style={styles.tagBox}>
                                                                {tagArr.map((tagObject: any, idx: number) => (
                                                                    <View key={`tag_${idx}`} >
                                                                        <View style={styles.tag}>
                                                                            <Text style={styles.tagText}>
                                                                                {tagObject.charAt(0).toUpperCase() + tagObject.slice(1)}
                                                                            </Text>
                                                                            <View style={styles.cancelTagContainer} >
                                                                                <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => {
                                                                                    const tempArr = tagArr.slice();
                                                                                    tempArr.splice(idx, 1);
                                                                                    setTagArr(tempArr);
                                                                                    const tmpIDArr = tagIDArr.slice();
                                                                                    tmpIDArr.splice(idx, 1);
                                                                                    setTagIDArr(tmpIDArr);
                                                                                }}>
                                                                                    <Icon name='closecircle' size={15} style={styles.cancelTag} />
                                                                                </TouchableOpacity>
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                ))}
                                                            </View>
                                                        </>
                                                    }
                                                    <View>
                                                        <SearchBar
                                                            style={{
                                                                fontSize: FONT_SIZE * 0.9,
                                                                fontFamily: 'syncopate'
                                                            }}
                                                            containerStyle={styles.searchBar}
                                                            platform="ios"
                                                            onChangeText={updateQuery}
                                                            value={query}
                                                            placeholder="Search tag here"
                                                            showCancel={false}
                                                            //@ts-ignore
                                                            clearIcon={null}
                                                            cancelButtonProps={{ buttonStyle: { display: 'none' } }}
                                                        />
                                                    </View>
                                                </View>
                                            </View>
                                        </SettingScreenLinearGradient>
                                    </View>
                                </View>
                            </>
                        }
                        ListFooterComponent={
                            <>
                                <View style={{ width: WINDOW_WIDTH, alignItems: 'center', justifyContent: 'flex-start' }}>
                                    <View style={styles.divContainer}>
                                        <SettingScreenLinearGradient>
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Title </Text>
                                                    <Controller
                                                        control={control}
                                                        render={({ onChange, onBlur, value }) => (
                                                            <TextInput
                                                                autoCapitalize="none"
                                                                autoCorrect={false}
                                                                style={[styles.input]}
                                                                onBlur={onBlur}
                                                                onChangeText={(value) => {
                                                                    onChange(value);
                                                                }}
                                                                value={value}
                                                            />
                                                        )}
                                                        name="title"
                                                        rules={{
                                                            required: true,
                                                            pattern: {
                                                                value: /^[^!*#%+=,?.|\~":<>[\]{}\\()';@&$]+$/,
                                                                message: 'Special characters other than - or _ are not allowed',
                                                            },
                                                        }}
                                                        defaultValue=""
                                                    />
                                                    {errors.title && (
                                                        <Text style={styles.errorMessage}>
                                                            Please input a valid title.
                                                        </Text>
                                                    )}
                                                </View>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Job Description </Text>
                                                    <Controller
                                                        control={control}
                                                        render={({ onChange, onBlur, value }) => (
                                                            <TextInput
                                                                multiline={true}
                                                                autoCapitalize="none"
                                                                autoCorrect={false}
                                                                style={styles.detail}
                                                                onBlur={onBlur}
                                                                onChangeText={(value) => {
                                                                    onChange(value);
                                                                }}
                                                                value={value}
                                                            />
                                                        )}
                                                        name="details"
                                                        rules={{
                                                            required: true,
                                                        }}
                                                        defaultValue=""
                                                    />
                                                    {errors.detail && (
                                                        <Text style={styles.errorMessage}>
                                                            Details required.
                                                        </Text>
                                                    )}
                                                </View>

                                                <View style={styles.pairContainer}>

                                                    <Text style={styles.text}> Company name (Optional) </Text>
                                                    <Controller
                                                        control={control}
                                                        render={({ onChange, onBlur, value }) => (
                                                            <TextInput
                                                                autoCapitalize="none"
                                                                autoCorrect={false}
                                                                style={styles.input}
                                                                onBlur={onBlur}
                                                                onChangeText={(value) => {
                                                                    onChange(value);
                                                                }}
                                                                value={value}
                                                            />
                                                        )}
                                                        name="company"
                                                        rules={{
                                                            required: false,
                                                        }}
                                                        defaultValue=""
                                                    />
                                                </View>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Event Venue (Optional) </Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Controller
                                                            control={control}
                                                            render={({ onChange, onBlur, value }) => (
                                                                <TextInput
                                                                    multiline={true}
                                                                    autoCapitalize="none"
                                                                    autoCorrect={false}
                                                                    style={styles.inputVenue}
                                                                    onBlur={onBlur}
                                                                    onChangeText={(value) => {
                                                                        onChange(value);
                                                                    }}
                                                                    value={value}
                                                                />
                                                            )}
                                                            name="venue"
                                                            rules={{
                                                                required: false,
                                                            }}
                                                            defaultValue=""
                                                        />
                                                    </View>
                                                </View>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Budget Range in HKD$ (Optional)</Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Controller
                                                            control={control}
                                                            render={({ onChange, onBlur, value }) => (
                                                                <TextInput
                                                                    keyboardType="number-pad"
                                                                    multiline={true}
                                                                    autoCapitalize="none"
                                                                    autoCorrect={false}
                                                                    style={styles.budget}
                                                                    onBlur={onBlur}
                                                                    onChangeText={(value) => {
                                                                        onChange(value);
                                                                    }}
                                                                    value={value}
                                                                    placeholder={"Minimum budget"}
                                                                />
                                                            )}
                                                            name="budget_min"
                                                            rules={{
                                                                required: false,
                                                            }}
                                                            defaultValue=""
                                                        />
                                                        <Text style={{ alignSelf: 'center', paddingBottom: 10, fontSize: 20, fontFamily: 'Varela Round' }}> ~ </Text>
                                                        <Controller
                                                            control={control}
                                                            render={({ onChange, onBlur, value }) => (
                                                                <TextInput
                                                                    keyboardType="number-pad"
                                                                    multiline={true}
                                                                    autoCapitalize="none"
                                                                    autoCorrect={false}
                                                                    style={styles.budget}
                                                                    onBlur={onBlur}
                                                                    onChangeText={(value) => {
                                                                        onChange(value);
                                                                    }}
                                                                    value={value}
                                                                    placeholder={"Maximum budget"}
                                                                />
                                                            )}
                                                            name="budget_max"
                                                            rules={{
                                                                required: false,
                                                            }}
                                                            defaultValue=""
                                                        />
                                                    </View>
                                                </View>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Reference Picture (Optional)</Text>
                                                    <View style={styles.refPicContainer}>
                                                        <View style={styles.imageContainer}>
                                                            {isProjectPic &&
                                                                projectPicArr.map((projectPic, idx) => {
                                                                    return (
                                                                        <Image
                                                                            key={`image_${idx}`}
                                                                            source={{ uri: projectPic }}
                                                                            style={{ aspectRatio: 1, marginVertical: 8, resizeMode: 'contain', minWidth: 200, minHeight: 200, maxWidth: 250, maxHeight: 250 }}
                                                                            resizeMode={'cover'}
                                                                        />
                                                                    )
                                                                }
                                                                )}
                                                        </View>
                                                        {isProjectPic === false && <TouchableOpacity
                                                            style={styles.upload}
                                                            onPress={() => {
                                                                ImagePicker.openPicker({
                                                                    width: 200,
                                                                    height: 200,
                                                                    cropperCircleOverlay: false,
                                                                    avoidEmptySpaceAroundImage: true,
                                                                    cropping: false,
                                                                    multiple: true,
                                                                }).then((images) => {
                                                                    setUploadProjectPic(images);
                                                                    const sourceURLArry: any = images.map(img => img.sourceURL)
                                                                    setProjectPicArr(sourceURLArry);
                                                                    setIsProjectPic(true);
                                                                });
                                                            }}>
                                                            <Text style={styles.buttonText}> Upload </Text>
                                                        </TouchableOpacity>}
                                                        {isProjectPic === true && <TouchableOpacity
                                                            style={styles.cancel}
                                                            onPress={() => {
                                                                setProjectPicArr([]);
                                                                setIsProjectPic(false);
                                                            }
                                                            }>

                                                            <Text style={styles.buttonText}> Cancel </Text>
                                                        </TouchableOpacity>}
                                                    </View>
                                                </View>
                                            </View>
                                        </SettingScreenLinearGradient>
                                    </View>
                                    <View style={styles.divContainer}>
                                        <SettingScreenLinearGradient>
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> Starting Date (Optional) </Text>
                                                    {timeStart !== '' && <Text style={styles.timeStart}>{timeStart}</Text>}
                                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                        <View style={[styles.datePickButton]}>
                                                            <Text style={styles.datePickText} onPress={showDatePicker}> Select Date </Text>
                                                        </View>
                                                        <DateTimePickerModal
                                                            isVisible={isDatePickerVisible}
                                                            mode="date"
                                                            onConfirm={handleConfirm}
                                                            onCancel={hideDatePicker}
                                                        />
                                                    </View>
                                                    <View style={{ marginTop: 10 }}></View>
                                                    <Text style={styles.text}> Duration of job (days) </Text>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Controller
                                                            control={control}
                                                            render={({ onChange, onBlur, value }) => (
                                                                <TextInput
                                                                    keyboardType="number-pad"
                                                                    multiline={true}
                                                                    autoCapitalize="none"
                                                                    autoCorrect={false}
                                                                    style={[styles.input, { paddingTop: 10 }]}
                                                                    onBlur={onBlur}
                                                                    onChangeText={(value) => {
                                                                        onChange(value);
                                                                    }}
                                                                    value={value}
                                                                />
                                                            )}
                                                            name="duration"
                                                            rules={{
                                                                required: true,
                                                            }}
                                                            defaultValue=""
                                                        />
                                                    </View>
                                                    {errors.duration && (
                                                        <Text style={styles.errorMessage}>
                                                            Details required.
                                                        </Text>
                                                    )}
                                                </View>
                                            </View>
                                        </SettingScreenLinearGradient>
                                    </View>
                                    <View style={styles.divContainer}>
                                        <SettingScreenLinearGradient>
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <View style={styles.pairContainer}>
                                                    <Text style={styles.text}> {'Expiry date of post'}</Text>
                                                    <Text style={styles.subtext}> {'   (This job will not be searchable after this date)'}</Text>
                                                    <View>
                                                        <ExpiryDateDropdown />
                                                    </View>
                                                </View>
                                            </View>
                                        </SettingScreenLinearGradient>
                                    </View>
                                    <TouchableOpacity
                                        style={styles.submit}
                                        onPress={(handleSubmit(onSubmit))}>
                                        <Text style={styles.buttonText}> publish job </Text>
                                    </TouchableOpacity>
                                </View>
                            </>
                        }
                    />}
                </View>
            </LinearGradientCreateJob>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    SafeAreaView: {
        height: '100%',
        overflow: 'scroll'
    },
    newProject: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: WINDOW_WIDTH,
        overflow: 'scroll'
    },

    header: {
        marginVertical: 0,
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT * 0.05,
        paddingHorizontal: 10,
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,

    },
    headerText: {
        fontFamily: 'syncopate',
        fontSize: FONT_SIZE,
        color: '#000',
    },
    divContainer: {
        marginVertical: 12,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFE',
        borderRadius: 7,
        shadowColor: '#000a',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.8,
        shadowRadius: 6,
    },
    pairContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: WINDOW_WIDTH * 0.8,

    },
    text: {
        alignSelf: 'flex-start',
        paddingLeft: 10,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE,
        color: '#222',
        marginBottom: 5,
    },

    input: {
        height: FONT_SIZE + 20,
        width: INPUT_BOX_WIDTH,
        borderColor: '#999',
        borderStyle: 'solid',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
        marginBottom: 15,
    },
    inputVenue: {
        paddingTop: 10,
        paddingBottom: 8,
        minHeight: FONT_SIZE + 20,
        width: INPUT_BOX_WIDTH,
        borderColor: '#999',
        borderStyle: 'solid',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
        marginBottom: 15,
    },

    detail: {
        paddingTop: 10,
        paddingBottom: 10,
        minHeight: FONT_SIZE + 20,
        width: INPUT_BOX_WIDTH,
        borderColor: '#999',
        borderStyle: 'solid',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        marginBottom: 18,
        paddingLeft: 10,

    },

    tags: {
        minHeight: FONT_SIZE + 20,
        width: INPUT_BOX_WIDTH,
        borderColor: '#aaa',
        borderStyle: 'solid',
        borderRadius: 10,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
        flexWrap: 'wrap',

        fontSize: FONT_SIZE,
        color: 'black',
        marginBottom: 5,
        textAlign: 'center',
        paddingVertical: WINDOW_WIDTH * 0.02
    },

    inputIOS: {
        height: FONT_SIZE + 20,
        width: INPUT_BOX_WIDTH,
        borderWidth: 1,
        borderColor: '#aaa',
        borderRadius: 10,
        textAlign: 'center',
    },

    budget: {
        marginTop: 5,
        height: FONT_SIZE + 20,
        width: WINDOW_WIDTH * 0.37,
        borderColor: '#aaa',
        borderStyle: 'solid',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 8,
        paddingTop: 8,
        textAlign: 'center',
        marginBottom: 18,
    },

    timeStart: {
        height: FONT_SIZE + 20,
        width: INPUT_BOX_WIDTH,
        borderColor: '#aaa',
        borderStyle: 'solid',
        borderRadius: 6,
        borderWidth: 1,

        fontFamily: 'Syncopate',
        paddingHorizontal: 10,
        fontSize: FONT_SIZE,
        color: 'black',
        marginBottom: 5,
        textAlign: 'center',
        paddingVertical: WINDOW_WIDTH * 0.025
    },

    upload: {
        marginVertical: 10,
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: BUTTON_WIDTH,
        height: BUTTON_HEIGHT,
        borderRadius: 7,
        padding: 10,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },
    datePickButton: {
        marginTop: 10,
        backgroundColor: '#4EC9B0',
        fontSize: 20,
        width: BUTTON_WIDTH * 1.8,
        height: BUTTON_HEIGHT,
        borderRadius: 7,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
        marginBottom: 20,
    },
    datePickText: {
        width: '100%',
        height: '100%',
        paddingTop: 13,
        textAlign: 'center',
        color: 'white',
        textShadowColor: '#aaa',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 4,
        shadowColor: 'white',
        shadowOpacity: 4,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 4,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE,
    },

    cancel: {
        marginVertical: 10,
        backgroundColor: '#CD455A',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: BUTTON_WIDTH,
        height: BUTTON_HEIGHT,
        borderRadius: 7,
        padding: 10,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },

    buttonText: {
        color: 'white',
        textShadowColor: '#aaa',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 4,
        shadowColor: 'white',
        shadowOpacity: 4,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 4,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE,
    },
    subtext: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        alignSelf: 'flex-start'
    },
    refPicContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    imageContainer: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 'auto',
        marginVertical: 10,
    },

    submit: {
        marginTop: 40,
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        paddingHorizontal: 15,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
        marginBottom: 230,
    },

    searchTagContainer: {
        alignSelf: 'center',
        width: WINDOW_WIDTH * 0.8,
        backgroundColor: '#333c',
        margin: 4,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    flatList: {
        textAlign: 'center',
        paddingVertical: 10,
        fontSize: 16,
        fontFamily: 'Syncopate',
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },

    searchBar: {
        backgroundColor: 'transparent',
        width: INPUT_BOX_WIDTH,
    },

    tagBox: {
        paddingLeft: 8,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },

    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 8,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },

    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },

    cancelTagContainer: {
        position: 'absolute',
        top: -10,
        right: -12,
        width: 17,
        height: 17,
        backgroundColor: '#FFF',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cancelTag: {
        color: '#c9382f',
        fontSize: 17,
    },

    errorMessage: {
        fontFamily: 'Varela Round',
        color: 'red',
    },
    lowerHalfSection: {

    }
})

export default NewProjectScreen;