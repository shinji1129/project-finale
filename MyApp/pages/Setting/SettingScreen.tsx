import React, { useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Menu from '../../components/Menu';
import { useDispatch, useSelector } from 'react-redux';
import { logoutThunk } from '../../redux/auth/thunks';
import LogoLinearGradient from '../../components/LogoLinearGradient';
import { useNavigation, StackActions, CommonActions } from '@react-navigation/native';
import SettingLinearGradient from '../../components/SettingLinearGradient';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import IconFeather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { IRootState } from '../../redux/store';
import { getProfile } from '../../redux/profile/profile/thunk';
import { getArtwork } from '../../redux/profile/artwork/thunk';
import { getTags } from '../../redux/profile/tag/thunk';
import { getVideoByID } from '../../redux/profile/video/thunk';



const WINDOW_HEIGHT = Dimensions.get('window').height
const WINDOW_WIDTH = Dimensions.get('window').width
const SettingScreen: React.FC = (props) => {
    const navigation = useNavigation();
    const dispatch = useDispatch()
    const token = useSelector((state: IRootState) => state.auth.token);
    const userId = useSelector((state: IRootState) => state.auth.user?.id);


    const onLogout = () => {
        dispatch(logoutThunk());
    };

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (!token) {
                return
            }
            dispatch(getProfile(token))
            dispatch(getArtwork(token))
            dispatch(getTags(token))
            if (!userId) {
                return
            }
            dispatch(getVideoByID(token, userId))
        })

        return unsubscribe;
    }, [navigation])



    return (
        <SafeAreaView>
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',

            }}>
                <SettingLinearGradient>
                    <View style={styles.textContainer}>
                        <TouchableOpacity
                            style={styles.buttonText}
                            onPress={() => {
                                navigation.navigate('Preview Profile');
                            }}>
                            <Text style={styles.text}>Preview Profile</Text>
                            <IconSimpleLineIcons style={styles.icon} name="eyeglass" />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.buttonText}
                            onPress={() => {
                                navigation.navigate('Edit Profile')
                            }}>
                            <Text style={styles.text}>Edit Profile </Text>
                            <IconFeather style={styles.icon} name="edit" />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.buttonText}
                            onPress={() => {
                                navigation.navigate('Bookmark')
                            }}>
                            <Text style={styles.text}>Bookmark</Text>
                            <Ionicons style={styles.icon} name="bookmark" />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonText}
                            onPress={onLogout}>
                            <Text style={styles.text}>Logout</Text>
                            <IconMaterialIcons style={styles.icon} name="logout" />
                        </TouchableOpacity>
                    </View>
                </SettingLinearGradient>
            </View>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    textContainer: {
        height: '100%',
        width: WINDOW_WIDTH,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        paddingVertical: WINDOW_HEIGHT * 0.15
    },
    buttonText: {
        width: WINDOW_WIDTH * 0.76,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    text: {
        fontFamily: 'Syncopate',
        color: '#fff',
        fontSize: WINDOW_HEIGHT * 0.025,
        textShadowOffset: { width: 0, height: 0 },
        textShadowColor: '#fff',
        textShadowRadius: 3,
        shadowColor: '#fff',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 3,
    },
    icon: {
        top: -3,
        fontSize: WINDOW_HEIGHT * 0.035,
        color: '#fff',
        textShadowOffset: { width: 0, height: 0 },
        textShadowColor: '#fff',
        textShadowRadius: 3,
        shadowColor: '#fff',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 3,
    }, chatbox: {
        fontFamily: 'Syncopate',
        color: 'grey',
        fontSize: WINDOW_HEIGHT * 0.025,
        textShadowOffset: { width: 0, height: 0 },
        textShadowColor: '#fff',
        textShadowRadius: 3,
    }

})

export default SettingScreen;