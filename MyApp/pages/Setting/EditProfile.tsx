import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions, Image, TextInput, Modal, Alert, FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useForm, Controller } from 'react-hook-form';
import ImagePicker, { Video } from 'react-native-image-crop-picker';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../redux/store';
import ImageViewer from 'react-native-image-zoom-viewer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { deleteArtwork, getArtwork, updateArtwork, updateArtworkDes } from '../../redux/profile/artwork/thunk';
import { updateProfileThunk, resetProfilePic, getProfile } from '../../redux/profile/profile/thunk';
import { SearchBar } from 'react-native-elements';
import { config } from '../../config';
import { useNavigation, StackActions } from '@react-navigation/native';
import { getTags } from '../../redux/profile/tag/thunk';
import Icon from 'react-native-vector-icons/AntDesign';
import SettingScreenLinearGradient from '../../components/SettingScreenLinearGradient';
import VideoPlayer from 'react-native-video-player'
import { KeyboardAwareFlatList } from '@codler/react-native-keyboard-aware-scroll-view';
import { getVideoByID, updateVideo, deleteVideoByID } from '../../redux/profile/video/thunk';


const FONT_SIZE = Dimensions.get('window').width * 0.04;
const BUTTON_WIDTH = Dimensions.get('window').width * 0.3;
const BUTTON_HEIGHT = FONT_SIZE * 2.5;
const WINDOW_WIDTH = Dimensions.get('window').width;


const EditProfileScreen: React.FC = () => {
  const dispatch = useDispatch();
  const { handleSubmit, register, errors, control } = useForm();
  const token = useSelector((state: IRootState) => state.auth.token)
  const isProcessing_video = useSelector((state: IRootState) => state.video.isProcessing)


  //profile
  const profile = useSelector((state: IRootState) => state.profile.profile);
  const [profilePic, setProfilePic] = useState('');
  const currentPic = useSelector((state: IRootState) => state.profile.profile.profile_pic);
  const [uploadProfilePic, setUploadProfilePic] = useState({
    path: '',
    size: 0,
    width: 0,
    height: 0,
    mime: '',
  });


  //Introduction
  const introduction = useSelector((state: IRootState) => state.profile.profile.self_intro);
  const [indexOfImage, setIndexOfImage] = useState(0)
  const [uploadArtworkArr, setUploadArtworkArr] = useState([])
  const [uploadArtwork, setUploadArtwork] = useState([{
    path: '',
    size: 0,
    width: 0,
    height: 0,
    mime: '',
  }])
  const [uploadVideo, setUploadVideo] = useState<Array<Video>>([]);
  const [UploadVideoArr, setUploadVideoArr] = useState<Array<Video>>([]);

  const [tags, setTags] = useState([]);
  const [tagArr, setTagArr] = useState<string[]>([])
  const [tagIDArr, setTagIDArr] = useState<number[]>([])
  const [currentTagArr, setCurrentTagArr] = useState([])

  const [query, setQuery] = useState('');
  const [data, setData] = useState([]);

  const videos = useSelector((state: IRootState) => state.video.video);
  const userId = useSelector((state: IRootState) => state.auth.user?.id);


  //Artworks
  const artworks = useSelector((state: IRootState) => state.artwork.artworks);
  const navigation = useNavigation();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (!token) {
        return
      }
      dispatch(getProfile(token))
      dispatch(getArtwork(token))
      dispatch(getTags(token))
      if (!userId) {
        return
      }
      dispatch(getVideoByID(token, userId))
    })

    return unsubscribe;
  }, [navigation])

  let images: Array<{ url: string, props: any }> = [];
  artworks.map((artwork, idx) => {
    images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
  })


  const [modalVisible, setModalVisible] = useState(false);

  const fetchTag = async () => {
    try {
      const res = await fetch(`${config.BACKEND_URL}/request/tags`, {
        method: 'GET',
      });
      const json = await res.json();
      setData(json);
      setTags(json.slice());

    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchTag();
    fetchCurrentTag();
    setTagArr([]);
    setTagIDArr([]);
  }, [dispatch])

  useEffect(() => {
    const remove_content = navigation.addListener('blur', () => {
      setTagArr([]);
      setTagIDArr([]);
      setQuery('');
    })

    return remove_content;
  }, [navigation])


  const fetchCurrentTag = async () => {
    try {
      const res = await fetch(`${config.BACKEND_URL}/profile/tag`, {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + token,
        }
      });
      const json = await res.json();
      const currentTag: any = [];
      const currentTagID: number[] = [];
      json.map((tags: any) => {
        currentTag.push(tags.tag)
        currentTagID.push(tags.id)
      })

      setCurrentTagArr(currentTag)
      setTagArr(currentTag)
      setTagIDArr(currentTagID)

    } catch (err) {
      console.log(err);
    }
  };


  const updateQuery = async (input: string) => {
    setTags(data.slice());
    setQuery(input);
  }

  interface Tag {
    id: number,
    tag: string
  }

  const filterTags = (tag: Tag) => {
    let search = query.toLowerCase()
    if (tag['tag'].startsWith(search)) {
      return formatTags(tag);
    } else {
      //@ts-ignore
      tags.splice(tags.indexOf(tag), 1);
      return null;
    }
  }

  const formatTags = (tag: Tag) => {
    let tagName = tag['tag'].charAt(0).toUpperCase() + tag['tag'].slice(1);
    return tagName;
  }

  const renderItem = ({ item }: any) => {
    if (filterTags(item) !== null) {
      return (
        <TouchableOpacity onPress={() => {
          const tempArr = tagArr.slice();
          //@ts-ignore
          tempArr.push(item.tag)
          const newArr = tempArr.reduce((unique, item) => {
            return unique.includes(item) ? unique : [...unique, item]
          }, [] as string[])
          setTagArr(newArr)

          const tmpIDArr = tagIDArr.slice();
          //@ts-ignore
          tmpIDArr.push(item.id)
          const newIDArr = tmpIDArr.reduce((unique, item) => {
            return unique.includes(item) ? unique : [...unique, item]
          }, [] as number[])
          setTagIDArr(newIDArr)
          setQuery('');
        }}>
          {query !== '' &&
            <>
              <View style={styles.searchTagContainer}>
                <Text style={styles.flatList}>{filterTags(item)}</Text>
              </View>
            </>
          }
        </TouchableOpacity>
      )

    } else {
      return <Text style={{ display: 'none' }}>{null}</Text>
    }
  }
  useEffect(() => {
    fetchCurrentTag();
    fetchTag();
  }, [])

  useEffect(() => {
    if (currentPic) {
      setProfilePic(currentPic)
    }
  }, [])

  useEffect(() => {
    let images: Array<{ url: string, props: any }> = [];
    artworks.map((artwork, idx) => {
      images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
    })
      , [artworks]
  })

  const onSubmit = async (values: Record<string, any>) => {
    if (!token) {
      return
    }
    if (!userId) {
      return
    }
    if (tagIDArr.length > 5) {
      return (Alert.alert(
        "Error",
        "The number of tags cannot exceed 5.",
        [
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
        { cancelable: false }
      ));
    }

    return (
      uploadVideo[0] ? await dispatch(updateVideo(token, uploadVideo[0], values.VideoDes_0)) : '',
      uploadVideo[1] ? await dispatch(updateVideo(token, uploadVideo[1], values.VideoDes_1)) : '',
      uploadVideo[2] ? await dispatch(updateVideo(token, uploadVideo[2], values.VideoDes_2)) : '',
      await dispatch(updateArtwork(token, uploadArtwork)),
      await dispatch(updateProfileThunk(token, values.introduction, tagIDArr, uploadProfilePic)),
      await navigation.dispatch(StackActions.popToTop())
    )
  };


  return (
    <SafeAreaView style={{ flex: 1 }}>
      {<KeyboardAwareFlatList
        extraHeight={250}
        enableResetScrollToCoords={false}
        style={{ height: Dimensions.get('window').height }} data={tags} keyExtractor={(item: any) => item.tag.toString()}
        extraData={query}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={
          <>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 10
            }}>
              <View style={styles.divContainer}>
                <SettingScreenLinearGradient>
                  <View style={styles.innerContainer}>
                    <Text style={styles.title}>Profile picture</Text>
                    <View style={styles.imageContainer}>
                      {profilePic === '' && (
                        <Image
                          source={require('../../sample/image/defaultAvatar_3.png')}
                          style={{ width: 200, height: 200, borderRadius: 100 }}
                        />
                      )}
                      {profilePic != '' && profilePic != currentPic && (
                        <Image
                          source={{ uri: profilePic }}
                          style={{ width: 200, height: 200, borderRadius: 100 }}
                          resizeMode={'cover'}
                        />
                      )}
                      {profilePic != '' && profilePic === currentPic && (
                        <Image
                          source={{ uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${profilePic}` }}
                          style={{ width: 200, height: 200, borderRadius: 100 }}
                          resizeMode={'cover'}
                        />
                      )}
                    </View>
                    <View style={{ marginVertical: 10, flexDirection: 'row', justifyContent: 'space-around', width: WINDOW_WIDTH * 0.8 }}>
                      <TouchableOpacity
                        style={[styles.upload, { backgroundColor: '#6a6a6a' }]}
                        onPress={() => {
                          setProfilePic('');
                          if (token) {
                            dispatch(resetProfilePic(token))
                          }
                        }}>
                        <Text style={styles.buttonText}>Default</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.upload}
                        onPress={() => {
                          ImagePicker.openPicker({
                            mediaType: 'photo',
                            width: 200,
                            height: 200,
                            avoidEmptySpaceAroundImage: true,
                          }).then((image) => {
                            setUploadProfilePic(image);
                            image.sourceURL ? setProfilePic(image.sourceURL) : '';
                          });
                        }}>
                        <Text style={styles.buttonText}>Upload</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </SettingScreenLinearGradient>
              </View>
              <View style={styles.divContainer}>
                <SettingScreenLinearGradient>
                  <View style={styles.innerContainer}>
                    <Text style={styles.title}>Tags</Text>
                    {tagArr !== [] &&
                      <>
                        <View style={styles.tagBox}>
                          {tagArr.map((tagObject: any, idx: number) => (
                            <View key={`tag_${idx}`} >
                              <View style={styles.tag}>
                                <Text style={styles.tagText}>
                                  {tagObject.charAt(0).toUpperCase() + tagObject.slice(1)}
                                </Text>
                                <View style={styles.cancelTagContainer} >
                                  <TouchableOpacity
                                    style={{ alignItems: 'center', width: 40, height: 47, justifyContent: 'center' }}
                                    onPress={() => {
                                      const tempArr = tagArr.slice();
                                      tempArr.splice(idx, 1);
                                      setTagArr(tempArr);
                                      const tmpIDArr = tagIDArr.slice();
                                      tmpIDArr.splice(idx, 1);
                                      setTagIDArr(tmpIDArr);
                                    }}>
                                    <Icon name='closecircle' size={15} style={styles.cancelTag} />
                                  </TouchableOpacity>
                                </View>
                              </View>
                            </View>
                          ))}
                        </View>
                      </>
                    }
                    <View>
                      <SearchBar
                        containerStyle={styles.searchBar}
                        platform="ios"
                        onChangeText={updateQuery}
                        value={query}
                        placeholder="Type Here..."
                      />
                    </View>
                  </View>
                </SettingScreenLinearGradient>
              </View>

            </View>
          </>
        }

        ListFooterComponent={
          <>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <View style={styles.divContainer}>
                <SettingScreenLinearGradient>
                  <View style={styles.innerContainer}>
                    <Text style={styles.subTitle}>Self-Introduction</Text>
                    <Controller
                      control={control}
                      render={({ onChange, onBlur, value }) => (
                        <TextInput
                          multiline={true}
                          style={styles.selfIntroInput}
                          onBlur={onBlur}
                          onChangeText={(value) => onChange(value)}
                          value={value}
                        />
                      )}
                      name="introduction"
                      rules={{ required: true }}
                      defaultValue={introduction}
                    />
                    {errors.introduction && (
                      <Text style={styles.errorMessage}>
                        Introduction cannot be empty.
                      </Text>
                    )}
                  </View>
                </SettingScreenLinearGradient>
              </View>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 50 }}>
              <View style={styles.divContainer}>
                <SettingScreenLinearGradient>
                  <View style={styles.innerContainer}>
                    <Text style={styles.title}>Artworks</Text>
                    <View style={{ width: WINDOW_WIDTH * 0.8 }}>
                      <View
                        style={{
                          justifyContent: 'flex-start',
                          flexDirection: 'row',
                          flexWrap: 'wrap',
                          maxHeight: "auto",
                          overflow: 'hidden',
                        }}>
                        {artworks.length !== 0 && (
                          <>
                            {artworks.map((artworksObject, index) => (
                              <View key={`artworks_${index}`}>
                                <TouchableOpacity
                                  onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                                  <Image
                                    style={{
                                      height: 100,
                                      width: 100,
                                      marginHorizontal: 5,
                                      marginVertical: 5,
                                      borderWidth: 1,
                                      borderColor: "#aaa"
                                    }}
                                    source={{
                                      uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artworksObject.filename}`,
                                    }}
                                  />
                                </TouchableOpacity>
                              </View>
                            ))}
                          </>
                        )}
                        <>
                          {uploadArtworkArr.map((uploadArtworks, index) => {
                            return (
                              <View key={`uploadArtworks_${index}`} >
                                <TouchableOpacity
                                  onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                                  <Image
                                    style={{
                                      height: 100,
                                      width: 100,
                                      marginHorizontal: 5,
                                      marginVertical: 5,
                                      borderWidth: 1,
                                      borderColor: "#aaa"
                                    }}
                                    source={{ uri: uploadArtworks }}
                                  />
                                </TouchableOpacity>
                              </View>)
                          })}
                        </>
                        {artworks.length == 0 && (
                          <Text style={{ width: '100%', textAlign: 'center', paddingTop: 15, color: 'grey', fontFamily: 'varela round' }}>
                            You haven't uploaded any artwork yet
                          </Text>
                        )}
                        <View style={{ height: BUTTON_HEIGHT + 30, width: '100%', justifyContent: 'flex-start', alignItems: 'center', marginTop: 20 }}>
                          <TouchableOpacity
                            style={styles.upload}
                            onPress={() => {
                              ImagePicker.openPicker({
                                width: 200,
                                height: 200,
                                multiple: true,
                                cropperCircleOverlay: true,
                                avoidEmptySpaceAroundImage: true,
                                cropping: true,
                              }).then((images) => {
                                console.log("images:", images);
                                setUploadArtwork(images)
                                const sourceURLArry: any = images.map(img => img.sourceURL)
                                setUploadArtworkArr(sourceURLArry);
                              });
                            }}>
                            <Text style={styles.buttonText}>add new</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <Modal visible={modalVisible} transparent={true} >
                        <View style={{ height: 'auto', position: 'absolute', flexDirection: "row", top: 70, left: "5%", zIndex: 999, width: WINDOW_WIDTH * 0.9, justifyContent: 'space-between' }}>
                          <TouchableOpacity onPress={() => {
                            Alert.prompt(
                              'Enter Description',
                              'Enter your description for this artwork.',
                              (text) => {
                                token ? dispatch(updateArtworkDes(token, artworks[indexOfImage].filename, text)) : ''
                              },
                            )
                          }}>
                            <View style={{
                              backgroundColor: '#8885',
                              width: 60,
                              height: 60,
                              borderRadius: 50,
                              justifyContent: 'center',
                              alignItems: 'center'
                            }}>
                              <Ionicons name="chatbox-ellipses-outline" color="white" size={35} />
                            </View>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => {
                            Alert.alert(
                              'Delete Artwork',
                              'Are you sure to delete this artwork?',
                              [
                                {
                                  text: 'Yes',
                                  onPress: () => {
                                    console.log('Yes delete :Pressed'),
                                      token ? dispatch(deleteArtwork(token, artworks[indexOfImage].filename)) : '';
                                    setModalVisible(false)
                                  }
                                },
                                {
                                  text: 'Cancel',
                                  onPress: () => console.log('Cancel Pressed'),
                                  style: 'cancel'
                                },
                              ],
                              { cancelable: false }
                            );
                          }}>
                            <View style={{
                              backgroundColor: '#8885',
                              width: 60,
                              height: 60,
                              borderRadius: 30,
                              justifyContent: 'center',
                              alignItems: 'center',
                              paddingLeft: 3
                            }}>
                              <Ionicons name="trash-outline" color="white" size={33} />
                            </View>
                          </TouchableOpacity>
                        </View>
                        <ImageViewer
                          imageUrls={images}
                          menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                          onChange={(index) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                          index={indexOfImage}
                          renderFooter={(currentIndex) => {
                            return (
                              <>
                                {(artworks[currentIndex].description.length >= 1) && (
                                  <View style={styles.artworkTextContainer}>
                                    <Text style={styles.viewArtworkText}>{artworks[currentIndex].description}</Text>
                                  </View>
                                )}
                              </>
                            )
                          }}
                          enableSwipeDown={true}
                          enablePreload={true}
                          onSwipeDown={() => {
                            setModalVisible(false)
                          }} />
                      </Modal>
                    </View>

                  </View>
                </SettingScreenLinearGradient>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 50 }}>
                <View style={styles.divContainer}>
                  <SettingScreenLinearGradient>
                    <View style={styles.innerContainer}>
                      <Text style={styles.title}>Video</Text>
                      {UploadVideoArr !== [] &&
                        <>
                          <View style={{ width: WINDOW_WIDTH * 0.8 }}>
                            {videos.map((video: any, idx: number) => {
                              return (
                                <View key={`video_${idx}`} >
                                  <TouchableOpacity style={{ alignItems: 'center', alignSelf: 'flex-end' }} onPress={() => {
                                    Alert.alert(
                                      'Delete Video',
                                      'Are you sure to delete this video?',
                                      [
                                        {
                                          text: 'Yes',
                                          onPress: () => {
                                            console.log('Yes delete :Pressed');
                                            if (!token || !userId) { return }
                                            dispatch(deleteVideoByID(token, userId, video.filename))
                                          }
                                        },
                                        {
                                          text: 'Cancel',
                                          onPress: () => console.log('Cancel Pressed'),
                                          style: 'cancel'
                                        },
                                      ],
                                      { cancelable: false }
                                    );
                                  }}>
                                    <View>

                                      <Icon name='closecircle' style={styles.cancelTag2} />
                                    </View>
                                  </TouchableOpacity>
                                  <VideoPlayer
                                    video={{ uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${video.filename}` }}
                                    videoWidth={1600}
                                    videoHeight={900}
                                    thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
                                  />
                                  <View style={{ marginTop: 20 }}>
                                    <Text style={styles.subTitle}>Video Title:</Text>
                                    <Text style={{ fontFamily: 'Varela Round', fontSize: 15 }}>{video.description}</Text>
                                  </View>
                                </View>
                              )
                            })}
                            {UploadVideoArr.map((uploadVideo: any, idx: number) => (
                              <View key={`video_${idx}`} >
                                <VideoPlayer
                                  video={{ uri: uploadVideo.sourceURL }}
                                  videoWidth={1600}
                                  videoHeight={900}
                                  thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
                                />
                                <View style={{ marginTop: 30 }}>
                                  <Text style={styles.subTitle}>Video Description:</Text>
                                  <Controller
                                    control={control}
                                    render={({ onChange, onBlur, value }) => (
                                      <TextInput
                                        multiline={true}
                                        style={styles.selfIntroInput}
                                        onBlur={onBlur}
                                        onChangeText={(value) => onChange(value)}
                                        value={value}
                                      />
                                    )}
                                    name={`VideoDes_${idx}`}
                                    rules={{ required: false }}
                                    defaultValue={''}
                                  />
                                </View>
                              </View>
                            ))}
                            <View style={{ height: BUTTON_HEIGHT + 30, width: '100%', justifyContent: 'flex-start', alignItems: 'center', marginTop: 20 }}>
                              <TouchableOpacity style={styles.upload}
                                onPress={() => {
                                  ImagePicker.openPicker({
                                    mediaType: "video",
                                  }).then((video) => {
                                    let newUploadVideoArr = UploadVideoArr.slice();
                                    if (newUploadVideoArr.length + videos.length > 2) {
                                      return (Alert.alert(
                                        "Upload Failed",
                                        "Each account can only upload 3 videos",
                                        [
                                          { text: "OK", onPress: () => console.log("OK Pressed") }
                                        ],
                                        { cancelable: false }
                                      ))
                                    };
                                    newUploadVideoArr.push(video)
                                    setUploadVideo(newUploadVideoArr);
                                    setUploadVideoArr(newUploadVideoArr)
                                  });
                                }}>
                                <Text style={styles.buttonText}>add new</Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </>
                      }
                    </View>
                  </SettingScreenLinearGradient>
                </View>
              </View>
              <TouchableOpacity
                style={styles.submit}
                onPress={handleSubmit(onSubmit)}>
                <Text style={styles.buttonText}>save changes</Text>
              </TouchableOpacity>
            </View>
          </>
        }
      />}
    </SafeAreaView >
  )
}

export default EditProfileScreen;


const styles = StyleSheet.create({
  upload: {
    marginVertical: 10,
    backgroundColor: '#4EC9B0',
    // backgroundColor: '#5F9AFF',
    // padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,
    borderRadius: 7,
    // padding: 10,
    // backgroundColor: '#6a6a6a',
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 0.7,

  },
  title: {
    // paddingTop: 2,
    marginVertical: 8,
    // fontFamily: 'Varela Round',
    fontFamily: 'Syncopate',
    // fontSize: 22,
    fontSize: FONT_SIZE * 1.5,
    color: '#444',
  },
  divContainer: {
    // padding: 5,
    marginVertical: 15,
    width: WINDOW_WIDTH * 0.88,
    // width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#aaa4',
    borderBottomColor: '#aaa6',
    borderWidth: 1,
    shadowColor: '#000a',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 6,
  },
  innerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  subTitle: {
    // paddingTop: 2,
    marginBottom: 10,
    // fontFamily: 'Varela Round',
    fontFamily: 'Syncopate',
    fontSize: FONT_SIZE * 0.97,
    color: '#000',
  },


  selfIntroInput: {
    marginTop: 8,
    minHeight: FONT_SIZE + 50,
    height: 'auto',
    width: WINDOW_WIDTH * 0.8,
    borderColor: '#888c',
    borderStyle: 'solid',
    borderRadius: 6,
    borderWidth: 1,
    fontFamily: 'Varela Round',
    paddingHorizontal: 10,
  },
  text: {
    fontFamily: 'Varela Round',
    fontSize: FONT_SIZE,
    color: 'grey',
    marginBottom: 5,
  },
  tags: {
    minHeight: FONT_SIZE + 20,
    width: WINDOW_WIDTH * 0.8,
    borderColor: '#aaa',
    borderStyle: 'solid',
    borderRadius: 10,
    borderWidth: 1,
    fontFamily: 'Varela Round',
    paddingHorizontal: 10,
    flexWrap: 'wrap',

    fontSize: FONT_SIZE,
    color: 'black',
    marginBottom: 5,
    textAlign: 'center',
    paddingVertical: WINDOW_WIDTH * 0.02
  },

  searchBar: {
    backgroundColor: 'transparent',
    width: WINDOW_WIDTH * 0.8,
    // color: '#fff',
    // fontFamily: 'Varela Round',
    // fontSize: 40,
  },

  buttonText: {
    paddingHorizontal: 5,
    color: '#fff',
    textShadowColor: '#eee',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 2,
    // shadowColor: '#777',
    shadowColor: '#fff',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    fontFamily: 'Syncopate',
    // fontFamily: 'varela round',
    // textTransform: 'uppercase',
    fontSize: FONT_SIZE,
  },
  imageContainer: {
    // width:300,
    // height:300,
    // borderRadius:75
    width: 'auto',
    height: 'auto',
    marginVertical: 10,
  },
  searchTagContainer: {
    alignSelf: 'center',
    width: WINDOW_WIDTH * 0.8,
    backgroundColor: '#333c',
    margin: 4,
    paddingHorizontal: 8,
    borderRadius: 9,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: '#4441',
    // borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 1,
  },
  flatList: {
    textAlign: 'center',
    // paddingLeft: 15,
    // marginTop: 15,
    paddingVertical: 10,
    fontSize: 16,
    fontFamily: 'Syncopate',
    // borderBottomColor: '#26a69a',
    // borderBottomWidth: 1,
    // borderWidth: 1,
    // borderColor: 'black',
    color: 'cyan',
    // backgroundColor: 'red',
    textShadowColor: 'cyan',
    textShadowRadius: 5,
  },
  tagBox: {
    // backgroundColor: 'red',
    marginTop: 3,
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
    shadowColor: '#222',
    shadowOpacity: 0.8,
    shadowRadius: 3,
    shadowOffset: { width: 2, height: 3 }
  },
  tag: {
    justifyContent: 'center',
    width: 'auto',
    height: 25,
    backgroundColor: '#333c',
    margin: 12,
    paddingHorizontal: 8,
    borderRadius: 9,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: '#4441',
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 1,
  },
  tagText: {
    fontFamily: 'Varela Round',
    fontSize: 13,
    color: 'cyan',
    textShadowColor: 'cyan',
    textShadowRadius: 5,
  },
  cancelTagContainer: {
    // backgroundColor: 'red',
    position: 'absolute',
    marginTop: 13,
    top: -17,
    right: -12,
    width: 17,
    height: 17,
    backgroundColor: '#FFF',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelTag: {
    color: '#c9382f',
    fontSize: 17,
  },
  cancelTag2: {
    color: '#c9382f',
    fontSize: 25,
  },
  artworkTextContainer: {
    width: WINDOW_WIDTH,
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewArtworkText: {
    width: '100%',
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: '#8885',
    // opacity: 0.8,
    fontFamily: 'Varela Round',
    color: '#fff',
    position: 'absolute',
    fontSize: 20,
    bottom: 150,
    // left: 15,
    textShadowRadius: 3,
    textShadowColor: '#000',
    textShadowOffset: { width: 0, height: 0 },
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
  },
  submit: {
    marginTop: 25,
    // backgroundColor: '#4EC9B0',
    backgroundColor: '#5F9AFF',
    // padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    width: Dimensions.get('window').width * 0.55,
    height: FONT_SIZE * 2.5,
    borderRadius: 7,
    padding: 10,
    // backgroundColor: '#6a6a6a',
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 0.7,
    marginBottom: 40
  },
  errorMessage: {
    fontFamily: 'Varela Round',
    color: 'red',
  },
})

