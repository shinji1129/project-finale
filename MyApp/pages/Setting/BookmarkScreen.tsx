import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import { useSelector } from 'react-redux';
import { config } from '../../config';
import { IRootState } from '../../redux/store';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import BlackWhiteLinearGradient from '../../components/BlackWhiteLinearGradient';
import { IProjectList } from '../../model/model';


const WINDOW_WIDTH = Dimensions.get('window').width;
const FONT_SIZE = 15;

const BookmarkListScreen: React.FC = () => {

    const [requestList, setRequestList] = useState<IProjectList[]>([]);
    const token = useSelector((state: IRootState) => state.auth.token);
    const navigation = useNavigation();

    useEffect(() => {
        const loadData = navigation.addListener('focus', () => {
            const fetchRequest = async () => {
                const res = await fetch(`${config.BACKEND_URL}/request/bookmarkList`, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + token,
                    }
                })
                const requestListObj = await res.json();
                setRequestList(requestListObj);
            }
            try {
                fetchRequest();
            } catch (err) {
                console.error(err.message)
            }
        })

        return loadData;
    }, [navigation])

    var List: any = [];

    for (let i = 0; i < requestList.length; i++) {
        List.push(
            <TouchableOpacity key={`request_${requestList[i].id}`} style={styles.requestBoard} onPress={() => {
                navigation.navigate('Bookmarked Request', {
                    request_id: requestList[i].request_id,
                    requester_id: requestList[i].requester_id,
                    requester: requestList[i].requester,
                    title: requestList[i].title,
                    freelancer_id: requestList[i].freelancer_id,
                    freelancer: requestList[i].freelancer,
                    status: requestList[i].status,
                    images: requestList[i].images,
                    company_name: requestList[i].company_name,
                    details: requestList[i].details,
                    budget_min: requestList[i].budget_min,
                    budget_max: requestList[i].budget_max,
                    confirmed_budget: requestList[i].confirmed_budget,
                    time_start: new Date(requestList[i].time_start).toDateString(),
                    duration: requestList[i].duration,
                    expiry_date: new Date(requestList[i].expiry_date).toDateString(),
                    category_id: requestList[i].category_id,
                    venue: requestList[i].venue,
                    category: requestList[i].category,
                    tag: requestList[i].tag,
                })
            }}>
                <BlackWhiteLinearGradient>
                    <View style={[{ flexDirection: 'row', paddingHorizontal: 5, justifyContent: 'center', alignItems: 'flex-start' }, { backgroundColor: 'transparent' }]}>
                        <View style={styles.innerContent}>
                            <View style={styles.upperLayer}>
                                <Text style={styles.requestTitle}>{requestList[i].title} {requestList[i].request_id}</Text>
                                {requestList[i].tag.length >= 1 &&
                                    <View key={`request_${i + 1}`} style={styles.tagBox}>
                                        {requestList[i].tag.map((tag, idx) => {
                                            return (
                                                <View key={`tag_${idx}`} style={styles.tag}>
                                                    <View>
                                                        <Text style={styles.tagText}>{tag.charAt(0).toUpperCase() + tag.slice(1)}</Text>
                                                    </View>
                                                </View>)
                                        })}
                                    </View>}
                            </View>
                            <View style={styles.lowerLayer}>
                                {requestList[i].company_name && (<Text style={styles.requestText}>{requestList[i].company_name}</Text>)}
                                {requestList[i].budget_min && (<Text style={styles.requestText}>Budget: ${requestList[i].budget_min} ~ {requestList[i].budget_max}</Text>)}
                                {requestList[i].venue && (<Text style={styles.requestText}>Venue: {requestList[i].venue}</Text>)}
                                {requestList[i].time_start && (<Text style={styles.requestText}>Time: {new Date(requestList[i].time_start).toDateString()} </Text>)}
                                {requestList[i].invite_budget && (<Text style={styles.requestText}>This is invited project </Text>)}
                                {requestList[i].apply_budget && (<Text style={styles.requestText}>This is applied project </Text>)}
                            </View>
                        </View>
                        <View style={{ height: '100%', position: 'absolute', right: '4%', top: 5, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                            {requestList[i].bookmark === true && <Icon name='bookmark' size={30} style={styles.bookmark} />}
                            {(requestList[i].status === 2 && requestList[i].bookmark === true) && (
                                <View style={{ paddingTop: 0, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.appliedButtonText}>Invited</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 2 && requestList[i].bookmark === false) && (
                                <View style={{ paddingTop: 40, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.appliedButtonText}>Invited</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 3 && requestList[i].bookmark === true) && (
                                <View style={{ paddingTop: 0, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.buttonText}>Assigned</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 3 && requestList[i].bookmark === false) && (
                                <View style={{ paddingTop: 40, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.buttonText}>Assigned</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 4 && requestList[i].bookmark === true) && (
                                <View style={{ paddingTop: 0, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.confirmationButtonText}>Confirmation</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 4 && requestList[i].bookmark === false) && (
                                <View style={{ paddingTop: 40, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.confirmationButtonText}>Confirmation</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 5 && requestList[i].bookmark === true) && (
                                <View style={{ paddingTop: 0, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.completedButtonText}>Completed</Text>
                                    </View>
                                </View>
                            )}
                            {(requestList[i].status === 5 && requestList[i].bookmark === false) && (
                                <View style={{ paddingTop: 40, paddingHorizontal: 6 }}>
                                    <View style={styles.statusTag}>
                                        <Text style={styles.completedButtonText}>Completed</Text>
                                    </View>
                                </View>
                            )}
                        </View>
                    </View>
                </BlackWhiteLinearGradient>
            </TouchableOpacity >
        )
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 0.9,
                    }}>
                    {(<View style={styles.container}>
                        <View>
                            {List}
                        </View>
                    </View>)}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default BookmarkListScreen;

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'center',
        alignContent: 'flex-start',
        flexWrap: "wrap",
        flexDirection: "row",
    },
    requestBoard: {
        width: Dimensions.get('window').width * 0.9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginVertical: 10,
        backgroundColor: '#C4FFF9',
        borderRadius: 7,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.4,
        shadowRadius: 6,
    },
    innerContent: {
        width: '98%',
        paddingVertical: 7
    },
    upperLayer: {
        marginTop: 5,
        paddingHorizontal: 3,
        marginBottom: 6,
    },
    lowerLayer: {
        paddingHorizontal: 7,
    },
    requestTitle: {
        maxWidth: WINDOW_WIDTH * 0.535,
        paddingLeft: 9,
        fontFamily: 'Varela Round',
        flexWrap: 'wrap',
        fontSize: 17.5,
        marginBottom: 3,
        marginTop: 3,
    },
    tagBox: {
        flexDirection: 'row',
        maxWidth: WINDOW_WIDTH * 0.535,
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    requestText: {
        fontFamily: 'Varela Round',
        fontSize: 14,
        marginVertical: 4,
        paddingHorizontal: 5,
    },
    buttonText: {
        paddingHorizontal: 10,
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.75,
    },
    appliedButtonText: {
        paddingHorizontal: 10,
        color: '#639EFF',
        textShadowColor: '#fff',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 1,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 0,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.75,
    },
    confirmationButtonText: {
        paddingHorizontal: 10,
        color: '#f07',
        textShadowColor: '#fff',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 10,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'varela round',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.75,
    },
    completedButtonText: {
        paddingHorizontal: 10,
        color: '#7ff',
        textShadowColor: '#7ff',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.75,
    },
    statusTag: {
        top: -5,
        right: -7,
        backgroundColor: '#6a6a6a',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 1.8,
        borderRadius: 11,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 5,
        shadowOpacity: 0.45,
    },
    notification: {
        position: 'relative',
        top: '4%',
        backgroundColor: '#ED6C60',
        justifyContent: 'center',
        alignItems: 'center',
        width: 30,
        height: 30,
        borderRadius: 20
    },
    bookmark: {
        top: '-7.5%',
        height: 43,
        color: '#7BFEFF',
        textShadowColor: '#0007',
        shadowOpacity: 0.4,
        textShadowRadius: 6,
        textShadowOffset: { width: 0, height: 8 },
        zIndex: 1,
    },
    flatList: {
        paddingLeft: 15,
        marginTop: 15,
        paddingBottom: 15,
        fontSize: 20,
        borderBottomColor: '#26a69a',
        borderBottomWidth: 1
    }
})
