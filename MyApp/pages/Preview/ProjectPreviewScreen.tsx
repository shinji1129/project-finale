import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions, Image, Modal } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { getBookmark } from '../../redux/request/bookmark/thunk';
import { currentApplication } from '../../redux/request/application/thunk';
import { currentInvitation } from '../../redux/request/invitation/thunk';
import { config } from '../../config';
import BlackWhiteLinearGradient from '../../components/BlackWhiteLinearGradient';
import { resetBookmarkSuccess } from '../../redux/request/bookmark/actions';
import { resetApplicationSuccess } from '../../redux/request/application/actions';
import { resetInvitationSuccess } from '../../redux/request/invitation/action';
import { useNavigation } from '@react-navigation/native';
import { currentStatus } from '../../redux/request/confirmation/thunk';
import { Artworks } from '../../redux/profile/artwork/state'
import { ImageViewer } from 'react-native-image-zoom-viewer';
import { isCommentThunk } from '../../redux/request/comment/thunk';
import CommentBox from '../../components/commentbox';
import { resetConfirmationSuccess } from '../../redux/request/confirmation/action';



const FONT_SIZE = Dimensions.get('window').width * 0.038;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const BUTTON_WIDTH = Dimensions.get('window').width * 0.3;
const BUTTON_HEIGHT = FONT_SIZE * 2.5;

const ProjectPreviewScreen: React.FC = ({ route }: any) => {
    const dispatch = useDispatch();
    const token = useSelector((state: IRootState) => state.auth.token);
    // Bookmark
    const bookmarkErrorMessage = useSelector((state: IRootState) => state.bookmark.errorMessage);

    // Application


    // Invitation


    // Confirmation
    const userId = useSelector((state: IRootState) => state.auth.user?.id)

    // RequestImage
    const [requestImages, setRequestImages] = useState<Artworks[]>([])
    const [modalVisible, setModalVisible] = useState(false);
    const [indexOfImage, setIndexOfImage] = useState(0);

    const isCommentBefore = useSelector((state: IRootState) => state.comment.isComment)
    const isBothComment = useSelector((state: IRootState) => state.comment.isBothComment)


    let images: Array<{ url: string, props: any }> = [];
    requestImages.map((requestImage, idx) => {
        images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
    })

    useEffect(() => {
        let images: Array<{ url: string, props: any }> = [];
        requestImages.map((requestImage, idx) => {
            images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
        })
    }, [requestImages])

    const navigation = useNavigation()

    const { request_id, requester, title, tag, time_start, duration, venue, budget_min, budget_max, freelancer_id, confirmed_budget, expiry_date, status, tag_id } = route.params;


    const fetchRequestImages = async (request_id: number) => {
        const res = await fetch(`${config.BACKEND_URL}/request/images/${request_id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        })
        const requestImgObj = await res.json();
        setRequestImages(requestImgObj);
    }

    // get bookmark
    useEffect(() => {
        if (token === null) {
            return;
        }
        if (bookmarkErrorMessage) {
            console.log(bookmarkErrorMessage)
        }
        if (userId) {
            dispatch(isCommentThunk(token, userId, request_id))
        }
        dispatch(getBookmark(token, request_id))
        fetchRequestImages(request_id)
        return (() => {
            dispatch(resetBookmarkSuccess());
        })
    }, []);


    useEffect(() => {
        const loadData = navigation.addListener('focus', ()=> {
            if (token === null) {
                return;
            }
            dispatch(currentApplication(token, request_id))
            dispatch(currentInvitation(token, request_id))
            dispatch(currentStatus(token, request_id))
        })

        return loadData;
    }, [navigation])


    useEffect(() => {
        const removeData = navigation.addListener('blur', ()=> {
            dispatch(resetInvitationSuccess());
            dispatch(resetApplicationSuccess());
            dispatch(resetConfirmationSuccess());
        })

        return removeData;
    }, [navigation])


    return (
        <SafeAreaView >
            <BlackWhiteLinearGradient>
                <ScrollView style={{ height: 'auto', width: WINDOW_WIDTH }}>
                    <View style={{ minHeight: WINDOW_HEIGHT * 0.9 }}>
                        <View style={styles.container}>
                            <View>
                                <Text style={styles.titleText}>{title}</Text>
                                {requester.length >= 1 && <Text style={styles.text}>Job posted by: <Text style={styles.requester}>{requester}</Text> </Text>}

                                <Text style={{ fontFamily: 'Varela Round', paddingLeft: 5, marginTop: 8 }}>Require:</Text>
                                {tag && tag.length >= 1 && <View style={styles.tagBox}>{tag.map((string: string, idx: number) => {
                                    return (
                                        <View style={styles.tag} >
                                            <Text style={styles.tagText} key={`tag_${idx}`}>{string.charAt(0).toUpperCase() + string.slice(1)}</Text>
                                        </View>
                                    )
                                })}
                                </View>
                                }
                                {images && images.length >= 1 && <Text style={styles.tag}>Images:</Text>}
                            </View>
                            <View style={styles.border}></View>
                            <View style={styles.middleHalf}>
                            {time_start !== new Date(0).toDateString() && time_start.length >= 1 && <Text style={styles.text}>Date: {time_start}</Text>}
                                {venue && <Text style={styles.text}>Venue: {venue} </Text>}
                                <Text style={styles.text}>Reference image: </Text>
                                <View style={{
                                    marginTop: 10,
                                    paddingHorizontal: 5,
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                    maxHeight: 440,
                                    overflow: 'scroll',
                                }}>
                                    {requestImages.length >= 1 &&
                                        <>
                                            {requestImages.map((requestImage, index) => (
                                                <TouchableOpacity key={`requestImage_${index}`} onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                                                    <Image
                                                        style={{
                                                            height: 100,
                                                            width: 100,
                                                            marginRight: 10,
                                                            marginBottom: 10,
                                                        }}
                                                        source={{
                                                            uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`,
                                                        }}
                                                    />
                                                </TouchableOpacity>
                                            ))}
                                        </>}
                                </View>
                                <Modal visible={modalVisible} transparent={true}>
                                    <View style={{ backgroundColor: 'black', height: 'auto', position: 'absolute', top: 50, right: 15, zIndex: 999 }}>
                                    </View>
                                    <ImageViewer
                                        imageUrls={images}
                                        menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                                        onChange={(index) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                                        index={indexOfImage}
                                        enableSwipeDown={true}
                                        enablePreload={true}
                                        onSwipeDown={() => {
                                            setModalVisible(false)
                                        }} />
                                </Modal>
                            </View>
                        </View>
                        {isBothComment === true && (
                            <View style={{ width: '100%' }}>
                                <CommentBox requestId={request_id} />
                            </View>
                        )}
                    </View>
                </ScrollView>
            </BlackWhiteLinearGradient>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        minWidth: WINDOW_WIDTH,
        alignContent: 'center',
        flexDirection: "column",
        padding: 15,

    },
    titleText: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.45,
        textTransform: 'capitalize',
        borderRadius: 5,
        padding: 5,
        width: WINDOW_WIDTH * 0.9,
    },
    text: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE,
        borderRadius: 5,
        padding: 5,
        color: '#090909',
    },
    requester: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.40,
        textTransform: 'capitalize',
    },
    tagBox: {
        marginTop: 15,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    border: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: WINDOW_WIDTH * 0.9,
        borderBottomColor: '#7775',
        borderBottomWidth: 1,
        paddingBottom: 18,
        justifyContent: 'flex-start',
    },
    middleHalf: {
        marginTop: 15,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: 15
    },
    pageEndButtons: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    rows: {
        marginTop: WINDOW_HEIGHT * 0.015,
        minWidth: WINDOW_WIDTH * 0.5,
    },
    buttonText: {
        paddingHorizontal: 5,
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.9,
    },
    chatBox: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    unbookmark: {
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    invitation: {
        maxWidth: WINDOW_WIDTH * 0.9,
        marginVertical: 15,
        borderRadius: 6,
        shadowColor: '#0009',
        shadowOffset: { width: 3, height: 7 },
        shadowOpacity: 1,
        shadowRadius: 8
    },
    accept: {
        width: '100%',
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    decline: {
        width: '100%',
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    bookmark: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    input: {
        marginTop: 15,
        height: FONT_SIZE + 20,
        width: WINDOW_WIDTH * 0.8,
        borderColor: '#aaa',
        borderStyle: 'solid',
        borderRadius: 30,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
    },
    apply: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
});

export default ProjectPreviewScreen;