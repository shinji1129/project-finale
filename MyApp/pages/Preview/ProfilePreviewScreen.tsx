import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Button,
  Image,
  Dimensions,
  Animated,
  TouchableWithoutFeedback,
  Easing,
  Modal,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { config } from '../../config';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../redux/store';
import { BaseNavigationContainer, useNavigation } from '@react-navigation/native';

import IconAntDesign from 'react-native-vector-icons/AntDesign';
import { getProfile } from '../../redux/profile/profile/thunk';
import { getArtwork, deleteArtwork } from '../../redux/profile/artwork/thunk';
import { getTags } from '../../redux/profile/tag/thunk';
import { PieChart } from 'react-native-chart-kit';
import { resetArtwork } from '../../redux/profile/artwork/actions';
import { resetProfile } from '../../redux/profile/profile/actions';
import { resetTags } from '../../redux/profile/tag/actions';
import { IList } from '../../model/model'
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImageViewer from 'react-native-image-zoom-viewer';
import ProfileLinearGradient from '../../components/ProfileLinearGradient';
import RatingInProfile from '../../components/RatingInProfile';


const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const FONT_SIZE = 15;

const ProfilePreviewScreen: React.FC = (props: any) => {
  const dispatch = useDispatch();
  const Stack = createStackNavigator();
  const token = useSelector((state: IRootState) => state.auth.token);
  const userId = useSelector((state: IRootState) => state.auth.user?.id);

  const isProcessing_Profile = useSelector((state: IRootState) => state.profile.isProcessing)
  const isProcessing_Artwork = useSelector((state: IRootState) => state.artwork.isProcessing)
  const isProcessing_Tags = useSelector((state: IRootState) => state.tags.isProcessing)


  const [modalVisible, setModalVisible] = useState(false);
  const [indexOfImage, setIndexOfImage] = useState(0)

  const AnimatedIconAntDesign = Animated.createAnimatedComponent(IconAntDesign);
  let heightValue = new Animated.Value(60);
  const colorValue = new Animated.Value(0);
  let rotateValue = new Animated.Value(0);
  let opacityValue = new Animated.Value(0.25);
  let isOpen = false;

  const handlePress2 = () => {
    if (isOpen === false) {
      Animated.timing(heightValue, {
        toValue: 170,
        duration: 600,
        easing: Easing.elastic(2),
        useNativeDriver: false,
      }).start();

      Animated.timing(colorValue, {
        toValue: 1,
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.spring(rotateValue, {
        toValue: 1,
        useNativeDriver: true,
      }).start(),
        Animated.timing(opacityValue, {
          toValue: 1,
          duration: 500,
          delay: 400,
          useNativeDriver: false,
        }).start(),
        (isOpen = true);
    } else {
      Animated.timing(heightValue, {
        toValue: 60,
        duration: 600,
        easing: Easing.elastic(1),
        useNativeDriver: false,
      }).start();
      Animated.timing(colorValue, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.spring(rotateValue, {
        toValue: 0,
        useNativeDriver: true,
      }).start(),
        Animated.timing(opacityValue, {
          toValue: 0.25,
          duration: 200,
          useNativeDriver: false,
        }).start(),
        (isOpen = false);
    }
  };

  const rotationInterpolate = rotateValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '180deg'],
  });
  const rotation = {
    transform: [{ rotate: rotationInterpolate }],
  };
  const changeColor = colorValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['#555', '#4EC9B0'],
  });
  const finalColor = {
    color: changeColor,
  };

  const navigation = useNavigation();
  const [requestList, setRequestList] = useState([]);
  const [postedJobList, setPostedJobList] = useState([]);
  const [accCategories, setAccCategories] = useState([]);
  const [accCategoriesOfPostedJob, setAccCategoriesOfPostedJob] = useState([]);
  const video = useSelector((state: IRootState) => state.video.video);


  // things to do as this screen is focus (did mount)
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      if (!token) {
        return
      }
      dispatch(getProfile(token))
      dispatch(getArtwork(token))
      dispatch(getTags(token))
    })

    return unsubscribe;
  }, [navigation])

  const profile = useSelector((state: IRootState) => state.profile.profile);
  const artworks = useSelector((state: IRootState) => state.artwork.artworks);
  const tags = useSelector((state: IRootState) => state.tags.tags);

  //data for projects
  const dataKey = Object.keys(accCategories);
  const dataValue = Object.values(accCategories);
  const color = ['#158685', '#00AEC8', '#125F80', '#0B234C', '#00e070', '#00e038', '#a8e000', '#ffff00']
  const data = [];

  for (let i = 0; i < dataKey.length; i++) {
    data.push({
      name: dataKey[i],
      population: dataValue[i],
      color: color[i],
      legendFontColor: "#7F7F7F",
      legendFontSize: 13
    })
  }

  //data for posted jobs
  const dataKeyProject = Object.keys(accCategoriesOfPostedJob);
  const dataValueProject = Object.values(accCategoriesOfPostedJob);
  const colorProject = ['#158685', '#00AEC8', '#125F80', '#0B234C', '#00e070', '#00e038', '#a8e000', '#ffff00']
  const dataProject = [];

  for (let i = 0; i < dataKeyProject.length; i++) {
    dataProject.push({
      name: dataKeyProject[i],
      population: dataValueProject[i],
      color: colorProject[i],
      legendFontColor: "#7F7F7F",
      legendFontSize: 13
    })
  }

  let images: Array<{ url: string, props: any }> = [];
  artworks.map((artwork, idx) => {
    images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
  })

  useEffect(() => {
    let images: Array<{ url: string, props: any }> = [];
    artworks.map((artwork, idx) => {
      images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
    })
      , [artworks]
  })

  useEffect(() => {
    if (token === null) {
      return
    } else {
      dispatch(getProfile(token))
      dispatch(getArtwork(token))
      dispatch(getTags(token))

      const fetchRequest = async () => {
        const res = await fetch(`${config.BACKEND_URL}/profile/projects/${userId}`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
          }
        })
        const requestListObj = await res.json();
        setRequestList(requestListObj);
        let accCategories: any = {};
        const categories = await getCategory();

        await requestListObj.map((request: IList) => {
          accCategories[categories[request.category_id - 1]] == null ?
            accCategories[categories[request.category_id - 1]] = 1 : accCategories[categories[request.category_id - 1]] += 1;
        })
        setAccCategories(accCategories);
      }

      const fetchPostedJob = async () => {
        const res = await fetch(`${config.BACKEND_URL}/profile/publishedJob/${userId}`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
          }
        })
        const postedJobListObj = await res.json();
        setPostedJobList(postedJobListObj);
        let accPostedJob: any = {};
        const categoriesOfPostedJob = await getCategory();

        await postedJobListObj.map((request: IList) => {
          accPostedJob[categoriesOfPostedJob[request.category_id - 1]] == null ?
            accPostedJob[categoriesOfPostedJob[request.category_id - 1]] = 1 : accPostedJob[categoriesOfPostedJob[request.category_id - 1]] += 1;
        })
        setAccCategoriesOfPostedJob(accPostedJob);
      }


      const getCategory = async () => {
        const res = await fetch(`${config.BACKEND_URL}/request/category`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
          }
        })
        const categoriesResult = await res.json();
        const categories: Array<string> = [];
        categoriesResult.map((category: { id: string, category: string }) => {
          return categories.push(category.category)
        })
        return categories;
      }
      try {
        fetchRequest();
        fetchPostedJob();
      } catch (err) {
        console.error(err.message)
      }
    }

    return (() => {
      dispatch(resetProfile())
      dispatch(resetArtwork())
      dispatch(resetTags())
    })
  }, [token]);


  let customString = profile.self_intro;
  if (customString === null) {
    customString = " "
  }

  let ELASTIC_HEIGHT =
    Math.floor(customString.length / 46) * (FONT_SIZE + 4) + 100;
  if (customString.includes(`\n`)) {
    let timesOccurred = customString.match(/\n/g).length
    ELASTIC_HEIGHT = ELASTIC_HEIGHT + timesOccurred * FONT_SIZE
  }

  var heightInterpolate = heightValue.interpolate({
    inputRange: [60, 170],
    outputRange: [124, ELASTIC_HEIGHT],
  });


  if (ELASTIC_HEIGHT < 400 && ELASTIC_HEIGHT > 124) {
    var heightInterpolate = heightValue.interpolate({
      inputRange: [60, 170],
      outputRange: [124, ELASTIC_HEIGHT],
    });
  } else if (ELASTIC_HEIGHT <= 124) {
    var heightInterpolate = heightValue.interpolate({
      inputRange: [60, 60],
      outputRange: [124, 124],
    });
  } else if (ELASTIC_HEIGHT >= 400) {
    var heightInterpolate = heightValue.interpolate({
      inputRange: [60, 170],
      outputRange: [124, 400],
    });
  }

  let finalHeight = {
    height: heightInterpolate,
  };
  if (customString === " ") {
    finalHeight = {
      height: FONT_SIZE * 3.6
    }
  }

  return (
    <>
      <SafeAreaView
        style={{
          alignSelf: 'center',
          alignItems: 'center',
          justifyContent: 'center',
          width: WINDOW_WIDTH,
        }}>
        <ScrollView
          contentContainerStyle={{
            maxWidth: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 30
          }}>
          <View
            style={{
              justifyContent: 'flex-start',
              alignItems: 'center',
              overflow: 'hidden',
            }}>
            <ProfileLinearGradient>
              <View style={styles.topContainer}>
                <View style={styles.profilePicBox}>
                  {profile.profile_pic === '' && <Image
                    style={styles.profilePic}
                    source={{
                      uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/profilePic-000000001.png`,
                    }} />}
                  {profile.profile_pic != '' && <Image
                    style={styles.profilePic}
                    source={{
                      uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${profile.profile_pic}`,
                    }}
                  />}
                </View>
                <View style={styles.usernameBox}>
                  <View style={styles.nameAndLocation}>
                    <Text style={styles.username}>{profile.username}</Text>
                    <Text style={styles.location}>{profile.email}</Text>
                  </View>
                  {tags.length !== 0 && (
                    <View style={styles.tagBox}>
                      {tags.map((tagObject, index) => (
                        <View key={`tag_${index}`} style={styles.tag}>
                          <Text style={styles.tagText}>{tagObject.tag}</Text>
                        </View>
                      ))}
                    </View>
                  )}
                </View>
              </View>
            </ProfileLinearGradient>
          </View>
          <View style={styles.IntroBoxShadow}>
            <Animated.View style={[styles.IntroBox, finalHeight]}>
              <View style={styles.upperLayerContainer}>
                <Text style={styles.title}>About me</Text>
                {customString != " " &&
                  <TouchableWithoutFeedback
                    style={[
                      {
                        width: 20,
                        height: 20,
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center',
                      },
                    ]}
                    onPress={() => {
                      handlePress2();
                    }}>
                    <Animated.View style={[rotation]}>
                      <AnimatedIconAntDesign
                        style={[styles.icon, finalColor]}
                        name="downcircle"
                      />
                    </Animated.View>
                  </TouchableWithoutFeedback>
                }
              </View>
              {customString != " " &&
                <View style={styles.lowerLayerContainer}>
                  <ScrollView>
                    <Animated.View style={{ opacity: opacityValue }}>
                      <Text style={styles.intro}>
                        {customString}
                      </Text>
                    </Animated.View>
                  </ScrollView>
                </View>
              }
            </Animated.View>
          </View>
          <View style={styles.ArtworkBox}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 6 }}>
              <Text style={styles.title}>Artwork</Text>
              {artworks.length !== 0 && (
                <TouchableOpacity style={styles.view} onPress={() => {
                  props.navigation.navigate('Artwork Preview')
                }}>
                  <Text style={styles.buttonText}>View</Text>
                </TouchableOpacity>
              )}
            </View>
            <View
              style={{
                marginTop: 10,
                paddingHorizontal: 5,
                flexDirection: 'row',
                flexWrap: 'wrap',
                maxHeight: 440,
                overflow: 'scroll',
              }}>
              {artworks.length !== 0 && (
                <>
                  {artworks.map((artworksObject, index) => (
                    <TouchableOpacity key={`artworks_${index}`} onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                      <Image
                        style={{
                          height: 100,
                          width: 100,
                          marginRight: 10,
                          marginBottom: 10,
                          borderWidth: 1,
                          borderColor: '#aaa5'
                        }}
                        source={{
                          uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artworksObject.filename}`,
                        }}
                      />
                    </TouchableOpacity>
                  ))}
                </>
              )}
              {artworks.length == 0 && (
                <Text style={{ color: 'grey' }}>
                  User did not upload any artwork.
                </Text>
              )}
            </View>


            <Modal visible={modalVisible} transparent={true}>
              <View style={{ backgroundColor: 'black', height: 'auto', position: 'absolute', top: 50, right: 15, zIndex: 999 }}>
              </View>
              <ImageViewer
                imageUrls={images}
                menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                onChange={(index) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                index={indexOfImage}
                renderFooter={(currentIndex) => {
                  return (
                    <>
                      {(artworks[currentIndex].description.length >= 1) && (
                        <View style={styles.artworkTextContainer}>
                          <Text style={styles.viewArtworkText}>{artworks[currentIndex].description}</Text>
                        </View>
                      )}
                    </>
                  )
                }}
                enableSwipeDown={true}
                enablePreload={true}
                onSwipeDown={() => {
                  setModalVisible(false)
                }} />
            </Modal>

          </View>
          {video.length >= 1 && <View style={styles.ArtworkBox}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 6 }}>
              <Text style={styles.title}>Videos</Text>
              {video.length !== 0 && (
                <TouchableOpacity style={styles.view} onPress={() => {
                  props.navigation.navigate('Video Preview')
                }}>
                  <Text style={styles.buttonText}>View</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>}
          <View style={styles.ProjectBox}>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 6,
                }}>
                <Text style={styles.title}>Projects: {requestList.length}</Text>
                {requestList.length !== 0 && (
                  <TouchableOpacity style={styles.view} onPress={() => {
                    props.navigation.navigate('ProjectList Preview');
                  }}>
                    <Text style={styles.buttonText}>view</Text>
                  </TouchableOpacity>)}
              </View>
              {requestList.length !== 0 && (
                <View style={styles.projectBoxContainer}>
                  <View style={styles.projectBox}>
                    <PieChart
                      data={data}
                      width={Dimensions.get("window").width * 0.80}
                      height={220}
                      chartConfig={{
                        backgroundColor: "#e26a00",
                        backgroundGradientFrom: "#fb8c00",
                        backgroundGradientTo: "#ffa726",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                          borderRadius: 16
                        },
                        propsForDots: {
                          r: "6",
                          strokeWidth: "2",
                          stroke: "#ffa726"
                        }
                      }}
                      accessor="population"
                      backgroundColor="transparent"
                      paddingLeft="15"
                      absolute
                    />
                  </View>
                </View>
              )}
            </View>
            {userId && <RatingInProfile userId={userId} ratingOn={"freelancer"} />}
          </View>

          <View style={styles.ProjectBox}>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: 6,
                }}>
                <Text style={styles.title}>Published Jobs: {postedJobList.length}</Text>
                {postedJobList.length !== 0 && (<TouchableOpacity style={styles.view} onPress={() => {
                  props.navigation.navigate('Published Jobs List Preview')
                }}>
                  <Text style={styles.buttonText}>view</Text>
                </TouchableOpacity>)}
              </View>
              {postedJobList.length !== 0 && (
                <View style={styles.projectBoxContainer}>
                  <View style={styles.projectBox}>
                    <PieChart
                      data={dataProject}
                      width={Dimensions.get("window").width * 0.80}
                      height={220}
                      chartConfig={{
                        backgroundColor: "#e26a00",
                        backgroundGradientFrom: "#fb8c00",
                        backgroundGradientTo: "#ffa726",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                          borderRadius: 16
                        },
                        propsForDots: {
                          r: "6",
                          strokeWidth: "2",
                          stroke: "#ffa726"
                        }
                      }}
                      accessor="population"
                      backgroundColor="transparent"
                      paddingLeft="15"
                      absolute
                    />
                  </View>
                </View>
              )}
            </View>
            {userId && <RatingInProfile userId={userId} ratingOn={"requester"} />}
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ProfilePreviewScreen;

const styles = StyleSheet.create({
  topContainer: {
    width: WINDOW_WIDTH,
    maxWidth: WINDOW_WIDTH,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  profilePicBox: {
    alignItems: 'center',
    justifyContent: 'center',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  profilePic: {
    borderRadius: WINDOW_WIDTH * 0.175,
    width: WINDOW_WIDTH * 0.35,
    height: WINDOW_WIDTH * 0.35,
    overflow: 'hidden',
  },
  usernameBox: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    minWidth: WINDOW_WIDTH * 0.55,
    maxWidth: WINDOW_WIDTH * 0.61,
    minHeight: WINDOW_HEIGHT * 0.2,
  },
  nameAndLocation: {
    marginVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  username: {
    fontFamily: 'Varela Round',
    fontSize: 18,
    textShadowColor: '#fff',
    textShadowRadius: 5,
    shadowColor: '#fff',
    shadowOpacity: 0.3,
    shadowRadius: 3,
  },
  location: {
    fontFamily: 'Varela Round',
    fontSize: 13,
  },
  tagBox: {
    marginTop: 10,
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
    shadowColor: '#222',
    shadowOpacity: 0.8,
    shadowRadius: 3,
    shadowOffset: { width: 2, height: 3 }
  },
  tag: {
    justifyContent: 'center',
    width: 'auto',
    height: 25,
    backgroundColor: '#111',
    margin: 4,
    marginRight: 5,
    paddingHorizontal: 8,
    borderRadius: 9,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: '#4441',
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 1,
  },
  tagText: {
    fontFamily: 'Varela Round',
    fontSize: 13,
    color: 'cyan',
    textShadowColor: 'cyan',
    textShadowRadius: 5,
  },
  intro: {
    marginVertical: 10,
    fontFamily: 'Varela Round',
    fontSize: FONT_SIZE,
    height: 'auto',
  },
  IntroBoxShadow: {
    width: WINDOW_WIDTH * 0.9,
    height: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  IntroBox: {
    width: WINDOW_WIDTH * 0.9,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingTop: 8,
    marginTop: 15,
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ddd',
    borderWidth: 1,
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    overflow: 'hidden',
  },
  upperLayerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 4,
  },
  lowerLayerContainer: {
    marginTop: 5,
    height: 'auto',
    maxHeight: '81%',
    paddingLeft: 4,
  },
  ArtworkBox: {
    width: WINDOW_WIDTH * 0.9,
    height: 'auto',
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginTop: 16,
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ddd',
    borderWidth: 1,
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  artworkTextContainer: {
    width: WINDOW_WIDTH,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewArtworkText: {
    width: '100%',
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: '#8885',
    fontFamily: 'Varela Round',
    color: '#fff',
    position: 'absolute',
    fontSize: 20,
    bottom: 150,
    textShadowRadius: 3,
    textShadowColor: '#000',
    textShadowOffset: { width: 0, height: 0 },
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
  },
  ProjectBox: {
    width: WINDOW_WIDTH * 0.9,
    height: 'auto',
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginTop: 16,
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ddd',
    borderWidth: 1,
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  title: {
    paddingVertical: 3,
    marginVertical: 8,
    fontFamily: 'Syncopate',
    fontSize: 16,
    color: '#444',
  },

  buttonText: {
    paddingHorizontal: 15,
    color: '#fff',
    textShadowColor: '#eee',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 2,
    shadowColor: '#fff',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 3,
    fontFamily: 'Syncopate',
    textTransform: 'uppercase',
    fontSize: FONT_SIZE * 0.85,
  },
  view: {
    backgroundColor: '#639EFF',
    justifyContent: 'center',
    alignItems: 'center',
    width: WINDOW_WIDTH * 0.19,
    height: FONT_SIZE * 1.8,
    borderRadius: 4,
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 5,
    shadowOpacity: 0.45,
  },
  icon: {
    fontSize: 30,
  },
  ratingIcon: {
    textShadowColor: '#666',
    textShadowRadius: 3,
  },
  emptyStarIcon: {
    color: '#aaa'
  },
  projectBoxContainer: {
    flexDirection: 'row',
  },
  projectBox: {
    height: 200,
    paddingLeft: 10,
    paddingRight: 10,
    shadowColor: '#0006',
    shadowOpacity: 1,
    shadowRadius: 4,
    shadowOffset: { width: 0, height: 6 }
  },
});
