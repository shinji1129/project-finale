import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions, TextInput, Alert, Image, Modal } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { config } from '../../config';
import BlackWhiteLinearGradient from '../../components/BlackWhiteLinearGradient';
import { useNavigation } from '@react-navigation/native';
import { currentProjectStatus } from '../../redux/project/thunk';
import { isCommentThunk } from '../../redux/request/comment/thunk';
import CommentBox from '../../components/commentbox';
import { Artworks } from '../../redux/profile/artwork/state';
import ImageViewer from 'react-native-image-zoom-viewer';
import { IProjectList } from '../../model/model';


const FONT_SIZE = Dimensions.get('window').width * 0.038;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


const PublishedJobPreviewScreen: React.FC = ({ route }: any) => {
    const dispatch = useDispatch();
    const [postedJobList, setPostedJobList] = useState<IProjectList[]>([]);
    const token = useSelector((state: IRootState) => state.auth.token);
    // ProjectStatus
    const projectStatus = useSelector((status: IRootState) => status.project.status);

    const isBothComment = useSelector((state: IRootState) => state.comment.isBothComment)

    //Image
    const [requestImages, setRequestImages] = useState<Artworks[]>([])
    const [modalVisible, setModalVisible] = useState(false);
    const [indexOfImage, setIndexOfImage] = useState(0);


    //Rating
    const [rating, setRating] = useState(0);

    const userId = useSelector((state: IRootState) => state.auth.user?.id)


    const navigation = useNavigation()

    const [expectBudget, setExpectBudget] = useState('');

    const { request_id, requester, title, tag, time_start, duration, venue, budget_min, budget_max, freelancer_id, freelancer, confirmed_budget, expiry_date, status } = route.params;


    const fetchRequestImages = async (request_id: number) => {
        const res = await fetch(`${config.BACKEND_URL}/request/images/${request_id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        })
        const requestImgObj = await res.json();
        setRequestImages(requestImgObj);
    }

    const statusConverter = (status: number | null) => {
        if (status === null) {
            return
        }
        switch (status) {
            case 1:
                return "Pending"
            case 2:
                return "Pending"
            case 3:
                return "Assigned"
            case 4:
                return "Confirming"
            case 5:
                return "Completed"
            case 6:
                return "Commented"
            default:
                return ""
        }
    }

    useEffect(() => {
        if (token) {
            dispatch(currentProjectStatus(token, request_id))

        }
        if (userId && token) {
            console.log('userId inside useEffect!!!!!:', userId)
            dispatch(isCommentThunk(token, userId, request_id))
        }
        fetchRequestImages(request_id)
    }, [projectStatus])

    const applicantStatus = useSelector((state: IRootState) => state.applicant.status)
    const invitedStatus = useSelector((state: IRootState) => state.invited.status);


    useEffect(() => {
        if (token) {
            dispatch(currentProjectStatus(token, request_id))
        }
    }, [applicantStatus, invitedStatus])


    var List: any = [];

    for (let i = 0; i < postedJobList.length; i++) {
        List.push(
            <TouchableOpacity key={`request_${postedJobList[i].id}`} style={styles.requestBoard} onPress={() => {
                navigation.navigate('Request Preview', {
                    request_id: postedJobList[i].id,
                    requester_id: postedJobList[i].requester_id,
                    requester: postedJobList[i].requester,
                    title: postedJobList[i].title,
                    freelancer_id: postedJobList[i].freelancer_id,
                    status: postedJobList[i].status,
                    images: postedJobList[i].images,
                    company_name: postedJobList[i].company_name,
                    details: postedJobList[i].details,
                    budget_min: postedJobList[i].budget_min,
                    budget_max: postedJobList[i].budget_max,
                    confirmed_budget: postedJobList[i].confirmed_budget,
                    time_start: new Date(postedJobList[i].time_start).toDateString(),
                    duration: postedJobList[i].duration,
                    expiry_date: new Date(postedJobList[i].expiry_date).toDateString(),
                    category_id: postedJobList[i].category_id,
                    venue: postedJobList[i].venue,
                    category: postedJobList[i].category,
                    tag: postedJobList[i].tag,
                })
            }}>
                <BlackWhiteLinearGradient>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <View style={styles.innerContent}>
                            <View style={styles.upperLayer}>
                                <Text style={styles.requestTitle}>{postedJobList[i].title}</Text>
                                {postedJobList[i].tag.length > 0 &&
                                    <View key={`request_${i + 1}`} style={styles.tagBox}>
                                        {postedJobList[i].tag.map((tag, idx) => {
                                            return (
                                                <View key={`tag_${idx}`} style={styles.tag}>
                                                    <View>
                                                        <Text style={styles.tagText}>{postedJobList[i].tag.join(' ')}</Text>
                                                    </View>
                                                </View>)
                                        })}
                                    </View>}
                            </View>
                            <View style={styles.lowerLayer}>
                                {postedJobList[i].company_name && (<Text style={styles.requestText}>{postedJobList[i].company_name}</Text>)}
                                {postedJobList[i].venue && (<Text style={styles.requestText}>Venue: {postedJobList[i].venue}</Text>)}
                                {postedJobList[i].time_start && (<Text style={styles.requestText}>Time: {new Date(postedJobList[i].time_start).toDateString()} </Text>)}
                            </View>
                        </View>
                    </View>
                </BlackWhiteLinearGradient>
            </TouchableOpacity>
        )
    }

    useEffect(() => {
        const loadData = navigation.addListener('focus', () => {
            if (token) {
                dispatch(currentProjectStatus(token, request_id))
            }
        })

        return loadData
    }, [navigation])

    let images: Array<{ url: string, props: any }> = [];
    requestImages.map((requestImage, idx) => {
        images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
    })

    useEffect(() => {
        let images: Array<{ url: string, props: any }> = [];
        requestImages.map((requestImage, idx) => {
            images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
        })
    }, [requestImages])

    return (
        <SafeAreaView >
            <BlackWhiteLinearGradient>
                <ScrollView style={{ height: 'auto', width: WINDOW_WIDTH }}>
                    <View style={styles.container}>
                        <View>
                            <View style={{ padding: 15 }}>
                                <Text style={styles.titleText}>{title}</Text>
                                {requester.length >= 1 && <Text style={styles.text}>Job posted by: <Text style={styles.requester}>{requester}</Text> </Text>}

                                <Text style={{ fontFamily: 'Varela Round', paddingLeft: 5, marginTop: 8 }}>Require:</Text>
                                {tag && tag.length >= 1 && <View style={styles.tagBox}>{tag.map((string: string, idx: number) => {
                                    return (
                                        <View key={`tag_${idx}`} style={styles.tag}>
                                            <Text style={styles.tagText} key={`tag_${idx}`}>{string.charAt(0).toUpperCase() + string.slice(1)}</Text>
                                        </View>
                                    )
                                })}
                                </View>
                                }
                            </View>

                        </View>
                        <View style={styles.border}></View>
                        <View style={{ padding: 15 }}>

                            <View style={styles.middleHalf}>
                                {time_start !== new Date(0).toDateString() && time_start.length >= 1 && <Text style={styles.text}>Date: {time_start}</Text>}
                                {venue && <Text style={styles.text}>Venue: {venue} </Text>}
                            </View>
                            <Text style={styles.text}>Reference Image: </Text>
                            <View style={{
                                marginTop: 10,
                                paddingHorizontal: 5,
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                maxHeight: 440,
                                overflow: 'scroll',
                            }}>
                                {requestImages.length >= 1 &&
                                    <>
                                        {requestImages.map((requestImage, index) => (
                                            <TouchableOpacity key={`requestImage_${index}`} onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                                                <Image
                                                    style={{
                                                        height: 100,
                                                        width: 100,
                                                        marginRight: 10,
                                                        marginBottom: 10,
                                                    }}
                                                    source={{
                                                        uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`,
                                                    }}
                                                />
                                            </TouchableOpacity>
                                        ))}
                                    </>}
                            </View>
                        </View>
                        <Modal visible={modalVisible} transparent={true}>
                            <View style={{ backgroundColor: 'black', height: 'auto', position: 'absolute', top: 50, right: 15, zIndex: 999 }}>
                            </View>
                            <ImageViewer
                                imageUrls={images}
                                menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                                onChange={(index) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                                index={indexOfImage}
                                enableSwipeDown={true}
                                enablePreload={true}
                                onSwipeDown={() => {
                                    setModalVisible(false)
                                }} />
                        </Modal>
                        {isBothComment === true && <CommentBox requestId={request_id} />}
                    </View>
                </ScrollView>
            </BlackWhiteLinearGradient>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        minHeight: WINDOW_HEIGHT,
        justifyContent: 'flex-start',
        alignContent: 'center',
        flexWrap: "wrap",
        flexDirection: "column",
    },
    requestBoard: {
        width: Dimensions.get('window').width * 0.9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginVertical: 10,
        backgroundColor: '#C4FFF9', // color for jobs
        borderRadius: 7,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.4,
        shadowRadius: 6,
    },

    titleText: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.45,
        textTransform: 'capitalize',
        borderRadius: 5,
        padding: 5,
        width: WINDOW_WIDTH * 0.9,
    },

    text: {
        maxWidth: '95%',
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE,
        borderRadius: 5,
        padding: 5,
        color: '#090909',
    },

    requester: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.40,
        textTransform: 'capitalize',
    },

    tagBox: {
        marginTop: 15,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },

    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    innerContent: {
        width: '98%',
        paddingVertical: 7
    },
    requestTitle: {
        paddingLeft: 9,
        fontFamily: 'Varela Round',
        fontSize: 19,
        marginBottom: 3,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    upperLayer: {
        paddingHorizontal: 3,
        marginBottom: 6,
    },
    lowerLayer: {
        paddingHorizontal: 7,
    },
    border: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: WINDOW_WIDTH * 0.9,
        borderBottomColor: '#7775',
        borderBottomWidth: 1,
        paddingBottom: 18,
        justifyContent: 'flex-start',
    },
    middleHalf: {
        marginTop: 15,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: 1
    },
    pageEndButtons: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30

    },
    rows: {
        marginTop: WINDOW_HEIGHT * 0.015,
        minWidth: WINDOW_WIDTH * 0.5,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    buttonText: {
        paddingHorizontal: 5,
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.9,
    },
    chatBox: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    errorMessage: {
        fontFamily: 'Varela Round',
        color: 'red',
    },
    applicantList: {
        backgroundColor: '#rgb(0,184,155)',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    requestText: {
        fontFamily: 'Varela Round',
        fontSize: 14,
        marginVertical: 4,
        paddingHorizontal: 5,
    },

    invitedList: {
        backgroundColor: '#548EF6',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },

    invitation: {
        maxWidth: WINDOW_WIDTH * 0.9,
        marginVertical: 15,
        borderRadius: 6,
        shadowColor: '#0009',
        shadowOffset: { width: 3, height: 7 },
        shadowOpacity: 1,
        shadowRadius: 8
    },
    accept: {
        width: '100%',
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    decline: {
        width: '100%',
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    bookmark: {
        top: '-12%',
        height: 43,
        color: '#7BFEFF',
        textShadowColor: '#0007',
        shadowOpacity: 0.4,
        textShadowRadius: 6,
        textShadowOffset: { width: 0, height: 8 },
        zIndex: 1,
    },
    input: {
        padding: 5,
        marginTop: 10,
        minHeight: FONT_SIZE + 20,
        width: WINDOW_WIDTH * 0.8,
        borderColor: '#888',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
    },
    apply: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },

    submit: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: Dimensions.get('window').width * 0.3,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },
    
    cancel: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: Dimensions.get('window').width * 0.3,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        backgroundColor: '#BF4848',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },
    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
    },
    emptyStarIcon: {
        color: '#ccc'
    },
});

export default PublishedJobPreviewScreen;