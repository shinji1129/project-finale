import React, { useEffect } from 'react';
import { View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../redux/store';
import { restoreLoginThunk } from '../../redux/auth/thunks';
import { StackActions, useNavigation } from '@react-navigation/native';
import LoadingBall from '../../components/LoadingBall';
import LinearGradient from 'react-native-linear-gradient';

const LoadingScreen: React.FC = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated,
  );

  useEffect(() => {
    dispatch(restoreLoginThunk());
  }, [useDispatch]);

  useEffect(() => {
    if (isAuthenticated == null) {
      return;
    }
    if (isAuthenticated) {
      if (navigation.canGoBack()) {
        navigation.dispatch(StackActions.popToTop());
      }
      navigation.dispatch(StackActions.push('Navbar'));
    } else {
      if (navigation.canGoBack()) {
        navigation.dispatch(StackActions.popToTop());
      }
      navigation.dispatch(StackActions.push('Landing'));
    }
  }, [isAuthenticated]);

  return (
    <LinearGradient
      colors={['#FFF', 'transparent', '#FFF']}
      useAngle={true}
      angle={180}>
      <View
        style={{
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <LoadingBall />
      </View>
    </LinearGradient>
  );
};

export default LoadingScreen;
