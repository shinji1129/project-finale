import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  SafeAreaView,
  Dimensions,
  Animated,
} from 'react-native';
import {
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { loginThunk } from '../../redux/auth/thunks';
import { useNavigation } from '@react-navigation/native';
import { IRootState } from '../../redux/store';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import JCLogo from '../../components/JCLogo';

const FONT_SIZE = Dimensions.get('window').width * 0.04;

const LoginScreen: React.FC = () => {
  let mode = new Animated.Value(0);
  let buttonSize = new Animated.Value(1);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();

  const navigation = useNavigation();
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated,
  );

  const onLogin = () => {
    dispatch(loginThunk(username, password));
  };

  useEffect(() => {
    if (isAuthenticated) {
      navigation.navigate('Navbar');
    }
  }, [dispatch]);

  const handlePress = () => {
    Animated.sequence([
      Animated.timing(buttonSize, {
        toValue: 0.9,
        duration: 80,
        useNativeDriver: false,
      }),
      Animated.timing(buttonSize, {
        toValue: 1,
        useNativeDriver: false,
      }),
      Animated.timing(mode, {
        toValue: 0 ? 1 : 0,
        useNativeDriver: false,
      }),
    ]).start();
  };

  const sizeStyle = {
    transform: [{ scale: buttonSize }],
  };
  return (
    <SafeAreaView>
      <KeyboardAwareScrollView
        extraHeight={180}
        contentContainerStyle={styles.login}
        resetScrollToCoords={{ x: 0, y: 0 }}
        scrollEnabled={false}>
        <View
          style={{
            height: Dimensions.get('window').height * 0.55,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <JCLogo />
        </View>
        <TextInput
          autoCapitalize={'none'}
          placeholder="U S E R N A M E"
          style={styles.text}
          onChangeText={setUsername}
          autoCorrect={false}
        />
        <TextInput
          secureTextEntry={true}
          autoCapitalize={'none'}
          placeholder="P A S S W O R D"
          style={styles.text}
          onChangeText={setPassword}
          autoCorrect={false}
        />
        <Animated.View style={sizeStyle}>
          <TouchableWithoutFeedback
            style={styles.submit}
            onPress={() => {
              handlePress();
              onLogin();
            }}>
            <Text style={styles.buttonText}>submit</Text>
          </TouchableWithoutFeedback>
        </Animated.View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  login: {
    height: Dimensions.get('window').height,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  text: {
    paddingTop: FONT_SIZE * 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: FONT_SIZE + 20,
    width: Dimensions.get('window').width * 0.8,
    borderRadius: (Dimensions.get('window').width * 0.8) / 2,
    borderWidth: 1,
    borderColor: '#aaa',
    marginVertical: 7,
    fontFamily: 'varela round',
    fontSize: FONT_SIZE,
    color: '#333',
  },
  rows: {
    width: '70%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  submit: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    marginTop: 20,
    width: Dimensions.get('window').width * 0.3,
    height: FONT_SIZE * 2.5,
    borderRadius: 7,
    padding: 10,
    backgroundColor: '#6a6a6a',
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 0.7,
  },
  buttonText: {
    color: 'white',
    textShadowColor: 'white',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 4,
    shadowColor: 'white',
    shadowOpacity: 4,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 4,
    fontFamily: 'Syncopate',
    textTransform: 'uppercase',
    fontSize: FONT_SIZE,
  },
});

export default LoginScreen;
