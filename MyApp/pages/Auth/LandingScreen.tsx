
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Dimensions,
  useWindowDimensions,
} from 'react-native';

import LoginButtons from '../../components/LoginButtons';
import JCLogo from '../../components/JCLogo';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';

declare const global: {HermesInternal: null | {}};

const LandingScreen: React.FC = (props: any) => {
  const navigation = useNavigation();
  const dimension = useWindowDimensions();
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated,
  );

  useEffect(() => {
    if (isAuthenticated) {
      navigation.dispatch(
        CommonActions.navigate({
          name:'Navbar'
        })
      );
    }
  }, [isAuthenticated]);

  return (
    <>
      <SafeAreaView>
        <View style={styles.container}>
          <View style={{marginVertical: dimension.height * 0.07}}>
            <JCLogo />
          </View>
          <LoginButtons props={props} />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  logoMargin: {
    marginVertical: Dimensions.get('window').height * 0.07,
  },
});

export default LandingScreen;
