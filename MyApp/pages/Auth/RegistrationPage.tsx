import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, TextInput, Image, Dimensions } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import JCLogo from '../../components/JCLogo';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-crop-picker';
import { useDispatch, useSelector } from 'react-redux';
import { registerAccountThunk } from '../../redux/register/thunks';
import { IRootState } from '../../redux/store';
import { useNavigation } from '@react-navigation/native';
import LoadingBall from '../../components/LoadingBall';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';

const FONT_SIZE = Dimensions.get('window').width * 0.04;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const BUTTON_WIDTH = Dimensions.get('window').width * 0.3;
const BUTTON_HEIGHT = FONT_SIZE * 2.5;
const noSpecial = /^[^!*#%+=,?.|\~":<>[\]{}\\() ';@&$]+$/;


const RegistrationPage: React.FC = () => {
  const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
  const navigation = useNavigation();

  const { handleSubmit, register, errors, control } = useForm();
  const [profilePic, setProfilePic] = useState(
    '../../sample/image/defaultAvatar_3.png',
  );
  const [uploadProfilePic, setUploadProfilePic] = useState({
    path: '',
    size: 0,
    width: 0,
    height: 0,
    mime: '',
  });
  const [isProPic, setIsProPic] = useState(false);
  const dispatch = useDispatch();
  const onSubmit = (values: Record<string, any>) => {
    return (
      dispatch(
        registerAccountThunk(
          values.username,
          values.password,
          values.confirmPassword,
          values.email,
          uploadProfilePic,
          values.introduction,
        ),
      ),
      console.log(values, uploadProfilePic)
    );
  };
  const onCancel = () => {
    return navigation.navigate('Landing');
  };

  const isProcessing = useSelector((state: IRootState) => state.register.isProcessing)

  useEffect(() => {
    if (isAuthenticated == null) {
      return;
    }
    if (isAuthenticated) {
      navigation.navigate('Navbar');
    } else {
      navigation.navigate('Register');
    }
  }, [isAuthenticated]);



  useEffect(() => {
    console.log('register is processing:', isProcessing)
  }, [isProcessing])

  console.log('isAuthenticated', isAuthenticated);

  return (
    <>
      {(isProcessing) &&
        <View style={{ height: WINDOW_HEIGHT * 0.85, width: WINDOW_WIDTH, justifyContent: 'center', opacity: 0.6, position: 'absolute', zIndex: 2, backgroundColor: 'white' }}>
          <LoadingBall />
        </View>}
      <SafeAreaView
        style={styles.SafeAreaView}>
        <KeyboardAwareScrollView
          extraHeight={180}
          // contentContainerStyle={styles.login}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}>
          {/* {isProcessing && <View style={{backgroundColor: "black", height:"100%", width:"100%"}}></View>} */}
          <View style={styles.registration}>
            <JCLogo />
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Username</Text>
              <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                  <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}
                    onBlur={onBlur}
                    onChangeText={(value) => {
                      onChange(value);
                      // checkInput(value);
                    }}
                    // onChangeText={setUsername}
                    value={value}
                  />
                )}
                name="username"
                rules={{
                  required: true,
                  pattern: {
                    value: /^[^!*#%+=,?.|\~":<>[\]{}\\() ';@&$]+$/,
                    message: 'Special characters other than - or _ are not allowed',
                  },
                }}
                defaultValue=""
              />
              {errors.username && (
                <Text style={styles.errorMessage}>This field is required.</Text>
              )}
              {errors.username && (
                <Text style={styles.errorMessage}>{errors.username.message}</Text>
              )}

            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Password</Text>
              <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                  <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}
                    onBlur={onBlur}
                    secureTextEntry={true}
                    onChangeText={(value) => onChange(value)}
                    // onChangeText={setPassword}
                    value={value}
                  />
                )}
                name="password"
                rules={{ required: true }}
                defaultValue=""
              />
              {errors.password && (
                <Text style={styles.errorMessage}>This field is required.</Text>
              )}
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Confirm Password</Text>
              <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                  <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}
                    onBlur={onBlur}
                    secureTextEntry={true}
                    onChangeText={(value) => onChange(value)}
                    // onChangeText={setConfirmedPassword}
                    value={value}
                  />
                )}
                name="confirmPassword"
                rules={{ required: true }}
                defaultValue=""
              />
              {errors.confirmPassword && (
                <Text style={styles.errorMessage}>This field is required.</Text>
              )}
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Email</Text>
              <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                  <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={styles.input}
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    // onChangeText={setEmail}
                    value={value}
                  />
                )}
                name="email"
                rules={{
                  required: true,
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                    message: 'invalid email address',
                  },
                }}
                defaultValue=""
              />
              {errors.email && (
                <Text style={styles.errorMessage}>
                  Please input a valid email address.
                </Text>
              )}
            </View>
            <View style={styles.optionalContainer}>
              <Text style={styles.text}>Profile Picture: (Optional)</Text>
              <View style={styles.imageContainer}>
                {!isProPic && (
                  <Image
                    source={require('../../sample/image/defaultAvatar_3.png')}
                    style={{ width: 200, height: 200, borderRadius: 100 }}
                  />
                )}
                {isProPic && (
                  <Image
                    source={{ uri: profilePic }}
                    style={{ width: 200, height: 200, borderRadius: 100 }}
                    resizeMode={'cover'}
                  />
                )}
              </View>
              <TouchableOpacity
                style={styles.upload}
                onPress={() => {
                  ImagePicker.openPicker({
                    mediaType:'photo',
                    width: 200,
                    height: 200,
                    cropperCircleOverlay: false,
                    avoidEmptySpaceAroundImage: true,
                    cropping: false,
                  }).then((image) => {
                    console.log(image);
                    setUploadProfilePic(image);
                    // image.sourceURL?setProfilePic(image.path):'';
                    image.sourceURL ? setProfilePic(image.sourceURL) : '';
                    image.sourceURL ? setIsProPic(true) : '';
                  });
                }}>
                <Text style={styles.buttonText}> Upload</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Self-Introduction:(Optional)</Text>
              <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                  <TextInput
                    multiline={true}
                    style={styles.selfIntroInput}
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    // onChangeText={setIntro}
                    value={value}
                  />
                )}
                name="introduction"
                rules={{ required: false }}
                defaultValue=""
              />
            </View>
            <View style={styles.rows}>
              <TouchableOpacity style={styles.cancel} onPress={onCancel}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.submit}
                onPress={handleSubmit(onSubmit)}>
                <Text style={styles.buttonText}>confirm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  SafeAreaView: {
    minHeight: WINDOW_HEIGHT,
    height: 'auto',
    alignItems: 'center',
  },
  registration: {
    marginBottom: 200,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'grey',
    width: WINDOW_WIDTH,
    paddingHorizontal: 20,
  },
  inputContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 13,
  },
  optionalContainer: {
    justifyContent: 'center',
    alignItems: 'center',

    marginTop: 35,
    marginBottom: 25,
  },
  text: {
    fontFamily: 'Varela Round',
    fontSize: FONT_SIZE,
    color: 'grey',
    marginBottom: 5,
  },
  input: {
    height: FONT_SIZE + 20,
    width: WINDOW_WIDTH * 0.8,
    borderColor: '#aaa',
    borderStyle: 'solid',
    borderRadius: 30,
    borderWidth: 1,
    fontFamily: 'Varela Round',
    paddingHorizontal: 10,
  },
  selfIntroInput: {
    minHeight: FONT_SIZE + 50,
    height: 'auto',
    width: WINDOW_WIDTH * 0.8,
    borderColor: '#aaa',
    borderStyle: 'solid',
    borderRadius: 7,
    borderWidth: 1,
    fontFamily: 'Varela Round',
    paddingHorizontal: 10,
  },
  rows: {
    marginTop: WINDOW_HEIGHT * 0.1,
    width: WINDOW_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  upload: {
    marginVertical: 10,
    backgroundColor: '#4EC9B0',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,
    borderRadius: 7,
    padding: 10,
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 0.7,
  },
  buttonText: {
    color: 'white',
    textShadowColor: '#aaa',
    textShadowOffset: { width: 0, height: 0 },
    textShadowRadius: 4,
    shadowColor: 'white',
    shadowOpacity: 4,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 4,
    fontFamily: 'Syncopate',
    textTransform: 'uppercase',
    fontSize: FONT_SIZE,
  },
  submit: {
    backgroundColor: '#4EC9B0',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    width: Dimensions.get('window').width * 0.3,
    height: FONT_SIZE * 2.5,
    borderRadius: 7,
    padding: 10,
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 0.7,
  },
  cancel: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 20,
    width: Dimensions.get('window').width * 0.3,
    height: FONT_SIZE * 2.5,
    borderRadius: 7,
    padding: 10,
    backgroundColor: '#BF4848',
    shadowColor: '#666',
    shadowOffset: { width: 0, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 0.7,
  },
  imageContainer: {
    width: 'auto',
    height: 'auto',
    marginVertical: 10,
  },
  errorMessage: {
    fontFamily: 'Varela Round',
    color: 'red',
  },
});

export default RegistrationPage;
