import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { config } from '../../../config';
import { IRootState } from '../../../redux/store';
import { useNavigation } from '@react-navigation/native';
import BlackWhiteLinearGradient from '../../../components/BlackWhiteLinearGradient';
import { IProjectList } from '../../../model/model';


const PublishedJobPublisherJobList: React.FC = ({ route }: any) => {

    const [postedJobList, setPostedJobList] = useState<IProjectList[]>([]);
    const token = useSelector((state: IRootState) => state.auth.token);
    const navigation = useNavigation();

    const { user_id } = route.params;

    useEffect(() => {
        const loadData = navigation.addListener('focus', () => {
            const fetchRequest = async () => {
                const res = await fetch(`${config.BACKEND_URL}/profile/publishedJob/${user_id}`, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + token,
                    }
                })
                const postedJobListObj = await res.json();
                setPostedJobList(postedJobListObj);
            }
            try {
                fetchRequest();
            } catch (err) {
                console.error(err.message)
            }
        })
        return loadData;
    }, [navigation])

    var List: any = [];


    for (let i = 0; i < postedJobList.length; i++) {
        List.push(
            <TouchableOpacity key={`request_${postedJobList[i].id}`} style={styles.requestBoard} onPress={() => {
                navigation.navigate('Published Job', {
                    request_id: postedJobList[i].id,
                    requester_id: postedJobList[i].requester_id,
                    requester: postedJobList[i].requester,
                    title: postedJobList[i].title,
                    freelancer_id: postedJobList[i].freelancer_id,
                    freelancer: postedJobList[i].freelancer,
                    status: postedJobList[i].status,
                    images: postedJobList[i].images,
                    company_name: postedJobList[i].company_name,
                    details: postedJobList[i].details,
                    budget_min: postedJobList[i].budget_min,
                    budget_max: postedJobList[i].budget_max,
                    confirmed_budget: postedJobList[i].confirmed_budget,
                    time_start: new Date(postedJobList[i].time_start).toDateString(),
                    duration: postedJobList[i].duration,
                    expiry_date: new Date(postedJobList[i].expiry_date).toDateString(),
                    category_id: postedJobList[i].category_id,
                    venue: postedJobList[i].venue,
                    category: postedJobList[i].category,
                    tag: postedJobList[i].tag,
                })
            }}>
                <BlackWhiteLinearGradient>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 5, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <View style={styles.innerContent}>
                            <View style={styles.upperLayer}>
                                <Text style={styles.requestTitle}>{postedJobList[i].title}</Text>
                                {postedJobList[i].tag.length > 0 &&
                                    <View key={`request_${i + 1}`} style={styles.tagBox}>
                                        {postedJobList[i].tag.map((tag, idx) => {
                                            return (
                                                <View key={`tag_${idx}`} style={styles.tag}>
                                                    <View>
                                                        <Text style={styles.tagText}>{postedJobList[i].tag.join(' ')}</Text>
                                                    </View>
                                                </View>)
                                        })}
                                    </View>}
                            </View>
                            <View style={styles.lowerLayer}>
                                {postedJobList[i].company_name && (<Text style={styles.requestText}>{postedJobList[i].company_name}</Text>)}
                                {postedJobList[i].venue && (<Text style={styles.requestText}>Venue: {postedJobList[i].venue}</Text>)}
                                {postedJobList[i].time_start && (<Text style={styles.requestText}>Time: {new Date(postedJobList[i].time_start).toDateString()}</Text>)}
                            </View>
                        </View>
                    </View>
                </BlackWhiteLinearGradient>
            </TouchableOpacity>
        )
    }





    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 0.9,
                    }}>
                    {(<View style={styles.container}>
                        <View>
                            {List}
                        </View>
                    </View>)}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default PublishedJobPublisherJobList;


const styles = StyleSheet.create({
    container: {
        minHeight: '100%',
        justifyContent: 'center',
        alignContent: 'flex-start',
        flexWrap: "wrap",
        flexDirection: "row",
        paddingBottom: 30

    },
    requestBoard: {
        width: Dimensions.get('window').width * 0.9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginVertical: 10,
        backgroundColor: '#C4FFF9', // color for jobs
        borderRadius: 7,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.4,
        shadowRadius: 6,
    },

    innerContent: {
        width: '98%',
        paddingVertical: 7
    },
    upperLayer: {
        paddingHorizontal: 3,
        marginBottom: 6,
    },
    lowerLayer: {
        paddingHorizontal: 7,
        },
    requestTitle: {
        paddingLeft: 9,
        maxWidth: '95%',
        fontFamily: 'Varela Round',
        fontSize: 19,
        marginBottom: 3,
    },
    tagBox: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    requestText: {
        fontFamily: 'Varela Round',
        fontSize: 14,
        marginVertical: 4,
        paddingHorizontal: 5,
    },

    notification: {
        position: 'relative',
        top: '4%',
        backgroundColor: '#ED6C60',
        justifyContent: 'center',
        alignItems: 'center',
        width: 30,
        height: 30,
        borderRadius: 20
    },

    bookmark: {
        top: '-12%',
        height: 43,
        color: '#7BFEFF',
        textShadowColor: '#0007',
        shadowOpacity: 0.4,
        textShadowRadius: 6,
        textShadowOffset: { width: 0, height: 8 },
        zIndex: 1,
    },

    flatList: {
        paddingLeft: 15,
        marginTop: 15,
        paddingBottom: 15,
        fontSize: 20,
        borderBottomColor: '#26a69a',
        borderBottomWidth: 1
    }
})
