import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { config } from '../../../config';
import { useNavigation } from '@react-navigation/native';
import RequestSearchBar from '../../../components/requestSearchBar';


const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HomeScreen: React.FC = () => {
  const [categories, setCategories] = useState([]);
  const [categoryID, setCategoryID] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    const fetchCategory = async () => {
      const res = await fetch(`${config.BACKEND_URL}/request/category`);
      const categoriesObj = await res.json();
      const categories = categoriesObj.map(
        (category: string) => Object.values(category)[1],
      );
      setCategories(categories);
      const categoryID = categoriesObj.map(
        (category: string) => Object.values(category)[0],
      );
      setCategoryID(categoryID);
    };
    try {
      fetchCategory();
    } catch (err) {
      console.error(err.message);
    }
  }, []);

  function switchPic(categories: string) {

    switch (categories) {
      case 'Design':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/design.png')}
            />
          </View>
        );
      case 'Computer Programming':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/programming.png')}
            />
          </View>
        );
      case 'Make-up':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/makeUp.png')}
            />
          </View>
        );
      case 'Translation':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/translation.png')}
            />
          </View>
        );
      case 'Creative Writing':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/creativeWriting.png')}
            />
          </View>
        );
      case 'Music':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/music.png')}
            />
          </View>
        );
      case 'Photography':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/photography.png')}
            />
          </View>
        );
      case 'Video Related':
        return (
          <View style={styles.imageBox}>
            <Image
              style={styles.image}
              source={require('../../../assets/videoEdit.png')}
            />
          </View>
        );
      default:
        <View></View>;
    }
    // }
  }

  useEffect(() => {
    const fetchCategory = async () => {
      const res = await fetch(`${config.BACKEND_URL}/request/category`);
      const categoriesObj = await res.json();
      const categories = categoriesObj.map(
        (category: string) => Object.values(category)[1],
      );
      setCategories(categories);
    };
    try {
      fetchCategory();
    } catch (err) {
      console.error(err.message);
    }
  }, []);

  return (
    <SafeAreaView>
      <RequestSearchBar />
      {categories && (
        <ScrollView>
          <View style={styles.container}>
            {categories.map((category, idx) => {
              return (
                <View key={`category_${idx}`} style={styles.categoryBoard}>
                  <TouchableOpacity
                    style={styles.categoryButton}
                    onPress={() => {
                      navigation.navigate('RequestList', {
                        category: category,
                        category_id: categoryID[idx],
                      });
                    }}>
                    {switchPic(category)}
                    <Text style={styles.categoryText}> {category} </Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  categoryBoard: {
    width: '43%',
    height: '17%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 6,
    margin: 10,
    borderColor: '#eee',
    borderWidth: 1,
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  categoryText: {
    fontSize: 13,
    textAlign: 'center',
    fontFamily: 'Syncopate',
  },
  categoryButton: {
    width: '100%',
    height: '100%',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  imageBox: {
    maxWidth: WINDOW_WIDTH * 0.3,
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 2,
    marginBottom: 8
  },
  image: {
    width: undefined,
    height: '100%',
    aspectRatio: 1,
  },
});
