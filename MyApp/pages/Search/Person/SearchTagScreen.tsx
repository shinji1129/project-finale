import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions, ScrollView, FlatList } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { config } from '../../../config';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector } from 'react-redux';
import { IRootState } from '../../../redux/store';
import RequestSearchBar from '../../../components/requestSearchBar';
import BlackWhiteLinearGradient from '../../../components/BlackWhiteLinearGradient';
import { IList } from '../../../model/model';

const FONT_SIZE = Dimensions.get('window').width * 0.038;
const WINDOW_WIDTH = Dimensions.get('window').width;

const SearchTagScreen: React.FC = ({ route }: any) => {

    const [requestList, setRequestList] = useState<IList[]>([]);
    const token = useSelector((state: IRootState) => state.auth.token);
    //bookmark
    const bookmark = useSelector((state: IRootState) => state.bookmark.bookmark);

    const navigation = useNavigation();

    const { tag_id, categoryID } = route.params;


    useEffect(() => {
        const fetchRequests = async () => {
            const res = await fetch(`${config.BACKEND_URL}/request/requestList/Search/${tag_id}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            })
            const requestListObj = await res.json();
            setRequestList(requestListObj);
        }

        const fetchRequestsByCategory = async () => {
            const res = await fetch(`${config.BACKEND_URL}/request/requestList/Search/${categoryID}/${tag_id}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            })
            const requestListObj = await res.json();
            setRequestList(requestListObj);
        }
        try {
            if (categoryID) {
                fetchRequestsByCategory();
            } else {
                fetchRequests();
            }
        } catch (err) {
            console.error(err.message)
        }
    }, [bookmark])

    var List: any = [];

    for (let i = 0; i < requestList.length; i++) {

        List.push(
            <TouchableOpacity key={`request_${requestList[i].request_id}`} style={styles.requestBoard} onPress={() => {
                navigation.navigate('Request', {
                    request_id: requestList[i].request_id,
                    requester_id: requestList[i].requester_id,
                    requester: requestList[i].requester,
                    title: requestList[i].title,
                    freelancer_id: requestList[i].freelancer_id,
                    status: requestList[i].status,
                    images: requestList[i].images,
                    company_name: requestList[i].company_name,
                    details: requestList[i].details,
                    budget_min: requestList[i].budget_min,
                    budget_max: requestList[i].budget_max,
                    confirmed_budget: requestList[i].confirmed_budget,
                    time_start: new Date(requestList[i].time_start).toDateString(),
                    duration: requestList[i].duration,
                    expiry_date: new Date(requestList[i].expiry_date).toDateString(),
                    category_id: requestList[i].category_id,
                    venue: requestList[i].venue,
                    category: requestList[i].category,
                    tag: requestList[i].tag,
                    tag_id: requestList[i].tag_id
                })
            }}>
                <BlackWhiteLinearGradient>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 5, paddingVertical: 3, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <View style={styles.innerContent}>
                            <View style={styles.upperLayer}>
                                <Text style={styles.requestTitle}>{requestList[i].title}</Text>
                                {requestList[i].tag.length >= 1 &&
                                    <View style={styles.tagBox}>
                                        {requestList[i].tag.map((tag, idx) => {
                                            return (
                                                <View key={`tag_${idx}`} style={styles.tag}>
                                                    <TouchableOpacity onPress={() => {
                                                        //@ts-ignore
                                                        navigation.push('Search', {
                                                            tag: tag,
                                                            tag_id: requestList[i].tag_id[idx]
                                                        })
                                                    }}>
                                                        <Text style={styles.tagText}>{tag.charAt(0).toUpperCase() + tag.slice(1)}</Text>
                                                    </TouchableOpacity>
                                                </View>)
                                        })}
                                    </View>}
                            </View>
                            <View style={styles.lowerLayer}>
                                {requestList[i].company_name && (<Text style={styles.requestText}>{requestList[i].company_name}</Text>)}
                                {requestList[i].budget_min && (<Text style={styles.requestText}>Budget:$ {requestList[i].budget_min} ~ {requestList[i].budget_max}</Text>)}
                                {requestList[i].venue && (<Text style={styles.requestText}>Venue: {requestList[i].venue}</Text>)}
                                {requestList[i].time_start && (<Text style={styles.requestText}>Time: {new Date(requestList[i].time_start).toDateString()}</Text>)}
                            </View>
                        </View>
                        <View style={{ height: '100%', position: 'absolute', right: '4%', top: 5, justifyContent: 'flex-start', alignItems: 'flex-end' }}>
                            {requestList[i].bookmark === true && <Icon name='bookmark' size={30} style={styles.bookmark} />}
                        </View>
                    </View>
                </BlackWhiteLinearGradient>
            </TouchableOpacity>

        )
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView >
                <RequestSearchBar />
                {(<View style={styles.container}>
                    <View>
                        {List}
                        <View style={{ height: 30 }}></View>
                    </View>
                </View>)}

            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignContent: 'flex-start',
        flexWrap: "wrap",
        flexDirection: "row",
    },
    requestBoard: {
        width: Dimensions.get('window').width * 0.9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginVertical: 10,
        backgroundColor: '#C4FFF9', // color for jobs
        borderRadius: 7,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.4,
        shadowRadius: 6,
    },
    innerContent: {
        width: '98%',
        paddingVertical: 7
    },
    upperLayer: {
        paddingHorizontal: 3,
        marginBottom: 6,
    },
    lowerLayer: {
        paddingHorizontal: 7,
    },
    requestTitle: {
        maxWidth: WINDOW_WIDTH * 0.535,
        paddingLeft: 9,
        fontFamily: 'Varela Round',
        flexWrap: 'wrap',
        fontSize: 17.5,
        marginBottom: 3,
        marginTop: 5,
    },
    tagBox: {
        flexDirection: 'row',
        maxWidth: WINDOW_WIDTH * 0.535,
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    requestText: {
        fontFamily: 'Varela Round',
        fontSize: 14,
        marginVertical: 4,
        paddingHorizontal: 5,
    },

    bookmarkButtonContainer: {
        top: -6,
        zIndex: 1,
        padding: 4
    },

    bookmark: {
        top: '-7.5%',
        height: 43,
        color: '#7BFEFF',
        textShadowColor: '#0007',
        shadowOpacity: 0.4,
        textShadowRadius: 6,
        textShadowOffset: { width: 0, height: 8 },
        zIndex: 1,
    },

    flatList: {
        paddingLeft: 15,
        marginTop: 15,
        paddingBottom: 15,
        fontSize: 20,
        borderBottomColor: '#26a69a',
        borderBottomWidth: 1
    }
});


export default SearchTagScreen;