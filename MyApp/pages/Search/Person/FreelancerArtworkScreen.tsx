import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, Modal, Image, Dimensions, StyleSheet } from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Menu from '../../../components/Menu';
import ImageViewer from 'react-native-image-zoom-viewer';
import { config } from '../../../config';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../../redux/store';
import { useNavigation } from '@react-navigation/native';
import { getArtworkByID } from '../../../redux/profile/artwork/thunk';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


const FreelancerArtworkScreen: React.FC = ({ route }: any) => {
  const [modalVisible, setModalVisible] = useState(false);
  const artworks = useSelector((state: IRootState) => state.artwork.artworks);
  const [indexOfImage, setIndexOfImage] = useState(0)

  const images: Array<{ url: string, props: any }> = [];

  artworks.map((artwork, idx) => {
    images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
  })

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView>
        <View style={{
          maxWidth: WINDOW_WIDTH,
          height: WINDOW_HEIGHT * 0.9,
          overflow: 'hidden',
        }}>
          <View
            style={{
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
              flexWrap: 'wrap',
              overflow: 'hidden',
            }}>
            {artworks.length !== 0 && (
              <>
                {artworks.map((artworksObject, index) => (
                  <View key={`artworks_${index}`}>
                    <TouchableOpacity
                      onPress={() => { setModalVisible(true), setIndexOfImage(index) }} style={{ margin: 4 }}>
                      <Image
                        style={{
                          borderWidth: 1,
                          borderColor: '#aaa7',
                          height: 110,
                          width: 110,
                          margin: 10
                        }}
                        source={{
                          uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artworksObject.filename}`,
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                ))}
              </>
            )}
            {artworks.length == 0 && (
              <Text style={{ color: 'grey' }}>
                User did not upload any artworks.
              </Text>
            )}
          </View>
          <Modal visible={modalVisible} transparent={true}>
            <ImageViewer
              imageUrls={images}
              index={indexOfImage}
              renderFooter={(currentIndex) => {
                return (
                  <>
                    {(artworks[currentIndex].description.length >= 1) && (
                      <View style={styles.artworkTextContainer}>
                        <Text style={styles.viewArtworkText}>{artworks[currentIndex].description}</Text>
                      </View>
                    )}
                  </>

                )
              }}
              enableSwipeDown={true}
              onSwipeDown={() => {
                setModalVisible(false)
              }} />
          </Modal>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default FreelancerArtworkScreen;

const styles = StyleSheet.create({
  ArtworkBox: {
    width: WINDOW_WIDTH * 0.9,
    height: 'auto',
    marginHorizontal: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginTop: 16,
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ddd',
    borderWidth: 1,
    shadowColor: '#0006',
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
  },
  artworkTextContainer: {
    width: WINDOW_WIDTH,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewArtworkText: {
    width: '100%',
    paddingVertical: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: '#8885',
    fontFamily: 'Varela Round',
    color: '#fff',
    position: 'absolute',
    fontSize: 20,
    bottom: 150,
    textShadowRadius: 3,
    textShadowColor: '#000',
    textShadowOffset: { width: 0, height: 0 },
    shadowRadius: 1,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
  },
})