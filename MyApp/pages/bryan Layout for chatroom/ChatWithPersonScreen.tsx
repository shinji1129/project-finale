import React, { useState } from 'react'
import { Animated, Dimensions, Keyboard, KeyboardAvoidingView, StyleSheet, Text, View } from 'react-native'
import { ScrollView, TextInput, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import MsgBlackWhiteLinearGradient from '../../components/MsgBlackWhiteLinearGradient'
import MsgInvitationGradient from '../../components/MsgInvitationGradient'
import { Controller, useForm } from 'react-hook-form'
import InputMsgBlackWhiteLinearGradient from '../../components/InputMsgBlackWhiteLinearGradient'
import { Platform } from 'react-native'

const WINDOW_HEIGHT = Dimensions.get('window').height
const WINDOW_WIDTH = Dimensions.get('window').width

const FONT_SIZE = Dimensions.get('window').width * 0.04;
const INPUT_BOX_WIDTH = Dimensions.get('window').width * 0.73;


const ChatWithPersonScreen: React.FC = () => {

    const { handleSubmit, register, errors, control, reset } = useForm();
    const [query, setQuery] = useState('');

    const currentDate = new Date().getDate(); //get today's date
    const currentMonth = new Date().getMonth() + 1; //get current Month
    const currentYear = new Date().getFullYear(); //get current Year
    // const hours = new Date().getHours(); //Current Hours
    // const min = new Date().getMinutes(); //Current Minutes
    const todayExact = `${currentYear}` + '/' + `${currentMonth}` + '/' + `${currentDate}`

    //fetch SQL message date here
    let messageDate = '2020/10/15'


    if (messageDate === (`${currentYear}` + '/' + `${currentMonth}` + '/' + `${currentDate - 1}`)) {
        messageDate = 'Yesterday'
    }

    let mode = new Animated.Value(0);
    let buttonSize = new Animated.Value(1);

    const handlePress = () => {
        Animated.sequence([
            Animated.timing(buttonSize, {
                toValue: 0.9,
                duration: 80,
                useNativeDriver: false,
            }),
            Animated.timing(buttonSize, {
                toValue: 1,
                useNativeDriver: false,
            }),
            Animated.timing(mode, {
                toValue: 0 ? 1 : 0,
                useNativeDriver: false,
            }),
        ]).start();
    };

    const sizeStyle = {
        transform: [{ scale: buttonSize }],
    };

    return (
        <>
            <ScrollView style={styles.background}>
                <View style={{ padding: 15 }}>

                    <View>
                        <View style={styles.opponentMsg}>

                            <MsgBlackWhiteLinearGradient>
                                <View style={styles.msgBubbleA}>
                                    {/* <View style={styles.triangleA} /> */}
                                    <Text style={styles.msgText}>Opponent's message Opponent's message Opponent's message Opponent's message Opponent's message Opponent's message Opponent's message Opponent's message Opponent's message </Text>
                                    <View style={{ alignSelf: 'flex-end', marginTop: 7, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                        {messageDate !== todayExact && <Text style={styles.timeStampText}>{messageDate}</Text>}
                                        <Text style={styles.timeStampText}>{11:09}</Text>
                                    </View>
                                </View>
                            </MsgBlackWhiteLinearGradient>
                        </View>

                        <View style={styles.myMsg}>
                            <MsgInvitationGradient>

                                <View style={styles.msgBubbleB}>
                                    {/* <View style={styles.triangleB} /> */}

                                    <Text style={styles.msgText}>Ok</Text>
                                    <View style={{ alignSelf: 'flex-end', marginTop: 7, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                        {messageDate !== todayExact && <Text style={styles.timeStampText}>{messageDate}</Text>}
                                        <Text style={styles.timeStampText}>11:09</Text>
                                    </View>

                                </View>
                            </MsgInvitationGradient>

                        </View>
                    </View>

                </View>


            </ScrollView>

            <KeyboardAvoidingView
                keyboardVerticalOffset={WINDOW_HEIGHT * 0.095}
                behavior={Platform.OS == "ios" ? "position" : "height"}>
                <View style={styles.inputAbsolute}>
                    <InputMsgBlackWhiteLinearGradient>
                        <View style={styles.inputContainer}>
                            <Controller
                                control={control}
                                render={({ onChange, onBlur, value }) => (
                                    <TextInput
                                        autoCapitalize="sentences"
                                        placeholder="Type a message"
                                        multiline={true}
                                        autoCorrect={false}
                                        style={[styles.input]}
                                        onBlur={onBlur}
                                        onChangeText={(value) => {
                                            onChange(value);
                                            // checkInput(value);
                                        }}
                                        // onChangeText={setUsername}
                                        value={value}
                                    />
                                )}
                                name="message"
                                defaultValue=""
                            />
                            {/* <TryKeyboard /> */}
                            <Animated.View style={sizeStyle}>
                                <TouchableWithoutFeedback
                                    style={styles.submit}
                                    onPress={() => {
                                        handlePress();
                                        Keyboard.dismiss();
                                    }}>
                                    <Text style={styles.buttonText}>Send</Text>
                                </TouchableWithoutFeedback>
                            </Animated.View>
                        </View>
                    </InputMsgBlackWhiteLinearGradient>
                </View>
            </KeyboardAvoidingView>



        </>
    )
}

const styles = StyleSheet.create({
    background: {
        // backgroundColor: 'red',
        // backgroundColor: '#191919ee', //color 1
        backgroundColor: '#eee', //color 2
        minHeight: WINDOW_HEIGHT * 0.5,


    },
    opponentMsg: {
        // minHeight: WINDOW_HEIGHT * 0.1,

        alignItems: "flex-start",
        marginVertical: 5,
        borderRadius: 6,

        shadowColor: '#0006',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
    myMsg: {
        // backgroundColor: 'red',
        // minHeight: WINDOW_HEIGHT * 0.1,
        alignItems: "flex-end",
        marginVertical: 5,
        borderRadius: 6,
        shadowColor: '#0006',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
    msgBubbleA: {

        // height: '100%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        minHeight: WINDOW_HEIGHT * 0.01,
        padding: 10,
        minWidth: WINDOW_WIDTH * 0.1,
        maxWidth: WINDOW_WIDTH * 0.7,
        // paddingBottom: 20,
        borderColor: '#aaa6',
        borderWidth: 0.5,
        borderRadius: 6,

    },
    triangleA: {
        // backgroundColor: '#ddd',
        // position: 'absolute',
        // left: -8,
        // bottom: 3,
        // transform: [{ rotate: '120deg' }, { rotateX: '45deg' }, { rotateY: '45deg' }],
        // width: 20,
        // height: 20,
    },
    msgBubbleB: {
        // backgroundColor: 'red',
        // height: '100%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        minHeight: WINDOW_HEIGHT * 0.01,
        padding: 10,
        minWidth: WINDOW_WIDTH * 0.1,
        maxWidth: WINDOW_WIDTH * 0.7,
        // paddingBottom: 20,
        borderRadius: 6,

    },
    triangleB: {
        // backgroundColor: '#7BA9B2',
        // position: 'absolute',
        // right: -5,
        // bottom: 0,
        // transform: [{ rotate: '120deg' }, { rotateX: '45deg' }, { rotateY: '45deg' }],
        // width: 20,
        // height: 20,

    },
    msgText: {
        fontFamily: 'Varela Round',
        color: '#111',
        fontSize: FONT_SIZE,
        textAlign: 'left',
        alignSelf: 'flex-start',
    },
    timeStampText: {
        fontFamily: 'Syncopate',
        color: '#555e',
        fontSize: 11,
        marginHorizontal: 5
        // textAlign: 'right',
        // alignSelf: 'flex-end',
        // justifyContent: 'flex-end'
    },
    inputAbsolute: {

    },
    input: {
        // backgroundColor: 'red',
        minHeight: FONT_SIZE + 20,
        fontSize: FONT_SIZE,
        maxHeight: WINDOW_HEIGHT * 0.27,
        width: INPUT_BOX_WIDTH,
        borderColor: '#bbb',
        borderStyle: 'solid',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
        paddingTop: 7,
        paddingBottom: 7,

        // marginBottom: 15,
        // paddingLeft: 10,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        overflow: 'scroll'
        // paddingTop: 10
    },
    inputContainer: {
        width: WINDOW_WIDTH,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    submit: {
        alignSelf: 'flex-start',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        // marginTop: 20,
        width: Dimensions.get('window').width * 0.19,
        height: FONT_SIZE * 2,
        borderRadius: 7,
        // borderWidth: 2,
        // borderColor: '#666',
        padding: 8,
        backgroundColor: '#548EF6',
        shadowColor: '#888',
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 3,
        shadowOpacity: 0.7,
    },
    buttonText: {
        color: 'white',
        textShadowColor: 'white',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 4,
        shadowColor: 'white',
        shadowOpacity: 4,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 4,
        fontFamily: 'Syncopate',
        // fontFamily: 'varela round',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE,
    },
})

export default ChatWithPersonScreen
