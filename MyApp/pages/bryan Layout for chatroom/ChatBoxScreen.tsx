import React from 'react';
import { View, Text, SafeAreaView, Image, StyleSheet, Dimensions, Button } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import BlackWhiteLinearGradient from '../components/BlackWhiteLinearGradient';

const WINDOW_HEIGHT = Dimensions.get('window').height
const WINDOW_WIDTH = Dimensions.get('window').width
const IMAGE_WIDTH = Dimensions.get('window').width * 0.16



const ChatBoxScreen: React.FC = () => {
    const navigation = useNavigation();

    return (
        <BlackWhiteLinearGradient>
            <SafeAreaView style={styles.whole}>
                <ScrollView>

                    <View style={styles.container}>
                        <TouchableOpacity>
                            {/* ---------------------- START FETCH HERE ---------------------- */}
                            <View>
                                <View style={styles.contactPerson}>
                                    <View style={styles.imgContainer}>
                                        <Image style={styles.image} source={require('../assets/JCLogo.png')} />
                                    </View>
                                    <View style={styles.textDetails}>
                                        <Text style={styles.name}>Admin</Text>
                                        <Text style={styles.lastMessage}>How is the progress</Text>
                                    </View>
                                </View>
                                <View style={styles.msgInfo}>
                                    <Text style={styles.time}>11:09</Text>
                                    <View style={styles.numberBall}>
                                        <Text style={styles.numberText}>2</Text>
                                    </View>
                                    <Text> </Text>
                                </View>
                                <View style={styles.border}></View>
                            </View>

                            {/* ---------------------- END FETCH HERE ---------------------- */}

                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                // navigation.navigate('${USERNAME} ChatBox');
                            }}>
                            {/* ---------------------- DUMMY PERSON ---------------------- */}

                            <View style={styles.contactPerson}>
                                <View style={styles.imgContainer}>
                                    <Image style={styles.image} source={require('../assets/JCLogo.png')} />
                                </View>
                                <View style={styles.textDetails}>
                                    <Text style={styles.name}>Somebody</Text>
                                    <Text style={styles.lastMessage}>Dummy Message</Text>
                                </View>
                            </View>
                            <View style={styles.msgInfo}>
                                <Text style={styles.time}>11:09</Text>
                                <View style={styles.numberBall}>
                                    <Text style={styles.numberText}>2</Text>
                                </View>
                                <Text> </Text>
                            </View>
                            <View style={styles.border}></View>

                            {/* ---------------------- DUMMY PERSON ---------------------- */}

                        </TouchableOpacity>

                        {/* <Button title="Notify" onPress={onPress}/> */}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </BlackWhiteLinearGradient>
    )
}


import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { useNavigation } from '@react-navigation/native';
var PushNotification = require("react-native-push-notification");

// Must be outside of any component LifeCycle (such as `componentDidMount`).
PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token: string) {
        console.log("TOKEN:", token);
    },

    // (required) Called when a remote is received or opened, or local notification is opened
    onNotification: function (notification: any) {
        console.log("NOTIFICATION:", notification);

        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
    onAction: function (notification: any) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);

        // process the action
    },

    // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
    onRegistrationError: function (err: any) {
        console.error(err.message, err);
    },

    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
        alert: true,
        badge: true,
        sound: true,
    },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
     * (optional) default: true
     * - Specified if permissions (ios) and token (android and ios) will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     * - if you are not using remote notification or do not have Firebase installed, use this:
     *     requestPermissions: Platform.OS === 'ios'
     */
    requestPermissions: true,
});


const styles = StyleSheet.create({
    whole: {
        minHeight: WINDOW_HEIGHT,

    },
    container: {
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-start',

    },
    contactPerson: {
        // backgroundColor: 'red',
        padding: 20,
        marginTop: 8,
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT * 0.11,
        flexDirection: 'row',
        alignItems: 'center',

    },
    imgContainer: {
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        backgroundColor: 'black',
        maxWidth: IMAGE_WIDTH,
        height: IMAGE_WIDTH,
        borderRadius: IMAGE_WIDTH / 2
    },
    textDetails: {
        // backgroundColor: 'red',
        height: '100%',
        marginLeft: 20,
        justifyContent: 'flex-start',
        width: WINDOW_WIDTH * 0.6,
        overflow: 'hidden'
    },
    name: {
        // backgroundColor: 'red',
        fontFamily: 'Varela Round',
        fontSize: 20,
        marginVertical: 3,
    },
    lastMessage: {
        // backgroundColor: 'red',
        fontFamily: 'Varela Round',
        fontSize: 14,
        color: '#888b',
        marginVertical: 3,
    },
    border: {
        width: WINDOW_WIDTH,
        borderBottomColor: '#AAA5',
        borderBottomWidth: 1,
    },
    msgInfo: {
        // backgroundColor: 'green',
        position: 'absolute',
        height: '100%',
        top: 0,
        right: 0,
        paddingVertical: 10,
        paddingRight: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    time: {
        // backgroundColor: 'red',
        fontFamily: 'Syncopate',
        paddingTop: 3,
    },
    numberBall: {
        backgroundColor: '#87F3FD',
        width: 26,
        height: 26,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        paddingTop: 2,
        paddingLeft: 1
    },
    numberText: {
        alignSelf: 'center',
        textAlign: 'center',
        fontFamily: 'syncopate',
        color: '#444',
    },

})
export default ChatBoxScreen;