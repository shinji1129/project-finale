import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { IRootState } from '../../redux/store'
import { View, StyleSheet, Text, SafeAreaView, Image, Dimensions } from 'react-native'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import { getChatRoomByID } from '../../redux/chatroom/thunk'
import BlackWhiteLinearGradient from '../../components/BlackWhiteLinearGradient'

const WINDOW_HEIGHT = Dimensions.get('window').height
const WINDOW_WIDTH = Dimensions.get('window').width
const IMAGE_WIDTH = Dimensions.get('window').width * 0.16



export function ChatRoomListScene(){
    const chatRooms = useSelector((state: IRootState)=> state.chatroom?.chatRooms)
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const token = useSelector((state: IRootState) => state.auth.token);
    const userId = useSelector((state: IRootState) => state.auth.user?.id);

    const IsToday = (chatRoom:any)=>{
        return new Date(chatRoom[chatRoom.length-1]?.updated_at).getFullYear() === new Date().getFullYear() &&
        new Date(chatRoom[chatRoom.length-1]?.updated_at).getMonth() === new Date().getMonth() &&
        new Date(chatRoom[chatRoom.length-1]?.updated_at).getDate() === new Date().getDate()
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
          if (!token || !userId) {
            return
          }
          dispatch(getChatRoomByID(token,userId))
        })
        return unsubscribe;
      }, [navigation])

    useEffect(()=>{
        console.log('chatRooms', chatRooms)
        console.log('chatRoom', chatRooms[0])

    },[])
     

    return(
        <>
        <BlackWhiteLinearGradient>
            <SafeAreaView style={styles.whole}>
                <ScrollView>

                    <View style={styles.container}>
                            {chatRooms?.map((chatRoom,i)=> (
                        <TouchableOpacity onPress={()=>
                            navigation.navigate('ChatBox', {
                                chatRoom_id: chatRoom?.[0].room_id,
                                chatRoom: chatRoom,
                                index: i
                            })
                        }>
                            {/* ---------------------- START FETCH HERE ---------------------- */}

                            <View key={`index_${i}`}>
                                <View style={styles.contactPerson}>
                                    <View style={styles.imgContainer}>
                                        <Image style={styles.image}                                         
                                            source={{
                                                uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${chatRoom[chatRoom.length-1]?.profile_pic}`,
                                        }} />
                                    </View>
                                    <View style={styles.textDetails}>
                                        <Text style={styles.name}>{chatRoom[chatRoom.length-1]?.contactUser}</Text>
                                        <Text style={styles.lastMessage}>{chatRoom[chatRoom.length-1]?.message.split('\n')[0]}</Text>
                                    </View>
                                </View>
                                <View style={styles.msgInfo}>
                                    <Text style={styles.time}>
                                        {IsToday(chatRoom) && "Today"}
                                        {!IsToday(chatRoom) && new Date(chatRoom[chatRoom.length-1]?.updated_at).getDate() + '/' + (new Date(chatRoom[chatRoom.length-1]?.updated_at).getMonth() + 1)}</Text>
                                    {/* <View style={styles.numberBall}>
                                        <Text style={styles.numberText}>2</Text>
                                    </View> */}
                                    <Text> </Text>
                                </View>
                                <View style={styles.border}></View>
                            </View>
                            {/* ---------------------- END FETCH HERE ---------------------- */}
                        </TouchableOpacity>
                             ))}
                        {/* <Button title="Notify" onPress={onPress}/> */}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </BlackWhiteLinearGradient>
        </>
    )
}

export default ChatRoomListScene;

const styles = StyleSheet.create({
    chatRoom: {
        margin:10,
    },
    whole: {
        minHeight: WINDOW_HEIGHT,

    },
    container: {
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-start',

    },
    contactPerson: {
        // backgroundColor: 'red',
        padding: 20,
        marginTop: 8,
        width: WINDOW_WIDTH,
        height: WINDOW_HEIGHT * 0.11,
        flexDirection: 'row',
        alignItems: 'center',

    },
    imgContainer: {
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        backgroundColor: 'black',
        // maxWidth: IMAGE_WIDTH,
        height: IMAGE_WIDTH,
        width: IMAGE_WIDTH,
        borderRadius: IMAGE_WIDTH / 2
    },
    textDetails: {
        // backgroundColor: 'red',
        height: '100%',
        marginLeft: 20,
        justifyContent: 'flex-start',
        width: WINDOW_WIDTH * 0.6,
        overflow: 'hidden'
    },
    name: {
        // backgroundColor: 'red',
        fontFamily: 'Varela Round',
        fontSize: 20,
        marginVertical: 3,
    },
    lastMessage: {
        // backgroundColor: 'red',
        fontFamily: 'Varela Round',
        fontSize: 14,
        color: '#888b',
        marginVertical: 3,
    },
    border: {
        width: WINDOW_WIDTH,
        borderBottomColor: '#AAA5',
        borderBottomWidth: 1,
    },
    msgInfo: {
        // backgroundColor: 'green',
        position: 'absolute',
        height: '100%',
        top: 0,
        right: 0,
        paddingVertical: 10,
        paddingRight: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    time: {
        // backgroundColor: 'red',
        fontFamily: 'Syncopate',
        paddingTop: 3,
    },
    numberBall: {
        backgroundColor: '#87F3FD',
        width: 26,
        height: 26,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        paddingTop: 2,
        paddingLeft: 1
    },
    numberText: {
        alignSelf: 'center',
        textAlign: 'center',
        fontFamily: 'syncopate',
        color: '#444',
    },

})