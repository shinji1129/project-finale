import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet, Dimensions, Image } from 'react-native';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import { config } from '../../config';
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { useNavigation } from '@react-navigation/native';
import LGFreelancer from '../../components/LGFreelancer';

export interface IUsers {
    id: number,
    username: string,
    profile_pic: string
}

const WINDOW_HEIGHT = Dimensions.get('window').height
const WINDOW_WIDTH = Dimensions.get('window').width
const IMAGE_WIDTH = Dimensions.get('window').width * 0.5

const ApplicantListScreen: React.FC = ({ route }: any) => {
    const { applicantsList, requestID } = route.params;
    const token = useSelector((state: IRootState) => state.auth.token);
    const navigation = useNavigation();

    // infinite scroll
    const [page, setPage] = useState(1);
    const [userData, setUserData] = useState<IUsers[]>([]);

    useEffect(() => {
        console.log('applicantsList route params', applicantsList)
    }, [])

    async function fetchData() {

        const applicants = { applicantsList: applicantsList }
        const formDate = new FormData();
        formDate.append('applicants', JSON.stringify(applicants))

        console.log(formDate)
        console.log(`fetching: ${config.BACKEND_URL}/user/applicants`)
        const res = await fetch(`${config.BACKEND_URL}/user/applicants`, {
            method: "POST",
            headers: {
                'Content-Type': "multipart/form-data",
                Authorization: 'Bearer ' + token,
            },
            body: formDate,
        });

        const usersList = await res.json();
        console.log('usersList123', usersList)


        let currentDataList = userData;
        let newDataList = currentDataList.concat(usersList)

        setUserData(newDataList);
        setPage(page + 1);

    }

    useEffect(() => {
        fetchData();
    }, [])


    const renderItem = ({ item }: any) => {
        return (
            <TouchableOpacity key={`freelancer_${item.id}`}
                onPress={() => {
                    navigation.navigate('User Profile', {
                        user_id: item.id,
                        requestID: requestID
                    });
                }}
                style={styles.freelancer}>
                <LGFreelancer>
                    <View style={styles.innerFreelancer}>
                        <View style={styles.imgContainer}>
                            {item.profile_pic !== '' && <Image style={styles.image} source={{
                                uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${item.profile_pic}`,
                            }} />}
                            {item.profile_pic === '' && <Image style={styles.image} source={require('../../assets/JCLogo.png')} />}
                        </View>
                        <View style={styles.freelancerDetails}>
                            <Text style={styles.name}>{item.username}</Text>
                            <View style={styles.tagBox}>
                                {item.tag.map((tagObject: any, idx: number) => (
                                    <View key={`freelancer_${item.id}tag_${idx}`}>
                                        <View style={styles.tag}>
                                            <Text style={styles.tagText}>{tagObject}</Text>
                                        </View>
                                    </View>
                                ))}
                            </View>
                        </View>
                    </View>
                </LGFreelancer>
            </TouchableOpacity>
        )
    }



    return (
        <SafeAreaView>
            <View style={{ minHeight: WINDOW_HEIGHT, zIndex: 1 }}>
                <FlatList data={userData} keyExtractor={item => item.id.toString()}
                    contentContainerStyle={{ flexDirection: 'column', paddingBottom: 150 }}
                    renderItem={renderItem}
                />
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    freelancer: {
        alignSelf: 'center',
        width: WINDOW_WIDTH * 0.92,
        minHeight: WINDOW_HEIGHT * 0.15,
        marginVertical: 10,
        borderRadius: 6,
        shadowColor: '#0007',
        shadowOffset: { width: 3, height: 6 },
        shadowOpacity: 1,
        shadowRadius: 5,
    },
    innerFreelancer: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    imgContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    image: {
        backgroundColor: 'black',
        borderRadius: IMAGE_WIDTH * 0.25,
        width: IMAGE_WIDTH * 0.5,
        height: IMAGE_WIDTH * 0.5,
        overflow: 'hidden',
    },

    freelancerDetails: {
        alignSelf: 'center',
        marginHorizontal: 10,
        paddingVertical: 10,
        paddingHorizontal: 4,
        justifyContent: 'center',
        width: WINDOW_WIDTH * 0.54,
        maxWidth: WINDOW_WIDTH * 0.57,
        overflow: 'hidden'
    },

    name: {
        fontFamily: 'Varela Round',
        fontSize: 20,
    },

    tagBox: {
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        marginVertical: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },

    flatList: {
        paddingLeft: 15,
        paddingVertical: 7,
        fontSize: 20,
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: 'grey',
    }

})

export default ApplicantListScreen;
