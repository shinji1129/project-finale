import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions, TextInput, Alert } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { getBookmark, setBookmark } from '../../redux/request/bookmark/thunk';
import { setApplication, cancelApplication } from '../../redux/request/application/thunk';
import { setInvitation } from '../../redux/request/invitation/thunk';
import BlackWhiteLinearGradient from '../../components/BlackWhiteLinearGradient';
import InvitationGradient from '../../components/InvitationGradient';
import { resetBookmarkSuccess } from '../../redux/request/bookmark/actions';
import { useNavigation } from '@react-navigation/native';
import CommentBox from '../../components/commentbox';



const FONT_SIZE = Dimensions.get('window').width * 0.038;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const BUTTON_WIDTH = Dimensions.get('window').width * 0.3;
const BUTTON_HEIGHT = FONT_SIZE * 2.5;

const PublishedJobRelatedProfile_ProjectPreviewScreen: React.FC = ({ route }: any) => {
    const dispatch = useDispatch();
    const token = useSelector((state: IRootState) => state.auth.token);
    // Bookmark
    const bookmarkErrorMessage = useSelector((state: IRootState) => state.bookmark.errorMessage);

    // Application

    const applicantStatus = useSelector((state: IRootState) => state.applicant.status)
    const applicantApply_budget = useSelector((state: IRootState) => state.applicant.apply_budget)

    // Invitation
    const invitationStatus = useSelector((state: IRootState) => state.invitation.status)
    const invite_budget = useSelector((state: IRootState) => state.invitation.invite_budget);

    const [expectBudget, setExpectBudget] = useState('');

    const { request_id, requester, title, tag, images, time_start, duration, venue, budget_min, budget_max, freelancer_id, confirmed_budget, expiry_date, status, tag_id } = route.params;



    const statusConverter = (status: number) => {
        switch (status) {
            case 1:
                return ""
            case 2:
                return "Pending"
            case 3:
                return "Accepted"
            case 4:
                return "Confirmation"
            case 5:
                return "Completed"
            default:
                return ""
        }
    }

    const BookmarkClickHandler = async () => {
        if (token === null) {
            return;
        }
        dispatch(setBookmark(token, request_id))
    }

    const ApplyButtonHandler = async () => {
        if (isNaN(parseInt(expectBudget))) {
            if (expectBudget === '') {
                Alert.alert("Invalid input",
                    "Please input number",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
                setExpectBudget('')
                return
            }
            Alert.alert("Invalid input",
                "Please input number only",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
            setExpectBudget('')
            return
        } else {
            if (token) {
                dispatch(setApplication(token, request_id, parseInt(expectBudget)))
                setExpectBudget('')
            }
        }
    }

    const CancelButtonHandler = async () => {
        if (token) {
            dispatch(cancelApplication(token, request_id))
        }
    }

    const AcceptInvitationHandler = async () => {
        if (token) {
            dispatch(setInvitation(token, request_id, "accept", invite_budget))
        }
    }

    const DeclineInvitationHandler = async () => {
        if (token) {
            dispatch(setInvitation(token, request_id, "decline"))
        }
    }

    // get bookmark
    useEffect(() => {
        if (token === null) {
            return;
        }
        if (bookmarkErrorMessage) {
            console.log(bookmarkErrorMessage)
        }
        dispatch(getBookmark(token, request_id))

        return (() => {
            dispatch(resetBookmarkSuccess());
        })
    }, []);


    return (
        <SafeAreaView >
            <BlackWhiteLinearGradient>
                <ScrollView style={{ height: 'auto', width: WINDOW_WIDTH }}>
                    <View style={styles.container}>
                        <View>
                            {status <= 2 && invitationStatus === 1 && (
                                <>
                                    <View style={styles.invitation}>
                                        <InvitationGradient>
                                            <View >
                                                <Text style={styles.text}>{requester} would like to assign you to this job with budget: $ {invite_budget}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-evenly', alignItems: 'center', marginTop: 10, }}>
                                                <TouchableOpacity style={styles.decline} onPress={DeclineInvitationHandler}>
                                                    <Text style={styles.buttonText}> Decline </Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.accept} onPress={AcceptInvitationHandler}>
                                                    <Text style={styles.buttonText}> Accept </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </InvitationGradient>

                                    </View>
                                </>
                            )}
                            <Text style={styles.titleText}>{title}</Text>
                            {requester.length >= 1 && <Text style={styles.text}>Job posted by: <Text style={styles.requester}>{requester}</Text> </Text>}

                            <Text style={{ fontFamily: 'Varela Round', paddingLeft: 5, marginTop: 8 }}>Require:</Text>
                            {tag && tag.length >= 1 &&
                                <View style={styles.tagBox}>{tag.map((string: string, idx: number) => {
                                    return (
                                        <View key={`tag_${idx}`} style={styles.tag}>
                                            <Text style={styles.tagText} key={`tag_${idx}`}>{string.charAt(0).toUpperCase() + string.slice(1)}</Text>
                                        </View>
                                    )
                                })}
                                </View>
                            }
                            {images && images.length >= 1 && <Text style={styles.tag}>Images: {tag.map((string: string, idx: number) => {
                                return <Text key={`image_${idx}`}>{string} </Text>
                            })}
                            </Text>
                            }
                        </View>
                        <View style={styles.border}></View>
                        <View style={styles.middleHalf}>
                        {time_start !== new Date(0).toDateString() && time_start.length >= 1 && <Text style={styles.text}>Date: {time_start}</Text>}
                            {venue && <Text style={styles.text}>Venue: {venue} </Text>}
                        </View>
                        <CommentBox requestId={request_id} />
                    </View>
                </ScrollView>
            </BlackWhiteLinearGradient>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        minWidth: WINDOW_WIDTH,
        maxWidth: WINDOW_WIDTH,
        minHeight: WINDOW_HEIGHT * 0.9,
        alignContent: 'center',
        flexWrap: "wrap",
        flexDirection: "column",
        padding: 5,
    },
    titleText: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.45,
        textTransform: 'capitalize',
        borderRadius: 5,
        padding: 5,
        width: WINDOW_WIDTH * 0.9,
    },
    text: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE,
        borderRadius: 5,
        padding: 5,
        color: '#090909',
    },
    requester: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.40,
        textTransform: 'capitalize',
    },
    tagBox: {
        marginTop: 15,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    border: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: WINDOW_WIDTH * 0.9,
        borderBottomColor: '#7775',
        borderBottomWidth: 1,
        paddingBottom: 18,
        justifyContent: 'flex-start',
    },
    middleHalf: {
        marginTop: 15,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: 15
    },
    pageEndButtons: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    rows: {
        marginTop: WINDOW_HEIGHT * 0.015,
        minWidth: WINDOW_WIDTH * 0.5,
    },
    buttonText: {
        paddingHorizontal: 5,
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        textTransform: 'uppercase',
        fontSize: FONT_SIZE * 0.9,
    },
    chatBox: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    unbookmark: {
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    invitation: {
        maxWidth: WINDOW_WIDTH * 0.9,
        marginVertical: 15,
        borderRadius: 6,
        shadowColor: '#0009',
        shadowOffset: { width: 3, height: 7 },
        shadowOpacity: 1,
        shadowRadius: 8
    },
    accept: {
        width: '100%',
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    decline: {
        width: '100%',
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',

        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    bookmark: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    input: {
        marginTop: 15,
        height: FONT_SIZE + 20,
        width: WINDOW_WIDTH * 0.8,
        borderColor: '#aaa',
        borderStyle: 'solid',
        borderRadius: 30,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
    },
    apply: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
});

export default PublishedJobRelatedProfile_ProjectPreviewScreen;