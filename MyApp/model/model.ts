export interface IList {
    request_id: number,
    requester: string,
    id: number,
    title: string,
    requester_id: number,
    freelancer_id: number,
    freelancer: string,
    status: number,
    images: string[],
    company_name: string,
    details: string,
    budget_min: number,
    budget_max: number,
    confirmed_budget: number,
    time_start: Date,
    duration: number,
    expiry_date: Date,
    created_at: string,
    updated_at: string,
    category_id: number,
    venue: string,
    category: string,
    tag: Array<string>,
    tag_id: Array<number>,
    bookmark: boolean
}

export interface IProjectList {
    request_id: number,
    requester: string,
    id: number,
    title: string,
    requester_id: number,
    freelancer_id: number,
    freelancer: string,
    status: number,
    images: string[],
    company_name: string,
    details: string,
    budget_min: number,
    budget_max: number,
    confirmed_budget: number,
    time_start: Date,
    duration: number,
    expiry_date: Date,
    created_at: string,
    updated_at: string,
    category_id: number,
    venue: string,
    category: string,
    tag: Array<string>,
    tag_id: Array<number>,
    bookmark: boolean,
    invite_budget: number,
    apply_budget: number
}

export interface Tag {
    id: number,
    tag: string
}

export interface Message {
    id: number
    user_one:number
    user_two:number
    message: string
    status: number
    room_id: number
    contactUser:string
}