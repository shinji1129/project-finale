/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  StyleSheet,
  StatusBar
} from 'react-native';


import LandingScreen from './pages/Auth/LandingScreen';
import RegistrationPage from './pages/Auth/RegistrationPage';
import LoginScreen from './pages/Auth/LoginScreen';
import LoadingScreen from './pages/Auth/LoadingScreen';
import { Provider } from 'react-redux';
import store from './redux/store';
import Navbar2 from './components/navbar2';



declare const global: { HermesInternal: null | {} };

const Stack = createStackNavigator();


const App = () => {

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Loading">
            <Stack.Screen
              name="Loading"
              component={LoadingScreen}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Landing"
              component={LandingScreen}
              options={{
                title: 'back',
                headerShown: false,
                gestureEnabled: false,
              }}
            />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Register" component={RegistrationPage} />
            <Stack.Screen
              name="Navbar"
              component={Navbar2}
              options={{
                headerShown: false,
                gestureEnabled: false,
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    </>
  );
};


const styles = StyleSheet.create({
  scrollView: {
    minHeight: '100%',
    padding: 5,
  },
});

export default App;
