export interface INewRequestState {
    title: string,
    category_id: number | null,
    images: [] | null,
    company_name: string | null,
    details: string | null,
    budget_min: number | null,
    budget_max: number | null,
    time_start: string | null,
    duration: number | null,
    expiry_date: string | null,
    isProcessing: boolean,
}