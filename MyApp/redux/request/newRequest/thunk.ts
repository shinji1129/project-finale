import { newProjectSuccess, newProjectProcessing, newProjectFail, INewRequestActions } from './actions'
import { ThunkDispatch } from '../../store';
import { ImageOrVideo } from 'react-native-image-crop-picker';
import { config } from '../../../config';



export function newProjectThunk(token: string, submit: any) {
    return async (dispatch: ThunkDispatch) => {
        try {
                dispatch(newProjectProcessing());

                const newRequestFormData = new FormData();
                newRequestFormData.append('submit', JSON.stringify(submit))
                const images = submit.images

                images.map((image:ImageOrVideo)=>{
                    if(image.filename && image.mime && image.path){
                        newRequestFormData.append('request_image',{
                            name: image.filename,
                            type: image.mime,
                            uri: image.sourceURL
                        });
                    }
                })

                const res = await fetch(`${config.BACKEND_URL}/request/newRequest`, {
                    method: "POST",
                    headers: {
                        'Content-Type': "multipart/form-data",
                        Authorization: 'Bearer ' + token,
                    },
                    body: newRequestFormData,
                });

                const data = await res.json();

                if (res.status == 200) {
                    dispatch(newProjectSuccess());
                } else {
                    dispatch(newProjectFail(data.message));
                }
            // } else {
                // console.log('error')
                // dispatch(newProjectFail('internal server error'))
            // }
        } catch (err) {
            console.log("errMessage is here:", err)
            dispatch(newProjectFail('internal server error:001'))
        }
    }
}
