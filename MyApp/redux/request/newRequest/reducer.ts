import { INewRequestState } from './state';
import { INewRequestActions } from './actions';

const initNewRequestState: INewRequestState = {
    title: '',
    category_id: null,
    images: null,
    company_name: null,
    details: null,
    budget_min: null,
    budget_max: null,
    time_start: null,
    duration: null,
    expiry_date: null,
    isProcessing: false,
}

export const newRequestReducers = (state:INewRequestState = initNewRequestState, action: INewRequestActions):INewRequestState => {
    switch(action.type){
        case '@@request/NEW_PROJECT_SUCCESS':
            return {
                ...state,
                isProcessing:false,
            }
        case "@@request/NEW_PROJECT_PROCESSING":
            return {
                ...state,
                isProcessing:true,
            }
        case "@@request/NEW_PROJECT_FAIL":
            return {
                ...state,
                isProcessing:false,
            }
        case "@@request/NEW_PROJECT_FINISH_PROCESSING":
            return {
                ...state,
                isProcessing:false
            }
        default:
            return state
    }
}