
export const newProjectProcessing = () => {
    return {
        type: "@@request/NEW_PROJECT_PROCESSING" as "@@request/NEW_PROJECT_PROCESSING"
    }
}

export const newProjectSuccess = () => {
    return {
        type: "@@request/NEW_PROJECT_SUCCESS" as "@@request/NEW_PROJECT_SUCCESS"
    }
}

export const newProjectFail = (message: string) => {
    return {
        type: "@@request/NEW_PROJECT_FAIL" as "@@request/NEW_PROJECT_FAIL",
        message: message
    }
}

export const newProjectFinishProcessing = () => {
    return {
        type: "@@request/NEW_PROJECT_FINISH_PROCESSING" as "@@request/NEW_PROJECT_FINISH_PROCESSING",
    }
}

type NewRequestCreators =
    typeof newProjectSuccess |
    typeof newProjectProcessing |
    typeof newProjectFail |
    typeof newProjectFinishProcessing

export type INewRequestActions = ReturnType<NewRequestCreators>;
