import { IConfirmationState } from "./state";
import { IConfirmationActions } from "./action";


const initConfirmationState: IConfirmationState = {
    status: null,
    isProcessing: false
}

export const confirmationReducer = (state: IConfirmationState = initConfirmationState, action: IConfirmationActions): IConfirmationState => {
    switch (action.type) {
        case "@@request/GET_CONFIRMATION_SUCCESS":
            return{
                ...state,
                isProcessing: false,
                status: action.status
            }
        case "@@request/CONFIRMATION_PROCESSING":
            return {
                ...state,
                isProcessing: true,
            }
        case "@@request/CONFIRMATION_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status
            }
        case "@@request/CONFIRMATION_RESET_SUCCESS":
            return {
                ...initConfirmationState
            }
        default:
            return state;
    }
}