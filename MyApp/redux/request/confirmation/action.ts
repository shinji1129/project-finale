export function setConfirmationSuccess (status:number){
    return {
        type: "@@request/CONFIRMATION_SUCCESS" as "@@request/CONFIRMATION_SUCCESS",
        status
    }
}

export function setConfirmationProcessing (){
    return {
        type: "@@request/CONFIRMATION_PROCESSING" as  "@@request/CONFIRMATION_PROCESSING"
    }
}

export function getConfirmationSuccess (status:number){
    return {
        type: "@@request/GET_CONFIRMATION_SUCCESS" as "@@request/GET_CONFIRMATION_SUCCESS",
        status
    }
}

export function resetConfirmationSuccess (){
    return {
        type: "@@request/CONFIRMATION_RESET_SUCCESS" as "@@request/CONFIRMATION_RESET_SUCCESS",
    }
}

type FAILED = "@@request/CONFIRMATION_FAIL" | "@@request/GET_CONFIRMATION_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type IConfirmationActionCreator = 
typeof setConfirmationSuccess |
typeof setConfirmationProcessing |
typeof getConfirmationSuccess |
typeof resetConfirmationSuccess |
typeof failed


export type IConfirmationActions = ReturnType<IConfirmationActionCreator> 