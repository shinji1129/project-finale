export interface IConfirmationState {
    status: number | null,
    isProcessing: boolean,
}