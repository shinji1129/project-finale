import { Dispatch } from "react";
import { IConfirmationActions, setConfirmationProcessing, failed, getConfirmationSuccess, setConfirmationSuccess } from "./action";
import { config } from "../../../config";


export function currentStatus(token: string, requestId: number) {
    return async (dispatch: Dispatch<IConfirmationActions>) => {
        try {
            dispatch(setConfirmationProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/confirmation/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            })
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/GET_CONFIRMATION_FAIL", data.message))
                return
            }

            dispatch(getConfirmationSuccess(data.status))

        } catch (err) {
            dispatch(failed("@@request/GET_CONFIRMATION_FAIL", err.message))
            console.log(err);
        }
    }
}

export function setConfirmationStatus(token: string, requestId: number, response: string) {
    return async (dispatch: Dispatch<IConfirmationActions>) => {
        try {
            dispatch(setConfirmationProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/confirmation/${response}/${requestId}`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/CONFIRMATION_FAIL", data.message));
                return
            }

            dispatch(setConfirmationSuccess(data.status));
        } catch (err) {
            dispatch(failed("@@request/CONFIRMATION_FAIL", err.message))
            console.log(err.message);
        }
    }
}