import { IRatingState } from './state';
import { IRatingActions } from './actions';

const initRatingState: IRatingState = {
    RatingOnFreelancer:-1,
    RatingOnRequester:-1,
    isProcessing:false,
    isRating:false,
    isBothRating:false
}

export const RatingReducers = (state:IRatingState = initRatingState, action: IRatingActions):IRatingState => {
    switch(action.type){
        case '@@request/RATING__SUCCESS':
            return {
                ...state,
                isProcessing:false,
            }
        case "@@request/RATING_PROCESSING":
            return {
                ...state,
                isProcessing:true,
            }
        case "@@request/GET_RATING_PROCESSING":
            return{
                ...state,
                isProcessing:true,
            }
        case "@@request/GET_RATING_SUCCESS":
            return {
                ...state,
                RatingOnFreelancer:action.RatingOnFreelancer,
                RatingOnRequester:action.RatingOnRequester,
                isProcessing:true,
            }
        case "@@request/GET_RATING_SUCCESS":
            return {
                ...state,
                isProcessing:true,
            }

        default:
            return state
    }
}