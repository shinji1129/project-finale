import { ratingProcessing, failed, ratingSuccess, getRatingProcessing, getRatingSuccess } from './actions'
import { ThunkDispatch } from '../../store';
import { config } from '../../../config';

export function addRatingThunk(token: string, requestId:number , rating: number) {
    return async (dispatch: ThunkDispatch) => {
    try{
        dispatch(ratingProcessing());

        const formObject = {rating:-1};
        formObject['rating'] = rating;

        const res = await fetch(`${config.BACKEND_URL}/request/rating/${requestId}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
            body: JSON.stringify(formObject)
        })

        const data = await res.json();

        if (res.status !== 200) {
            dispatch(failed("@@request/RATING_FAIL", data.message))
            return
        }
        dispatch(ratingSuccess(data.rating))
    } catch (err) {
        dispatch(failed("@@request/RATING_FAIL", err.message))
        console.log(err);
    }
    }
}

export function getRatingThunk(token: string, requestId: number){
    return async (dispatch: ThunkDispatch) => {
        try {
            dispatch(getRatingProcessing());
            console.log(`fetching ${config.BACKEND_URL}/request/rating/${requestId}`)
            const res = await fetch(`${config.BACKEND_URL}/request/rating/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/GET_RATING_FAIL", data.message))
                return
            }
            dispatch(getRatingSuccess(data.ratingOnFreelancer,data.ratingOnRequester))
        } catch (err) {
            dispatch(failed("@@request/GET_RATING_FAIL", err.message))
            console.log(err);
        }
    }
}
