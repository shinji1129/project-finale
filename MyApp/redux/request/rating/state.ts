export interface IRatingState {
    RatingOnFreelancer:number,
    RatingOnRequester:number,
    isProcessing: boolean
    isRating:boolean,
    isBothRating:boolean
}