
export const ratingProcessing = () => {
    return {
        type: "@@request/RATING_PROCESSING" as "@@request/RATING_PROCESSING"
    }
}

export const ratingSuccess = (rating:number) => {
    return {
        type: "@@request/RATING__SUCCESS" as "@@request/RATING__SUCCESS",
        rating
    }
}



export const getRatingProcessing = () => {
    return {
        type: "@@request/GET_RATING_PROCESSING" as "@@request/GET_RATING_PROCESSING"
    }
}

export const getRatingSuccess = (RatingOnFreelancer:number, RatingOnRequester:number) =>{
    return{
        type: "@@request/GET_RATING_SUCCESS" as "@@request/GET_RATING_SUCCESS",
        RatingOnFreelancer:RatingOnFreelancer,
        RatingOnRequester:RatingOnRequester
    }
}


type FAILED = "@@request/RATING_FAIL" | "@@request/GET_RATING_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}


type RatingCreators = 
typeof ratingProcessing |
typeof ratingSuccess |
typeof getRatingProcessing |
typeof getRatingSuccess |
typeof failed


export type IRatingActions = ReturnType<RatingCreators>;
