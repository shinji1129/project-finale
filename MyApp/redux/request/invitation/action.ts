
export function setInvitationSuccess(status: number | null){
    return{
        type: "@@request/INVITATION_SUCCESS" as "@@request/INVITATION_SUCCESS",
        status,
    }
}

export function setInvitationProcessing() {
    return {
        type: "@@request/INVITATION_PROCESSING" as "@@request/INVITATION_PROCESSING"
    }
}

export function getInvitationSuccess (status: number, invite_budget: number){
    return {
        type: "@@request/GET_INVITATION_SUCCESS" as "@@request/GET_INVITATION_SUCCESS",
        status,
        invite_budget,
    }
}

export function resetInvitationSuccess (){
    return {
        type: "@@request/INVITATION_RESET_SUCCESS" as "@@request/INVITATION_RESET_SUCCESS",
    }
}

type FAILED = "@@request/INVITATION_FAIL" | "@@request/GET_INVITATION_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type InvitationActionCreators = 
typeof setInvitationSuccess |
typeof getInvitationSuccess |
typeof setInvitationProcessing |
typeof resetInvitationSuccess |
typeof failed 

export type IInvitationActions = ReturnType<InvitationActionCreators>