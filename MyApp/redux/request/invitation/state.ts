export interface IInvitationState {
    status: number | null,
    isProcessing: boolean,
    invite_budget: number,
}

