import { IInvitationActions, setInvitationSuccess, setInvitationProcessing, getInvitationSuccess, failed } from './action';
import { Dispatch } from 'react';
import { config } from '../../../config';

export function currentInvitation(token: string, requestId: number) {
    return async (dispatch: Dispatch<IInvitationActions>) => {
        try {
            dispatch(setInvitationProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/invitation/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/GET_INVITATION_FAIL", data.message))
                return
            }

            const {status, invite_budget, ...other} = data

            if (!status){
                dispatch(getInvitationSuccess(0, 0))
                return
            }
            dispatch(getInvitationSuccess(status, invite_budget))

        } catch (err) {
            dispatch(failed("@@request/GET_INVITATION_FAIL", err.message))
            console.log(err);
        }
    }
}

export function setInvitation(token: string, requestId: number, response: string, invite_budget?:number) {
    return async (dispatch: Dispatch<IInvitationActions>) => {
        try {
            dispatch(setInvitationProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/invitation/${response}/${requestId}/${invite_budget}`,{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/INVITATION_FAIL",data.message));
                return
            }

    
            dispatch(setInvitationSuccess(data.status));


        } catch (err) {
            dispatch(failed("@@request/INVITATION_FAIL",err.message))
            console.log(err.message);
        }
    }
}