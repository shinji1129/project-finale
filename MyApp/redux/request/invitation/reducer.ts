import { IInvitationState } from './state';
import { IInvitationActions } from './action';

const initInvitationState: IInvitationState = {
    status: 0,
    isProcessing: false,
    invite_budget: 0,
}

export const invitationReducers = (state: IInvitationState = initInvitationState, action: IInvitationActions): IInvitationState => {
    switch (action.type) {
        case "@@request/GET_INVITATION_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                invite_budget: action.invite_budget,
                status: action.status

            }
        case "@@request/INVITATION_PROCESSING":
            return {
                ...state,
                isProcessing: true,
            }
        case "@@request/INVITATION_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status,
            }
        case "@@request/INVITATION_RESET_SUCCESS":
            return {
                isProcessing: false,
                ...initInvitationState
            }
        default:
            return state

    }
}