import { Dispatch } from "react";
import { ICommentActions, addCommentProcessing, failed, addCommentSuccess, isCommentProcessing, isCommentSuccess, isBothCommentSuccess } from "./action";
import { config } from "../../../config";


export function addCommentThunk(token: string, requestId: number, comment: string){
    return async (dispatch: Dispatch<ICommentActions>) => {
        try {
            dispatch(addCommentProcessing());

            const formObject = {comment:''};
            formObject['comment'] = comment;

            const res = await fetch(`${config.BACKEND_URL}/request/comment/${requestId}`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
                body: JSON.stringify(formObject)
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/COMMENT_FAIL", data.message))
                return
            }
            dispatch(addCommentSuccess(data.comment))
        } catch (err) {
            dispatch(failed("@@request/COMMENT_FAIL", err.message))
            console.log(err);
        }
    }
}


export function isCommentThunk(token: string, userId:number, requestId: number){
    return async (dispatch: Dispatch<ICommentActions>) => {
        try {
            dispatch(isCommentProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/comment/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/ISCOMMENT_FAIL", data.message))
                return
            }
            
            const commentArr:boolean[] = [];

            data.map((comment:any)=>{
                if(comment.users_id ===  userId){
                    commentArr.push(true)
                } else {
                    commentArr.push(false)
                }
            })


            dispatch(isCommentSuccess(commentArr.includes(true)))
            dispatch(isBothCommentSuccess(commentArr.length >= 2))
        } catch (err) {
            dispatch(failed("@@request/ISCOMMENT_FAIL", err.message))
            console.log(err);
        }
    }
}