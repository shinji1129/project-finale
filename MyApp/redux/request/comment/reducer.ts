import { ICommentState } from "./state";
import { ICommentActions } from "./action";


const initConfirmationState: ICommentState = {
    comment: '',
    isComment: false,
    isProcessing: false,
    isBothComment:false
}

export const commentReducer = (state: ICommentState = initConfirmationState, action: ICommentActions): ICommentState => {
    switch (action.type) {
        case "@@request/COMMENT_SUCCESS":
            return{
                ...state,
                isProcessing: false,
                comment: action.comment
            }
        case "@@request/COMMENT_PROCESSING":
            return {
                ...state,
                isProcessing: true,
            }
        case "@@request/ISCOMMENT_Success":
            return {
                ...state,
                isProcessing: false,
                isComment: action.isComment
            }
        case "@@request/ISBOTHCOMMENT_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                isBothComment: action.isBothComment
            }
        default:
            return state;
    }
}