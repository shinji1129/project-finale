export function addCommentSuccess (comment:string){
    return {
        type: "@@request/COMMENT_SUCCESS" as "@@request/COMMENT_SUCCESS",
        comment
    }
}

export function addCommentProcessing (){
    return {
        type: "@@request/COMMENT_PROCESSING" as  "@@request/COMMENT_PROCESSING"
    }
}


export function isCommentSuccess(isComment:boolean){
    return{
        type: "@@request/ISCOMMENT_Success" as "@@request/ISCOMMENT_Success",
        isComment
    }
}

export function isCommentProcessing(){
    return{
        type: "@@request/ISCOMMENT_PROCESSING" as "@@request/ISCOMMENT_PROCESSING"
    }
}

export function isBothCommentSuccess(isBothComment:boolean){
    return{
        type: "@@request/ISBOTHCOMMENT_SUCCESS" as "@@request/ISBOTHCOMMENT_SUCCESS",
        isBothComment
    }
}

type FAILED = "@@request/COMMENT_FAIL" | "@@request/ISCOMMENT_FAIL" 

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type ICommentActionCreator = 
typeof addCommentSuccess |
typeof addCommentProcessing |
typeof isCommentSuccess |
typeof isCommentProcessing |
typeof isBothCommentSuccess |
typeof failed


export type ICommentActions = ReturnType<ICommentActionCreator> 