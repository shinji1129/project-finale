export interface ICommentState {
    comment: string,
    isProcessing: boolean,
    isComment:boolean,
    isBothComment:boolean
}