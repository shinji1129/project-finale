import { IApplicationActions, setApplicationSuccess, setApplicationProcessing, failed, getApplicationSuccess } from './actions';
import { Dispatch } from 'react';
import { config } from '../../../config';


export function currentApplication(token: string, requestId: number) {
    return async (dispatch: Dispatch<IApplicationActions>) => {
        try {
            dispatch(setApplicationProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/application/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            const data = await res.json();           

            if (res.status !== 200) {
                dispatch(failed("@@request/GET_APPLICATION_FAIL", data.message))
                return
            }


            if (data.status === undefined) {
                dispatch(getApplicationSuccess(0))
                return;
            }

            dispatch(getApplicationSuccess(data.status));
        } catch (err) {
            dispatch(failed("@@request/GET_APPLICATION_FAIL", err.message));
            console.log(err);
        }
    }
}

export function setApplication(token: string, requestId:number, expectBudget: number){
    return async (dispatch: Dispatch<IApplicationActions>) => {
        try{
            dispatch(setApplicationProcessing());

            console.log('inside setApplication thunk')

            const res = await fetch(`${config.BACKEND_URL}/request/application/${expectBudget}/${requestId}`,{
                method: "POST",
                headers: {
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/APPLICATION_FAIL",data.message));
                return
            }

            console.log('in setApplication data.status', data.status)
            
            dispatch(setApplicationSuccess(data.status));

        } catch(err){
            dispatch(failed("@@request/APPLICATION_FAIL", err.message));
            console.log(err.message);
        }
    }
}

export function cancelApplication(token: string, requestId: number) {
    return async (dispatch: Dispatch<IApplicationActions>) => {
        try {
            dispatch(setApplicationProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/application/${requestId}`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            
            const data = await res.json();           

            
            if (res.status !== 200) {
                dispatch(failed("@@request/APPLICATION_FAIL", data.message))
                return
            }

            dispatch(setApplicationSuccess(data));

        } catch (err) {
            dispatch(failed("@@request/GET_APPLICATION_FAIL", err.message));
            console.log(err);
        }
    }
}