
export function setApplicationSuccess(status:number | null){
    return {
        type: "@@request/APPLICATION_SUCCESS" as "@@request/APPLICATION_SUCCESS",
        status,
    }  
}

export function setApplicationProcessing(){
    return {
        type: "@@request/APPLICATION_PROCESSING" as "@@request/APPLICATION_PROCESSING", 
    }
}

export function getApplicationSuccess (status:number | null){
    return {
        type: "@@request/GET_APPLICATION_SUCCESS" as "@@request/GET_APPLICATION_SUCCESS",
        status,
    }
}

export function resetApplicationSuccess (){
    return{
        type: "@@request/APPLICATION_RESET_SUCCESS" as "@@request/APPLICATION_RESET_SUCCESS",
    }
}

type FAILED = "@@request/APPLICATION_FAIL" | "@@request/GET_APPLICATION_FAIL"


export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type ApplicationActionCreators = 
typeof setApplicationSuccess |
typeof getApplicationSuccess |
typeof setApplicationProcessing |
typeof resetApplicationSuccess |
typeof failed

export type IApplicationActions = ReturnType<ApplicationActionCreators>