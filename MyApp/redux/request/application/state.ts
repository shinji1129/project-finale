export interface IApplicationState {
    status: number | null,
    expected_budget: number,
    isProcessing: boolean,
    errorMessage: null | string,
}
