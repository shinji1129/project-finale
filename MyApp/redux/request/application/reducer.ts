import { IApplicationState } from './state';
import { IApplicationActions } from './actions';
import { act } from 'react-test-renderer';

const initApplicationState: IApplicationState = {
    status: null,
    expected_budget: 0,
    isProcessing: false,
    errorMessage: null,
}

export const applicationReducers = (state: IApplicationState = initApplicationState, action: IApplicationActions):IApplicationState => {
    switch(action.type){
        case "@@request/GET_APPLICATION_SUCCESS":
            return{
                ...state,
                isProcessing:false,
                status: action.status,
                errorMessage: null,
            }
        case "@@request/APPLICATION_SUCCESS":
            return{
                ...state,
                isProcessing:false,
                status: action.status,
                errorMessage: null,
            }
        case "@@request/APPLICATION_RESET_SUCCESS":
            return{
                ...initApplicationState,
            }
        default:
            return state
    }
} 