
export function setBookmarkSuccess() {
    return {
        type: "@@request/BOOKMARK_SUCCESS" as "@@request/BOOKMARK_SUCCESS"
    }
}

export function setBookmarkProcessing() {
    return {
        type: "@@request/BOOKMARK_PROCESSING" as "@@request/BOOKMARK_PROCESSING"
    }
}

export function getBookmarkSuccess (bookmark: boolean) {
    return{
        type: "@@request/GET_BOOKMARK_SUCCESS" as "@@request/GET_BOOKMARK_SUCCESS",
        bookmark,
    }
}

export function resetBookmarkSuccess () {
    return{
        type: "@@request/BOOKMARK_RESET_SUCCESS" as "@@request/BOOKMARK_RESET_SUCCESS",
    }
}

type FAILED = "@@request/BOOKMARK_FAIL" | "@@request/APPLICATION_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type BookmarkActionCreators = 
typeof setBookmarkSuccess | 
typeof getBookmarkSuccess | 
typeof setBookmarkProcessing |
typeof resetBookmarkSuccess | 
typeof failed

export type IBookmarkActions = ReturnType<BookmarkActionCreators>
