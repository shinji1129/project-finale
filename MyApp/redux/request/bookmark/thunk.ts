import { IBookmarkActions, failed } from './actions';
import { Dispatch } from 'redux';
import { setBookmarkSuccess, setBookmarkProcessing, getBookmarkSuccess } from './actions';
import { config } from '../../../config';

export function getBookmark( token:string ,requestId: number) {
    
    return async (dispatch: Dispatch<IBookmarkActions>) => {
        try {
            dispatch(setBookmarkProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/bookmark/${requestId}`, {
                method: 'GET',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + token,
                },
            })
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/BOOKMARK_FAIL",data.message));
                return
            }

            data.length >= 1? dispatch(getBookmarkSuccess(true)):dispatch(getBookmarkSuccess(false));
            

        } catch (err) {
            dispatch(failed("@@request/BOOKMARK_FAIL", err.message));
            console.log(err.message);
        }
    }
}



export function setBookmark(token:string ,requestId: number) {
    return async (dispatch: Dispatch<IBookmarkActions>) => {
        try {
            dispatch(setBookmarkProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/bookmark/${requestId}`, {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@request/BOOKMARK_FAIL",data.message));
                return
            }

            dispatch(setBookmarkSuccess());

        } catch (err) {
            dispatch(failed("@@request/BOOKMARK_FAIL", err.message));
            console.log(err.message);
        }
    }
}
