import { IBookmarkState } from './state';
import { IBookmarkActions } from './actions';

const initBookmarkState: IBookmarkState = {
    bookmark: false,
    isProcessing: false,
    errorMessage: null
}


export const bookmarkReducers = (state: IBookmarkState = initBookmarkState, action: IBookmarkActions): IBookmarkState => {
    switch (action.type) {
        case "@@request/BOOKMARK_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                bookmark: !state.bookmark,
                errorMessage: null,
            }
        case "@@request/BOOKMARK_PROCESSING":
            return {
                ...state,
                isProcessing: true,
                errorMessage: null,
            }
        case "@@request/GET_BOOKMARK_SUCCESS":
            return {
                ...state,
                bookmark: action.bookmark,
                isProcessing: false,
                errorMessage: null,
            }
        case "@@request/BOOKMARK_RESET_SUCCESS":
            return {
                ...initBookmarkState
            }
        default: {
            return {
                ...state
            }
        }
    }
}