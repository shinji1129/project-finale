export interface IBookmarkState {
    bookmark: boolean,
    isProcessing: boolean,
    errorMessage: null | string,
}
