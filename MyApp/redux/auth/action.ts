export interface User {
    id: number;
    username: string;
}

export const loginSuccess = (token: string, user: User) => {
    return {
        type: "@@auth/LOGIN_SUCCESS" as "@@auth/LOGIN_SUCCESS",
        token,
        user
    }
}

export const loginFail = (message: string) => {
    return {
        type: "@@auth/LOGIN_FAIL" as "@@auth/LOGIN_FAIL",
        message: message
    }
}

export const logoutSuccess = () => {
    return {
        type: "@@auth/LOGOUT" as "@@auth/LOGOUT",
    }
}

type AuthActionCreators = typeof loginSuccess | typeof loginFail | typeof logoutSuccess;

export type IAuthActions = ReturnType<AuthActionCreators>