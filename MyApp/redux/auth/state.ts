import { User } from "./action";

export interface IAuthState {
    isAuthenticated: boolean | null;
    token: string | null;
    user: User | null;
    message: string | null;
}

