import { IAuthState } from './state';
import { IAuthActions } from './action';

const initAuthState: IAuthState = {
    isAuthenticated: null,
    token: null,
    user: null,
    message: null
}

export const authReducers = (state: IAuthState = initAuthState, action: IAuthActions): IAuthState => {
    switch (action.type) {
        case "@@auth/LOGIN_SUCCESS":
            return {
                ...state,
                isAuthenticated: true,
                token: action.token,
                user: action.user
            }
        case "@@auth/LOGIN_FAIL":
            return {
                ...state,
                isAuthenticated: false,
                token: null,
                user: null,
                message: action.message
            }
        case "@@auth/LOGOUT":
            return {
                ...state,
                isAuthenticated: false,
                token: null,
                user: null
            }
        default: {
            return {
                ...state
            }
        }
    }
}