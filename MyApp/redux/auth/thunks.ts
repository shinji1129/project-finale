import { Dispatch } from 'redux';
import { loginSuccess, loginFail, logoutSuccess } from './action'
import  AsyncStorage from '@react-native-community/async-storage';
import { ThunkDispatch, IRootState } from '../store';
import { config } from '../../config';
import { Alert } from 'react-native';

export const restoreLoginThunk = () => {
    return async (dispatch: Dispatch) => {
        try{
            const token = await AsyncStorage.getItem('token');
            console.log("inside restoreLogin Thunk", token)
            console.log("inside restoreLogin Thunk", config.BACKEND_URL)
            if (token) {
                const res = await fetch(`${config.BACKEND_URL}/user/info`, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                });
                const data = await res.json();
                if (res.status === 200) {
                    dispatch(loginSuccess(token, data));
                } else {
                    dispatch(logoutSuccess());
                }
            } else {
                dispatch(logoutSuccess());
            }
        } catch (e){    
            console.error(e.message)
            dispatch(logoutSuccess())
        }

    }
}

// Login
export const loginThunk = (username: string, password: string) => {
    return async function (dispatch: ThunkDispatch) {
        try {
            const res = await fetch(`${config.BACKEND_URL}/user/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ username, password })
            })

            const data = await res.json();
            
            if (res.status !== 200) {
                if (res.status === 401){
                    Alert.alert("Login Failed",
                    "Invalid username/password",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
                }
                return dispatch(loginFail(data.message))
            }

            if (!data.token) {
                return dispatch(loginFail('Network Error!'))
            }

            // React Native => AsyncStorage
            await AsyncStorage.setItem('token', data.token);
            dispatch(loginSuccess(data.token, data.user));

            // Nev to homepage
        } catch (e) {
            console.error(e.message);
            dispatch(loginFail('Internal Server Error'));
        }
    }
}

// social media
export function loginFacebook(accessToken:string){
    return async(dispatch:ThunkDispatch)=>{
        try {
            const res = await fetch(`${config.BACKEND_URL}/user/login/Facebook`,{
                method:'POST',
                headers:{
                    "Content-Type":"application/json; charset=utf-8"
                },
                body: JSON.stringify({ accessToken})
            })
            const result = await res.json();
            if(res.status!==200){
                dispatch(loginFail(result.msg));
            }else{
                console.log('FB login success, token', result.token)
                console.log('FB login success, user', result.user)
                await AsyncStorage.setItem('token',result.token);
                dispatch(loginSuccess(result.token, result.user))
            }
        } catch (e){
            console.error(e.message)
            dispatch(loginFail('Facebook login Error!'))
        }
    }
}

// Logout
export const logoutThunk = () => {
    return async (dispatch: ThunkDispatch) => {
        await AsyncStorage.removeItem('token')
        dispatch(logoutSuccess());
        // Nev to homepage
    }
}