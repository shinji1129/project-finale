import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { IAuthState } from './auth/state';
import { IAuthActions } from './auth/action';
import { authReducers } from './auth/reducers';
import { IRegisterActions } from './register/actions';
import { registerReducers } from './register/reducers';
import { IRegisterState } from './register/state';
import { IBookmarkActions } from './request/bookmark/actions';
import { bookmarkReducers } from './request/bookmark/reducer'
import { IBookmarkState } from './request/bookmark/state';
import { IApplicationState } from './request/application/state';
import { IApplicationActions } from './request/application/actions';
import { applicationReducers } from './request/application/reducer';
import { IInvitationState } from './request/invitation/state';
import { IInvitationActions } from './request/invitation/action';
import { invitationReducers } from './request/invitation/reducer';
import { IConfirmationState } from './request/confirmation/state';
import { IConfirmationActions } from './request/confirmation/action';
import { confirmationReducer } from './request/confirmation/reducer';
import { IProfileActions } from './profile/profile/actions';
import { profileReducers } from './profile/profile/reducers';
import { IProfileState } from './profile/profile/state';
import { IArtworkState } from './profile/artwork/state';
import { IArtworkActions } from './profile/artwork/actions';
import { artworksReducers } from './profile/artwork/reducer';
import { ITagsActions } from './profile/tag/actions';
import { ITagsState } from './profile/tag/state';
import { tagsReducers } from './profile/tag/reducers';
import { INewRequestState } from './request/newRequest/state';
import { INewRequestActions } from './request/newRequest/actions';
import { newRequestReducers } from './request/newRequest/reducer';
import { projectReducer } from './project/reduecr';
import { IProjectState } from './project/state';
import { IProjectActions } from './project/actions';
import { ICommentState } from './request/comment/state';
import { ICommentActions } from './request/comment/action';
import { commentReducer } from './request/comment/reducer';
import { IApplicantState } from './project/applicant/state';
import { IApplicantActions } from './project/applicant/action';
import { applicantReducers } from './project/applicant/reducer';
import { IInvitedState } from './project/invited/state';
import { IInvitedActions } from './project/invited/actions';
import { invitedReducers } from './project/invited/reducer';
import { IRatingState } from './request/rating/state';
import { IRatingActions } from './request/rating/actions';
import { RatingReducers } from './request/rating/reducer';
import { IVideoActions } from './profile/video/actions';
import { IVideoState } from './profile/video/state';
import { videosReducers } from './profile/video/reducer';
import { IAnimationAction } from './animation/action';
import { IBallState } from './animation/state';
import { ballReducers } from './animation/reducers';
import { IChatRoomsState } from './chatroom/state';
import { chatRoomsReducer } from './chatroom/reducer';


export interface IRootState {
    auth: IAuthState
    register: IRegisterState
    bookmark: IBookmarkState
    application: IApplicationState
    invitation: IInvitationState
    confirmation: IConfirmationState
    profile: IProfileState
    artwork: IArtworkState
    video: IVideoState
    tags: ITagsState
    newRequest: INewRequestState
    project: IProjectState
    comment: ICommentState
    applicant: IApplicantState
    rating: IRatingState
    invited: IInvitedState
    animation: IBallState
    chatroom: IChatRoomsState
}

type IRootAction = IAuthActions | IRegisterActions | IBookmarkActions | IApplicationActions | IInvitationActions | IConfirmationActions | IProfileActions
    | IArtworkActions | IVideoActions | ITagsActions | INewRequestActions | IProjectActions | ICommentActions | IApplicantActions | IRatingActions | IInvitedActions | IAnimationAction;

const rootReducer = combineReducers<IRootState>({
    auth: authReducers,
    register: registerReducers,
    bookmark: bookmarkReducers,
    application: applicationReducers,
    invitation: invitationReducers,
    confirmation: confirmationReducer,
    profile: profileReducers,
    artwork: artworksReducers,
    video: videosReducers,
    tags: tagsReducers,
    newRequest: newRequestReducers,
    project: projectReducer,
    comment: commentReducer,
    applicant: applicantReducers,
    invited: invitedReducers,
    rating: RatingReducers,
    animation: ballReducers,
    chatroom: chatRoomsReducer
})


declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}



// thunk
export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>

const composeEnhancers = compose;

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(thunk),
    ));

