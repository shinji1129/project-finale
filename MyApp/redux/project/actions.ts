export function getProjectStatusSuccess(status: number, applicantList: number[], invitedList: number[]) {
    return {
        type: "@@project/GET_PROJECT_STATUS_SUCCESS" as "@@project/GET_PROJECT_STATUS_SUCCESS",
        status,
        applicantList,
        invitedList
    }
}

export function setApplicantList(applicantList: number[]) {
    return {
        type: "@@project/APPLICANT_LIST_SUCCESS" as "@@project/APPLICANT_LIST_SUCCESS",
        applicantList,
    }
}

export function setInvitedList(invitedList: number[]) {
    return {
        type: "@@project/INVITED_LIST_SUCCESS" as "@@project/INVITED_LIST_SUCCESS",
        invitedList,
    }
}

export function projectProcessing() {
    return {
        type: "@@project/PROJECT_PROCESSING" as "@@project/PROJECT_PROCESSING",
    }
}

export function setProjectStatus(status: number){
    return {
        type: "@@project/SET_PROJECT_STATUS_SUCCESS" as "@@project/SET_PROJECT_STATUS_SUCCESS",
        status
    }
}

type FAILED = "@@project/GET_PROJECT_STATUS_FAIL" | "@@project/APPLICANT_LIST_FAIL" | "@@project/INVITED_LIST_FAIL" | "@@project/SET_PROJECT_STATUS_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type ProjectActionCreators =
typeof projectProcessing |
typeof getProjectStatusSuccess |
typeof setApplicantList |
typeof setInvitedList |
typeof setProjectStatus |
typeof failed


export type IProjectActions = ReturnType<ProjectActionCreators> 