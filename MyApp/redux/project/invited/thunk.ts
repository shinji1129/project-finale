import { Dispatch } from 'react';
import { config } from '../../../config';
import { IInvitedActions, setInvitedProcessing, failed, getInvitedSuccess, setInvitedSuccess } from './actions';

export function currentInvitedState(token: string, requestId: number, user_id: number) {
    return async (dispatch: Dispatch<IInvitedActions>) => {
        try {
            dispatch(setInvitedProcessing());
            const res = await fetch(`${config.BACKEND_URL}/request/invitation/${requestId}/${user_id}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@project/GET_INVITED_FAIL", data.message))
                return
            }

            if (data.status === undefined) {
                dispatch(failed("@@project/GET_INVITED_FAIL", " record not found!"))
                return;
            }

            dispatch(getInvitedSuccess(data.status, parseInt(data.invite_budget)));
        } catch (err) {
            dispatch(failed("@@project/GET_INVITED_FAIL", err.message));
            console.log(err);
        }
    }
}

export function setInvited(token: string, requestId: number, invite_budget: number, user_id: number) {
    return async (dispatch: Dispatch<IInvitedActions>) => {
        try {
            dispatch(setInvitedProcessing());

            const res = await fetch(`${config.BACKEND_URL}/project/invited/${requestId}/${user_id}/${invite_budget}`, {
                method: "POST",
                headers: {
                    Authorization: 'Bearer ' + token,
                },
            })


            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@project/INVITED_FAIL", data.message));
                return
            }


            dispatch(setInvitedSuccess(data.status));

        } catch (err) {
            dispatch(failed("@@project/INVITED_FAIL", err.message));
            console.log(err.message);
        }
    }
}

export function cancelInvited(token: string, requestId: number, user_id: number) {
    return async (dispatch: Dispatch<IInvitedActions>) => {
        try {
            dispatch(setInvitedProcessing());

            const res = await fetch(`${config.BACKEND_URL}/project/invited/${requestId}/${user_id}`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@project/INVITED_FAIL", data.message))
                return
            }

            dispatch(setInvitedSuccess(data.status));

        } catch (err) {
            dispatch(failed("@@project/INVITED_FAIL", err.message));
            console.log(err);
        }
    }
}