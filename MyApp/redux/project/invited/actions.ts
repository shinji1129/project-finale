
export function setInvitedSuccess(status:number){
    return {
        type: "@@project/INVITED_SUCCESS" as "@@project/INVITED_SUCCESS",
        status,
    }  
}

export function setInvitedProcessing(){
    return {
        type: "@@project/INVITED_PROCESSING" as  "@@project/INVITED_PROCESSING", 
    }
}

export function getInvitedSuccess (status:number, invite_budget: number){
    return {
        type: "@@project/GET_INVITED_SUCCESS" as "@@project/GET_INVITED_SUCCESS",
        status,
        invite_budget
    }
}

export function resetInvitedSuccess (){
    return{
        type: "@@project/INVITED_RESET_SUCCESS" as "@@project/INVITED_RESET_SUCCESS",
    }
}

type FAILED = "@@project/INVITED_FAIL" | "@@project/GET_INVITED_FAIL"


export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type InvitedActionCreators = 
typeof setInvitedSuccess |
typeof setInvitedProcessing |
typeof getInvitedSuccess |
typeof resetInvitedSuccess |
typeof failed

export type IInvitedActions = ReturnType<InvitedActionCreators>