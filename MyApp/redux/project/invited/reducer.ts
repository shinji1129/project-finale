import { IInvitedState } from './state';
import { IInvitedActions } from './actions';

const initInvitedState: IInvitedState = {
    status: null,
    invite_budget: null,
    isProcessing: false,
}

export const invitedReducers = (state: IInvitedState = initInvitedState, action: IInvitedActions): IInvitedState => {
    switch (action.type) {
        case "@@project/GET_INVITED_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status,
                invite_budget: action.invite_budget
            }
        case "@@project/INVITED_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status,
            }
        case "@@project/INVITED_RESET_SUCCESS":
            return {
                ...initInvitedState,
            }
        default:
            return state
    }
} 