export interface IInvitedState {
    status: number | null,
    invite_budget: number | null,
    isProcessing: boolean,
}