export interface IProjectState {
    status: number | null,
    applicantList: number[],
    invitedList: number[],
    isProcessing: boolean,
}
