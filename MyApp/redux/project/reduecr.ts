import { IProjectState } from './state';

import { IProjectActions } from './actions';

const initProjectState: IProjectState = {
    status: null,
    applicantList: [],
    invitedList: [],
    isProcessing: false,
}

export const projectReducer = (state: IProjectState = initProjectState, action: IProjectActions): IProjectState => {
    switch (action.type) {
        case "@@project/GET_PROJECT_STATUS_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status,
                applicantList: action.applicantList,
                invitedList: action.invitedList
            }
        case "@@project/APPLICANT_LIST_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                applicantList: action.applicantList
            }
        case "@@project/INVITED_LIST_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                invitedList: action.invitedList
            }
        case "@@project/SET_PROJECT_STATUS_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status
            }
        case "@@project/PROJECT_PROCESSING":
            return {
                ...state,
                isProcessing: true
            }
        case "@@project/GET_PROJECT_STATUS_FAIL":
            return {
                ...state,
                isProcessing: false
            }
        default:
            return state
    }
}