
import { Dispatch } from 'react';
import { IProjectActions, projectProcessing, failed, getProjectStatusSuccess, setProjectStatus } from './actions';
import { config } from '../../config';


export function currentProjectStatus(token: string, requestID: number) {
    return async (dispatch: Dispatch<IProjectActions>) => {
        try {
            dispatch(projectProcessing());

            const res = await fetch(`${config.BACKEND_URL}/project/${requestID}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@project/GET_PROJECT_STATUS_FAIL", data.message))
                return
            }

            dispatch(getProjectStatusSuccess(data.status, data.applicants, data.invitedList))

        } catch (err){
            dispatch(failed("@@project/GET_PROJECT_STATUS_FAIL", err.message));
            console.log(err);
        }
    }
}

export function setProjectStatusThunk(token:string, requestID:number){
    return async (dispatch: Dispatch<IProjectActions>) => {
        try {
            dispatch(projectProcessing());

            const res = await fetch(`${config.BACKEND_URL}/project/${requestID}/confirmation`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();
            
            if (res.status !== 200) {
                dispatch(failed('@@project/SET_PROJECT_STATUS_FAIL', data.message));
                return
            }

            dispatch(setProjectStatus(data.status))
        } catch (err) {
            dispatch(failed('@@project/SET_PROJECT_STATUS_FAIL', err.message));
            console.log(err);
        }
    }
}