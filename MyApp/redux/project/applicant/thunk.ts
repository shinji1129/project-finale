import { Dispatch } from 'react';
import { config } from '../../../config';
import { IApplicantActions, setApplicantProcessing, failed, getApplicantSuccess, setApplicantSuccess } from './action';


export function currentApplicant(token: string, requestID: number, userID: number) {
    return async (dispatch: Dispatch<IApplicantActions>) => {
        try {
            dispatch(setApplicantProcessing());

            const res = await fetch(`${config.BACKEND_URL}/request/application/${requestID}/${userID}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })
            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@project/GET_APPLICANT_FAIL", data.message))
                return
            }

            const {status, apply_budget, ...other} = data

            if (!status || status === 0){
                dispatch(getApplicantSuccess(0, 0))
                return
            }

            dispatch(getApplicantSuccess(status, apply_budget))
        } catch (err) {
            dispatch(failed("@@project/GET_APPLICANT_FAIL", err.message))
            console.log(err);
        }
    }
}

export function setApplicant(token: string, requestId: number, userID:number ,response: string, apply_budget:number) {
    return async (dispatch: Dispatch<IApplicantActions>) => {
        try {
            dispatch(setApplicantProcessing());

            const res = await fetch(`${config.BACKEND_URL}/project/applicant/${requestId}/${userID}/${response}/${apply_budget}`,{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            })

            const data = await res.json();

            if (res.status !== 200) {
                dispatch(failed("@@project/APPLICANT_FAIL",data.message));
                return
            }
    
            dispatch(setApplicantSuccess(data.status));


        } catch (err) {
            dispatch(failed("@@project/APPLICANT_FAIL",err.message))
            console.log(err.message);
        }
    }
}