import { IApplicantState } from './state';
import { IApplicantActions } from './action';

const initApplicantState: IApplicantState = {
    status: null,
    apply_budget: 0,
    isProcessing: false,
}

export const applicantReducers = (state: IApplicantState = initApplicantState, action: IApplicantActions): IApplicantState => {
    switch (action.type) {
        case "@@project/GET_APPLICANT_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status,
                apply_budget: action.apply_budget
            }
        case "@@project/APPLICANT_SUCCESS":
            return {
                ...state,
                isProcessing: false,
                status: action.status,
            }
        case "@@project/APPLICANT_RESET_SUCCESS":
            return {
                ...initApplicantState,
            }
        default:
            return state
    }
} 