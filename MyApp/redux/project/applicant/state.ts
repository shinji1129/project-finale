export interface IApplicantState {
    status: number | null,
    apply_budget: number,
    isProcessing: boolean,
}

