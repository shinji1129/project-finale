
export function setApplicantSuccess(status: number | null){
    return{
        type: "@@project/APPLICANT_SUCCESS" as "@@project/APPLICANT_SUCCESS",
        status,
    }
}


export function setApplicantProcessing(){
    return{
        type: "@@project/APPLICANT_PROCESSING" as "@@project/APPLICANT_PROCESSING",
    }
}

export function getApplicantSuccess(status: number | null, apply_budget: number){
    return{
        type: "@@project/GET_APPLICANT_SUCCESS" as "@@project/GET_APPLICANT_SUCCESS",
        status,
        apply_budget
    }
}

export function resetApplicant(){
    return{
        type: "@@project/APPLICANT_RESET_SUCCESS" as "@@project/APPLICANT_RESET_SUCCESS",
    }
}

type FAILED = "@@project/APPLICANT_FAIL" | "@@project/GET_APPLICANT_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type ApplicantActionCreators = 
typeof setApplicantProcessing |
typeof getApplicantSuccess |
typeof resetApplicant |
typeof setApplicantSuccess |
typeof failed


export type IApplicantActions = ReturnType<ApplicantActionCreators>