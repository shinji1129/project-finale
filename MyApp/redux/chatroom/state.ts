export interface message {
    id: number
    room_id: number
    user_one: number
    user_two: number
    message: string
    status: number
    contactUser:string
    profile_pic:string
    updated_at:string
}


export interface IChatRoomsState {
    chatRooms: Array<Array<message>>;
}
