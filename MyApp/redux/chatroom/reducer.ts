import { IChatRoomsState } from "./state";
import { IChatRoomActions } from "./actions";

const initialChatRoomsState: IChatRoomsState = {
    chatRooms:[]
}

export const chatRoomsReducer = (state:IChatRoomsState = initialChatRoomsState, action: IChatRoomActions): IChatRoomsState => {
    switch (action.type) {
        case "@@CHATROOM/GET_CHATROOM_SUCCESS":
            return {
                ...state,
                chatRooms: action.chatRooms
            }
        case "@@CHATROOM/ADD_MESSAGE_SUCCESS":
            const newChatRooms = state.chatRooms.slice();
            newChatRooms[action.index].push(action.message);
            return {
                ...state,
                chatRooms: newChatRooms
            }
        default:
            return state    }
}