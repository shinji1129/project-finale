import { Message } from "../../model/model"

export const getChatRoomProcessing = () => {
    return {
        type: "@@CHATROOM/GET_CHATROOM_PROCESSING" as "@@CHATROOM/GET_CHATROOM_PROCESSING"
    }
}

export const getChatRoomSuccess = (chatRooms:[]) => {
    return {
        type: "@@CHATROOM/GET_CHATROOM_SUCCESS" as "@@CHATROOM/GET_CHATROOM_SUCCESS",
        chatRooms
    }
}

export const addMessageSuccess = (message:Message,index:number) => {
    return {
        type: "@@CHATROOM/ADD_MESSAGE_SUCCESS" as "@@CHATROOM/ADD_MESSAGE_SUCCESS",
        index,
        message
    }
}


export const loadMessages = (message:String[], roomId:number)=>{
    return {
        type: "@@CHATROOM/LOAD_MESSAGE_SUCCESS" as "@@CHATROOM/LOAD_MESSAGE_SUCCESS",
        message,
        roomId
    }
}

type FAILED = "@@CHATROOM/GET_CHATROOM_FAIL"

export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}


// Union Types

type ChatRoomActionCreators = typeof getChatRoomProcessing | typeof getChatRoomSuccess | typeof loadMessages | typeof addMessageSuccess| typeof failed

export type IChatRoomActions = ReturnType<ChatRoomActionCreators>

