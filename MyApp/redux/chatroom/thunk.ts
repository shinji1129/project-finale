import { Dispatch } from 'react';
import { config } from '../../config';
import { getChatRoomProcessing, getChatRoomSuccess, IChatRoomActions, failed} from './actions';

export function getChatRoomByID (token:string, userID:number){
    return async (dispatch:Dispatch<IChatRoomActions>) => {
        dispatch(getChatRoomProcessing());

        const res = await fetch(`${config.BACKEND_URL}/chatroom`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        });
        const chatRooms = await res.json();
        // console.log('res.status',res.status)
        if(res.status !==  200){
            await dispatch(failed("@@CHATROOM/GET_CHATROOM_FAIL","Internal Server Error"));
            return
        }
        console.log('ChatRoom in thunk:', chatRooms);

        await dispatch(getChatRoomSuccess(chatRooms));
    }
}