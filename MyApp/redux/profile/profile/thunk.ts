import { Dispatch } from 'react';
import { config } from '../../../config';
import { IProfileActions, failed, getProfileSuccess, updateProfileProcessing, updateProfileSuccess } from './actions';
import { Alert } from 'react-native';
import { ThunkDispatch } from '../../store';
import { ImageOrVideo } from 'react-native-image-crop-picker';


export function getProfile (token:string){
    return async (dispatch:Dispatch<IProfileActions>) => {
        const res = await fetch(`${config.BACKEND_URL}/profile/all`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        });
        const profile = await res.json();
        if(res.status !==  200){
            dispatch(failed("@@profile/PROFILE_FAIL",profile.message));
            return
        }
        dispatch(getProfileSuccess(profile));
    }
}

export function resetProfilePic(token:string){
    return async (dispatch:ThunkDispatch) =>{
        try{
            const res = await fetch(`${config.BACKEND_URL}/profile/picture`,{
                method: "DELETE",
                headers: {
                    Authorization: 'Bearer ' + token,
                },
            });

            const data = await res.json();
            console.log(data)

            if(res.status == 200){
                dispatch(updateProfileSuccess());
                Alert.alert(
                    "Update Success",
                    "Your profile picture has been removed.",
                    [{text: "OK"}],{ cancelable: false }
                )
            } else if( res.status !== 200){
                Alert.alert(
                    "Update Failed",
                    data.message,
                    [{text: "OK"}],{ cancelable: false }
                )
                dispatch(failed("@@profile/UPDATE_FAIL",data.message))
            }
        } catch(err){
            console.log(err)
            dispatch(failed("@@profile/UPDATE_FAIL",'Internal Server Error'));
        }
    }
}


export function updateProfileThunk(token:string, introduction:string, tagArr:number[], profilePic?:ImageOrVideo,){
    return async (dispatch:ThunkDispatch) =>{
        try{
            console.log('profile 1')
            dispatch(updateProfileProcessing())
            const formData = new FormData();
            if(introduction){
                console.log('profile 2')

                formData.append('introduction', introduction);
            }
            console.log('profile 3')

            if(profilePic?.filename && profilePic?.mime && profilePic?.path){
                formData.append('profilePic',{
                    name: profilePic.filename,
                    type: profilePic.mime,
                    uri: profilePic.sourceURL
                });
            }
            console.log('profile 4')

            if(tagArr){
                formData.append('tagArr', tagArr.join());
                console.log('profile 5')

            }
            console.log('profile 6')

            const res = await fetch(`${config.BACKEND_URL}/profile/update`,{
                method: "POST",
                headers: {
                    'Content-Type': "multipart/form-data",
                    Authorization: 'Bearer ' + token,
                },
                body: formData
            });
            console.log('profile 6')

            const data = await res.json();
            if(res.status == 200){
                console.log('profile 7')

                dispatch(updateProfileSuccess());
                Alert.alert(
                    "Update Success",
                    "Your profile has been updated.",
                    [{text: "OK"}],{ cancelable: false }
                )
            } else if( res.status !== 200){
                console.log('profile 8')

                Alert.alert(
                    "Update Failed",
                    data.message,
                    [{text: "OK"}],{ cancelable: false }
                )
                dispatch(failed("@@profile/UPDATE_FAIL",'Internal Server Error'))
            }
        } catch(err){
            console.log(err)
            dispatch(failed("@@profile/UPDATE_FAIL",'Internal Server Error'));
        }
    }
}

export function getProfileByID (token:string, userID:number){
    return async (dispatch:Dispatch<IProfileActions>) => {
        dispatch(updateProfileProcessing());

        const res = await fetch(`${config.BACKEND_URL}/profile/all/${userID}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            },
        });
        const profile = await res.json();
        if(res.status !==  200){
            dispatch(failed("@@profile/PROFILE_FAIL",profile.message));
            return
        }
        dispatch(getProfileSuccess(profile));
    }
}