export interface IProfileState {
    profile: Profile,
    isProcessing: boolean,
    errorMessage: null | string,
}

interface Profile {
    id: number | null;
    username: string | null;
    self_intro: string;
    profile_pic: string | null;
    email: string;
    freelancer: boolean | null;
    requester: boolean | null;
    created_at: string;
    updated_at: string;
    facebook_id: number | null;
    google_id: number | null;
}

