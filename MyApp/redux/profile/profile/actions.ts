export function getProfileSuccess(profile: any) {
    return {
        type: "@@profile/PROFILE_SUCCESS" as "@@profile/PROFILE_SUCCESS",
        profile: profile
    }
}

export function resetProfile() {
    return {
        type: "@@profile/PROFILE_RESET_SUCCESS" as "@@profile/PROFILE_RESET_SUCCESS",
    }
}

export function updateProfileProcessing() {
    return {
        type: "@@profile/UPDATE_PROCESSING" as "@@profile/UPDATE_PROCESSING",
    }
}

export function updateProfileSuccess() {
    return {
        type: "@@profile/UPDATE_SUCCESS" as "@@profile/UPDATE_SUCCESS"
    }
}


type FAILED = "@@profile/PROFILE_FAIL" | "@@profile/UPDATE_FAIL"


export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type ProfileActionCreators =
    typeof getProfileSuccess |
    typeof resetProfile |
    typeof updateProfileProcessing |
    typeof updateProfileSuccess |
    typeof failed


export type IProfileActions = ReturnType<ProfileActionCreators>