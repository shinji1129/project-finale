import { IProfileState } from './state';
import { IProfileActions } from './actions';

export const initProfileState: IProfileState = {
    profile: {
        id: null,
        username: '',
        self_intro: '',
        profile_pic: null,
        email: '',
        freelancer: null,
        requester: null,
        created_at: '',
        updated_at: '',
        facebook_id: null,
        google_id: null,
    },
    isProcessing: false,
    errorMessage: null,
}


export const profileReducers = (state: IProfileState = initProfileState, action: IProfileActions): IProfileState => {
    switch (action.type) {
        case "@@profile/PROFILE_SUCCESS":
            return {
                ...state,
                profile: action.profile,
                isProcessing: false
            }
        case "@@profile/PROFILE_RESET_SUCCESS":
            return {
                ...state,
                isProcessing: false

            }
        case "@@profile/UPDATE_SUCCESS":
            return {
                ...state,
                isProcessing: false
            }
        case "@@profile/UPDATE_PROCESSING":
            return {
                ...state,
                isProcessing: true
            }
        case "@@profile/UPDATE_FAIL":
            return {
                ...state,
                isProcessing: false
            }
        default:
            return state
    }
} 