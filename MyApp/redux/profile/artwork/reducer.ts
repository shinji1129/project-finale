import { IArtworkState } from './state';
import { IArtworkActions } from './actions';

export const initArtworksState: IArtworkState = {
    artworks: [],
    isProcessing: false,
    errorMessage: null,
}

export const artworksReducers = (state: IArtworkState = initArtworksState, action: IArtworkActions):IArtworkState => {
    switch(action.type){
        case "@@profile/ARTWORK_PROCESSING":
            return {
                ...state,
                isProcessing:true
            }
        case "@@profile/ARTWORKS_SUCCESS":
            return{
                ...state,
                artworks: action.artworks,
                isProcessing:false
            }
        case "@@profile/ARTWORKS_RESET_SUCCESS":
            return{
                ...state,
                artworks:action.artworks,
                isProcessing:false
            }
        case "@@profile/DELETE_ARTWORKS_SUCCESS":
            return{
                ...state,
                artworks:action.artworks,
                isProcessing:false
            }
        case "@@profile/DELETE_ARTWORKS_FAIL":
            return{
                ...state,
                errorMessage: action.message,
                isProcessing:false
            }
        case "@@profile/UPDATE_ARTWORKS_SUCCESS":
            return{
                ...state,
                artworks:action.artworks,
                isProcessing:false
            }
        case "@@profile/UPDATE_ARTWORKS_FAIL":
            return{
                ...state,
                errorMessage: action.message,
                isProcessing:false
            }
        
        default:
            return state
    }
} 