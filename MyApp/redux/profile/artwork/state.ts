export interface IArtworkState {
    artworks: Array<Artworks>,
    isProcessing: boolean,
    errorMessage: null | string,
}


export interface Artworks {
    id: number | null;
    users_id: number;
    filename: string;
    original_name: string;
    description: string;
    extension: string;
  }


export interface Video {
    id: number | null;
    users_id: number;
    filename: string;
    original_name: string;
    description: string;
    extension: string;
  }