import { initArtworksState } from "./reducer"

export function getArtworksSuccess(artworks: any){
    return {
        type: "@@profile/ARTWORKS_SUCCESS" as "@@profile/ARTWORKS_SUCCESS",
        artworks: artworks
    }  
}

export function deleteArtworksSuccess(artworks:any){
    return{ 
        type: "@@profile/DELETE_ARTWORKS_SUCCESS" as "@@profile/DELETE_ARTWORKS_SUCCESS",
        artworks: artworks,
        isProcessing: false
    }
}

export function resetArtwork(){
    return{
        type:"@@profile/ARTWORKS_RESET_SUCCESS" as "@@profile/ARTWORKS_RESET_SUCCESS",
        artworks: initArtworksState.artworks
    }
}

export function updateArtworksSuccess(artworks:any){
    return{
        type:"@@profile/UPDATE_ARTWORKS_SUCCESS" as "@@profile/UPDATE_ARTWORKS_SUCCESS",
        artworks: artworks,
        isProcessing: false
    }
}

export function artworkProcessing(){
    return{
        type: "@@profile/ARTWORK_PROCESSING" as "@@profile/ARTWORK_PROCESSING",
        isProcessing: true
    }
}

export function updateVideoSuccess(video:any){
    return{
        type:"@@profile/UPDATE_VIDEO_SUCCESS" as "@@profile/UPDATE_VIDEO_SUCCESS",
        video: video,
        isProcessing: false
    }
}

export function videoProcessing(){
    return{
        type: "@@profile/VIDEO_PROCESSING" as "@@profile/VIDEO_PROCESSING",
        isProcessing: true
    }
}


type FAILED = "@@profile/ARTWORKS_FAIL" | "@@profile/DELETE_ARTWORKS_FAIL" | "@@profile/UPDATE_ARTWORKS_FAIL" 


export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type ArtworkActionCreators =
typeof getArtworksSuccess |
typeof deleteArtworksSuccess |
typeof resetArtwork | 
typeof updateArtworksSuccess |
typeof artworkProcessing |
typeof videoProcessing |
typeof updateVideoSuccess |
typeof failed

export type IArtworkActions = ReturnType<ArtworkActionCreators>