import { Dispatch } from 'react';
import { config } from '../../../config';
import { failed, getArtworksSuccess, IArtworkActions, deleteArtworksSuccess, updateArtworksSuccess, artworkProcessing, updateVideoSuccess } from './actions';
import { IRootState, ThunkDispatch } from '../../store';
import { ImageOrVideo, Video } from 'react-native-image-crop-picker';


export function getArtwork(token: string) {
  return async (dispatch: Dispatch<IArtworkActions>, getState: () => IRootState) => {
    dispatch(artworkProcessing());
    const res = await fetch(`${config.BACKEND_URL}/profile/artworks`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + getState().auth.token,
      },
    });
    const artworks = await res.json();
    if (res.status !== 200) {
      dispatch(failed("@@profile/ARTWORKS_FAIL", artworks.message));
      return
    }
    dispatch(getArtworksSuccess(artworks));
  }
}

export function deleteArtwork(token: string, artworkName: string) {
  return async (dispatch: Dispatch<IArtworkActions>) => {
    const res = await fetch(`${config.BACKEND_URL}/profile/artworks/${artworkName}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    });

    const getRes = await fetch(`${config.BACKEND_URL}/profile/artworks`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    });
    const artworks = await getRes.json();

    if (res.status !== 200) {
      dispatch(failed("@@profile/DELETE_ARTWORKS_FAIL", artworks.message));
      return
    }
    dispatch(deleteArtworksSuccess(artworks));
  }
}


export function updateArtwork(token: string, updateArtworks: ImageOrVideo[]) {
  return async (dispatch: ThunkDispatch) => {
    try {
      dispatch(artworkProcessing());
      const formData = new FormData();
      updateArtworks.map(async (artwork) => {
        if (artwork.filename && artwork.mime && artwork.path) {
          formData.append('artworks', {
            name: artwork.filename,
            type: artwork.mime,
            uri: artwork.sourceURL
          });
        }
      })
      const res = await fetch(`${config.BACKEND_URL}/profile/artwork/update`, {
        method: "POST",
        headers: {
          Authorization: 'Bearer ' + token,
        },
        body: formData
      });
      const data = await res.json();
      const getRes = await fetch(`${config.BACKEND_URL}/profile/artworks`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      });
      const artworks = await getRes.json();
      if (res.status == 200) {
        dispatch(updateArtworksSuccess(artworks));
      } else if (res.status !== 200) {
        dispatch(failed("@@profile/UPDATE_ARTWORKS_FAIL", data.message))
      }
    } catch (err) {
      console.log(err)
      dispatch(failed("@@profile/UPDATE_ARTWORKS_FAIL", 'Internal Server Error'));
    }
  }
}


export function updateArtworkDes(token: string, artworkName: string, description: string) {
  return async (dispatch: ThunkDispatch) => {
    try {
      dispatch(artworkProcessing());
      const formObject = {
        "artworkName": '',
        "description": ''
      };
      formObject['artworkName'] = artworkName;
      formObject['description'] = description;

      const res = await fetch(`${config.BACKEND_URL}/profile/artwork/description`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: 'Bearer ' + token,
        },
        body: JSON.stringify(formObject)
      });

      const data = await res.json();

      const getRes = await fetch(`${config.BACKEND_URL}/profile/artworks`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      });
      const artworks = await getRes.json();

      if (res.status == 200) {
        dispatch(updateArtworksSuccess(artworks));
      } else if (res.status !== 200) {
        dispatch(failed("@@profile/UPDATE_ARTWORKS_FAIL", data.message))
      }

    } catch (err) {
      console.log(err)
      dispatch(failed("@@profile/UPDATE_ARTWORKS_FAIL", 'Internal Server Error'));
    }
  }
}

export function getArtworkByID(token: string, userID: number) {
  return async (dispatch: Dispatch<IArtworkActions>) => {
    dispatch(artworkProcessing());
    const res = await fetch(`${config.BACKEND_URL}/profile/artworks/${userID}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    });
    const artworks = await res.json();
    if (res.status !== 200) {
      dispatch(failed("@@profile/ARTWORKS_FAIL", artworks.message));
    }
    dispatch(getArtworksSuccess(artworks));
  }
}
