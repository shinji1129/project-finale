import { IVideoState } from "./state";
import { IVideoActions } from './actions';


export const initVideoState: IVideoState = {
    video: [],
    isProcessing: false,
    errorMessage: null,
}

export const videosReducers = (state: IVideoState = initVideoState, action: IVideoActions):IVideoState => {
    switch(action.type){
        case "@@profile/VIDEO_PROCESSING":
            return {
                ...state,
                isProcessing:true
            }
        case "@@profile/VIDEO_SUCCESS":
            return{
                ...state,
                video: action.video,
                isProcessing:false
            }
        case "@@profile/DELETE_VIDEO_SUCCESS":
            return{
                ...state,
                video:action.video,
                isProcessing:false
            }
        case "@@profile/VIDEO_RESET_SUCCESS":
            return{
                ...state,
                video:initVideoState.video,
                isProcessing:false
            }
        case "@@profile/UPDATE_VIDEO_SUCCESS":
            return{
                ...state,
                isProcessing:false
            }
        case "@@profile/UPDATE_VIDEO_FAIL":
            return{
                ...state,
                errorMessage: action.message,
                isProcessing:false
            }
        default:
            return state
    }
} 