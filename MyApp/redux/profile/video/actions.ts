import { initVideoState } from "./reducer"

export function getVideoSuccess(video: any){
    return {
        type: "@@profile/VIDEO_SUCCESS" as "@@profile/VIDEO_SUCCESS",
        video: video,
    }  
}

export function deleteVideoSuccess(video:any){
    return{ 
        type: "@@profile/DELETE_VIDEO_SUCCESS" as "@@profile/DELETE_VIDEO_SUCCESS",
        video: video,
    }
}

export function resetVideo(){
    return{
        type:"@@profile/VIDEO_RESET_SUCCESS" as "@@profile/VIDEO_RESET_SUCCESS",
        video: initVideoState.video
    }
}

export function updateVideoSuccess(video:any){
    return{
        type:"@@profile/UPDATE_VIDEO_SUCCESS" as "@@profile/UPDATE_VIDEO_SUCCESS",
        video: video,
    }
}

export function videoProcessing(){
    return{
        type: "@@profile/VIDEO_PROCESSING" as "@@profile/VIDEO_PROCESSING",
    }
}


type FAILED = "@@profile/VIDEO_FAIL" | "@@profile/DELETE_VIDEO_FAIL" |"@@profile/UPDATE_VIDEO_FAIL"


export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type VideoActionCreators =
typeof getVideoSuccess |
typeof videoProcessing |
typeof deleteVideoSuccess |
typeof updateVideoSuccess |
typeof resetVideo |
typeof deleteVideoSuccess |
typeof failed

export type IVideoActions = ReturnType<VideoActionCreators>