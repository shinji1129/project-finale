import { Video } from "react-native-image-crop-picker";
import { ThunkDispatch } from "../../store";
import { videoProcessing, failed, updateVideoSuccess, IVideoActions, getVideoSuccess, deleteVideoSuccess } from "./actions";
import { config } from "../../../config";
import { Dispatch } from "react";

export function updateVideo(token: string, uploadVideo?: Video, description?:string) {
    return async (dispatch: ThunkDispatch) => {
      try {
        console.log('update video 1')
        dispatch(videoProcessing());
        const formData = new FormData();
        console.log("uploadVideo inside thunk:", uploadVideo)
        console.log("description inside thunk:", description)
        if(!uploadVideo){  
          console.log('update video 2')

           dispatch(failed("@@profile/UPDATE_VIDEO_FAIL", "no video"))
           return
           }
        console.log('update video 3')
          if (uploadVideo.filename && uploadVideo.mime && uploadVideo.path) {
            formData.append('video', {
              name: uploadVideo.filename,
              type: uploadVideo.mime,
              uri: uploadVideo.sourceURL
            });
            console.log('update video 4')

          }
          if(description){ 
            console.log('update video 5')
            formData.append(`description`, description)
          }
        console.log('formdata inside updateVideo', formData)
        const res = await fetch(`${config.BACKEND_URL}/profile/video/update`, {
          method: "POST",
          headers: {
            Authorization: 'Bearer ' + token,
          },
          body: formData
        });
        console.log('update video 6')

        const data = await res.json();
        const getRes = await fetch(`${config.BACKEND_URL}/profile/video`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
          },
        });

        console.log('update video 7')

        console.log('getREs status:' , getRes.status)
        const video = await getRes.json();
        if (res.status !== 200) {
          console.log('update video 8')
           dispatch(failed("@@profile/UPDATE_VIDEO_FAIL", data.message))
        } else if (res.status === 200){
          console.log('update video 9')
          dispatch(updateVideoSuccess(video));
        }
        console.log('update video 10')
      } catch (err) {
        console.log(err)
        console.log('update video 10')

        dispatch(failed("@@profile/UPDATE_VIDEO_FAIL", 'Internal Server Error'));
      }
    }
  }


export function getVideoByID(token: string, userID: number) {
    return async (dispatch: Dispatch<IVideoActions>) => {
      dispatch(videoProcessing());
      const res = await fetch(`${config.BACKEND_URL}/profile/video/${userID}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      });
      const videos = await res.json();
      if (res.status !== 200) {
        dispatch(failed("@@profile/VIDEO_FAIL", videos.message));
        return
      }
      dispatch(getVideoSuccess(videos));
    }
  }
  

export function deleteVideoByID(token: string, userID: number , videoName: string) {
    return async (dispatch: Dispatch<IVideoActions>) => {
      dispatch(videoProcessing());
      const res = await fetch(`${config.BACKEND_URL}/profile/video/${videoName}`, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      });
  
      const getRes = await fetch(`${config.BACKEND_URL}/profile/video/${userID}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      });
  
      const video = await getRes.json();
      if (res.status !== 200) {
        dispatch(failed("@@profile/VIDEO_FAIL", video.messagee));
        return
      }
      console.log('dispatch success')
      dispatch(deleteVideoSuccess(video));
    }
  }