export interface IVideoState {
    video: Array<Video>
    isProcessing: boolean,
    errorMessage: null | string,
}



export interface Video {
    id: number | null;
    users_id: number;
    filename: string;
    original_name: string;
    description: string;
    extension: string;
  }