import { initTagsState } from "./reducers"

export function getTagsSuccess(tags: any){
    return {
        type: "@@profile/TAGS_SUCCESS" as "@@profile/TAGS_SUCCESS",
        tags: tags
    }  
}

export function resetTags(){
    return{
        type:"@@profile/TAGS_RESET_SUCCESS" as "@@profile/TAGS_RESET_SUCCESS",
        tags: initTagsState.tags
    }
}

export function tagIsProcessing(){
    return {
        type: "@@/profile/TAGS_IS_PROCESSING" as "@@/profile/TAGS_IS_PROCESSING", 
    }
}

type FAILED = "@@profile/TAG_FAIL" 


export function failed(type: FAILED, message: string) {
    return {
        type,
        message
    }
}

type TagsActionCreators = 
typeof getTagsSuccess |
typeof resetTags |
typeof tagIsProcessing |
typeof failed


export type ITagsActions = ReturnType<TagsActionCreators>