import { ITagsState } from './state';
import { ITagsActions } from './actions';

export const initTagsState: ITagsState = {
    tags: [],
    isProcessing: false,
    errorMessage: null,
}

export const tagsReducers = (state: ITagsState = initTagsState, action: ITagsActions): ITagsState => {
    switch (action.type) {
        case "@@profile/TAGS_SUCCESS":
            return {
                ...state,
                tags: action.tags,
                isProcessing: false,
            }
        case "@@profile/TAGS_RESET_SUCCESS":
            return {
                ...state,
                tags: action.tags,
                isProcessing: false,
            }
        case "@@/profile/TAGS_IS_PROCESSING":
            return {
                ...state,
                isProcessing: true
            }
        default:
            return state
    }
} 