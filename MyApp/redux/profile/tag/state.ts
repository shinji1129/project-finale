export interface ITagsState {
    tags: Array<Tag>,
    isProcessing: boolean,
    errorMessage: null | string,
}

interface Tag {
    id: number | null;
    users_id: number;
    tags_id: number;
    created_at: string;
    updated_at: string;
    tag: string;
  }