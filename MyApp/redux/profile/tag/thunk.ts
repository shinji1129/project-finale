import { Dispatch } from 'react';
import { config } from '../../../config';
import { ITagsActions, failed, getTagsSuccess, tagIsProcessing } from './actions';


export function getTags (token:string){
    return async (dispatch:Dispatch<ITagsActions>) => {
        dispatch(tagIsProcessing());

        const res = await fetch(`${config.BACKEND_URL}/profile/tag`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
        },
        });
        const tags = await res.json();
        if(res.status !==  200){
            dispatch(failed("@@profile/TAG_FAIL",tags.message));
            return
        }
        dispatch(getTagsSuccess(tags));
    }
}

export function getTagsByID (token:string, userID:number){
    return async (dispatch:Dispatch<ITagsActions>) => {
        dispatch(tagIsProcessing());

        const res = await fetch(`${config.BACKEND_URL}/profile/tag/${userID}`, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
        },
        });
        const tags = await res.json();
        if(res.status !==  200){
            dispatch(failed("@@profile/TAG_FAIL",tags.message));
            return
        }
        dispatch(getTagsSuccess(tags));
    }
}