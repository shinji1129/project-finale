import { IRegisterState } from './state';
import { IRegisterActions } from './actions';

const initRegisterState: IRegisterState = {
    username: null,
    password: null,
    confirmPassword: null,
    email: null,
    profilePic:  null,
    isProcessing: false,
}


export const registerReducers = (state: IRegisterState = initRegisterState, action: IRegisterActions): IRegisterState => {
    switch(action.type){
        case "@@register/REGISTER_SUCCESS":
            return{
                ...state,
                isProcessing: false
            }
        case "@@register/REGISTER_PROCESSING":
            return{
                ...state,
                isProcessing: true
            }
        case "@@register/REGISTER_FAIL":
            return {
                ...state,
                isProcessing: false
            }
        default:{
            return {
                ...state
            }
        }
    }
}