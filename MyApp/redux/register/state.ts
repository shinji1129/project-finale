export interface IRegisterState {
    username: string | null
    password: string | null
    confirmPassword: string | null
    email: string | null
    profilePic: string | null
    isProcessing: boolean 
}

