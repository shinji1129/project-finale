
export const registerProcessing = () => {
    return {
        type: "@@register/REGISTER_PROCESSING" as "@@register/REGISTER_PROCESSING"
    }
}

export const registerSuccess = () => {
    return {
        type: "@@register/REGISTER_SUCCESS" as "@@register/REGISTER_SUCCESS"
    }
}

export const registerFail = (message: string) => {
    return {
        type: "@@register/REGISTER_FAIL" as "@@register/REGISTER_FAIL",
        message: message
    }
}



// Union Types

type RegisterActionCreators = typeof registerProcessing | typeof registerSuccess | typeof registerFail;

export type IRegisterActions = ReturnType<RegisterActionCreators>

