import { registerSuccess, registerFail, registerProcessing } from "./actions";
import { Alert } from 'react-native'
import { ThunkDispatch } from '../store';
import {ImageOrVideo} from 'react-native-image-crop-picker';
import { loginThunk } from "../auth/thunks";
import { config } from "../../config";



export function registerAccountThunk(username:string, password:string, confirmPassword:string, email: string, profilePic:ImageOrVideo, introduction:string){
    return async (dispatch:ThunkDispatch) =>{
        try{
            dispatch(registerProcessing())
            if (username && password && confirmPassword && email){
                const formData = new FormData();
                
                formData.append('username', username);
                formData.append('password', password);
                formData.append('confirmPassword',confirmPassword);
                formData.append('email', email);
                if(introduction){
                    formData.append('introduction', introduction);
                }
                if(profilePic.filename && profilePic.mime && profilePic.path){
                    formData.append('profilePic',{
                        name: profilePic.filename,
                        type: profilePic.mime,
                        uri: profilePic.sourceURL
                    });
                }

                const res = await fetch(`${config.BACKEND_URL}/user/register`,{
                    method: "POST",
                    body: formData
                });

                const data = await res.json();

                if(res.status == 200){
                    dispatch(registerSuccess());
                    dispatch(loginThunk(username, password))
                    Alert.alert(
                        "Register Success",
                        "Your registration is completed.",
                        [{text: "OK"}],{ cancelable: false }
                    )
                } else if( res.status !== 200){
                    Alert.alert(
                        "Register Failed",
                        data.message,
                        [{text: "OK"}],{ cancelable: false }
                    )
                    return dispatch(registerFail(data.message))
                }
            } else {
                dispatch(registerFail('Internal Server Error'));
            }
        } catch(err){
            console.log(err)
            dispatch(registerFail('Internal Server Error'));
        }
    }
}
