import { IAnimationAction } from "./action"
import { IBallState } from "./state"

export const initialBallState: IBallState = {
    isOpen: false,
    closeBall2: () => {
        console.log('Testing redux function closeBall')
    }
}

export const ballReducers = (state: IBallState = initialBallState, action: IAnimationAction): IBallState => {
    switch (action.type) {
        case "@@animation/OPEN_SUCCESS":
            return {
                ...state,
                isOpen: true,
            }
        case "@@animation/CLOSE_SUCCESS":
            return {
                ...state,
                isOpen: false,
            }
        default: {
            return {
                ...state
            }
        }
    }
}