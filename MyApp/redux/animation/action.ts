export function openAction() {
    return {
        type: "@@animation/OPEN_SUCCESS" as "@@animation/OPEN_SUCCESS",
    }
}
export function closeAction() {
    console.log('closeAction')
    return {
        type: "@@animation/CLOSE_SUCCESS" as "@@animation/CLOSE_SUCCESS",
    }
}

type AnimationActionCreator =
    typeof openAction |
    typeof closeAction



export type IAnimationAction = ReturnType<AnimationActionCreator>