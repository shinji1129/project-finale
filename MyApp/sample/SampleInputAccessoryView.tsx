import React, { Component } from 'react';
import { View, ScrollView, AppRegistry, TextInput, InputAccessoryView, Button, StyleSheet } from 'react-native';
import { KeyboardAvoidingView } from 'react-native';

const styles = StyleSheet.create({
    container: {
        height: 80,

    },
    holder: {
        paddingLeft: 20,
        paddingRight: 20,
        height: 60
    },
    inputBox: {
        height: 50,
        paddingLeft: 20,
        paddingRight: 20,
        borderStyle: 'solid',
        borderColor: 'black',
        borderWidth: 2,
        borderRadius: 10
    }
})


export default class SampleInputAccessoryView extends Component {
    constructor(props: string) {
        super(props);
        this.state = { text: 'Placeholder Text' };
    }

    render() {
        const inputAccessoryViewID = "uniqueID";
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                <ScrollView style={styles.holder} keyboardDismissMode="interactive">
                    <TextInput
                        style={styles.inputBox}
                        inputAccessoryViewID={inputAccessoryViewID}
                        onChangeText={text => this.setState({ text })}
                        value={this.state.text}
                    />
                </ScrollView>
                <InputAccessoryView nativeID={inputAccessoryViewID}>
                    <Button
                        onPress={() => this.setState({ text: 'Placeholder Text' })}
                        title="Reset Text"
                    />
                </InputAccessoryView>
            </KeyboardAvoidingView>
        );
    }
}
