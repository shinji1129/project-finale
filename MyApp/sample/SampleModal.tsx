import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    modal: {
        margin: 22,

    },
    showModal: {
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "black",
        borderWidth: 1,
        height: 20,
        borderRadius: 10,
    }

})

export default class SampleModal extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible: boolean) {
        this.setState({ modalVisible: visible });
    }

    render() {
        return (
            <View style={styles.modal}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        alert('Modal has been closed.');
                    }}>
                    <View style={{ marginTop: 22 }}>
                        <View>
                            <Text>Hello World!</Text>

                            <TouchableHighlight
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                <Text>Hide Modal</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>

                <TouchableHighlight
                    onPress={() => {
                        this.setModalVisible(true);
                    }}>
                    <Text style={styles.showModal}>Show Modal (I think this one is useful)</Text>
                </TouchableHighlight>
            </View>
        );
    }
}
