import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    stretch: {
        width: 50,
        height: 200
    }
});

export default class SampleImage extends Component {
    render() {
        return (
            <View>
                <Image
                    style={styles.stretch}
                    source={require('/react-native/img/favicon.png')}
                />
            </View>
        );
    }
}