import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../pages/Search/Projects/Category';
import { StyleSheet } from 'react-native';
import RequestListScreen from '../pages/Search/Projects/RequestListScreen';
import RequestScreen from '../pages/Search/Projects/RequestScreen';
import SearchTagScreen from '../pages/Search/Person/SearchTagScreen';
import PublishedJobProfileScreen from '../pages/Search/Projects/PublishedJobProfile';
import PublishedJobPublisherJobList from '../pages/Search/Projects/PublishedJobPublisherJobList';
import PublishedJobPreviewScreen from '../pages/Preview/PublishedJobPreviewScreen';

const ReqStack = createStackNavigator();

const RequestStack: React.FC = () => {
    return (
        <>
            <NavigationContainer independent={true}>
                <ReqStack.Navigator initialRouteName="Category">
                    <ReqStack.Screen name='Category' component={HomeScreen}
                        options={{
                            //@ts-ignore 
                            headerLeft: null,
                            gestureEnabled: false
                        }} />
                    <ReqStack.Screen name='RequestList' component={RequestListScreen} />
                    <ReqStack.Screen name='Request' component={RequestScreen} />

                    <ReqStack.Screen name='Search' component={SearchTagScreen} />
                    <ReqStack.Screen
                        name='Publisher'
                        component={PublishedJobProfileScreen}
                    />
                    <ReqStack.Screen
                        name='Publisher Published Job List'
                        component={PublishedJobPublisherJobList}
                    />
                    <ReqStack.Screen
                        name='Published Job'
                        component={PublishedJobPreviewScreen}
                    />
                </ReqStack.Navigator>
            </NavigationContainer>
        </>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        minHeight: '100%',
        padding: 5,
    },
});

export default RequestStack;