import React, { useState } from 'react';
import { View, Text, Modal, TouchableOpacity, Alert, StyleSheet, Dimensions, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { useNavigation } from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';

export interface ISelectRatingProps {
    requestId: number;
}

const WINDOW_WIDTH = Dimensions.get('window').width;
const FONT_SIZE = 15;

const SelectRating: React.FC<ISelectRatingProps> = (props) => {
    const [rating, setRating] = useState(0);

    return (
        <>
        <Text style={styles.text}>Rating:</Text>
        <View style={{flexDirection:'row', marginTop:10}}>
        { rating === 0?
        <TouchableOpacity onPress={()=>setRating(1)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"staro"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
        </TouchableOpacity>:
        <TouchableOpacity onPress={()=>setRating(1)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"star"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
            </TouchableOpacity>
        }
        { rating <= 1?
        <TouchableOpacity onPress={()=>setRating(2)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"staro"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
        </TouchableOpacity>:
        <TouchableOpacity onPress={()=>setRating(2)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"star"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
            </TouchableOpacity>
        }
        { rating <= 2?
        <TouchableOpacity onPress={()=>setRating(3)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"staro"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
        </TouchableOpacity>:
        <TouchableOpacity onPress={()=>setRating(3)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"star"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
            </TouchableOpacity>
        }
        { rating <= 3?
        <TouchableOpacity onPress={()=>setRating(4)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"staro"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
        </TouchableOpacity>:
        <TouchableOpacity onPress={()=>setRating(4)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"star"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
            </TouchableOpacity>
        }
        { rating <= 4?
        <TouchableOpacity onPress={()=>setRating(5)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"staro"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
        </TouchableOpacity>:
        <TouchableOpacity onPress={()=>setRating(5)}>
            <AntDesign
                style={styles.ratingIcon}
                name={"star"}
                size={24}
                color={"#ffff00"}>
            </AntDesign>
            </TouchableOpacity>
        }
        </View>
        </>)
}

export default SelectRating;

const styles = StyleSheet.create({
    mainContainer: {
        width: WINDOW_WIDTH * 0.95,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 3,
    },
    commentSection: {
        width: WINDOW_WIDTH * 0.95,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
    },
    title: {
        marginTop: 30,
        paddingLeft: 15,
        fontFamily: 'syncopate',
        fontSize: FONT_SIZE * 1.5
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: 15
    },
    aboutCommenter: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '35%',
        height: 'auto',
    },
    username: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },
    profilePicBox: {
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
    profilePic: {
        borderRadius: WINDOW_WIDTH * 0.175,
        width: WINDOW_WIDTH * 0.15,
        height: WINDOW_WIDTH * 0.15,
        overflow: 'hidden',
    },
    textContainer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'varela round',
        fontSize: FONT_SIZE
    },
    commentBox: {
        borderColor: '#aaa',
        borderWidth: 1,
        borderRadius: 6,
        width: WINDOW_WIDTH * 0.57,
        minHeight: WINDOW_WIDTH * 0.2,
        padding: 5,
        margin: 3

    },
    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
      },
    iconContainer:{
        flexDirection:'row'
    },
    ratingText: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },
})

