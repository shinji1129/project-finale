import React from 'react'
import { Dimensions, StyleSheet, View } from 'react-native'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import { useDispatch } from 'react-redux'
import { closeAction } from '../redux/animation/action'


const TryCloseBallOutside: React.FC = ({ children }) => {
    const dispatch = useDispatch();

    return (
        <View style={styles.allContainer}>
            <TouchableWithoutFeedback onPress={() => dispatch(closeAction())} >
                {children}
            </TouchableWithoutFeedback>
        </View >
    )
}

const styles = StyleSheet.create({
    allContainer: {
        minHeight: '100%',
        width: Dimensions.get('window').width
    }
})

export default TryCloseBallOutside

