import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions} from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { config } from '../config';
import { useNavigation } from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';

export interface IRatingProps {
    userId: number;
    ratingOn: 'freelancer' | 'requester'
}

interface Rating {
    AvgFreelancerRating?: number | null,
    AvgRequesterRating?: number | null,
}

const WINDOW_WIDTH = Dimensions.get('window').width;
const FONT_SIZE = 15;

const RatingInProfile: React.FC<IRatingProps> = (props) => {
    const token = useSelector((state: IRootState) => state.auth.token);

    const navigation = useNavigation();
    const [rating, setRating] = useState<Rating>();

    useEffect(() => {
        const fetchRatingByUserId = async (userId: number) => {
            const res = await fetch(`${config.BACKEND_URL}/profile/rating/${userId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            });
            const rating = await res.json();
            setRating(rating);
        }
        try {
            fetchRatingByUserId(props.userId);
        } catch (err) {
            console.error(err.message);
        }
    }, []);

    useEffect(() => {
        const didMount = navigation.addListener('focus', () => {
            const fetchRatingByUserId = async (userId: number) => {
                const res = await fetch(`${config.BACKEND_URL}/profile/rating/${userId}`, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + token,
                    }
                });
                const rating = await res.json();
                setRating(rating);
            };

            try {
                fetchRatingByUserId(props.userId);
            } catch (err) {
                console.error(err.message);
            }
        });
        return didMount;
    }, [navigation])

    const iconForLoop = (rating: number, iconName: string, color: string, style: any) => {
        const iconArr = [];
        for (let i = 0; i < rating; i++) {
            iconArr.push(
                <AntDesign key={i}
                    style={style}
                    name={iconName}
                    size={24}
                    color={color}>
                </AntDesign>);
        }
        return iconArr;
    }

    return (
        <View style={styles.iconContainer}>
            {props.ratingOn === 'freelancer' &&
                <>
                    <Text style={styles.title}>Overall Rating: </Text>
                    {rating && rating?.AvgFreelancerRating && iconForLoop(Math.ceil(rating?.AvgFreelancerRating), 'star', "#ffff00", {
                        textShadowColor: '#666',
                        textShadowRadius: 3,
                        paddingBottom: 5,
                        fontSize: 24
                    })}
                    {rating?.AvgFreelancerRating && iconForLoop(5 - Math.ceil(rating?.AvgFreelancerRating), 'staro', "#000000", {
                        color: '#aaa',
                        paddingBottom: 5,
                        fontSize: 24
                    })}
                    {rating && !rating.AvgFreelancerRating && <Text style={styles.title}>Not Rated</Text>}
                </>}
            {props.ratingOn === 'requester' &&
                <>
                    <Text style={styles.title}>Overall Rating: </Text>
                    {rating && rating?.AvgRequesterRating && iconForLoop(Math.ceil(rating?.AvgRequesterRating), 'star', "#ffff00", {
                        textShadowColor: '#666',
                        textShadowRadius: 3,
                        paddingBottom: 5,
                        fontSize: 24
                    })}
                    {rating?.AvgRequesterRating && iconForLoop(5 - Math.ceil(rating?.AvgRequesterRating), 'staro', "#000000", {
                        color: '#aaa',
                        paddingBottom: 5,
                        fontSize: 24
                    })}
                    {rating && !rating.AvgRequesterRating && <Text style={styles.title}>Not Rated</Text>}
                </>}

        </View>
    )
}

export default RatingInProfile;

const styles = StyleSheet.create({
    mainContainer: {
        width: WINDOW_WIDTH * 0.95,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 3,
    },
    commentSection: {
        width: WINDOW_WIDTH * 0.95,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
    },
    title: {
        paddingVertical: 3,
        marginVertical: 8,
        fontFamily: 'Syncopate',
        fontSize: 15,
        color: '#777',
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: 15
    },
    aboutCommenter: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '35%',
        height: 'auto',
    },
    username: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },
    profilePicBox: {
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
    profilePic: {
        borderRadius: WINDOW_WIDTH * 0.175,
        width: WINDOW_WIDTH * 0.15,
        height: WINDOW_WIDTH * 0.15,
        overflow: 'hidden',
    },
    textContainer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'varela round',
        fontSize: FONT_SIZE
    },
    commentBox: {
        borderColor: '#aaa',
        borderWidth: 1,
        borderRadius: 6,
        width: WINDOW_WIDTH * 0.57,
        minHeight: WINDOW_WIDTH * 0.2,
        padding: 5,
        margin: 3
    },
    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
    },
    emptyStarIcon: {
        color: '#aaa'
    },
    iconContainer: {
        marginTop: 10,
        flexDirection: 'row',
        paddingLeft: 7,
        alignItems: 'center'
    },
    ratingText: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },

})
