import React from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native';

const JCLogo: React.FC = () => {
  return (
    <View style={styles.imgContainer}>
      <Image style={styles.image} source={require('../assets/JCLogo.png')} />
    </View>
  );
};

const styles = StyleSheet.create({
  imgContainer: {
    width: Dimensions.get('window').width - 5,
    height: 'auto',
    marginVertical: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    resizeMode: 'contain',
    maxWidth: Dimensions.get('window').width - 3,
    height: Dimensions.get('window').height * 0.33,
  },
});

export default JCLogo;
