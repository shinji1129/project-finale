
import React from 'react';
import {
  Dimensions,
  Animated,
  StyleSheet,
  Text,
  View,
  Easing,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


function LoadingBall() {
  let rotateValue = new Animated.Value(0);

  Animated.loop(
    Animated.timing(rotateValue, {
      toValue: 1,
      duration: 1600,
      easing: Easing.linear,
      useNativeDriver: true,
    }),
  ).start();

  const rotationInterpolate = rotateValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });
  const rotation = {
    transform: [{rotate: rotationInterpolate}],
  };

  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <Animated.View style={rotation}>
        <LinearGradient
          colors={['#33eeee', 'transparent']}
          useAngle={true}
          angle={100}
          style={[styles.gradient]}>
          <View>
            <Animated.View style={[styles.ball, [rotation]]}></Animated.View>
            <Animated.View style={[styles.ball2, [rotation]]}></Animated.View>
          </View>
        </LinearGradient>
      </Animated.View>
      <LinearGradient
        colors={['#666', '#333a', '#666']}
        useAngle={true}
        angle={180}
        style={styles.innerCircle}>
        <View>
          <Text style={styles.text}>Loading</Text>
        </View>
      </LinearGradient>
    </View>
  );
}
const styles = StyleSheet.create({
  gradient: {
    width: 200,
    height: 200,
    borderRadius: 100,
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 0,
  },
  ball: {
    padding: 0,
    margin: 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#33eeee',
    shadowColor: '#33eeee',
    shadowOffset: {width: -10, height: 0},

    shadowOpacity: 1,
    shadowRadius: 6,
    textShadowColor: '#33eeee',
    textShadowOffset: {width: -13, height: 0},
    textShadowRadius: 3,
  },
  ball2: {
    padding: 0,
    margin: 0,
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#3ee1',
    shadowColor: '#33eeee',
    shadowOffset: {width: -10, height: -20},

    shadowOpacity: 1,
    shadowRadius: 6,
    textShadowColor: '#33eeee',
    textShadowOffset: {width: -10, height: -20},
    textShadowRadius: 3,
  },
  innerCircle: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 1,
    width: 160,
    height: 160,
    borderRadius: 80,
  },
  text: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'syncopate',
    shadowRadius: 3,
    shadowOpacity: 1,
    shadowColor: '#FFF',
    textShadowColor: '#FFF',
    textShadowRadius: 2,
  },
});
export default LoadingBall;
