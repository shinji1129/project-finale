import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


const InputMsgBlackWhiteLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#bbb', '#FFF']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.4 }}
      angle={358}
      style={styles.gradient}>
      <View style={styles.container}>{children}</View>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  container: {
    width: '100%',
  },
});
export default InputMsgBlackWhiteLinearGradient;
