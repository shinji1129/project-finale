import React, { useState, useEffect } from 'react';
import { View, Text, Modal, TouchableOpacity, Alert, StyleSheet, Dimensions, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { config } from '../config';
import { useNavigation } from '@react-navigation/native';
import LGCommentBox from './LGCommentBox';
import RatingInComment from './RatingInComment';

export interface ICommentProps {
    requestId: number;
}

interface Comment {
    id: number
    comment: string
    requests_id: number
    requester_id: number
    users_id: number
    username: string
    profile_pic: string
}

const WINDOW_WIDTH = Dimensions.get('window').width;
const FONT_SIZE = 15;

const CommentBox: React.FC<ICommentProps> = (props) => {
    const token = useSelector((state: IRootState) => state.auth.token);
    const navigation = useNavigation();
    const [comments, setComments] = useState<Comment[]>([]);

    useEffect(() => {
        const fetchComments = async (requestId: number) => {
            const res = await fetch(`${config.BACKEND_URL}/request/comment/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            });
            const comments = await res.json();
            setComments(comments);
        }
        try {
            fetchComments(props.requestId);

        } catch (err) {
            console.error(err.message);
        }
    }, []);

    useEffect(() => {
        const didMount = navigation.addListener('focus', () => {
            const fetchComments = async (requestId: number) => {
                const res = await fetch(`${config.BACKEND_URL}/request/comment/${requestId}`, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + token,
                    }
                });
                const comments = await res.json();
                setComments(comments);
            };

            try {
                fetchComments(props.requestId);
            } catch (err) {
                console.error(err.message);
            }
        });

        return didMount;
    }, [navigation])


    return (
        <View>
            {comments.length >= 1 &&
                <>
                    <Text style={styles.title}>comments</Text>
                    {comments.map((comment, idx) => {
                        return (
                            <View key={`comment_${idx}`} style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 5 }}>
                                <View style={styles.mainContainer}>
                                    <LGCommentBox>
                                        <View style={styles.commentSection}>
                                            {comment != undefined &&
                                                <>
                                                    {comment.requester_id === comment.users_id && <View style={styles.container}>
                                                        <View style={styles.aboutCommenter}>
                                                            <View>
                                                                <View style={styles.profilePicBox}>
                                                                    <Image
                                                                        style={styles.profilePic}
                                                                        source={{
                                                                            uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${comment["profile_pic"]}`,
                                                                        }}
                                                                    />
                                                                </View>
                                                            </View>
                                                            <View style={styles.textContainer}>
                                                                <View>
                                                                    <Text style={styles.username}>{comment["username"]}</Text>
                                                                </View>
                                                                <Text style={styles.text}>
                                                                    as Job publisher
                                                            </Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ flexDirection: 'column', }}>
                                                            <RatingInComment requestId={props.requestId} ratingOn={'requester'} />
                                                            <Text style={styles.commentBox}>{comment["comment"]}</Text>
                                                        </View>
                                                    </View>
                                                    }
                                                    {comment.requester_id != comment.users_id && <View style={styles.container}>
                                                        <View style={{ flexDirection: 'column' }}>
                                                            <RatingInComment requestId={props.requestId} ratingOn={'freelancer'} />
                                                            <Text style={styles.commentBox}>{comment["comment"]}</Text>
                                                        </View>
                                                        <View style={styles.aboutCommenter}>
                                                            <View>
                                                                <View style={styles.profilePicBox}>
                                                                    <Image
                                                                        style={styles.profilePic}
                                                                        source={{
                                                                            uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${comment["profile_pic"]}`,
                                                                        }}
                                                                    />
                                                                </View>
                                                            </View>
                                                            <View style={styles.textContainer}>
                                                                <View>
                                                                    <Text style={styles.username}>{comment["username"]}</Text>
                                                                </View>
                                                                <Text style={styles.text}>
                                                                    as Freelancer
                                                            </Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    }
                                                </>
                                            }
                                        </View>
                                    </LGCommentBox>
                                </View>
                            </View>
                        )
                    })}
                </>
            }
        </View>)
}

export default CommentBox;

const styles = StyleSheet.create({
    mainContainer: {
        width: WINDOW_WIDTH * 0.95,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 3,
    },
    commentSection: {
        width: WINDOW_WIDTH * 0.95,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,

    },
    title: {
        marginTop: 30,
        paddingLeft: 15,
        fontFamily: 'syncopate',
        fontSize: FONT_SIZE * 1.5
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: 15
    },
    aboutCommenter: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '35%',
        height: 'auto',
        margin: 2,
    },
    username: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },
    profilePicBox: {
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
    profilePic: {
        borderRadius: WINDOW_WIDTH * 0.175,
        width: WINDOW_WIDTH * 0.15,
        height: WINDOW_WIDTH * 0.15,
        overflow: 'hidden',
    },
    textContainer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'varela round',
        fontSize: FONT_SIZE
    },
    commentBox: {
        borderColor: '#aaa',
        borderWidth: 1,
        borderRadius: 6,
        width: WINDOW_WIDTH * 0.57,
        minHeight: WINDOW_WIDTH * 0.2,
        padding: 5,
        margin: 3

    },
    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
    },
    iconContainer: {
        flexDirection: 'row'
    },
    ratingText: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },
})