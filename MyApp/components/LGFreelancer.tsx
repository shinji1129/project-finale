import React from 'react';
import { StyleSheet, SafeAreaView, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const LGFreelancer: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#bbb', '#FFF']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.4 }}
      angle={353}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    padding: 10,
  },
  container: {
    flexDirection: 'row',
  },
});
export default LGFreelancer;
