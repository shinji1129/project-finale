import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import EditProfileScreen from '../pages/Setting/EditProfile';
import SettingScreen from '../pages/Setting/SettingScreen';
import ProjectListScreen from '../pages/Profile/Projects/ProjectListScreen';
import ProfilePreviewStack from './ProfilePreviewStack';
import ProjectScreen from '../pages/Search/Projects/PublishedJobListScreen';
import ChatRoomsList from '../pages/ChatRoom/chatRoomListScene';
import ChatRoom from '../pages/ChatRoom/ChatRoom';
import BookmarkStack from './BookmarkStack';
 '../pages/ChatRoom';
const SettingStack: React.FC = () => {
  const Stack = createStackNavigator();

  return (
    <>
      <NavigationContainer independent={true}>
        <Stack.Navigator initialRouteName="Setting">
          <Stack.Screen
            name="Setting"
            component={SettingScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Preview Profile"
            component={ProfilePreviewStack}
            options={{
              //@ts-ignore
              headerLeft: null,
              gestureEnabled: false,
              headerShown: false,
            }}></Stack.Screen>
          <Stack.Screen
            name="Project"
            component={ProjectListScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="Edit Profile"
            component={EditProfileScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="Posted Job"
            component={ProjectScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="Bookmark"
            component={BookmarkStack}
            options={{
              //@ts-ignore
              headerLeft: null,
              gestureEnabled: false,
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default SettingStack;
