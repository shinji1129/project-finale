import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Alert } from 'react-native';
import { NavigationProp } from '@react-navigation/native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { useDispatch } from 'react-redux';
import { loginFacebook } from '../redux/auth/thunks';
import { config } from '../config';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { LoginManager } from 'react-native-fbsdk';


function LoginButtons(props: any) {
  const dispatch = useDispatch();

  const FbLoginHandler = () => {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            if (data !== null){
              dispatch(loginFacebook(data.accessToken.toString()));
            }
          });
        }
      },
      function (error) {
        console.log('Login fail with error: ' + error);
      },
    );
  };

  return (
    <View style={styles.btnContainer}>
      <TouchableOpacity
        style={styles.button}

        onPress={() => {
          props.props.navigation.navigate('Login');
        }}>
        <Text style={styles.font}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.fbButton}

        onPress={FbLoginHandler}>
        <Text style={styles.fbFont}>Login in with Facebook</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.signUpButton}

        onPress={() => {
          props.props.navigation.navigate('Register');
        }}>
        <Text style={styles.signUpFont}>Sign up</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  btnContainer: {
    padding: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#7774',
    borderRadius: 10,
    width: '80%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  fbButton: {
    backgroundColor: '#3977EA',
    borderRadius: 10,
    width: '80%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  font: {
    fontFamily: 'Varela Round',
    textTransform: 'uppercase',
    fontSize: 17,
  },
  fbFont: {
    fontFamily: 'Varela Round',
    textTransform: 'uppercase',
    color: 'white',
    fontSize: 17,
  },
  signUpFont: {
    fontFamily: 'Varela Round',
    textTransform: 'uppercase',
    color: 'white',
    textShadowRadius: 5,
    textShadowColor: 'white',
    shadowColor: '#aaa',
    shadowOpacity: 1,
    shadowRadius: 1,
    shadowOffset: { width: 0, height: 1 },
    fontSize: 17,
  },

  signUpButton: {
    backgroundColor: '#4EC9B0',
    borderRadius: 10,
    width: '80%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
});

export default LoginButtons;
