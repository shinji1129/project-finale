import React from 'react';
import { StyleSheet, SafeAreaView, Text, View, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { initialWindowMetrics } from 'react-native-safe-area-context';

const ProfileLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#4b818b', '#84CFD5', '#84CFD5', '#84CFD5']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.4 }}
      angle={357}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    maxWidth: Dimensions.get('window').width,
    opacity: 0.85,
  },
  container: {
    maxWidth: Dimensions.get('window').width,
  },
});
export default ProfileLinearGradient;
