import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import BookmarkedListScreen from '../pages/Setting/BookmarkScreen';
import BookmarkedRequestScreen from '../pages/Setting/BookmarkRequestScreen';

const BStack = createStackNavigator();

const BookmarkStack: React.FC = () => {

    return (
        <>
            <NavigationContainer independent={true}>
                <BStack.Navigator initialRouteName="Bookmark">
                    <BStack.Screen name='Bookmark' component={BookmarkedListScreen} options={{
                        //@ts-ignore
                        headerLeft: null,
                        gestureEnabled: false,
                        headerShown: true,
                    }} />
                    <BStack.Screen name='Bookmarked Request' component={BookmarkedRequestScreen} />
                </BStack.Navigator>
            </NavigationContainer>
        </>
    );
};

export default BookmarkStack;