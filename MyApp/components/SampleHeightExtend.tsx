import React from 'react';
import {
  StyleSheet,
  Animated,
  Text,
  View,
  Dimensions,
  Easing,
  TouchableWithoutFeedback,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

function TestHeightExtend() {
  let heightValue = new Animated.Value(60);
  const fontColor = new Animated.Value(0);
  let rotateValue = new Animated.Value(0);
  let opacityValue = new Animated.Value(0);
  let isOpen = false;
  let iconName = 'downcircle';

  //----------------------------------------------------------------------
  const isX = () => {
    if (iconName === 'downcircle') {
      iconName = 'downcircleo';
      console.log(iconName);
    } else {
      iconName = 'downcircle';
      console.log(iconName);
    }
  };
  //----------------------------------------------------------------------
  const handlePress = () => {
    if (isOpen === false) {
      Animated.timing(heightValue, {
        toValue: 300,
        duration: 600,
        easing: Easing.elastic(2),
        useNativeDriver: false,
      }).start();
      Animated.timing(fontColor, {
        toValue: 100,
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.timing(rotateValue, {
        toValue: 1,
        duration: 200,
        easing: Easing.out(Easing.linear),
        useNativeDriver: true,
      }).start(),
        Animated.timing(opacityValue, {
          toValue: 1,
          duration: 500,
          delay: 400,
          useNativeDriver: false,
        }).start(),
        (isOpen = true);
    } else {
      Animated.timing(heightValue, {
        toValue: 60,
        duration: 600,
        easing: Easing.elastic(1),
        useNativeDriver: false,
      }).start();
      Animated.timing(fontColor, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.timing(rotateValue, {
        toValue: 0,
        duration: 200,
        easing: Easing.out(Easing.linear),
        useNativeDriver: true,
      }).start(),
        Animated.timing(opacityValue, {
          toValue: 0,
          duration: 200,
          useNativeDriver: false,
        }).start(),
        (isOpen = false);
    }
  };
  const fontColorInterpolate = fontColor.interpolate({
    inputRange: [0, 100],
    outputRange: ['#555', '#f00'],
  });

  const finalColor = {
    backgroundColor: fontColorInterpolate,
  };
  console.log('fnl', finalColor);
  const rotationInterpolate = rotateValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '180deg'],
  });

  const rotation = {
    transform: [{rotate: rotationInterpolate}],
  };
  return (
    <View style={styles.container}>
      <Animated.View
        {...finalColor}
        style={[styles.card, {height: heightValue}]}>
        <View style={styles.topLayerContainer}>
          <Text>Title</Text>

          <TouchableWithoutFeedback
            style={[
              {
                width: 20,
                height: 20,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              },
            ]}
            onPress={() => {
              handlePress();
              isX();
            }}>
            <Animated.View style={[rotation]}>
              <Animated.View {...finalColor}>
                <IconAntDesign
                  // style={finalColor}

                  name="downcircle"
                />
              </Animated.View>
            </Animated.View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.lowerLayerContainer}>
          <ScrollView>
            <Animated.View style={{opacity: opacityValue}}>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
              <Text>This is a demo passage</Text>
            </Animated.View>
          </ScrollView>
        </View>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  icon: {
    fontSize: 20,
  },
  container: {
    backgroundColor: '#444',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  card: {
    borderRadius: 7,
    padding: 8,
    width: WINDOW_WIDTH * 0.8,
    backgroundColor: 'white',
    overflow: 'hidden',
    paddingBottom: 30,
  },
  topLayerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  lowerLayerContainer: {
    marginTop: 20,
  },
});
export default TestHeightExtend;
