import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SearchFreelancerScreen from '../pages/Search/Person/SearchFreelancerScreen';
import FreelancerScreen from '../pages/Search/Person/FreelancerScreen';
import FreelancerSearchResult from '../pages/Search/Person/FreelancerSearchResultScreen';
import FreelancerProjectListScreen from '../pages/Search/Person/FreelancerProjectList';
import ProjectPreviewScreen from '../pages/Preview/ProjectPreviewScreen';
import PublishedJobPreviewScreen from '../pages/Preview/PublishedJobPreviewScreen';
import FreelancerPostedJobListScreen from '../pages/Search/Person/FreelancerPublishedJobList';
import ProfileVideoScreen from '../pages/Profile/ProfileVideoScreen';
import FreelancerArtworkScreen from '../pages/Search/Person/FreelancerArtworkScreen';

const FLStack = createStackNavigator();

const FreelancerStack: React.FC = () => {

    return (
        <>
            <NavigationContainer independent={true}>
                <FLStack.Navigator initialRouteName="Freelancers">
                    <FLStack.Screen name='Freelancers' component={SearchFreelancerScreen}
                        options={{
                            //@ts-ignore 
                            headerLeft: null,
                            gestureEnabled: false
                        }}
                    />
                    <FLStack.Screen name='FreelancerSearchResult' component={FreelancerSearchResult} />
                    <FLStack.Screen name='Freelancer' component={FreelancerScreen} />
                    <FLStack.Screen name='Artwork' component={FreelancerArtworkScreen}/>
                    <FLStack.Screen name="Video" component={ProfileVideoScreen}/>
                    <FLStack.Screen name='Projects' component={FreelancerProjectListScreen}/>
                    <FLStack.Screen name='Project' component={ProjectPreviewScreen}/>
                    <FLStack.Screen name='Published Jobs' component={FreelancerPostedJobListScreen}/>
                    <FLStack.Screen name='Posted Job' component={PublishedJobPreviewScreen}/>
                </FLStack.Navigator>
            </NavigationContainer>
        </>
    );
};

export default FreelancerStack;