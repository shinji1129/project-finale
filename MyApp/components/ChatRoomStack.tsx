





import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ChatRoomsList from '../pages/ChatRoom/chatRoomListScene';
import ChatRoom from '../pages/ChatRoom/ChatRoom';

const CStack = createStackNavigator();

const ChatRoomStack: React.FC = () => {

    return (
        <>
            <NavigationContainer independent={true}>
                <CStack.Navigator initialRouteName="Chat Rooms">
                    <CStack.Screen
                        name="Chat Rooms"
                        component={ChatRoomsList}
                        options={{
                            //@ts-ignore
                            headerLeft: null,
                            gestureEnabled: false,
                            headerShown: true
                        }}
                    />
                    <CStack.Screen
                        name="ChatBox"
                        component={ChatRoom}
                        options={{
                            headerShown: true,
                        }}
                    />
                </CStack.Navigator>
            </NavigationContainer>
        </>
    );
};

export default ChatRoomStack;