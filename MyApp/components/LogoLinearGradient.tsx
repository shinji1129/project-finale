import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const LogoLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#3a707a', '#4b818b', '#599cac', '#84CFD5']}
      useAngle={true}
      angle={25}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    flex: 1,
    opacity: 0.99,
  },
  container: {
    flex: 1,
    marginHorizontal: 15,
  },
});
export default LogoLinearGradient;
