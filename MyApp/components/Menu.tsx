import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Animated,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import LogoLinearGradient from './LogoLinearGradient';
import { useNavigation } from '@react-navigation/native';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


const Menu: React.FC = (props) => {
  let buttonSize = new Animated.Value(1);
  let bounceBack = useState(new Animated.ValueXY({ x: 0, y: 0 }))[0];
  let menuPosition = useState(
    new Animated.ValueXY({ x: -WINDOW_WIDTH, y: 0 }),
  )[0];

  const [isOpen, setIsOpen] = useState(false)
  const navigation = useNavigation();

  function setMenuOpen() {
    if (isOpen === false) {
      Animated.timing(menuPosition, {
        toValue: { x: -WINDOW_WIDTH * 0.04, y: 0 },
        duration: 500,
        useNativeDriver: false,
      }).start();
      setIsOpen(!isOpen)
    } else {
      Animated.timing(menuPosition, {
        toValue: { x: -WINDOW_WIDTH, y: 0 },
        duration: 500,
        useNativeDriver: false,
      }).start();
      setIsOpen(!isOpen)
    }
  }
  const handlePress = () => {
    Animated.sequence([
      Animated.timing(buttonSize, {
        toValue: 0.9,
        duration: 150,
        useNativeDriver: false,
      }),
      Animated.timing(buttonSize, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }),
    ]).start();
  };

  function bounceButton() {
    Animated.sequence([
      Animated.timing(bounceBack, {
        toValue: { x: 0, y: 5 },
        duration: 150,
        useNativeDriver: false,
      }),
      Animated.timing(bounceBack, {
        toValue: { x: 0, y: 0 },
        duration: 150,
        useNativeDriver: false,
      }),
    ]).start();
  }
  const sizeStyle = {
    transform: [{ scale: buttonSize }],
  };
  return (
    <View style={menuStyles.container}>
      <Animated.View style={[menuPosition.getLayout(), menuStyles.menuPage]}>
        <View style={menuStyles.menuBack}>
          <LogoLinearGradient>
            <View style={menuStyles.textContainer}>
              <TouchableOpacity
                onPress={() => {
                  setIsOpen(false);
                  navigation.navigate('Profile')
                }}>
                <Text style={menuStyles.text}>profile</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                navigation.navigate('Edit Profile')
              }}>
                <Text style={menuStyles.text}>edit profile</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={menuStyles.text}>project</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={menuStyles.text}>posted job</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={menuStyles.text}>Received offer</Text>
              </TouchableOpacity>
            </View>
          </LogoLinearGradient>
        </View>
      </Animated.View>

      <Animated.View style={[sizeStyle, bounceBack.getLayout()]}>
        <TouchableWithoutFeedback
          onPress={() => {
            setMenuOpen();
            handlePress();
            bounceButton();
          }}
          style={menuStyles.buttonPosition}>
          <IconSimpleLineIcons
            style={menuStyles.textShadow}
            name="menu"
            size={25}
          />
        </TouchableWithoutFeedback>
      </Animated.View>
    </View>
  );
};

const menuStyles = StyleSheet.create({
  container: {
    height: '100%',
    position: 'absolute',
    top: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 0,
    paddingRight: 5,
  },
  buttonPosition: {
    top: 0,
    right: 0,
    marginRight: 5,
    width: 38,
    height: 38,
    borderColor: '#eee',
    borderWidth: 1.5,
    borderRadius: 4,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#666',
    shadowOpacity: 0.9,
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 3,
  },
  textShadow: {
    color: '#222',
    fontSize: 20,
  },
  menuPage: {
    top: 43.8,
    width: WINDOW_WIDTH * 0.9,
    height: WINDOW_HEIGHT * 0.9,
    backgroundColor: 'transparent',
  },
  menuBack: {
    position: 'absolute',
    opacity: 1,
    width: '100%',
    height: '100%',
    shadowColor: '#444',
    shadowOffset: { width: 8, height: 9 },
    shadowRadius: 8,
    shadowOpacity: 1,
    zIndex: 0,
  },
  textContainer: {
    paddingTop: '23%',
    paddingRight: 15,
    height: '75%',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
  },
  text: {
    fontFamily: 'Syncopate',
    color: '#fff',
    fontSize: WINDOW_HEIGHT * 0.025,
    textShadowOffset: { width: 0, height: 0 },
    textShadowColor: '#fff',
    textShadowRadius: 3,
    shadowColor: '#fff',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    shadowRadius: 3,
  },
});

export default Menu;
