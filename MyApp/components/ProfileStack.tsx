import React from 'react';
// import Menu from '../components/Menu';
import { createStackNavigator } from '@react-navigation/stack';
import ProjectScreen from '../pages/Profile/Projects/ProjectScreen';
import profileScreen from '../pages/Profile/ProfileScreen';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import ProfileArtworkScreen from '../pages/Profile/ProfileArtworkScreen';
import RequestScreen from '../pages/Search/Projects/RequestScreen';
import SettingScreen from '../pages/Setting/SettingScreen';
import PublishedJobListScreen from '../pages/Search/Projects/PublishedJobListScreen';
import SearchTagScreen from '../pages/Search/Person/SearchTagScreen';
import ProjectListScreen from '../pages/Profile/Projects/ProjectListScreen';
import ApplicantListScreen from '../pages/PublishedJobs/ApplicantList';
import PublishedJobRelatedProfileScreen from '../pages/PublishedJobs/PublishedJobRelatedProfile';
import PublishedJobRelatedProfile_ProjectPreviewScreen from '../pages/PublishedJobs/PublishedJobRelatedProfile_RequestPreview';
import PublishedJobRelatedProfile_ProjectListPreviewScreen from '../pages/PublishedJobs/PublishedJobRelatedProfile_ProjectListPreview';
import InvitedStack from './InvitedStack';
import ProfileVideoScreen from '../pages/Profile/ProfileVideoScreen';
import PublishedJobProfileScreen from '../pages/Search/Projects/PublishedJobProfile';
import PublisherPublishedJobList from '../pages/Profile/PublisherPublishedJobList';
import PublishedJobPreviewScreen from '../pages/Preview/PublishedJobPreviewScreen';


const ProfileStack: React.FC = () => {
  const Stack = createStackNavigator();

  return (
    <>
      <NavigationContainer independent={true}>
        <Stack.Navigator initialRouteName="ProfileScreen">
          <Stack.Screen
            name="Profile"
            component={profileScreen}
            options={{
              //@ts-ignore
              headerLeft: null,
              gestureEnabled: false,
              headerShown: true,
            }}></Stack.Screen>
          <Stack.Screen
            name="Artwork"
            component={ProfileArtworkScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="Video"
            component={ProfileVideoScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="ProjectList"
            component={ProjectListScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name='Request'
            component={RequestScreen}
          />
          <Stack.Screen
            name='Publisher'
            component={PublishedJobProfileScreen}
          />
          <Stack.Screen
            name='Publisher Published Job List'
            component={PublisherPublishedJobList}
          />
          <Stack.Screen
            name="Published Job Preview"
            component={PublishedJobPreviewScreen}
            options={{
              headerShown: true,
            }}
          />

          <Stack.Screen
            name="Published Job List"
            component={PublishedJobListScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name='Posted Job'
            component={ProjectScreen}
          />

          <Stack.Screen
            name="Setting"
            component={SettingScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen name='Search' component={SearchTagScreen} />
          <Stack.Screen name='ApplicantList' component={ApplicantListScreen} />
          <Stack.Screen name='InvitedList' component={InvitedStack} />
          <Stack.Screen name='User Profile' component={PublishedJobRelatedProfileScreen} />
          <Stack.Screen name='PublishedJobList Preview' component={PublishedJobRelatedProfile_ProjectListPreviewScreen} />
          <Stack.Screen name='Request Preview' component={PublishedJobRelatedProfile_ProjectPreviewScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default ProfileStack;
