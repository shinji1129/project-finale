import React from 'react';
import { StyleSheet, SafeAreaView, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const SettingScreenLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#ccc3', '#fff']}
      useAngle={true}
      angleCenter={{ x: 0.9, y: 0.5 }}
      angle={353}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    opacity: 0.85,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: 7,
  },
  container: {
    paddingHorizontal: 15,
    paddingVertical: 30,
  },
});
export default SettingScreenLinearGradient;
