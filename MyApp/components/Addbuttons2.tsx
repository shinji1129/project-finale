import React, { useEffect, useState, Text } from 'react';
import {
  View,
  StyleSheet,
  TouchableHighlight,
  Animated,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { closeAction, openAction } from '../redux/animation/action';

const BUTTON_WIDTH = Dimensions.get('window').height * 0.09;
const NAV_BAR_HEIGHT = Math.round(Dimensions.get('window').height * 0.08);




function AddButtons() {


  let mode = new Animated.Value(0);
  let buttonSize = new Animated.Value(1);
  const ballValue = useState(new Animated.ValueXY({ x: 0, y: 0 }))[0];
  const ball2Value = useState(new Animated.ValueXY({ x: 0, y: 0 }))[0];
  const isOpenState = useSelector((state: IRootState) => state.animation.isOpen)
  const dispatch = useDispatch()
  const navigation = useNavigation();

  var closeBall = () => {
    if (isOpenState === true) {
      Animated.spring(ball2Value, {
        toValue: { x: 0, y: 0 },
        // duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.spring(ballValue, {
        toValue: { x: 0, y: 0 },
        // duration: 300,
        useNativeDriver: false,
      }).start();
      dispatch(closeAction());
    }
  }

  useEffect(()=>{
    console.log('isOpenState:',isOpenState)
  },[isOpenState])

  function moveBall() {
    if (isOpenState === false) {
      Animated.spring(ball2Value, {
        toValue: { x: -BUTTON_WIDTH * 1.1, y: -BUTTON_WIDTH * 0.7 },
        useNativeDriver: false,
      }).start();
      Animated.spring(ballValue, {
        toValue: { x: BUTTON_WIDTH, y: -BUTTON_WIDTH * 0.7 },
        useNativeDriver: false,
      }).start();
      dispatch(openAction());
    } else if (isOpenState === true) {
      closeBall();
    }

  }

  const handlePress = () => {
    Animated.sequence([
      Animated.timing(buttonSize, {
        toValue: 0.9,
        duration: 100,
        useNativeDriver: false,
      }),
      Animated.timing(buttonSize, {
        toValue: 1,
        useNativeDriver: false,
      }),
      Animated.timing(mode, {
        toValue: 0 ? 1 : 0,
        useNativeDriver: false,
      }),
    ]).start();
  };

  const sizeStyle = {
    transform: [{ scale: buttonSize }],
  };

  return (

    <>
      {isOpenState && <TouchableOpacity style={{height:"100%", width:"100%", position:'absolute'}} onPress={()=>{dispatch(closeAction());moveBall()}}></TouchableOpacity>}
      <View style={styles.mainContainer}>
        <View style={styles.searchButtons}>
          <Animated.View style={ball2Value.getLayout()}>
            <TouchableOpacity
              style={styles.touchArea1}
              onPress={() => {
                moveBall();
                navigation.navigate('Home_Tab');
              }}
            >
              <View style={styles.secondaryButton}>
                <IconMaterialIcons
                  style={styles.textShadow2}
                  selectionColor="transparent"
                  name="work-outline"
                />
              </View>
            </TouchableOpacity>
          </Animated.View>
          <Animated.View style={ballValue.getLayout()}>
            <TouchableOpacity
              style={styles.touchArea2}
              onPress={() => {
                moveBall();
                navigation.navigate('SearchFreelancer_Tab');
              }}>
              <View style={styles.secondaryButton}>
                <IconIonicons
                  style={styles.textShadow2}
                  name="people-outline"
                />
              </View>
            </TouchableOpacity>
          </Animated.View>


          <Animated.View style={[styles.button, sizeStyle]}>
            <TouchableHighlight
              onPress={() => {
                moveBall();
                handlePress();
              }}
              style={{
                justifyContent: 'center',
                alignItems: 'center',

                width: 60,
                height: 60,
                borderRadius: 30,
              }}
              // underlayColor="#71C6B1">
              underlayColor="#4EC9B0">
              <Animated.View>
                {/* <TouchableWithoutFeedback onPress={closeBall}> */}
                <IconAntDesign style={styles.textShadow} name="search1" />
                {/* </TouchableWithoutFeedback> */}
              </Animated.View>
            </TouchableHighlight>
          </Animated.View>
        </View>
      </View>
    </>
  );
}


const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    left: Dimensions.get('window').width * 0.505 - BUTTON_WIDTH / 2,
    bottom: NAV_BAR_HEIGHT - BUTTON_WIDTH / 1.4, //SE
  },
  searchButtons: {
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    paddingBottom: 3,
    alignItems: 'center',
    justifyContent: 'center',
    width: BUTTON_WIDTH,
    height: BUTTON_WIDTH,
    borderRadius: BUTTON_WIDTH / 2,
    backgroundColor: '#58f6ff',
    shadowColor: '#fff',
    shadowRadius: 5,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.6,
    borderWidth: BUTTON_WIDTH * 0.085,
    borderColor: '#fff',
    zIndex: 1,
  },
  touchArea1: {
    position: 'absolute',
    top: BUTTON_WIDTH * 0.07,
    left: -BUTTON_WIDTH * 0.4,
    width: BUTTON_WIDTH * 0.8,
    height: BUTTON_WIDTH * 0.8,
    borderRadius: BUTTON_WIDTH * 0.4,
    justifyContent: 'center',
    alignItems: 'center'

  },
  touchArea2: {
    position: 'absolute',
    top: BUTTON_WIDTH * 0.07,
    left: -BUTTON_WIDTH * 0.4,
    width: BUTTON_WIDTH * 0.8,
    height: BUTTON_WIDTH * 0.8,
    borderRadius: BUTTON_WIDTH * 0.4,
    justifyContent: 'center',
    alignItems: 'center'

  },
  secondaryButton: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: BUTTON_WIDTH * 0.7,
    height: BUTTON_WIDTH * 0.7,
    borderRadius: (BUTTON_WIDTH * 0.7) / 2,
    backgroundColor: '#58f6ff',
    shadowColor: '#222',
    shadowRadius: BUTTON_WIDTH * 0.7 * 0.12,
    shadowOffset: { width: 0, height: BUTTON_WIDTH * 0.7 * 0.1 },
    shadowOpacity: 0.6,
    borderWidth: BUTTON_WIDTH * 0.7 * 0.085,
    borderColor: '#eee',
    zIndex: 1,
  },
  textShadow2: {
    color: '#555',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    fontSize: BUTTON_WIDTH * 0.42,
  },
  textShadow: {
    color: '#555',
    fontWeight: '100',
    textShadowColor: '#555',
    textShadowOffset: { width: 0, height: 3 },
    textShadowRadius: 4,
    fontSize: BUTTON_WIDTH * 0.5,
  },
});

export default AddButtons;
