import React from 'react';
import {View, Animated, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

function TestColorAnimation() {
  const colorValue = new Animated.Value(0);
  function handlePress() {
    Animated.timing(colorValue, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: false,
    }).start();
  }
  const changeColor = colorValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['#FFF', '#000'],
  });
  const finalColor = {
    backgroundColor: changeColor,
  };
  return (
    <View style={styles.container}>
      <Animated.View style={[styles.box, finalColor]}></Animated.View>
      <TouchableOpacity onPress={handlePress}>
        <Text style={styles.Text}>Click Me</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 200,
    height: 200,
    backgroundColor: 'black',
  },
  Text: {
    fontSize: 20,
  },
});

export default TestColorAnimation;
