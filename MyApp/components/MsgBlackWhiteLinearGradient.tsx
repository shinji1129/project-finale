import { BaseNavigationContainer } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, SafeAreaView, Text, View, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


const MsgBlackWhiteLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#bbb', '#FFF']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.4 }}
      angle={353}
      style={styles.gradient}>
      <View style={styles.container}>{children}</View>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    opacity: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: 6,
  },
  container: {
    width: '100%',
  },
});
export default MsgBlackWhiteLinearGradient;
