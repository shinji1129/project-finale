import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import InvitedListScreen from '../pages/PublishedJobs/InvitedList';
import React from 'react';
const Tab = createMaterialTopTabNavigator();
import InvSearchStack from '../components/InvitedSearchStack';

const InvitedStack: React.FC = ({ route }: any) => {
    const { invitedList, requestID } = route.params;
    return (
            <Tab.Navigator initialRouteName='Invited' >
                <Tab.Screen name="InvitedList" children={() => <InvitedListScreen invitedList={invitedList} requestID={requestID} />} />
                <Tab.Screen name="Search" children={() => <InvSearchStack invitedList={invitedList} requestID={requestID} />} />
            </Tab.Navigator>
    );
}

export default InvitedStack;