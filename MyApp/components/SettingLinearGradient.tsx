import React from 'react';
import { StyleSheet, SafeAreaView, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const SettingLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#3a707a', '#4b818b', '#599cac', '#84CFD5']}
      useAngle={true}
      angle={25}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    opacity: 0.99,
  },
  container: {
    marginHorizontal: 15,
  },
});
export default SettingLinearGradient;
