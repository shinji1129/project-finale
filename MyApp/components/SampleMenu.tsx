import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Animated,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import LogoLinearGradient from './LogoLinearGradient';

const MenuTest: React.FC = () => {
  let menuPosition = useState(new Animated.ValueXY({x: -500, y: 0}))[0];
  let isOpen = false;

  function setMenuOpen() {
    if (isOpen === false) {
      Animated.timing(menuPosition, {
        toValue: {x: 0, y: 0},
        duration: 400,
        useNativeDriver: false,
      }).start();
      isOpen = true;
    } else {
      Animated.timing(menuPosition, {
        toValue: {x: -500, y: 0},
        duration: 400,
        useNativeDriver: false,
      }).start();
      isOpen = false;
    }
  }

  return (
    <View style={styles.container}>
      <Animated.View style={[menuPosition.getLayout(), styles.menuPage]}>
        {/* <Image
          source={require('./menuBackground.png')}
          style={styles.imageBack}
        /> */}

        <View style={styles.menuBack}>
          <View style={styles.textContainer}>
            <TouchableOpacity
              onPress={() => {
                console.log('profile');
              }}>
              <Text style={styles.text}>profile</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.text}>edit profile</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.text}>project</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.text}>posted job</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.text}>Received offer</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Animated.View>

      <TouchableOpacity
        onPress={() => {
          setMenuOpen();
        }}
        style={styles.buttonPosition}>
        <IconSimpleLineIcons style={styles.textShadow} name="menu" size={25} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: -10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  buttonPosition: {
    position: 'absolute',
    top: 0,
    right: 0,
    marginRight: 10,
    width: 40,
    height: 40,
    borderColor: '#888',
    borderWidth: 1.5,
    borderRadius: 4,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#666',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 5,
  },
  textShadow: {
    textShadowColor: '#fff',
    textShadowOffset: {width: 0, height: 2.5},
    textShadowRadius: 4,
  },
  menuPage: {
    top: 50,
    width: '80%',
    height: 800,
    backgroundColor: 'transparent',
    shadowColor: '#000',
    shadowOffset: {width: 8, height: 7},
    shadowRadius: 8,
    shadowOpacity: 1,
  },
  imageBack: {
    position: 'absolute',
    opacity: 0.7,
    width: '100%',
  },
  menuBack: {
    position: 'absolute',
    opacity: 1,
    width: '100%',
    height: 800,
    backgroundColor: 'black',
  },
  textContainer: {
    paddingTop: 70,
    paddingRight: 15,
    height: '70%',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
  },
  text: {
    fontFamily: 'Syncopate',
    color: '#fff',
    fontSize: 22,
    textShadowOffset: {width: 0, height: 0},
    textShadowColor: '#fff',
    textShadowRadius: 3,
    shadowColor: '#aaa',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.8,
    shadowRadius: 3,
  },
});

export default MenuTest;
