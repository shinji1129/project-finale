import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


const BlackWhiteLinearGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#bbb', '#FFF']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.4 }}
      angle={353}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    opacity: 0.85,
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: 6,
  },
  container: {
    width: '100%',
  },
});
export default BlackWhiteLinearGradient;
