import React, {useState} from 'react';
import { StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import IconAntDesign from 'react-native-vector-icons/AntDesign';

function CollapseList() {
  const [close, setClose] = useState(true);

  let containerHeight = close ? 60 : 'auto';

  let iconColor = close ? '#555' : '#34e5eb';
  let iconBackground = close ? 'transparent' : 'black';
  let shadowing = close
    ? {shadowColor: 'transparent'}
    : {
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 4,
      };
  const rotateZ = close ? '0deg' : '180deg';

  const handlePress = () => {
    setClose((prev) => !prev);
  };

  return (
    <View
      style={{
        height: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}>
      <View style={[styles.divContainer, {height: containerHeight}]}>
        <View style={styles.topLayerContainer}>
          <Text style={styles.title}>Title</Text>
          <TouchableOpacity
            style={[
              styles.touchButton,
              {backgroundColor: iconBackground},
              shadowing,
            ]}
            onPress={() => {
              handlePress();
            }}>
            <IconAntDesign
              style={[
                styles.icon,
                {color: iconColor},
                {transform: [{rotateZ}]},
              ]}
              name="downcircle"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.lowerLayerContainer}>
          <View>
            <Text style={{fontFamily: 'Varela Round', fontSize: 18}}>
              Jobs description
            </Text>
            <Text style={{fontFamily: 'Varela Round', fontSize: 18}}>
              $10000
            </Text>
            <Text style={{fontFamily: 'Varela Round', fontSize: 18}}>
              To be completed in: 3 days
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  divContainer: {
    width: '95%',
    height: '30%',
    backgroundColor: 'white',
    borderRadius: 20,
    borderColor: '#444',
    borderWidth: 1,
    padding: 15,
    overflow: 'hidden',
    flexWrap: 'nowrap',
  },
  topLayerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
    marginBottom: 15,
    flexWrap: 'nowrap',
  },
  lowerLayerContainer: {
    overflow: 'hidden',
  },
  title: {
    fontFamily: 'syncopate',
    fontSize: 20,
  },
  icon: {
    fontSize: 30,
  },

  touchButton: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderRadius: 15,
    marginTop: -8,
  },
});

export default CollapseList;
