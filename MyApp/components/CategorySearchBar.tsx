import React, { useState, useEffect } from 'react'
import { useNavigation } from '@react-navigation/native';
import { config } from '../config';
import { SearchBar, Text } from 'react-native-elements';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { StyleSheet, View, Dimensions } from 'react-native';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';

const WINDOW_WIDTH = Dimensions.get('window').width;

interface Tag {
    id: number,
    tag: string
}

interface CategoryProps {
    categoryID: number | null
}

const CategorySearchBar: React.FC<CategoryProps> = (props) => {
    // Search bar
    const [query, setQuery] = useState('');
    const [data, setData] = useState([]);
    const [tags, setTags] = useState([]);

    const navigation = useNavigation();
    const token = useSelector((state: IRootState) => state.auth.token);

    const updateQuery = async (input: string) => {
        setTags(data.slice());
        setQuery(input);
    }

    const filterTags = (tag: Tag) => {
        let search = query.toLowerCase()
        if (tag['tag'].startsWith(search)) {
            return formatTags(tag);
        } else {
            //@ts-ignore
            tags.splice(tags.indexOf(tag), 1);
            return null;
        }
    }

    const formatTags = (tag: Tag) => {
        let tagName = tag['tag'].charAt(0).toUpperCase() + tag['tag'].slice(1);
        return tagName;
    }

    const fetchTag = async () => {
        try {
            const res = await fetch(`${config.BACKEND_URL}/request/${props.categoryID}/tags`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            });
            ;
            const json = await res.json();
            setData(json);
            setTags(json.slice());
        } catch (err) {
            console.log(err);
        }
    };

    const renderItem = ({ item }: any) => {
        if (filterTags(item) !== null) {
            return (
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Search', {
                        tag: item.tag,
                        tag_id: item.id,
                        category_id: props.categoryID
                    })

                }}>
                    <View style={styles.searchTagContainer}>
                        <Text style={styles.flatList}>{filterTags(item)}</Text>
                    </View>
                </TouchableOpacity>
            )
        } else {
            return <Text style={{ display: 'none' }}>{null}</Text>
        }
    }

    useEffect(() => {
        fetchTag();
    }, [])


    return (
        <>
            <View>
                <SearchBar
                    platform="ios"
                    onChangeText={updateQuery}
                    value={query}
                    placeholder="Type Here..."
                />
                {query !== '' &&
                    <FlatList style={{ height: Dimensions.get('window').height }} data={tags} keyExtractor={(item: any) => item.tag.toString()}
                        extraData={query}
                        renderItem={renderItem}
                    />

                }
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    searchTagContainer: {
        alignSelf: 'center',
        width: WINDOW_WIDTH * 0.8,
        backgroundColor: '#333c',
        margin: 4,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    flatList: {
        textAlign: 'center',
        paddingVertical: 10,
        fontSize: 16,
        fontFamily: 'Syncopate',
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
});

export default CategorySearchBar;