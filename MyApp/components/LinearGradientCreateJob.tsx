import React from 'react';
import { StyleSheet, SafeAreaView, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const LinearGradientCreateJob: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#488f', '#8CD1']}
      useAngle={true}
      angle={340}
      style={styles.gradient}>
      <SafeAreaView>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    opacity: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default LinearGradientCreateJob;
