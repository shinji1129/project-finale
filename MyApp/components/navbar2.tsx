import React, { Component, useEffect } from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import AddButtons from './Addbuttons2';
import { useDispatch, useSelector } from 'react-redux';
import {
  useNavigation, TabActions,

} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { logoutThunk } from '../redux/auth/thunks';

import NewProjectScreen from '../pages/CreateNewFreelanceJob';

import RequestStack from './RequestStack';
import ProfileStack from './ProfileStack';

import FreelancerStack from './FreelancerStack';
import SettingStack from './SettingStack';
import LoadingBall from './LoadingBall';
import { IRootState } from '../redux/store';
import BookmarkStack from './BookmarkStack';
import chatRoomList from '../pages/ChatRoom/chatRoomListScene'
import ChatRoomStack from './ChatRoomStack';

// If change NAV_BAR_HEIGHT, pls also need to amend in Addbuttons2.tsx
const NAV_BAR_HEIGHT = Math.round(Dimensions.get('window').height * 0.08);
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

function Navbar2() {
  const dispatch = useDispatch();
  const Tab = createBottomTabNavigator();

  const onLogout = () => {
    dispatch(logoutThunk());
  };

  const isProcessing_artwork = useSelector((state: IRootState) => state.artwork.isProcessing)
  const isProcessing_video = useSelector((state: IRootState) => state.video.isProcessing)
  const isProcessing_Profile = useSelector((state: IRootState) => state.profile.isProcessing)
  const isProcessing_Tags = useSelector((state: IRootState) => state.tags.isProcessing)
  const applicationProcessing = useSelector((state: IRootState) => state.applicant.isProcessing)
  const invitationProcessing = useSelector((state: IRootState) => state.invitation.isProcessing);
  const confirmationProcessing = useSelector((state: IRootState) => state.confirmation.isProcessing)
  const commentProcessing = useSelector((state: IRootState) => state.comment.isProcessing)
  const applicantProcessing = useSelector((state: IRootState) => state.project.isProcessing)
  const newProjectProcessing = useSelector((state: IRootState) => state.newRequest.isProcessing)


  return (
    <>
      {(isProcessing_Profile || isProcessing_Tags || isProcessing_artwork || isProcessing_video ||
        applicationProcessing || invitationProcessing || confirmationProcessing || commentProcessing
        || applicantProcessing || newProjectProcessing) &&
        <View style={{ height: WINDOW_HEIGHT, width: WINDOW_WIDTH, justifyContent: 'center', opacity: 0.9, position: 'absolute', zIndex: 2, backgroundColor: 'white' }}>
          <LoadingBall />
        </View>}
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarButton: [
            "SearchFreelancer_Tab"
          ].includes(route.name) ? () => { return null; } : undefined,

          tabBarIcon: ({ focused, color, size }) => {
            let iconName: any;
            if (route.name === 'Profile_Tab') {
              iconName = focused ? 'person' : 'person-outline';
              return <Ionicons name={iconName} style={styles.icons} size={size} color={color} />;
            } else if (route.name === 'Home_Tab') {
              iconName = focused ? 'search' : 'search-outline';
              return <Ionicons name={iconName} style={styles.icons} size={1} color={'transparent'} />;
            } else if (route.name === 'NewProject_Tab') {
              if (focused === true) {
                iconName = 'note-add'
                return <MaterialIcons name={iconName} style={styles.icons} size={size} color={color} />;
              } else {
                iconName = 'addfile'
                return <IconAntDesign name={iconName} style={styles.icons} size={size} color={color} />;
              }
            } else if (route.name === 'ChatBox_Tab') {
              iconName = focused ? 'comment' : 'comment-o';
              return <FontAwesome name={iconName} style={styles.icons} size={size} color={color} />;
            } else if (route.name === 'Setting_Tab') {
              iconName = focused ? 'settings' : 'settings-outline';
              return <Ionicons name={iconName} style={styles.icons} size={size} color={color} />;
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: '#58f6ff',
          inactiveTintColor: 'gray',
          showLabel: false,

          style: {
            shadowOpacity: 0.9,
            shadowColor: '#AAA9',
            shadowOffset: { width: 0, height: -3 },
            shadowRadius: 4,
            backgroundColor: '#fff',
            height: NAV_BAR_HEIGHT,
          },
        }} initialRouteName="Profile_Tab">
        <Tab.Screen name="Profile_Tab" component={ProfileStack} options={{ unmountOnBlur: true, }} />
        <Tab.Screen name="ChatBox_Tab" component={ChatRoomStack} options={{ unmountOnBlur: true }} />
        <Tab.Screen name="Home_Tab" component={RequestStack} options={{ unmountOnBlur: true }} />
        <Tab.Screen name="NewProject_Tab" component={NewProjectScreen} options={{ unmountOnBlur: true }} />
        <Tab.Screen name="Setting_Tab" component={SettingStack} options={{ unmountOnBlur: true }} />
        <Tab.Screen name="SearchFreelancer_Tab" component={FreelancerStack} options={{ unmountOnBlur: true }} />
      </Tab.Navigator>
      <AddButtons />



    </>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    bottom: 0,
  },
  container: {
    backgroundColor: '#eee',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    height: 75,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    paddingBottom: 5,
    paddingHorizontal: 8,
  },
  buttonSize: {
    height: '100%',
    width: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchContainer: {
    position: 'absolute',
    bottom: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  textShadow: {
    textShadowColor: '#666',
    textShadowOffset: { width: 0, height: 2.5 },
    textShadowRadius: 4,
  },
  icons: {
    textShadowColor: '#666B',
    textShadowOffset: { width: 0, height: 2 },
    textShadowRadius: 5
  }
});

export default Navbar2;
