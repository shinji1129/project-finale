import React, { useState, useEffect } from 'react';
import { View, Text, Modal, TouchableOpacity, Alert, StyleSheet, Dimensions, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import { config } from '../config';
import { useNavigation } from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';

export interface IRatingProps {
    requestId: number;
    ratingOn: 'freelancer' | 'requester'
}

interface Rating {
    requests_id: number,
    ratingOnFreelancer?: number,
    requesterId?: number,
    ratingOnRequester?: number,
    freelancerId?: number,
    requesterName?: string,
    freelancerName?: string
}

const WINDOW_WIDTH = Dimensions.get('window').width;
const FONT_SIZE = 15;

const RatingInComment: React.FC<IRatingProps> = (props) => {
    const token = useSelector((state: IRootState) => state.auth.token);
    const userId = useSelector((state: IRootState) => state.auth.user?.id);

    const navigation = useNavigation();
    const [rating, setRating] = useState<Rating>();

    useEffect(() => {
        const fetchRatingByRequest = async (requestId: number) => {
            const res = await fetch(`${config.BACKEND_URL}/request/rating/${requestId}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                }
            });
            const rating = await res.json();
            setRating(rating);
        }

        try {
            fetchRatingByRequest(props.requestId);

        } catch (err) {
            console.error(err.message);
        }
    }, []);

    useEffect(() => {
        const didMount = navigation.addListener('focus', () => {
            const fetchRatingByRequest = async (requestId: number) => {
                const res = await fetch(`${config.BACKEND_URL}/request/rating/${requestId}`, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: 'Bearer ' + token,
                    }
                });
                const rating = await res.json();
                setRating(rating);
            };

            try {
                fetchRatingByRequest(props.requestId);
            } catch (err) {
                console.error(err.message);
            }
        });

        return didMount;
    }, [navigation])

    const iconForLoop = (rating: number, iconName: string, color: string, style: any) => {
        const iconArr = [];
        for (let i = 0; i < rating; i++) {
            iconArr.push(
                <AntDesign
                    key={`iconForLoop_${i+1}`}
                    style={style}
                    name={iconName}
                    size={24}
                    color={color}>
                </AntDesign>);
        }
        return iconArr;
    }

    return (
        <View style={styles.iconContainer}>
            {props.ratingOn === 'requester' &&
                <>
                    <Text style={styles.ratingText}>Satisfaction: </Text>
                    {rating?.ratingOnFreelancer && iconForLoop(rating?.ratingOnFreelancer, 'star', "#ffff00", {
                        textShadowColor: '#666',
                        textShadowRadius: 3,
                    })}
                    {rating?.ratingOnFreelancer && iconForLoop(5 - rating?.ratingOnFreelancer, 'staro', "#000000", {
                        color: '#aaa'
                    })}
                </>}
            {props.ratingOn === 'freelancer' &&
                <>
                    <Text style={styles.ratingText}>Satisfaction: </Text>
                    {rating?.ratingOnRequester && iconForLoop(rating?.ratingOnRequester, 'star', "#ffff00", {
                        textShadowColor: '#666',
                        textShadowRadius: 3,
                    })}
                    {rating?.ratingOnRequester && iconForLoop(5 - rating?.ratingOnRequester, 'staro', "#000000", {
                        color: '#aaa'
                    })}
                </>}

        </View>)
}

export default RatingInComment;

const styles = StyleSheet.create({
    mainContainer: {
        width: WINDOW_WIDTH * 0.95,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 3,
    },
    commentSection: {

        width: WINDOW_WIDTH * 0.95,
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
    },
    title: {
        marginTop: 30,
        paddingLeft: 15,
        fontFamily: 'syncopate',
        fontSize: FONT_SIZE * 1.5
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: 15
    },
    aboutCommenter: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '35%',
        height: 'auto',
    },
    username: {
        marginTop: 3,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.2
    },
    profilePicBox: {
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: { width: 2, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 4,
    },
    profilePic: {
        borderRadius: WINDOW_WIDTH * 0.175,
        width: WINDOW_WIDTH * 0.15,
        height: WINDOW_WIDTH * 0.15,
        overflow: 'hidden',
    },
    textContainer: {
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'varela round',
        fontSize: FONT_SIZE
    },
    commentBox: {
        borderColor: '#aaa',
        borderWidth: 1,
        borderRadius: 6,
        width: WINDOW_WIDTH * 0.57,
        minHeight: WINDOW_WIDTH * 0.2,
        padding: 5,
        margin: 3

    },
    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
    },
    emptyStarIcon: {
        color: '#eee'
    },
    iconContainer: {
        flexDirection: 'row'
    },
    ratingText: {
        marginBottom: 3,
        paddingLeft: 7,
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.1
    },
})

