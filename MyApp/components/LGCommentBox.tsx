import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


const LGCommentBox: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#aaae', '#FFF']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.4 }}
      angle={354}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    borderRadius: 6,
  },
  container: {
    flexDirection: 'row',
  },
});
export default LGCommentBox;
