import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, ScrollView, StyleSheet, Dimensions, TextInput, Alert, Image, Modal, Animated } from 'react-native';
import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import { getBookmark, setBookmark } from '../redux/request/bookmark/thunk';
import { currentApplication, setApplication, cancelApplication } from '../redux/request/application/thunk';
import { currentInvitation, setInvitation } from '../redux/request/invitation/thunk';
import { config } from '../config';
import BlackWhiteLinearGradient from '../components/BlackWhiteLinearGradient';
import InvitationGradient from '../components/InvitationGradient';
import { resetBookmarkSuccess } from '../redux/request/bookmark/actions';
import { resetApplicationSuccess } from '../redux/request/application/actions';
import { resetInvitationSuccess } from '../redux/request/invitation/action';
import { useNavigation } from '@react-navigation/native';
import { currentStatus, setConfirmationStatus } from '../redux/request/confirmation/thunk';
import { Artworks } from '../redux/profile/artwork/state'
import { ImageViewer } from 'react-native-image-zoom-viewer';
import { Controller, useForm } from 'react-hook-form';
import { addCommentThunk, isCommentThunk } from '../redux/request/comment/thunk';
import CommentBox from '../components/commentbox';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { addRatingThunk } from '../redux/request/rating/thunk';
import { resetConfirmationSuccess } from '../redux/request/confirmation/action';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'


const FONT_SIZE = Dimensions.get('window').width * 0.038;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const BUTTON_WIDTH = Dimensions.get('window').width * 0.3;
const BUTTON_HEIGHT = FONT_SIZE * 2.5;


const RequestScreenWithLink: React.FC = ({ route }: any) => {

    const dispatch = useDispatch();
    const token = useSelector((state: IRootState) => state.auth.token);
    // Bookmark
    const bookmark = useSelector((state: IRootState) => state.bookmark.bookmark);

    // Application
    const application = useSelector((state: IRootState) => state.application.status)


    // Invitation
    const invitationStatus = useSelector((state: IRootState) => state.invitation.status)
    const invite_budget = useSelector((state: IRootState) => state.invitation.invite_budget);

    // Confirmation
    const confirmationStatus = useSelector((state: IRootState) => state.confirmation.status)
    const userId = useSelector((state: IRootState) => state.auth.user?.id)

    // RequestImage
    const [requestImages, setRequestImages] = useState<Artworks[]>([])
    const [modalVisible, setModalVisible] = useState(false);
    const [indexOfImage, setIndexOfImage] = useState(0);

    const [isCommented, setIsCommented] = useState(false);
    const isCommentBefore = useSelector((state: IRootState) => state.comment.isComment)
    const isBothComment = useSelector((state: IRootState) => state.comment.isBothComment)

    //Rating
    const [rating, setRating] = useState(0);



    //Comment form 
    const { handleSubmit, register, errors, control } = useForm();
    const onSubmit = (values: Record<string, any>) => {
        if (!token) { return }
        if (rating === 0) {
            return Alert.alert(
                "Rating",
                "Rating is required",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        return (
            dispatch(
                addCommentThunk(
                    token,
                    request_id,
                    values.comment
                ),
            ),
            setIsCommented(true),
            dispatch(
                addRatingThunk(
                    token,
                    request_id,
                    rating
                )
            )
        );
    };

    const onCancel = () => {
        return console.log('cancel is pressed');
    };

    let images: Array<{ url: string, props: any }> = [];
    requestImages.map((requestImage, idx) => {
        images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
    })

    useEffect(() => {
        let images: Array<{ url: string, props: any }> = [];
        requestImages.map((requestImage, idx) => {
            images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`, props: {} })
        })
    }, [requestImages])

    const navigation = useNavigation()

    const [expectBudget, setExpectBudget] = useState('');

    const { request_id, requester, title, tag, time_start, duration, venue, budget_min, budget_max, confirmed_budget, expiry_date, status, tag_id } = route.params;

    const fetchRequestImages = async (request_id: number) => {
        const res = await fetch(`${config.BACKEND_URL}/request/images/${request_id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + token,
            }
        })
        const requestImgObj = await res.json();
        setRequestImages(requestImgObj);
    }

    const statusConverter = (status: number) => {
        switch (status) {
            case 1:
                return ""
            case 2:
                return "Pending"
            case 3:
                return "Accepted"
            case 4:
                return "Confirmation"
            case 5:
                return "Completed"
            case 6:
                return "Commented"
            default:
                return ""
        }
    }

    const BookmarkClickHandler = async () => {
        if (token === null) {
            return;
        }
        dispatch(setBookmark(token, request_id))
    }


    const ApplyButtonHandler = async () => {
        if (isNaN(parseInt(expectBudget))) {
            if (expectBudget === '') {
                Alert.alert("Invalid input",
                    "Please input number",
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                );
                setExpectBudget('')
                return
            }
            Alert.alert("Invalid input",
                "Please input number only",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
            setExpectBudget('')
            return
        } else {
            if (token) {
                dispatch(setApplication(token, request_id, parseInt(expectBudget)))
                setExpectBudget('')
            }
        }
    }

    const CancelButtonHandler = async () => {
        if (token) {
            dispatch(cancelApplication(token, request_id))
        }
    }

    const AcceptInvitationHandler = async () => {
        if (token) {
            dispatch(setInvitation(token, request_id, "accept", invite_budget))
        }
    }

    const DeclineInvitationHandler = async () => {
        if (token) {
            dispatch(setInvitation(token, request_id, "decline"))
        }
    }

    const AcceptConfirmation = async () => {
        if (token) {
            dispatch(setConfirmationStatus(token, request_id, "accept"))
        }
    }

    const DeclineConfirmation = async () => {
        if (token) {
            dispatch(setConfirmationStatus(token, request_id, "decline"))
        }
    }

    // get bookmark
    useEffect(() => {
        if (token === null) {
            return;
        }

        if (userId) {
            dispatch(isCommentThunk(token, userId, request_id))
        }
        dispatch(getBookmark(token, request_id))
        fetchRequestImages(request_id)
        return (() => {
            dispatch(resetBookmarkSuccess());
        })
    }, []);

    useEffect(() => {
        const loadData = navigation.addListener('focus', () => {
            if (token === null) {
                return;
            }
            dispatch(currentApplication(token, request_id))
            dispatch(currentInvitation(token, request_id))
            dispatch(currentStatus(token, request_id))
        })

        return loadData;
    }, [navigation])

    useEffect(() => {
        const removeData = navigation.addListener('blur', () => {
            dispatch(resetInvitationSuccess());
            dispatch(resetApplicationSuccess());
            dispatch(resetConfirmationSuccess());
        })

        return removeData;
    }, [navigation])
    return (
        <SafeAreaView >
            <BlackWhiteLinearGradient>
                <KeyboardAwareScrollView
                    extraHeight={100}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    scrollEnabled={true}
                    enableResetScrollToCoords={false}
                    style={{ height: 'auto', width: WINDOW_WIDTH }}
                    contentContainerStyle={{
                        maxWidth: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingBottom: 30
                    }}>
                    <View style={styles.container}>
                        <View style={{ paddingLeft: 10 }}>
                            {confirmationStatus && confirmationStatus <= 2 && invitationStatus === 1 && (
                                <>
                                    <View style={styles.invitation}>
                                        <InvitationGradient>
                                            <View >
                                                <Text style={styles.text}>{requester.charAt(0).toUpperCase() + requester.slice(1)} would like to assign you to this job with budget: $ {invite_budget}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-evenly', alignItems: 'center', marginTop: 10, }}>
                                                <TouchableOpacity style={styles.decline} onPress={DeclineInvitationHandler}>
                                                    <Text style={styles.buttonText}> Decline </Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.accept} onPress={AcceptInvitationHandler}>
                                                    <Text style={styles.buttonText}> Accept </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </InvitationGradient>
                                    </View>
                                </>
                            )}
                            {confirmationStatus === 4 && (
                                <>
                                    <View style={styles.invitation}>
                                        <InvitationGradient>
                                            <View >
                                                <Text style={styles.text}>{requester.charAt(0).toUpperCase() + requester.slice(1)} would like you to confirm the job is completed.</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-evenly', alignItems: 'center', marginTop: 10, }}>
                                                <TouchableOpacity style={styles.decline} onPress={DeclineConfirmation}>
                                                    <Text style={styles.buttonText}> Reject </Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.accept} onPress={AcceptConfirmation}>
                                                    <Text style={styles.buttonText}> Confirm </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </InvitationGradient>
                                    </View>
                                </>
                            )}
                            {confirmationStatus === 5 && isCommented === false && isCommentBefore === false && isBothComment === false && (
                                <>
                                    <View style={styles.invitation}>
                                        <InvitationGradient>
                                            <View>
                                                <Text style={styles.text}>Comment:</Text>
                                                <Controller
                                                    control={control}
                                                    render={({ onChange, onBlur, value }) => (
                                                        <TextInput
                                                            autoCapitalize="none"
                                                            autoCorrect={false}
                                                            style={styles.input}
                                                            onBlur={onBlur}
                                                            onChangeText={(value) => onChange(value)}
                                                            value={value}
                                                        />
                                                    )}
                                                    name="comment"
                                                    rules={{ required: true }}
                                                    defaultValue=""
                                                />
                                                {errors.comment && (
                                                    <Text style={styles.errorMessage}>This field is required.</Text>
                                                )}
                                                <Text style={styles.text}>Rating:</Text>
                                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                    {rating === 0 ?
                                                        <TouchableOpacity onPress={() => setRating(1)}>
                                                            <AntDesign
                                                                style={styles.emptyStarIcon}
                                                                name={"staro"}
                                                                size={24}
                                                                color={"#000000"}>
                                                            </AntDesign>
                                                        </TouchableOpacity> :
                                                        <TouchableOpacity onPress={() => setRating(1)}>
                                                            <AntDesign
                                                                style={styles.ratingIcon}
                                                                name={"star"}
                                                                size={24}
                                                                color={"#ffff00"}>
                                                            </AntDesign>
                                                        </TouchableOpacity>
                                                    }
                                                    {rating <= 1 ?
                                                        <TouchableOpacity onPress={() => setRating(2)}>
                                                            <AntDesign
                                                                style={styles.emptyStarIcon}
                                                                name={"staro"}
                                                                size={24}
                                                                color={"#000000"}>
                                                            </AntDesign>
                                                        </TouchableOpacity> :
                                                        <TouchableOpacity onPress={() => setRating(2)}>
                                                            <AntDesign
                                                                style={styles.ratingIcon}
                                                                name={"star"}
                                                                size={24}
                                                                color={"#ffff00"}>
                                                            </AntDesign>
                                                        </TouchableOpacity>
                                                    }
                                                    {rating <= 2 ?
                                                        <TouchableOpacity onPress={() => setRating(3)}>
                                                            <AntDesign
                                                                style={styles.emptyStarIcon}
                                                                name={"staro"}
                                                                size={24}
                                                                color={"#000000"}>
                                                            </AntDesign>
                                                        </TouchableOpacity> :
                                                        <TouchableOpacity onPress={() => setRating(3)}>
                                                            <AntDesign
                                                                style={styles.ratingIcon}
                                                                name={"star"}
                                                                size={24}
                                                                color={"#ffff00"}>
                                                            </AntDesign>
                                                        </TouchableOpacity>
                                                    }
                                                    {rating <= 3 ?
                                                        <TouchableOpacity onPress={() => setRating(4)}>
                                                            <AntDesign
                                                                style={styles.emptyStarIcon}
                                                                name={"staro"}
                                                                size={24}
                                                                color={"#000000"}>
                                                            </AntDesign>
                                                        </TouchableOpacity> :
                                                        <TouchableOpacity onPress={() => setRating(4)}>
                                                            <AntDesign
                                                                style={styles.ratingIcon}
                                                                name={"star"}
                                                                size={24}
                                                                color={"#ffff00"}>
                                                            </AntDesign>
                                                        </TouchableOpacity>
                                                    }
                                                    {rating <= 4 ?
                                                        <TouchableOpacity onPress={() => setRating(5)}>
                                                            <AntDesign
                                                                style={styles.emptyStarIcon}
                                                                name={"staro"}
                                                                size={24}
                                                                color={"#000000"}>
                                                            </AntDesign>
                                                        </TouchableOpacity> :
                                                        <TouchableOpacity onPress={() => setRating(5)}>
                                                            <AntDesign
                                                                style={styles.ratingIcon}
                                                                name={"star"}
                                                                size={24}
                                                                color={"#ffff00"}>
                                                            </AntDesign>
                                                        </TouchableOpacity>
                                                    }
                                                </View>
                                                <View style={styles.rows}>
                                                    <TouchableOpacity style={styles.cancel} onPress={onCancel}>
                                                        <Text style={styles.buttonText}>Cancel</Text>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                        style={styles.submit}
                                                        onPress={handleSubmit(onSubmit)}>
                                                        <Text style={styles.buttonText}>Confirm</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </InvitationGradient>
                                    </View>
                                </>
                            )}


                            <View style={{ marginTop: 8, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={styles.titleText}>{title}</Text>

                                {bookmark === true && (
                                    <TouchableOpacity style={styles.unbookmark} onPress={BookmarkClickHandler}>
                                        <IconMaterialCommunityIcons style={styles.bookmarkIcon} name="bookmark-off-outline" />
                                    </TouchableOpacity>
                                )}
                                {bookmark === false && (
                                    <TouchableOpacity style={styles.bookmark} onPress={BookmarkClickHandler}>
                                        <IconMaterialCommunityIcons style={styles.bookmarkIcon} name="bookmark-outline" />
                                    </TouchableOpacity>
                                )}

                            </View>
                            {/* {requester.length >= 1 && <Text style={styles.text}>Job posted by: <Text style={styles.requester}>{requester.charAt(0).toUpperCase() + requester.slice(1)}</Text> </Text>} */}

                            {requester.length >= 1 && <View style={{flexDirection:'row'}}>
                                    <Text style={styles.text}>Job posted by:</Text>
                                    <TouchableOpacity onPress={()=>{
                                        // navigation.navigate('Publisher');
                                        navigation.navigate('Publisher', {
                                            user_id: requester
                                        });
                                    }}>
                                        <Text style={styles.requester}>{requester.charAt(0).toUpperCase() + requester.slice(1)}</Text> 
                                    </TouchableOpacity>
                                </View>}

                            <Text style={{ fontFamily: 'Varela Round', paddingLeft: 5, marginTop: 8 }}>Require:</Text>
                            {tag && tag.length >= 1 && <View style={styles.tagBox}>{tag.map((string: string, idx: number) => {
                                return (
                                    <TouchableOpacity key={`tag_${idx}`} style={styles.tag} onPress={() => {
                                        navigation.navigate('Search', {
                                            tag: tag,
                                            tag_id: tag_id[idx]
                                        })
                                        console.log('Pressing ' + tag)
                                    }}>
                                        <Text style={styles.tagText} key={`tag_${idx}`}>{string.charAt(0).toUpperCase() + string.slice(1)}</Text>
                                    </TouchableOpacity>
                                )
                            })}
                            </View>
                            }
                           

                        </View>

                        <View style={styles.border}></View>
                        <View style={styles.middleHalf}>
                            {(budget_min && status < 3) &&
                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.text}>Budget:</Text>
                                    <Text style={styles.moneyText}>HKD${budget_min} ~ {budget_max}</Text>
                                </View>

                            }
                            {status >= 3 && (
                                <Text style={styles.text}>Confirmed Budget: <Text style={styles.moneyText}>HKD${confirmed_budget}</Text></Text>
                            )}
                            {time_start !== new Date(0).toDateString() && time_start.length >= 1 && <Text style={styles.text}>Date: {time_start}</Text>}
                            {duration && <Text style={styles.text}>To be completed in: {duration} days </Text>}
                            {venue && <Text style={styles.text}>Venue: {venue} </Text>}

                            {expiry_date && <Text style={styles.text}>Expiry date: {expiry_date} </Text>}
                            <Text style={styles.text}>Status: {statusConverter(status)} </Text>
                            <Text style={styles.text}>Reference image: </Text>
                            <View style={{
                                marginTop: 10,
                                paddingHorizontal: 5,
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                maxHeight: 440,
                                overflow: 'scroll',
                            }}>
                                {requestImages.length >= 1 &&
                                    <>
                                        {requestImages.map((requestImage, index) => (
                                            <TouchableOpacity key={`requestImage_${index}`} onPress={async () => { await setModalVisible(true), await setIndexOfImage(index) }}>
                                                <Image
                                                    style={{
                                                        height: 100,
                                                        width: 100,
                                                        marginRight: 10,
                                                        marginBottom: 10,
                                                    }}
                                                    source={{
                                                        uri: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${requestImage.filename}`,
                                                    }}
                                                />
                                            </TouchableOpacity>
                                        ))}
                                    </>}
                            </View>
                            <Modal visible={modalVisible} transparent={true}>
                                <View style={{ backgroundColor: 'black', height: 'auto', position: 'absolute', top: 50, right: 15, zIndex: 999 }}>
                                </View>
                                <ImageViewer
                                    imageUrls={images}
                                    menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                                    onChange={(index) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                                    index={indexOfImage}
                                    enableSwipeDown={true}
                                    enablePreload={true}
                                    onSwipeDown={() => {
                                        setModalVisible(false)
                                    }} />
                            </Modal>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                            {status <= 2 && invitationStatus === 0 && application === 0 && (
                                <>
                                    <TextInput
                                        placeholder="Expected Budget"
                                        keyboardType={'numeric'}
                                        style={styles.input}
                                        onChangeText={setExpectBudget}
                                        value={expectBudget}
                                        autoCorrect={false}
                                    />
                                    <View style={styles.rows}>
                                        <TouchableOpacity style={styles.apply} onPress={ApplyButtonHandler}>
                                            <Text style={styles.buttonText}> apply </Text>
                                        </TouchableOpacity>
                                    </View>
                                </>
                            )}
                            {status <= 2 && invitationStatus === 0 && application === 1 && (
                                <>
                                    <View style={styles.rows}>
                                        <TouchableOpacity style={styles.unbookmark} onPress={CancelButtonHandler}>
                                            <Text style={styles.buttonText}> Cancel apply</Text>
                                        </TouchableOpacity>
                                    </View>
                                </>
                            )}
                        </View>

                        <View style={styles.pageEndButtons}>
                            {isBothComment === true && (
                                <View style={{ width: '100%' }}>
                                    <CommentBox requestId={request_id} />
                                </View>
                            )}
                        </View>
                    </View>
                    <View style={{ height: 20 }}></View>
                </KeyboardAwareScrollView>
            </BlackWhiteLinearGradient>
        </SafeAreaView >
    )
}

const styles = StyleSheet.create({
    container: {
        minWidth: WINDOW_WIDTH,
        maxWidth: WINDOW_WIDTH,
        minHeight: WINDOW_HEIGHT * 0.9,
        alignContent: 'center',
        flexWrap: "wrap",
        flexDirection: "column",
        padding: 5,
        paddingLeft: 10
    },
    titleText: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.45,
        textTransform: 'capitalize',
        padding: 5,
        flexWrap: 'wrap',
        width: WINDOW_WIDTH * 0.8
    },
    text: {
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE,
        borderRadius: 5,
        padding: 5,
        color: '#090909',
    },
    moneyText: {
        fontFamily: 'Varela Round',
        fontWeight: "900",
        fontSize: FONT_SIZE * 1.2,
        borderRadius: 5,
        padding: 5,
        color: '#d06',


    },
    requester: {
        color:'#3677EA',
        fontFamily: 'Varela Round',
        fontSize: FONT_SIZE * 1.40,
        textTransform: 'capitalize',
    },
    tagBox: {
        marginTop: 15,
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        shadowColor: '#222',
        shadowOpacity: 0.8,
        shadowRadius: 3,
        shadowOffset: { width: 2, height: 3 }
    },
    tag: {
        justifyContent: 'center',
        width: 'auto',
        height: 25,
        backgroundColor: '#333c',
        margin: 4,
        marginRight: 5,
        paddingHorizontal: 8,
        borderRadius: 9,
        borderTopRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4441',
        borderWidth: 1,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 3 },
        shadowOpacity: 0.22,
        shadowRadius: 1,
    },
    tagText: {
        fontFamily: 'Varela Round',
        fontSize: 13,
        color: 'cyan',
        textShadowColor: 'cyan',
        textShadowRadius: 5,
    },
    border: {
        alignSelf: 'center',
        flexDirection: 'row',
        width: WINDOW_WIDTH * 0.9,
        borderBottomColor: '#7775',
        borderBottomWidth: 1,
        paddingBottom: 18,
        justifyContent: 'flex-start',
    },
    middleHalf: {
        marginTop: 15,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        marginBottom: 15,
        paddingLeft: 10
    },
    pageEndButtons: {
        width: '100%',
        marginLeft: -3,
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rows: {
        marginTop: WINDOW_HEIGHT * 0.015,
        minWidth: WINDOW_WIDTH * 0.5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        paddingHorizontal: 5,
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 2,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 3,
        fontFamily: 'Syncopate',
        fontSize: FONT_SIZE * 0.9,
    },
    chatBox: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        width: WINDOW_WIDTH * 0.4,
        height: FONT_SIZE * 2.5,
        borderRadius: 6,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    bookmark: {
        backgroundColor: '#59Fc',
        justifyContent: 'center',
        alignItems: 'center',
        width: FONT_SIZE * 3,
        height: FONT_SIZE * 3,
        borderRadius: FONT_SIZE * 1.5,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginRight: 5,
    },
    bookmarkIcon: {
        color: '#fff',
        textShadowColor: '#eee',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 0,
        shadowColor: '#fff',
        shadowOpacity: 1,
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 1,
        fontSize: FONT_SIZE * 2,
    },
    unbookmark: {
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        width: FONT_SIZE * 3,
        height: FONT_SIZE * 3,
        borderRadius: FONT_SIZE * 1.5,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginRight: 5,
    },
    invitation: {
        maxWidth: WINDOW_WIDTH * 0.9,
        marginVertical: 15,
        borderRadius: 6,
        shadowColor: '#0009',
        shadowOffset: { width: 3, height: 7 },
        shadowOpacity: 1,
        shadowRadius: 8
    },
    accept: {
        width: '100%',
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 6,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },
    decline: {
        width: '100%',
        backgroundColor: '#BF4848',
        justifyContent: 'center',
        alignItems: 'center',
        height: FONT_SIZE * 2.5,
        borderRadius: 6,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
        marginHorizontal: 3,
    },

    input: {
        padding: 5,
        marginTop: 10,
        minHeight: FONT_SIZE + 20,
        width: WINDOW_WIDTH * 0.8,
        borderColor: '#888',
        borderRadius: 6,
        borderWidth: 1,
        fontFamily: 'Varela Round',
        paddingHorizontal: 10,
    },
    apply: {
        backgroundColor: '#5F9AFF',
        justifyContent: 'center',
        alignItems: 'center',
        minWidth: WINDOW_WIDTH * 0.4,
        height: FONT_SIZE * 2.5,
        borderRadius: 6,
        padding: 8,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 7 },
        shadowRadius: 8,
        shadowOpacity: 0.6,
    },
    errorMessage: {
        fontFamily: 'Varela Round',
        color: 'red',
    },

    submit: {
        backgroundColor: '#4EC9B0',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: Dimensions.get('window').width * 0.3,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },
    cancel: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 20,
        width: Dimensions.get('window').width * 0.3,
        height: FONT_SIZE * 2.5,
        borderRadius: 7,
        padding: 10,
        backgroundColor: '#BF4848',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 9 },
        shadowRadius: 8,
        shadowOpacity: 0.7,
    },
    ratingIcon: {
        textShadowColor: '#666',
        textShadowRadius: 3,
    },
    emptyStarIcon: {
        color: '#ccc'
    },
});

export default RequestScreenWithLink;