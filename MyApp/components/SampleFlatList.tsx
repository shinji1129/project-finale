import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

export const cardDetails = [
  {
    header: 'Poster design job',
    description:
      'Design a poster, Design a poster, Design a poster, Design a poster, Design a poster, Design a poster',
    remark: '$10000',
  },
  {
    header: 'WebUI design job',
    description:
      'Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website',
    remark: '$12000',
  },
  {
    header: 'Music composition job',
    description: 'Compose a song for commercial ad',
    remark: '$15000',
  },
  {
    header: 'Drawing job',
    description: 'Draw a picture',
    remark: '$8000',
  },
  {
    header: 'Design Logo job',
    description: 'Design a Logo',
    remark: '$10000',
  },
  {
    header: 'WebUI design job',
    description:
      'Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website',
    remark: '$12000',
  },
  {
    header: 'Poster design job',
    description:
      'Design a poster, Design a poster, Design a poster, Design a poster, Design a poster, Design a poster',
    remark: '$10000',
  },
  {
    header: 'WebUI design job',
    description:
      'Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website',
    remark: '$12000',
  },
  {
    header: 'Music composition job',
    description: 'Compose a song for commercial ad',
    remark: '$15000',
  },
  {
    header: 'Drawing job',
    description: 'Draw a picture',
    remark: '$8000',
  },
  {
    header: 'Design Logo job',
    description: 'Design a Logo',
    remark: '$10000',
  },
  {
    header: 'WebUI design job',
    description:
      'Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website, Design a website',
    remark: '$12000',
  },
];

//[Optional] add {key: i} or any other thing u need into the above objects
for (let i = 0; i < cardDetails.length; i++) {
  Object.assign(cardDetails[i], {key: i, index: i + 1});
}

function SampleFlatList() {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {cardDetails.map((cardDetail) => (
        <View style={styles.container} key={cardDetail.index}>
          {/* Optional */}
          {/* <Text>{cardDetail.index}</Text> */}
          <Text style={styles.header}>{cardDetail.header}</Text>
          <Text style={styles.description}>{cardDetail.description}</Text>
          <Text style={styles.remark}>{cardDetail.remark}</Text>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderColor: '#eee',
    borderWidth: 1,
    borderRadius: 6,
    width: '80%',
    height: 'auto',
    padding: 10,
    margin: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.3,
    shadowRadius: 5,
  },
  header: {
    fontFamily: 'syncopate',
    marginVertical: 10,
    fontSize: 15,
  },
  description: {
    fontFamily: 'varela round',
    fontSize: 15,
  },
  remark: {fontFamily: 'varela round', fontSize: 15},
});
export default SampleFlatList;
