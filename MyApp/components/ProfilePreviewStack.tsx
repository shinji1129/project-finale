import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import ProfileArtworkScreen from '../pages/Profile/ProfileArtworkScreen';
import ProfilePreviewScreen from '../pages/Preview/ProfilePreviewScreen';
import ProjectPreviewScreen from '../pages/Preview/ProjectPreviewScreen';
import ProjectListPreviewScreen from '../pages/Preview/ProjectListPreviewScreen'
import PublishedJobListPreviewScreen from '../pages/Preview/PublishedJobListPreviewScreen';
import PublishedJobPreviewScreen from '../pages/Preview/PublishedJobPreviewScreen';
import ProfileVideoScreen from '../pages/Profile/ProfileVideoScreen';

const ProfilePreviewStack: React.FC = () => {
  const Stack = createStackNavigator();


  return (
    <>
      <NavigationContainer independent={true}>
        <Stack.Navigator initialRouteName="Profile Preview">
          <Stack.Screen
            name="Profile Preview"
            component={ProfilePreviewScreen}
            options={{
              //@ts-ignore
              headerLeft: null,
              gestureEnabled: false,
              headerShown: true,
            }}></Stack.Screen>
          <Stack.Screen
            name="Artwork Preview"
            component={ProfileArtworkScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="Video Preview"
            component={ProfileVideoScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="ProjectList Preview"
            component={ProjectListPreviewScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen name='Request Preview' component={ProjectPreviewScreen} />
          <Stack.Screen
            name="Published Jobs List Preview"
            component={PublishedJobListPreviewScreen}
            options={{
              headerShown: true,
            }}
          />
          <Stack.Screen
            name="Published Job Preview"
            component={PublishedJobPreviewScreen}
            options={{
              headerShown: true,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default ProfilePreviewStack;
