import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableHighlight,
  Animated,
  TouchableOpacity,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {StackActions, useNavigation} from '@react-navigation/native';

function AddButtons() {
  let mode = new Animated.Value(0);
  let buttonSize = new Animated.Value(1);
  const ballValue = useState(new Animated.ValueXY({x: 0, y: 0}))[0];
  const ball2Value = useState(new Animated.ValueXY({x: 0, y: 0}))[0];
  let isOpen = false;

  const navigation = useNavigation();

  function moveBall() {
    if (isOpen === false) {
      Animated.timing(ball2Value, {
        toValue: {x: -80, y: -45},
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.timing(ballValue, {
        toValue: {x: 80, y: -45},
        duration: 300,
        useNativeDriver: false,
      }).start();
      isOpen = true;
    } else {
      Animated.timing(ball2Value, {
        toValue: {x: 0, y: 0},
        duration: 300,
        useNativeDriver: false,
      }).start();
      Animated.timing(ballValue, {
        toValue: {x: 0, y: 0},
        duration: 300,
        useNativeDriver: false,
      }).start();
      isOpen = false;
    }
  }

  const handlePress = () => {
    Animated.sequence([
      Animated.timing(buttonSize, {
        toValue: 0.95,
        duration: 100,
        useNativeDriver: false,
      }),
      Animated.timing(buttonSize, {
        toValue: 1,
        useNativeDriver: false,
      }),
      Animated.timing(mode, {
        toValue: 0 ? 1 : 0,
        useNativeDriver: false,
      }),
    ]).start();
  };

  const sizeStyle = {
    transform: [{scale: buttonSize}],
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.searchButtons}>
        <Animated.View style={ball2Value.getLayout()}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home_Tab');
            }}>
            <View style={styles.secondButton}>
              <IconMaterialIcons
                style={styles.textShadow2}
                name="work-outline"
                size={40}
              />
            </View>
          </TouchableOpacity>
        </Animated.View>
        <Animated.View style={ballValue.getLayout()}>
          <TouchableOpacity>
            <View style={styles.thirdButton}>
              <IconIonicons
                style={styles.textShadow2}
                name="people-outline"
                size={40}
              />
            </View>
          </TouchableOpacity>
        </Animated.View>

        <Animated.View style={[styles.button, sizeStyle]}>
          <TouchableHighlight
            onPress={() => {
              moveBall();
              handlePress();
            }}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: 60,
              height: 60,
              borderRadius: 30,
            }}
            underlayColor="#71C6B1">
            <Animated.View>
              <IconAntDesign
                style={styles.textShadow}
                name="search1"
                size={40}
              />
            </Animated.View>
          </TouchableHighlight>
        </Animated.View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    left: '42.5%',
    bottom: 35,
  },
  searchButtons: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 72,
    height: 72,
    borderRadius: 36,
    backgroundColor: '#34e5eb',
    shadowColor: '#fff',
    shadowRadius: 7,
    shadowOffset: {width: 0, height: 5},
    shadowOpacity: 0.6,
    borderWidth: 5,
    borderColor: '#fff',
    zIndex: 1,
  },
  secondButton: {
    position: 'absolute',
    top: 5,
    left: -30,
    alignItems: 'center',
    justifyContent: 'center',
    width: 58,
    height: 58,
    borderRadius: 29,
    backgroundColor: '#34e5eb',
    // backgroundColor: '#6FA5AE',
    shadowColor: '#222',
    shadowRadius: 5,
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.6,
    borderWidth: 3,
    borderColor: '#eee',
    zIndex: 1,
  },
  thirdButton: {
    position: 'absolute',
    top: 5,
    left: -30,
    alignItems: 'center',
    justifyContent: 'center',
    width: 58,
    height: 58,
    borderRadius: 29,
    backgroundColor: '#34e5eb',
    shadowColor: '#222',
    shadowRadius: 5,
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.6,
    borderWidth: 3,
    borderColor: '#eee',
    zIndex: 1,
  },
  textShadow2: {
    fontWeight: '100',
  },
  textShadow: {
    fontWeight: '100',
    textShadowColor: '#555',
    textShadowOffset: {width: 0, height: 3},
    textShadowRadius: 4,
  },
});

export default AddButtons;
