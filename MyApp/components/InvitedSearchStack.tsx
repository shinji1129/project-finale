import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import InvitationSearchScreen from '../pages/PublishedJobs/InvitationSearchScreen';
import InvitationSearchResult from '../pages/PublishedJobs/InvitationSearchResultScreen';
import PublishedJobRelatedProfileScreen from '../pages/PublishedJobs/PublishedJobRelatedProfile';
import ProfileArtworkScreen from '../pages/Profile/ProfileArtworkScreen';
import PublishedJobRelatedProfile_ProjectListPreviewScreen from '../pages/PublishedJobs/PublishedJobRelatedProfile_ProjectListPreview';
import PublishedJobRelatedProfile_ProjectPreviewScreen from '../pages/PublishedJobs/PublishedJobRelatedProfile_RequestPreview';
import ProfileVideoScreen from '../pages/Profile/ProfileVideoScreen';


interface IInvitedListProp {
    invitedList: number[],
    requestID: number
}

const InvitedSearchStack = createStackNavigator();

const InvSearchStack: React.FC<IInvitedListProp> = (props) => {
    const { invitedList, requestID } = props
    return (
        <>
            <NavigationContainer independent={true}>
                <InvitedSearchStack.Navigator initialRouteName="InvitedSearch">
                    <InvitedSearchStack.Screen name='InvitedSearch' children={() => <InvitationSearchScreen invitedList={invitedList} requestID={requestID} />} />
                    <InvitedSearchStack.Screen name='InvitedSearchResult' component={InvitationSearchResult} />
                    <InvitedSearchStack.Screen name='User Profile' component={PublishedJobRelatedProfileScreen} />
                    <InvitedSearchStack.Screen name="Artwork" component={ProfileArtworkScreen} options={
                        {
                            headerShown: false,
                        }
                    }
                    />
                    <InvitedSearchStack.Screen name="Video" component={ProfileVideoScreen} options={
                        {
                            headerShown: false,
                        }
                    }
                    />
                    <InvitedSearchStack.Screen name='RequestList Preview' component={PublishedJobRelatedProfile_ProjectListPreviewScreen} />
                    <InvitedSearchStack.Screen name='Request Preview' component={PublishedJobRelatedProfile_ProjectPreviewScreen} />
                </InvitedSearchStack.Navigator>
            </NavigationContainer>
        </>
    );
};

export default InvSearchStack;