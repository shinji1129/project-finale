import React, { useState, useEffect } from 'react';
import { View, Text, Modal, TouchableOpacity, Alert } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { deleteArtwork } from '../redux/profile/artwork/thunk';

const IImageViewer: React.FC = () => {
    const [indexOfImage, setIndexOfImage] = useState(0)
    const [modalVisible, setModalVisible] = useState(false);
    const token = useSelector((state: IRootState) => state.auth.token);
    const dispatch = useDispatch();

    const artworks = useSelector((state: IRootState) => state.artwork.artworks);
    let images: Array<{ url: string, props: any }> = [];
    artworks.map((artwork, idx) => {
        images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
    })

    useEffect(() => {
        images = [];
        artworks.map((artwork, idx) => {
            images.push({ url: `http://jobscreative.s3-website-ap-southeast-1.amazonaws.com/${artwork.filename}`, props: {} })
        })
    }, [artworks])

    return (<View>
        <Modal visible={modalVisible} transparent={true}>
            <View style={{ backgroundColor: '#000', height: 'auto', position: 'absolute', top: 50, right: 15, zIndex: 999 }}>
                <TouchableOpacity onPress={() => {
                    Alert.alert(
                        'Delete Artwork',
                        'Are you sure to delete this artwork?',
                        [
                            {
                                text: 'Yes',
                                onPress: () => {
                                        token ? dispatch(deleteArtwork(token, artworks[indexOfImage].filename)) : '';
                                    setModalVisible(false)
                                }
                            },
                            {
                                text: 'Cancel',
                                onPress: () => {},
                                style: 'cancel'
                            },
                        ],
                        { cancelable: false }
                    );
                }}>
                    <Ionicons name="trash-outline" color="white" size={40} />
                </TouchableOpacity>
            </View>
            <ImageViewer
                imageUrls={images}
                menuContext={{ saveToLocal: 'back', cancel: 'cancel' }}
                onChange={(index: number | undefined) => index === undefined ? setIndexOfImage(0) : setIndexOfImage(index)}
                index={indexOfImage}
                renderFooter={(currentIndex: number) => {
                    return (
                        <View>
                            <Text style={{ color: 'white', position: 'absolute', fontSize: 20, bottom: 150, left: 15 }}>
                                {artworks[currentIndex].description}
                            </Text>
                        </View>
                    )
                }}
                enableSwipeDown={true}
                enablePreload={true}
                onSwipeDown={() => {
                    setModalVisible(false)
                }} />
        </Modal>
    </View>)
}

export default IImageViewer;