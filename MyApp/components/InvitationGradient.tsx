import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const InvitationGradient: React.FC = ({ children }) => {
  return (
    <LinearGradient
      colors={['#5c929c', '#96E1E7']}
      useAngle={true}
      angleCenter={{ x: 0.8, y: 0.5 }}
      angle={353}
      style={styles.gradient}>
      <SafeAreaView style={styles.container}>{children}</SafeAreaView>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  gradient: {
    opacity: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    borderRadius: 6,
    marginBottom: 10,
  },
  container: {
    width: '100%',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
});
export default InvitationGradient;
