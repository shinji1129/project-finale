import express from 'express';
import { userRoutes } from './routes/userRoutes';
import { requestRoutes } from './routes/requestRoutes';
import { profileRoutes } from './routes/profileRoutes';
import { projectRoutes } from './routes/projectRoutes';
import { chatRoomRoutes } from './routes/chatRoomRoutes';


export const routes = express.Router();
routes.use('/user', userRoutes)
routes.use('/request', requestRoutes)
routes.use('/profile', profileRoutes)
routes.use('/project',projectRoutes)
routes.use('/chatroom',chatRoomRoutes) 