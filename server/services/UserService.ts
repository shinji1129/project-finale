import Knex from 'knex';
import { IUser } from './models';

export class UserService {
    constructor(private knex: Knex) { }

    getUserByUsername = async (username: string) => {
        const user = await this.knex<IUser>('users').where('username', username).first();
        return user;
    }

    getUserById = async (id: number) => {
        const user = await this.knex<IUser>('users').where('id', id).first();
        return user;
    }

    getUserByFacebookId = async (id: number) => {
        const user = await this.knex('users').select('*').where('facebook_id', id).first()
        return user;
    }

    getUserByEmail = async (email: string) => {
        const user = await this.knex('users').select('*').where('email', email).first();
        return user;
    }

    createFacebookUser = async (username: string, facebookId: string, email: string) => {
        const id = await this.knex('users').insert({ username: username, facebook_id: facebookId, email: email }).returning('id')
        return id;
    }
    checkUserExist = async (username: string) => {
        return (await this.knex("users").select('username').where('username', username)).length !== 0
    }

    checkEmailExist = async (email: string) => {
        return (await this.knex("users").select('email').where('email', '=', email)).length !== 0
    }

    register = async (username: string, hashedPassword: string, profile_pic: string, email: string, introduction: string) => {
        return await this.knex("users").insert({ username: username, password: hashedPassword, profile_pic: profile_pic, email: email, self_intro: introduction })
    }

    getFreelancer = async (id: number, page: number) => {
        const users = await this.knex('user_tag').join('users', 'users.id', 'user_tag.users_id').distinct('users.id', 'users.username', 'users.profile_pic').whereNot('users.id', id).orderBy('users.id').limit(10).offset(page * 10 - 10)

        for (let user of users) {
            const tags = await this.knex('user_tag').join('tags', 'user_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('user_tag.users_id', user.id);
            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            user['tag'] = tagsArr
            user['tag_id'] = tagsIDArr
        }
        return users;
    }

    getFreelancerByTag = async (id: number, tagID: number, page: number) => {
        const users = await this.knex('user_tag').join('users', 'users.id', 'user_tag.users_id').distinct('users.id', 'users.username', 'users.profile_pic').where('user_tag.tags_id', tagID).andWhereNot('users.id', id).orderBy('users.id').limit(10).offset(page * 10 - 10)

        for (let user of users) {
            const tags = await this.knex('user_tag').join('tags', 'user_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('user_tag.users_id', user.id);
            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            user['tag'] = tagsArr
            user['tag_id'] = tagsIDArr
        }
        return users;
    }

    getApplicantById = async (id: number) => {
        
        const user = await this.knex('users').select('users.id', 'users.username', 'users.profile_pic').where('users.id', id).first()
        const tags = await this.knex('user_tag').join('tags', 'user_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('user_tag.users_id', id);
        const tagsArr: string[] = []
        const tagsIDArr: string[] = []

        for (let tag of tags) {
            tagsArr.push(tag['tag'])
            tagsIDArr.push(tag['id'])
        }

        user['tag'] = tagsArr
        user['tag_id'] = tagsIDArr

        return user;
    }
    
    getInvitedUserById = async (id: number) => {
        
        const user = await this.knex('users').select('users.id', 'users.username', 'users.profile_pic').where('users.id', id).first()
        const tags = await this.knex('user_tag').join('tags', 'user_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('user_tag.users_id', id);
        const tagsArr: string[] = []
        const tagsIDArr: string[] = []

        for (let tag of tags) {
            tagsArr.push(tag['tag'])
            tagsIDArr.push(tag['id'])
        }

        user['tag'] = tagsArr
        user['tag_id'] = tagsIDArr

        return user;
    }

}