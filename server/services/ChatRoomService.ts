import Knex from 'knex';

export class ChatRoomService {
    constructor(private knex: Knex) { }


    getMessageById = async (userId: number) => {
        const messages = await this.knex('chat_box').select('*').where('user_one', userId).orWhere('user_two',userId).orderBy('id','asc')
        return messages;
    }


    getUsernameById = async (userId: number) => {
        try {
            const username = await this.knex('users').select('username').where('id', userId).first();
            return username;
        } catch (err) {
            console.log(err)
            return;
        }
    }
    getProfilePicById = async (userId:number)=>{
        try{
            const profilePic = await this.knex('users').select('profile_pic').where('id', userId).first();
            return profilePic;
        }catch(err){
            console.log(err);
            return
        }
    }
}
