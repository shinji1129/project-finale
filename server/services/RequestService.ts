import Knex from 'knex';


export class RequestService {
    constructor(private knex: Knex) { }

    getTags = async () => {
        const tags = await this.knex('tags').select('*');
        return tags;
    }

    getTagsByCategory = async (userID: number, categoryID: number) => {
        const tags = await this.knex('tags').distinct('tags.id', 'tags.tag').join('request_tag', 'tags.id', 'request_tag.tags_id').join('requests', 'request_tag.requests_id', 'requests.id').join('categories', 'requests.category_id', 'categories.id').where('categories.id', categoryID).andWhereNot('requests.requester_id', userID);
        return tags;
    }

    getCategories = async () => {
        const categories = await this.knex('categories').select('*')
        return categories;
    }

    getRequestsByCategory = async (userID: number, categoryID: number) => {
        const requests = await this.knex('requests').join('categories', 'requests.category_id', 'categories.id').select('requests.id as request_id', '*').where('requests.category_id', categoryID).andWhere('requests.status', '<=', '2').andWhere('requests.expiry_date', '>=', this.knex.raw('CURRENT_DATE')).andWhereNot('requester_id', userID)
            .orWhere('requests.category_id', categoryID).andWhere('requests.freelancer_id', userID).andWhere('status', '<=', 4)

        for (let i = 0; i < requests.length; i++) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('request_tag.requests_id', requests[i].request_id);
            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            requests[i]['tag'] = tagsArr
            requests[i]['tag_id'] = tagsIDArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userID).andWhere('requests_id', requests[i].request_id);
            bookmark.length >= 1 ? requests[i]['bookmark'] = true : requests[i]['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', requests[i].requester_id).first();
            requests[i]['requester'] = requesterName.username
        }

        return requests;
    }

    getBookmarkedRequests = async (userID: number) => {
        const requests = await this.knex('requests')
            .join('bookmark', 'requests.id', 'bookmark.requests_id')
            .select('requests.id as request_id', '*')
            .where('bookmark.users_id', userID)
            .andWhere('requests.status', '<=', '2')
            .andWhere('requests.expiry_date', '>=', this.knex.raw('CURRENT_DATE'))
            .andWhereNot('requester_id', userID)
            .orWhere('bookmark.users_id', userID)
            .andWhere('requests.freelancer_id', userID)
            .andWhere('status', '<=', 4)

        for (let i = 0; i < requests.length; i++) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('request_tag.requests_id', requests[i].request_id);

            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            requests[i]['tag'] = tagsArr
            requests[i]['tag_id'] = tagsIDArr

            // select n+1 query
            const bookmark = await this.knex('bookmark').select('*').where('users_id', userID).andWhere('requests_id', requests[i].request_id);
            // bookmark.length >= 1 ? requests[i]['bookmark'] = true : requests[i]['bookmark'] = false;
            requests[i]['bookmark'] = bookmark.length >= 1 ? true : false;

            const requesterName = await this.knex('users').select('username').where('id', requests[i].requester_id).first();
            requests[i]['requester'] = requesterName.username
        }
        return requests;

    }

    getRequestsByTag = async (userID: number, tagID: number) => {
        const requests = await this.knex('tags').join('request_tag', 'request_tag.tags_id', 'tags.id').join('requests', 'request_tag.requests_id', 'requests.id').select('requests.id as request_id', '*').where('tags.id', tagID).andWhere('requests.status', '<=', '2').andWhereNot('requester_id', userID)
            .orWhere('tags.id', tagID).andWhere('requests.freelancer_id', userID).andWhere('status', '<=', 4);

        for (let request of requests) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('request_tag.requests_id', request.request_id);
            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }
            request['tag'] = tagsArr
            request['tag_id'] = tagsIDArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userID).andWhere('requests_id', request.request_id);
            bookmark.length >= 1 ? request['bookmark'] = true : request['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', request.requester_id).first();
            request['requester'] = requesterName.username
        }
        return requests;
    }

    getRequestsByTagAndCategory = async (userID: number, tagID: number, categoryID: number) => {
        const requests = await this.knex('tags').join('request_tag', 'request_tag.tags_id', 'tags.id').join('requests', 'request_tag.requests_id', 'requests.id').select('requests.id as request_id', '*').where('tags.id', tagID).andWhere('requests.status', '<=', '2').andWhere('requests.category_id', categoryID).andWhereNot('requester_id', userID)
            .orWhere('tags.id', tagID).andWhere('requests.freelancer_id', userID).andWhere('status', '<=', 4).andWhere('requests.category_id', categoryID);

        for (let request of requests) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag', 'tags.id').where('request_tag.requests_id', request.request_id);
            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            request['tag'] = tagsArr
            request['tag_id'] = tagsIDArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userID).andWhere('requests_id', request.request_id);
            bookmark.length >= 1 ? request['bookmark'] = true : request['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', request.requester_id).first();
            request['requester'] = requesterName.username
        }

        return requests;
    }

    getRequestTag = async (requestID: number) => {
        const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag').where('request_tag.requests_id', requestID);
        return tags;
    }


    getBookmark = async (userID: number, requestID: number) => {
        const bookmark = await this.knex('bookmark').select('*').where('users_id', userID).andWhere('requests_id', requestID);
        return bookmark;
    }

    postBookmark = async (userID: number, requestID: number) => {
        const bookmark = await this.knex('bookmark').select('*').where('users_id', userID).andWhere('requests_id', requestID).first();
        if (bookmark) {
            await this.knex('bookmark').del().where('id', bookmark.id)
            return
        } else {
            await this.knex('bookmark').insert({ users_id: userID, requests_id: requestID });
            return
        }
    }

    getApplication = async (userID: number, requestID: number) => {
        const application = await this.knex('request_application').select('*').where('freelancer_id', userID).andWhere('requests_id', requestID).first();
        return application;
    }

    postApplication = async (userID: number, requestID: number, apply_budget: number) => {
        const checkApplication = await this.knex('request_application').select('*').where('freelancer_id', userID).andWhere('requests_id', requestID);
        if (checkApplication.length >= 1) {
            const status = await this.knex('request_application').update({ status: 1, apply_budget: apply_budget }).where('freelancer_id', userID).andWhere('requests_id', requestID).returning('status');
            return status[0];

        } else {
            const status = await this.knex('request_application').insert({ requests_id: requestID, freelancer_id: userID, apply_budget: apply_budget, status: 1 }).returning('status');
            return status[0];
        }

    }

    cancelApplication = async (userID: number, requestID: number) => {
        const application = await this.knex('request_application').update({ status: 0, apply_budget: null }).where('freelancer_id', userID).andWhere('requests_id', requestID).returning('status');
        return application[0];
    }

    getInvitation = async (userID: number, requestID: number) => {
        const application = await this.knex('request_invitation').select('*').where('freelancer_id', userID).andWhere('requests_id', requestID).first();
        return application;
    }

    postInvitation = async (userID: number, requestID: number, respond: boolean, confirmed_budget?: number) => {
        if (respond === true && confirmed_budget) {
            await this.knex('request_invitation').update({ status: 2 }).where('freelancer_id', userID).andWhere('requests_id', requestID).returning('id');
            await this.knex('request_invitation').update({ status: -1 }).whereNot('freelancer_id', userID).andWhere('requests_id', requestID).returning('id');
            const requestId = await this.knex('requests').update({ status: 3, freelancer_id: userID, confirmed_budget: confirmed_budget }).where('id', requestID)
            return requestId;
        } else {
            const requestId = await this.knex('request_invitation').update({ status: 0 }).where('freelancer_id', userID).andWhere('requests_id', requestID).returning('requests_id');
            return requestId;
        }
    }

    getConfirmationStatus = async (userID: number, requestID: number) => {
        const confirmationStatus = await this.knex('requests').select('status').where('id', requestID).first();
        return confirmationStatus;
    }

    postConfirmationStatus = async (userID: number, requestID: number, respond: boolean) => {
        if (respond === true) {
            const requestId = await this.knex('requests').update({ status: 5 }).where('id', requestID).andWhere('freelancer_id', userID).returning('id')
            return requestId;
        } else {
            const requestId = await this.knex('requests').update({ status: 3 }).where('id', requestID).andWhere('freelancer_id', userID).returning('id')
            return requestId;
        }
    }

    postRequest = async (requester_id: number, title: string, category_id: number, duration: number, expiry_date: string, details: string) => {
        const id = await this.knex('requests').insert({ 'title': title, 'category_id': category_id, 'requester_id': requester_id, 'duration': duration, 'expiry_date': expiry_date, 'status': 1, 'details': details }).returning('id')
        return id[0];
    }

    addTags = async (id: number, tags: []) => {
        for (let tag of tags) {
            await this.knex('request_tag').insert({ 'requests_id': id, 'tags_id': tag })
        }
        return id;
    }


    addImages = async (requestId: number, image: string, originalname: string, mimetype: string) => {
        try {
            await this.knex('request_image').insert([{ request_id: requestId, filename: image, original_name: originalname, extension: mimetype }]).where('id', requestId)
        } catch (err) {
            console.log(err);
        }
    }

    getImages = async (requestId: number) => {
        const images = await this.knex('request_image').select('*').where('request_id', requestId);
        return images;
    }

    addCompany_name = async (id: number, company_name = '') => {
        const RequestId = await this.knex('requests').update({ 'company_name': company_name }).where('id', id).returning('id')
        return RequestId;
    }

    addBudget = async (id: number, budget_min = 0, budget_max = 0) => {
        const RequestId = await this.knex('requests').update({ 'budget_min': budget_min, 'budget_max': budget_max }).where('id', id).returning('id')
        return RequestId;
    }

    addVenue = async (id: number, venue = '') => {
        const RequestId = await this.knex('requests').update({ 'venue': venue }).where('id', id).returning('id');
        return RequestId;
    }

    addTimeStart = async (id: number, time_start = '') => {
        const RequestId = await this.knex('requests').update({ time_start: time_start }).where('id', id).returning('id')
        return RequestId;
    }

    getComments = async (requestId: number) => {
        const comments = await this.knex('users').join('comment', 'users.id', 'comment.users_id').select('comment.id', 'comment', 'requests_id', 'requester_id', 'users_id', 'username', 'profile_pic', 'comment.created_at', 'comment.updated_at').join('requests', 'requests.id', 'comment.requests_id').where('requests_id', requestId)
        return comments;
    }

    addComments = async (userId: number, requestId: number, comment: string) => {
        try {
            await this.knex('comment').insert({ users_id: userId, requests_id: requestId, comment: comment })
        } catch (err) {
            console.log(err);
        }
    }

    getRating = async (requestId: number) => {
        const rating = await this.knex('rating_on_freelancer').join('rating_on_requester', 'rating_on_freelancer.requests_id', 'rating_on_requester.requests_id').select('rating_on_freelancer.requests_id', 'rating_on_freelancer.rating as ratingOnFreelancer', 'rating_on_freelancer.requester_id as requesterId', 'rating_on_requester.rating as ratingOnRequester', 'rating_on_requester.freelancer_id as freelancerId').where('rating_on_freelancer.requests_id', requestId).first();
        return rating;
    }

    postRatingOnRequester = async (requestId: number, freelancerId: number, rating: number) => {
        await this.knex('rating_on_requester').insert({ requests_id: requestId, freelancer_id: freelancerId, rating: rating })
    }

    postRatingOnFreelancer = async (requestId: number, requesterId: number, rating: number) => {
        await this.knex('rating_on_freelancer').insert({ requests_id: requestId, requester_id: requesterId, rating: rating })
    }

    getUsernameById = async (userId: number) => {
        try {
            const username = await this.knex('users').select('username').where('id', userId).first();
            return username;
        } catch (err) {
            console.log(err)
            return;
        }
    }

    getUserIdByRequestId = async (requestId: number) => {
        try {
            const userIds = await this.knex('requests').select('requester_id', 'freelancer_id').where('id', requestId).first();
            return userIds;
        } catch (err) {
            console.log(err)
            return;
        }
    }
}