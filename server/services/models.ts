export interface IUser {
    id: number;
    username: string;
    password: string;
    self_intro?: string;
    email: string;
    freelancer?: boolean;
    requester?: boolean;
    created_at: string;
    updated_at: string;
}


declare global {
    namespace Express {
        interface Request {
            user?: {
                id: number;
                username: string;
                // password: string;
                self_intro?: string;
                email: string;
                freelancer?: boolean;
                requester?: boolean;
                created_at: string;
                updated_at: string;
            }
        }
    }
}

export interface MulterFile {
    key: string
    path:string
    mimi:string
}

export interface Message {
    id: number
}