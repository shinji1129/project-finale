import Knex from 'knex';

export class ProjectService {
    constructor(private knex: Knex) { }

    getProjectStatus = async (requestID: number) => {
        const status = await this.knex('requests').select('*').where('id', requestID).first();
        return status;
    }

    getApplicants = async (requestID: number) => {
        const applicants = await this.knex('request_application').select('freelancer_id', 'apply_budget', 'status').where('requests_id', requestID).andWhere('status', 1)
        return applicants;
    }

    getInviteList = async (requestID: number) => {
        const getInviteList = await this.knex('request_invitation').select('freelancer_id', 'invite_budget', 'status').where('requests_id', requestID).andWhere('status', 1)
        return getInviteList;
    }

    getInvitedUserById = async (id: number) => {
        const user = await this.knex('users').select('users.id', 'users.username', 'users.profile_pic').where('users.id', id).first()

        const tags = await this.knex('user_tag').join('tags', 'user_tag.tags_id', 'tags.id').select('tag', 'tags.id as id').where('user_tag.users_id', id);
        const tagsArr: string[] = []
        const tagsIDArr: string[] = []

        for (let tag of tags) {
            tagsArr.push(tag['tag'])
            tagsIDArr.push(tag['id'])
        }

        user['tag'] = tagsArr
        user['tag_id'] = tagsIDArr

        return user;
    }

    postApplicant = async (userID: number, requestID: number, respond: boolean, apply_budget: number) => {
        if (respond === true && apply_budget) {
            await this.knex('request_application').update({ status: 2 }).where('freelancer_id', userID).andWhere('requests_id', requestID).returning('id');
            await this.knex('request_application').update({ status: -1 }).whereNot('freelancer_id', userID).andWhere('requests_id', requestID).returning('id');
            const requestId = await this.knex('requests').update({ status: 3, freelancer_id: userID, confirmed_budget: apply_budget }).where('id', requestID)
            return requestId;
        } else {
            const requestId = await this.knex('request_application').update({ status: 0 }).where('freelancer_id', userID).andWhere('requests_id', requestID).returning('requests_id');
            return requestId;
        }
    }

    postInvited = async (requestID: number, invite_budget: number, freelancer_id: number) => {
        const checkInvited = await this.knex('request_invitation').select('*').where('freelancer_id', freelancer_id).andWhere('requests_id', requestID)
        if (checkInvited[0]) {
            const status = await this.knex('request_invitation').update({ status: 1, invite_budget: invite_budget }).where('freelancer_id', freelancer_id).andWhere('requests_id', requestID).returning('status')
            return status;
        }

        const status = await this.knex('request_invitation').insert({ requests_id: requestID, freelancer_id: freelancer_id, invite_budget: invite_budget, status: 1 }).returning('status');
        return status[0];
    }

    cancelInvited = async (freelancer_id: number, requestID: number) => {
        const invited = await this.knex('request_invitation').update({ status: 0, invite_budget: null }).where('freelancer_id', freelancer_id).andWhere('requests_id', requestID).andWhere('status', 1).returning('id')
        return invited[0];
    }

    postConfirmationStatus = async (requester_id: number, requestID: number) => {
        const requestId = await this.knex('requests').update({ status: 4 }).where('id', requestID).andWhere('requester_id', requester_id).andWhere('status', 3).returning('*')
        return requestId;
    }
}