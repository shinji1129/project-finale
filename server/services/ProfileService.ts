import Knex from 'knex';

export class ProfileService {
    constructor(private knex: Knex) { }

    getProfile = async (userId: number) => {
        const profileInfo = await this.knex('users').select('*').where('id', userId).first()
        return profileInfo;
    }

    getTags = async (userId: number) => {
        const tags = await this.knex.from('user_tag').innerJoin('tags', 'user_tag.tags_id', 'tags.id').where('users_id', userId)
        return tags;
    }

    addTags = async (userId: number, strTags: string) => {

        // transaction
        await this.knex('user_tag').where('users_id', userId).count('id')
        const tags = strTags.split(',')

        if (tags.length > 5) {
            console.log('message: tags exceed 5')
            return
        }

        await this.knex('user_tag').del().where('users_id', userId)

        for (let tag of tags) {
            await this.knex('user_tag').insert({ 'users_id': userId, 'tags_id': tag })
        }
    }

    getArtworks = async (userId: number) => {
        const artworks = await this.knex('artworks').select('*').where('users_id', userId).orderBy('id')
        return artworks;
    }

    getVideos = async (userId: number) => {
        const videos = await this.knex('videos').select('*').where('users_id', userId).orderBy('id')
        return videos;
    }

    deleteArtwork = async (userId: number, artworkName: string) => {
        await this.knex('artworks').where('filename', artworkName).andWhere('users_id', userId).del()
    }


    deleteVideo = async (userId: number, videoName: string) => {
        await this.knex('videos').where('filename', videoName).andWhere('users_id', userId).del()
    }

    getRequest = async (userId: number) => {
        const projects = await this.knex('requests').select('id as requests_id', '*').where('freelancer_id', userId)
        const appliedList = await this.knex('request_application').join('requests', 'requests.id', 'request_application.requests_id').select('requests_id', '*').where('request_application.status', 1).andWhere('request_application.freelancer_id', userId)
        const invitedList = await this.knex('request_invitation').join('requests', 'requests.id', 'request_invitation.requests_id').select('requests_id', '*').where('request_invitation.status', 1).andWhere('request_invitation.freelancer_id', userId)


        for (let applied of appliedList) {
            const requestIDArr = projects.map(item => Object.values(item)[0])
            if (!requestIDArr.includes(applied.requests_id)) {
                projects.push(applied);
            }
        }

        for (let invited of invitedList) {
            const requestIDArr = projects.map(item => Object.values(item)[0])
            if (!requestIDArr.includes(invited.requests_id)) {
                projects.push(invited)
            } else {
                projects[requestIDArr.indexOf(invited.requests_id)]['invite_budget'] = invited.invite_budget
            }
        }

        for (let i = 0; i < projects.length; i++) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag', 'tags.id').where('request_tag.requests_id', projects[i].id);
            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            projects[i]['tag'] = tagsArr
            projects[i]['tag_id'] = tagsIDArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userId).andWhere('requests_id', projects[i].id);
            bookmark.length >= 1 ? projects[i]['bookmark'] = true : projects[i]['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', projects[i].requester_id).first();
            projects[i]['requester'] = requesterName.username

            if (projects[i].freelancer_id) {
                const FreelancerName = await this.knex('users').select('username').where('id', projects[i].freelancer_id).first();
                projects[i]['freelancer'] = FreelancerName.username
            }
        }

        return projects;
    }

    getPreviewRequest = async (userId: number) => {
        const projects = await this.knex('requests').select('*').where('freelancer_id', userId).andWhere('status', '>=', 3)

        for (let i = 0; i < projects.length; i++) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag', 'tags.id').where('request_tag.requests_id', projects[i].id);

            const tagsArr: string[] = []
            const tagsIDArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
                tagsIDArr.push(tag['id'])
            }

            projects[i]['tag'] = tagsArr
            projects[i]['tag_id'] = tagsIDArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userId).andWhere('requests_id', projects[i].id);
            bookmark.length >= 1 ? projects[i]['bookmark'] = true : projects[i]['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', projects[i].requester_id).first();
            projects[i]['requester'] = requesterName.username

            if (projects[i].freelancer_id) {
                const FreelancerName = await this.knex('users').select('username').where('id', projects[i].freelancer_id).first();
                projects[i]['freelancer'] = FreelancerName.username
            }
        }

        return projects;
    }

    getPostedJob = async (userId: number) => {
        const projects = await this.knex('requests').select('*').where('requester_id', userId)

        for (let i = 0; i < projects.length; i++) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag').where('request_tag.requests_id', projects[i].id);
            const tagsArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
            }
            projects[i]['tag'] = tagsArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userId).andWhere('requests_id', projects[i].id);
            bookmark.length >= 1 ? projects[i]['bookmark'] = true : projects[i]['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', projects[i].requester_id).first();
            projects[i]['requester'] = requesterName.username

            if (projects[i].freelancer_id) {
                const FreelancerName = await this.knex('users').select('username').where('id', projects[i].freelancer_id).first();
                projects[i]['freelancer'] = FreelancerName.username
            }
        }

        return projects;
    }

    getPreviewPostedJob = async (userId: number) => {
        const projects = await this.knex('requests').select('*').where('requester_id', userId)

        for (let i = 0; i < projects.length; i++) {
            const tags = await this.knex('request_tag').join('tags', 'request_tag.tags_id', 'tags.id').select('tag').where('request_tag.requests_id', projects[i].id);
            const tagsArr: string[] = []

            for (let tag of tags) {
                tagsArr.push(tag['tag'])
            }

            projects[i]['tag'] = tagsArr

            const bookmark = await this.knex('bookmark').select('*').where('users_id', userId).andWhere('requests_id', projects[i].id);
            bookmark.length >= 1 ? projects[i]['bookmark'] = true : projects[i]['bookmark'] = false;

            const requesterName = await this.knex('users').select('username').where('id', projects[i].requester_id).first();
            projects[i]['requester'] = requesterName.username

            if (projects[i].freelancer_id) {
                const FreelancerName = await this.knex('users').select('username').where('id', projects[i].freelancer_id).first();
                projects[i]['freelancer'] = FreelancerName.username
            }
        }
        return projects;
    }

    postProfile = async (userId: number, introduction: string, profilePic?: string,) => {
        if (!profilePic) {
            await this.knex('users').update({ self_intro: introduction }).where('id', userId)
        } else {
            await this.knex('users').update({ profile_pic: profilePic, self_intro: introduction }).where('id', userId)
        }
    }


    deleteProfilePic = async (userId: number) => {
        try {
            await this.knex('users').update({ profile_pic: '' }).where('id', userId)
        } catch (err) {
            console.log(err);
        }
    }

    postArtworks = async (userId: number, artwork: string, originalname: string, mimetype: string) => {
        try {
            await this.knex('artworks').insert([{ users_id: userId, filename: artwork, original_name: originalname, description: '', extension: mimetype }]).where('id', userId)
        } catch (err) {
            console.log(err);
        }
    }

    postArtworksDes = async (userId: number, artworkName: string, description: string) => {
        try {
            await this.knex('artworks').update({ description: description }).where('filename', artworkName).andWhere('users_id', userId)
        } catch (err) {
            console.log(err);
        }
    }

    postVideo = async (userId: number, video: string, originalname: string, mimetype: string, description: string) => {
        try {
            await this.knex('videos').insert([{ users_id: userId, filename: video, original_name: originalname, description: description, extension: mimetype }]).where('id', userId)
        } catch (err) {
            console.log(err);
        }
    }

    getOverallRatingAsFreelancer = async (userId: number) => {
        try {
            const averageFreelancerRatings = await this.knex('rating_on_freelancer').join('rating_on_requester', 'rating_on_freelancer.requests_id', 'rating_on_requester.requests_id').select('rating_on_requester.freelancer_id as freelancerId').avg("rating_on_freelancer.rating as AvgFreelancer").groupBy('rating_on_requester.freelancer_id').where('rating_on_requester.freelancer_id', userId).first();
            return averageFreelancerRatings;
        } catch (err) {
            console.log(err);
            return
        }
    }

    getOverallRatingAsRequester = async (userId: number) => {
        try {
            const averageRequesterRatings = await this.knex('rating_on_freelancer').join('rating_on_requester', 'rating_on_freelancer.requests_id', 'rating_on_requester.requests_id').select('rating_on_freelancer.requester_id as requesterId').avg("rating_on_requester.rating as AvgRequester").groupBy('rating_on_freelancer.requester_id').where('rating_on_freelancer.requester_id', userId).first();
            return averageRequesterRatings;
        } catch (err) {
            console.log(err);
            return
        }
    }

}
