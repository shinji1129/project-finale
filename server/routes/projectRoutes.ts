import express from 'express';
import { projectController } from '../main';
import { isLoggedIn } from '../guard';

export const projectRoutes = express.Router();

projectRoutes.get('/:requestID', isLoggedIn, projectController.get)
projectRoutes.get('/:requestID/inviteList', isLoggedIn, projectController.getInviteList)
projectRoutes.get('/invitedList/:requestID', isLoggedIn, projectController.getInvitedUsers);

projectRoutes.post('/applicant/:requestID/:user_id/:response/:apply_budget', isLoggedIn, projectController.postApplicantByUserIDAndRequestID)

// cancel
projectRoutes.post('/invited/:requestID/:user_id', isLoggedIn, projectController.cancelInvitedByUserIDAndRequestID)

projectRoutes.post('/invited/:requestID/:user_id/:invite_budget', isLoggedIn,projectController.postInvited)

// confirmation
projectRoutes.post('/:requestID/confirmation', isLoggedIn, projectController.postConfirmationByUserIDAndRequestID)