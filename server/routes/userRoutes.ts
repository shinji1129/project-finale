import express from 'express';
import { userController, upload } from '../main';
import { isLoggedIn } from '../guard'

export const userRoutes = express.Router();

userRoutes.post('/login', userController.login)
userRoutes.get('/info', isLoggedIn, userController.get)
userRoutes.post('/login/Facebook', userController.loginFacebook)
userRoutes.post('/register', upload.single('profilePic'), userController.post)
userRoutes.get('/username/:id', isLoggedIn, userController.getUsername)
userRoutes.get('/freelancers/:page', isLoggedIn, userController.getFreelancers)
userRoutes.get('/freelancers/:tag_id/:page', isLoggedIn, userController.getFreelancerByTags)
userRoutes.post('/applicants', isLoggedIn, upload.array('profilePic'), userController.getApplicantsByIdArray)
userRoutes.post('/invitedList', isLoggedIn, upload.array('profilePic'), userController.getInvitedUserByIdArray)