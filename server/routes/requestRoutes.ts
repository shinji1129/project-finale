import express from 'express';
import { requestController, upload } from '../main';
import { isLoggedIn } from '../guard';


export const requestRoutes = express.Router();

// request
requestRoutes.get('/requestList/Search/:tagID', isLoggedIn, requestController.getRequestByTag)
requestRoutes.get('/requestList/:categoryID', isLoggedIn, requestController.getRequestByCategory)
requestRoutes.get('/requestList/Search/:categoryID/:tagID', isLoggedIn, requestController.getRequestsByTagAndCategory)
requestRoutes.post('/newRequest',isLoggedIn, upload.array('request_image',5),requestController.postRequest)
requestRoutes.get('/images/:requestId',isLoggedIn, requestController.getRequestImage)

// tags
requestRoutes.get('/tags',requestController.getTags)
requestRoutes.get('/tags/:requestID', requestController.getTagsByRequestId)
requestRoutes.get('/:categoryID/tags', isLoggedIn, requestController.getTagByCategory)


// category
requestRoutes.get('/category', requestController.getCategory)

// Bookmark
requestRoutes.get('/bookmark/:requestID', isLoggedIn, requestController.getBookmarkByUserIDAndRequestID)
requestRoutes.post('/bookmark/:requestID', isLoggedIn, requestController.postBookmark)
requestRoutes.get('/bookmarkList', isLoggedIn,requestController.getBookmarkList)

// Application
requestRoutes.get('/application/:requestID', isLoggedIn, requestController.getApplicantByRequestID)
requestRoutes.get('/application/:requestID/:userID', isLoggedIn, requestController.getApplicantByUserIDAndRequestID)
requestRoutes.post('/application/:expectPrice/:requestID', isLoggedIn, requestController.postApplication)
requestRoutes.post('/application/:requestID', isLoggedIn, requestController.cancelApplicantByUserIDAndRequestID)

// Invitation
requestRoutes.get('/invitation/:requestID', isLoggedIn, requestController.getInvitationByRequestID)
requestRoutes.post('/invitation/:response/:requestID/:invite_budget', isLoggedIn, requestController.postInvitationByUserIDAndRequestID)
requestRoutes.get('/invitation/:requestID/:userID', isLoggedIn, requestController.getInvitationByUserIDAndRequestID)


// Confirmation
requestRoutes.get('/confirmation/:requestID', isLoggedIn, requestController.getConfirmationStatus)
requestRoutes.post('/confirmation/:response/:requestID', isLoggedIn, requestController.postConfirmationByUserIDAndRequestID)

//Comments
requestRoutes.get('/comment/:requestId', isLoggedIn, requestController.getComments)
requestRoutes.post('/comment/:requestId',isLoggedIn,requestController.postCommentByRequestId)


//Rating
requestRoutes.get('/rating/:requestId',isLoggedIn, requestController.getRating)
requestRoutes.post('/rating/:requestId',isLoggedIn, requestController.postRating)