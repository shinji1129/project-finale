import express from 'express';
import { isLoggedIn } from '../guard'
import { chatroomController } from '../main';

export const chatRoomRoutes = express.Router();

chatRoomRoutes.get('/message/:id',isLoggedIn,chatroomController.getMessage)
chatRoomRoutes.get('/',isLoggedIn,chatroomController.getChatRoom)
