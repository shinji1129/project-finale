import express from 'express';
import { profileController, upload } from '../main';
import { isLoggedIn } from '../guard';

export const profileRoutes = express.Router();
 
profileRoutes.get('/all', isLoggedIn, profileController.getProfile),
profileRoutes.get('/tag', isLoggedIn, profileController.getTag),
profileRoutes.get('/artworks', isLoggedIn, profileController.getArtwork)
profileRoutes.delete('/artworks/:artworkName', isLoggedIn, profileController.deleteArtwork)
profileRoutes.delete('/video/:videoName', isLoggedIn, profileController.deleteVideo)
profileRoutes.get('/projects', isLoggedIn, profileController.getRequest)
profileRoutes.get('/projects/:userId', isLoggedIn, profileController.getRequestById)
profileRoutes.get('/publishedJob', isLoggedIn, profileController.getPostedJob)
profileRoutes.get('/publishedJob/:userId', isLoggedIn, profileController.getPostedJobById)
profileRoutes.post('/update',isLoggedIn, upload.single('profilePic'), profileController.updateProfile)
profileRoutes.delete('/picture',isLoggedIn, profileController.deleteProfilePic)
profileRoutes.post('/artwork/update',isLoggedIn,upload.array('artworks',5), profileController.updateArtworks)
profileRoutes.post('/artwork/description',isLoggedIn,profileController.updateArtworkDes)
profileRoutes.get('/video', isLoggedIn, profileController.getArtwork)
profileRoutes.post('/video/update',isLoggedIn,upload.single('video'), profileController.updateVideo)
profileRoutes.get('/all/:userID', isLoggedIn, profileController.getProfileByID),
profileRoutes.get('/tag/:userID', isLoggedIn, profileController.getTagByID),
profileRoutes.get('/artworks/:userID', isLoggedIn, profileController.getArtworkByID)
profileRoutes.get('/projects/:userID', isLoggedIn, profileController.getProjectByID)
profileRoutes.get('/video/:userID',isLoggedIn,profileController.getVideoByID)
profileRoutes.get('/rating/:userId',isLoggedIn,profileController.getOverallRatingAsFreelancer)
