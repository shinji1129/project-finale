import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    let tables = ['request_invitation', 'request_application', 'rating_on_freelancer', 'rating_on_requester', 'bookmark', 'comment']

    for (let table of tables) {
        await knex(table).del();
        await knex.raw(`ALTER SEQUENCE ${table}_id_seq RESTART WITH 1`)
    }

    // Inserts seed entries
    await knex("request_invitation").insert([
        { requests_id: 1, freelancer_id: 2, status: 1 , invite_budget: 123},
        { requests_id: 4, freelancer_id: 1, status: 1 , invite_budget: 2000},
        { requests_id: 4, freelancer_id: 4, status: 1 , invite_budget: 2500},
        { requests_id: 5, freelancer_id: 1, status: 1 , invite_budget: 5000},
        { requests_id: 5, freelancer_id: 2, status: 1 , invite_budget: 1234},
    ]);

    await knex("request_application").insert([
        { requests_id: 1, freelancer_id: 2, status: 1, apply_budget: 1000 },
        { requests_id: 6, freelancer_id: 1, status: 2, apply_budget: 2000 },
        { requests_id: 4, freelancer_id: 4, status: 1, apply_budget: 1500 },
    ]);

    await knex("rating_on_freelancer").insert([

        { requests_id: 17, requester_id: 1, rating: 5 },
        { requests_id: 18, requester_id: 3, rating: 5 },

    ]);

    await knex("rating_on_requester").insert([

        { requests_id: 17, freelancer_id: 2, rating: 4 },
        { requests_id: 18, freelancer_id: 1, rating: 5 },

    ]);

    await knex("bookmark").insert([
        { users_id: 2, requests_id: 1 },
        { users_id: 4, requests_id: 2 },
        { users_id: 4, requests_id: 3 }
    ]);

    await knex("comment").insert([
        { users_id: 1, requests_id: 18, comment: "very good customer" },
        { users_id: 3, requests_id: 18, comment: "very high quality works!" },
        { users_id: 2, requests_id: 17, comment: "reasonable price" },
        { users_id: 1, requests_id: 17, comment: "Nice." },
    ]);
};
