import * as Knex from "knex";

import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
  let tables = [
    "chat_box",
    "request_tag",
    "user_tag",
    "tags",
    "requests",
    "categories",
    "artworks",
    "users",
    "videos",
    'request_image'
  ];

  for (let table of tables) {
    await knex(table).del();
    await knex.raw(`ALTER SEQUENCE ${table}_id_seq RESTART WITH 1`);
  }

  // Inserts seed entries
  await knex("users").insert([
    {
      username: "admin",
      password: await hashPassword("admin"),
      self_intro: "Hi, im admin",
      profile_pic: "profilePic-1602058231481.jpeg",
      email: "admin@jobs_creative.com",
      freelancer: true,
      requester: true,
    },
    {
      username: "bryan",
      password: await hashPassword("bryan"),
      self_intro: `Graduated in London, Kingston University, major in music performance and composition.

I was once a contracted pop song composer in HK, my song was sang by the artist ChiLam and hit the top chart back in 2009.

Major instrument: Piano
Music genre: Classical, Pop, Rock, Brit Rock

Second instrument: Drums
Music genre: Rock, Pop, Brit Rock, J-Rock

I have a band names Speaka since Aug 2009,
And once again our song hit the top chart in 2010 with the song “I am fine”.
We had over 100 times of live performance, hence it will be your best option to choose us to perform at your stage.`,
      profile_pic: "profilePic-1602059534992.jpeg",
      email: "bryan@jobs_creative.com",
      freelancer: true,
      requester: true,
    },
    {
      username: "elvis",
      password: await hashPassword("elvis"),
      self_intro: "Hi, this is Elvis. I am currently a student in Tecky Academy. I am dedicated to programming. \n\n\nBesides, I am a word-lover who also take part in translation. \n\nI have been studying in Tokyo for one year as a exchange student. I also have experience as a Japanese interpreter in exhibition. Please feel free to contact me for job enquiry. CI/Cd testing~! also deployed backend.",
      profile_pic: "profilePic-1602057589046.jpeg",
      email: "elvis@jobs_creative.com",
      freelancer: false,
      requester: true,
    },
    {
      username: "zak",
      password: await hashPassword("zak"),
      self_intro:
        "Hello, I am Zak. Currently studying in Tecky Academy, I used to study in Tokyo for one year. I am open to new challenges.",
      profile_pic: "profilePic-1602059770101.jpeg",
      email: "zak@jobs_creative.com",
      freelancer: true,
      requester: false,
    },
  ]);

  // for (let i = 1; i < 51; i++) {
  //   await knex('users').insert(
  //     {
  //       username: `dummy person ${i}`,
  //       password: await hashPassword("dummy"),
  //       self_intro: `Hi, im dummy ${i}`,
  //       profile_pic: "profilePic-1600353794381.jpeg",
  //       email: `dummy${i}@jobs_creative.com`,
  //       freelancer: true,
  //       requester: true,
  //     },
  //   )
  // }

  await knex('request_image').insert([
    {
      request_id: 6,
      filename: 'request_image-1602149970423.jpeg',
      original_name: 'IMG_0885.JPG ',
      extension: 'image/jpeg'

    },
    {
      request_id: 1,
      filename: 'request_image-1602149970423.jpeg',
      original_name: 'IMG_0885.JPG ',
      extension: 'image/jpeg'

    }
  ])

  await knex("artworks").insert([
    {
      users_id: 1,
      filename: "profilePic-1600344092268.jpeg",
      original_name: "original_filename",
      description: "this is file description 1",
      extension: "png",
    },
    {
      users_id: 1,
      filename: "profilePic-1600354061544.jpeg",
      original_name: "original_filename",
      description: "this is file description 2",
      extension: "wav",
    },
    {
      users_id: 2,
      filename: "profilePic-1600368350270.jpeg",
      original_name: "original_filename",
      description: "Desert",
      extension: "wav",
    },
    {
      users_id: 3,
      filename: "artworks-1602083778630.jpeg",
      original_name: "stars",
      description: "stars",
      extension: "png",
    },
    {
      users_id: 3,
      filename: "artworks-1602083778680.jpeg",
      original_name: "galaxy",
      description: "galaxy",
      extension: "png",
    },
    {
      users_id: 4,
      filename: "artworks-1602083778616.jpeg",
      original_name: "original_filename",
      description: "paper flower",
      extension: "png",
    },
    {
      users_id: 4,
      filename: "artworks-1602084874891.jpeg",
      original_name: "original_filename",
      description: "strawberry",
      extension: "png",
    },

  ]);

  await knex("videos").insert([
    {
      users_id: 3,
      filename: "video-1602086143121.mp4",
      original_name: "original_filename",
      description: "tour",
      extension: "wav",
    },
  ])

  let categories: string[] = [
    "Design",
    "Computer Programming",
    "Make-up",
    "Translation",
    "Creative Writing",
    "Music",
    "Photography",
    "Video Related",
  ];
  for (let category of categories) {
    await knex("categories").insert([{ category: `${category}` }]);
  }

  await knex("requests").insert([
    {
      requester_id: 1,
      title: "Design Logo",
      status: 2,
      company_name: "jobs_creative",
      details: "Please help to design logo for Mobile App",
      budget_min: 100,
      budget_max: 500,
      duration: 3,
      expiry_date: "2020-10-10",
      category_id: 1,
    },
    {
      requester_id: 2,
      freelancer_id: 4,
      title: "Mini Project",
      status: 3,
      company_name: "jobs_creative",
      details: "Mini Web Game: Conway's Game of Life",
      budget_min: 250,
      budget_max: 1000,
      confirmed_budget: 300,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 2,
      venue: "office",
    },
    {
      requester_id: 3,
      freelancer_id: 1,
      status: 3,
      title: "Make up",
      details: "for taking photos, about 20 ~ 30 people",
      budget_min: 550,
      budget_max: 900,
      confirmed_budget: 600,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 3,
      venue: "Tecky",
    },
    {
      requester_id: 3,
      status: 2,
      company_name: "jobs_creative",
      title: "Mobile APP",
      time_start: "2020-09-22",
      details: "this is request 4 details",
      budget_min: 500,
      budget_max: 1500,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 2,
    },
    {
      requester_id: 3,
      status: 2,
      time_start: "2020-10-13",
      title: "Wedding photos makeup",
      details: "Urgent!!",
      budget_min: 500,
      budget_max: 1500,
      duration: 4,
      expiry_date: "2020-10-17",
      category_id: 3,
      venue: "CUHK",
    },
    {
      requester_id: 3,
      status: 3,
      confirmed_budget: 500,
      time_start: "2020-10-25",
      title: "logo design ",
      details: "Design a logo suits the company name 'Jobs Creative'",
      budget_min: 500,
      budget_max: 1200,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 1,
      venue: "Hong Kong",
    },
    {
      requester_id: 1,
      freelancer_id: 4,
      status: 3,
      time_start: "2020-10-25",
      title: "jobs_creative app development ",
      details: "freelancer platform",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 600,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 2,
      venue: "Hong Kong",
    },
    {
      requester_id: 2,
      freelancer_id: 3,
      status: 3,
      time_start: "2020-11-25",
      title: "wedding makeup ",
      details: "make up for both bride and groom",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 800,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 3,
      venue: "Hong Kong",
    },
    {
      requester_id: 3,
      freelancer_id: 2,
      status: 3,
      time_start: "2020-12-25",
      title: "cv translation",
      details: "Please help to translate my CV to Japanese",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 4,
      venue: "Hong Kong",
    },
    {
      requester_id: 4,
      freelancer_id: 1,
      status: 3,
      time_start: "2021-01-25",
      title: "Essay grammar checking ",
      details:
        "Please help to check grammar of my essay",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 1000,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 5,
      venue: "Hong Kong",
    },
    {
      requester_id: 3,
      freelancer_id: 4,
      status: 3,
      time_start: "2020-12-25",
      title: "Resume producer",
      details: "Help to write English resume",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 800,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 5,
      venue: "Hong Kong",
    },
    {
      requester_id: 1,
      freelancer_id: 3,
      status: 4,
      time_start: "2020-12-25",
      title: "Poem for proposing ",
      details: "please!",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 5,
      venue: "Hong Kong",
    },
    {
      requester_id: 2,
      freelancer_id: 4,
      status: 4,
      time_start: "2020-12-25",
      title: "Wedding photo ",
      details: "prefer take in Taiwan, will pay for the trip fee",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 7,
      venue: "Hong Kong",
    },
    {
      requester_id: 3,
      freelancer_id: 2,
      status: 4,
      time_start: "2020-12-25",
      title: "Music video production",
      details: "for 3 songs",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 8,
      venue: "Hong Kong",
    },
    {
      requester_id: 4,
      freelancer_id: 1,
      status: 4,
      time_start: "2020-12-25",
      title: "Game theme songs",
      details: "Please contact me",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 6,
      venue: "Hong Kong",
    },
    {
      requester_id: 2,
      freelancer_id: 4,
      status: 5,
      time_start: "2020-12-25",
      title: "completed lyrics ",
      details: "completed",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 3,
      expiry_date: "2020-10-10",
      category_id: 6,
      venue: "Hong Kong",
    },
    {
      requester_id: 1,
      freelancer_id: 2,
      status: 5,
      time_start: "2020-12-25",
      title: "completed poem ",
      details: "English poem",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 4,
      expiry_date: "2020-10-10",
      category_id: 5,
      venue: "Hong Kong",
    },
    {
      requester_id: 3,
      freelancer_id: 1,
      status: 5,
      time_start: "2020-12-25",
      title: "completed interior design",
      details: "for Jobs Creative Office",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 5,
      expiry_date: "2020-10-10",
      category_id: 1,
      venue: "Hong Kong",
    },
    {
      requester_id: 4,
      freelancer_id: 3,
      status: 5,
      time_start: "2020-12-25",
      title: "completed party room design",
      details: "very big",
      budget_min: 500,
      budget_max: 1200,
      confirmed_budget: 900,
      duration: 6,
      expiry_date: "2020-10-10",
      category_id: 1,
      venue: "Hong Kong",
    },
    {
      requester_id: 2,
      title: "jobs_creative interior Design",
      status: 1,
      company_name: "jobs_creative",
      details: "Please help to do the interior design",
      budget_min: 100,
      budget_max: 500,
      confirmed_budget: 400,
      duration: 3,
      expiry_date: "2020-10-10",
      category_id: 1,
      venue: "home",
    },
    {
      requester_id: 3,
      title: "Piano teacher",
      status: 1,
      company_name: "jobs_creative",
      details: "prefer female",
      budget_min: 100,
      budget_max: 500,
      confirmed_budget: 400,
      duration: 3,
      expiry_date: "2020-10-10",
      category_id: 6,
      venue: "home",
    },
    {
      requester_id: 4,
      title: " English essay writing",
      status: 1,
      company_name: "jobs_creative",
      details: "my elngish is baddest",
      budget_min: 100,
      budget_max: 500,
      confirmed_budget: 400,
      duration: 3,
      expiry_date: "2020-10-10",
      category_id: 4,
      venue: "home",
    },
    {
      requester_id: 4,
      title: "Japanese homework",
      status: 1,
      company_name: "jobs_creative",
      details: "I don't wanna fail",
      budget_min: 100,
      budget_max: 500,
      confirmed_budget: 400,
      duration: 3,
      expiry_date: "2020-10-10",
      category_id: 4,
      venue: "home",
    },
  ]);

  // tags

  let tags: string[] = [
    "design",
    "computer Programming",
    "make-up",
    "translation",
    "creative Writing",
    "music",
    "photography",
    "video Related",
    "illustration",
    'graphic',
    'interior',
    'webUI',
    'architecture',
    'product',
    'logo',
    'ad',
    'performance',
    'composition',
    'lyrics',
    'recording',
    'mixing',
    'piano',
    'drums',
    'violin',
    'classical Guitar',
    'electric Guitar',
    'cello',
    'viola',
    'bass Guitar',
    'frontend',
    'backend',
    'website',
    "company system",
    'sQL',
    'machine Learning AI',
    'python',
    'c',
    'uI',
    'uX',
    'javascript',
    "java",
    'swift',
    'xcode',
    'react',
    'react Native',
    'mobile App',
    'wedding',
    'party',
    'poem',
    'cantonese',
    'mandarin',
    'japanese',
    'english',
    'french',
    'dutch',
    'korean',
    'taiwanese',
    'document',
    'on site',
    'speaking',
    'writing',
    'filming',
    'editing',
    'mastering',
    'directing',
    'shooting',
    'model',
    'babies',
    'pets',
  ];
  for (let tag of tags) {
    await knex("tags").insert([{ tag: `${tag}` }]);
  }

  // End of tags

  let category_id = [1, 2, 3, 2, 3, 15, 46, 3 ,4, 61, 5, 53, 7, 6, 63, 19, 49, 1, 1, 1, 22, 5, 52, 6]
  console.log(category_id.length)
  for (let i = 1; i < 24; i++) {
    await knex('request_tag').insert([
      { requests_id: i, tags_id: category_id[i-1] },
    ])
  }

  await knex('request_tag').insert([
    { requests_id: 1, tags_id: 9 }
  ])

  await knex("user_tag").insert([
    { users_id: 1, tags_id: 1 },
    { users_id: 1, tags_id: 3 },
    { users_id: 1, tags_id: 5 },
    { users_id: 1, tags_id: 7 },
    { users_id: 1, tags_id: 9 },
    { users_id: 2, tags_id: 23 },
    { users_id: 2, tags_id: 2 },
    { users_id: 2, tags_id: 53 },
    { users_id: 2, tags_id: 6 },
    { users_id: 3, tags_id: 4 },
    { users_id: 3, tags_id: 53 },
    { users_id: 3, tags_id: 2 },
    { users_id: 3, tags_id: 9 },
    { users_id: 4, tags_id: 9 },
    { users_id: 4, tags_id: 1 },
    { users_id: 4, tags_id: 2 },
    { users_id: 4, tags_id: 15 },
  ]);

  // for (let i = 5; i < 55; i++) {
  //   await knex('user_tag').insert(
  //     {
  //       users_id: i, tags_id: Math.ceil(Math.random() * 8)
  //     }
  //   )
  // }

  await knex("chat_box").insert([
    {
      room_id: 1,
      user_one: 1,
      user_two: 2,
      message: "hi Bryan, do you want to do this?",
      status: 0,
    },
    {
      room_id: 1,
      user_one: 2,
      user_two: 1,
      message: "sure, makasete",
      status: 0,
    },
    {
      room_id: 2,
      user_one: 4,
      user_two: 3,
      message: "hi, do you want this job?",
      status: 0,
    },
    {
      room_id: 1,
      user_one: 1,
      user_two: 2,
      message: "how long do you need?",
      status: 0,
    },
    {
      room_id: 1,
      user_one: 2,
      user_two: 1,
      message: "3 days",
      status: 0,
    },
    {
      room_id: 2,
      user_one: 3,
      user_two: 4,
      message: "awaiting your quotation",
      status: 1,
    },
    {
      room_id: 3,
      user_one: 1,
      user_two: 3,
      message: "hi elvis",
      status: 1,
    },
    {
      room_id: 1,
      user_one: 1,
      user_two: 2,
      message: "awaiting your quotation",
      status: 1,
    },
  ]);
}
