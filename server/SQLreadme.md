psql
create database jobs_creative

update .env (following .env.sample)

yarn knex migrate:latest
yarn knex seed:run