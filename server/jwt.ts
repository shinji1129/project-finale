export default {
    jwtSecret: "$2a$10$py3fDWN4m.TLi8Rv10ZkKedXAVm.u.s.YIeZwln3ieSMwGxRr/.SC",
    jwtSession: {
        session: false
    }
}