import { UserService } from '../services/UserService';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { Request, Response } from 'express'
import { checkPassword, hashPassword } from '../hash';
import { MulterFile } from '../services/models'
//for facebook login
import fetch from 'node-fetch'

export class UserController {
    constructor(private userService: UserService) { }

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                res.status(401).json({ message: "Missing username/password!" });
                return
            }
            const user = await this.userService.getUserByUsername(req.body.username)

            if (!user || !(await checkPassword(req.body.password, user.password))) {
                res.status(401).json({ message: "Invalid username/password!" });
                return;
            }
            const payload = {
                id: user.id,
                user: user.username
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);

            const { created_at, updated_at, password, ...other } = user
            const userInfo = other
            res.json({
                token: token,
                user: userInfo
            })
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    get = async (req: Request, res: Response) => {
        try {
            if (!req.user) {
                res.status(401).json({ message: "Please login!" });
                return
            }
            const { created_at, updated_at, ...other } = req.user
            const user = { ...other };

            res.json(user)
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    getUsername = async (req: Request, res: Response) => {
        try {
            const { id } = req.params
            const user = await this.userService.getUserById(parseInt(id));

            if (user) {
                res.json(user?.username)
            }

            res.status(500).json({ message: "internal server error" })
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    loginFacebook = async (req: Request, res: Response) => {
        try {

            if (!req.body.accessToken) {
                res.status(401).json({ msg: "Access Token doesn't exist!" });
                return;
            }
            const { accessToken } = req.body;
            const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
            const result = await fetchResponse.json();

            if (result.error) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }

            // const trx = await this.knex.transaction();
            // const userService = new UserService(trx);

            // ...
            // await trx.commit();

            let user = await this.userService.getUserByFacebookId(result.id)

            if (!user) {
                user = await this.userService.getUserByEmail(result.email);
                if (!user) {
                    user = await this.userService.createFacebookUser(result.name, result.id, result.email)
                    user = await this.userService.getUserById(user.id)
                }
            }

            const payload = {
                id: user.id,
                username: user.username
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token,
                user: user.username
            });
        } catch (e) {
            res.status(500).json({ msg: e.toString() })
        }
    }

    post = async (req: Request & { file: MulterFile }, res: Response) => {
        try {
            if (req.body) {
                if (req.body.password !== req.body.confirmPassword) {
                    res.status(401).json({ message: "password must be the same as confirmPassword" })
                    return
                }
                const hashedPassword = await hashPassword(req.body.password)

                if (await this.userService.checkUserExist(req.body.username)) {
                    res.status(401).json({ message: "Username already exist" });
                    return
                }
                if (await this.userService.checkEmailExist(req.body.email)) {
                    res.status(401).json({ message: "Email already exist" });
                    return
                }
                if (req.file) {
                    await this.userService.register(req.body.username, hashedPassword, req.file.key, req.body.email, req.body.introduction)
                } else {
                    await this.userService.register(req.body.username, hashedPassword, '', req.body.email, req.body.introduction)
                }
                res.status(200).json({ message: "register success!" })
            } else {
                res.status(400);
            }
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    getFreelancers = async (req: Request, res: Response) => {
        try {
            const { page } = req.params;

            if (isNaN(parseInt(page))) {
                res.status(500).json({ message: "internal server error" })
                return
            }

            if (req.user) {
                const users = await this.userService.getFreelancer(req.user.id, parseInt(page));
                res.status(200).json(users)
                return;
            }

            res.status(500).json({ message: "internal server error" })
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    getFreelancerByTags = async (req: Request, res: Response) => {
        try {
            const { page, tag_id } = req.params;

            if (isNaN(parseInt(page))) {
                res.status(500).json({ message: "internal server error" })
                return
            }

            if (isNaN(parseInt(tag_id))) {
                res.status(500).json({ message: "internal server error" })
                return
            }


            if (req.user) {
                const users = await this.userService.getFreelancerByTag(req.user.id, parseInt(tag_id), parseInt(page));
                res.status(200).json(users)
                return;
            }

            res.status(500).json({ message: "internal server error" })
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    getApplicantsByIdArray = async (req: Request, res: Response) => {
        try {
            const { applicants } = req.body;

            const applicantArr = JSON.parse(applicants).applicantsList

            if (applicantArr.length < 1) {
                res.status(500).json({ message: "internal server error" })
                return
            }

            if (req.user) {
                const users: any = []
                for (let applicant of applicantArr) {
                    const user = await this.userService.getApplicantById(applicant)
                    users.push(user)
                }

                res.status(200).json(users)
                return;
            }

            res.status(500).json({ message: "internal server error" })
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: "internal server error" })
        }
    }

    getInvitedUserByIdArray = async (req: Request, res: Response) => {
        try {
            const { invitedList } = req.body;

            const invitedListArr = JSON.parse(invitedList).invitedList

            if (invitedListArr.length < 1) {
                res.status(400).json({ message: "Invited List should contain at least 1 value" })
                return
            }

            if (req.user) {
                const users: any = []
                for (let invited of invitedListArr) {
                    const user = await this.userService.getInvitedUserById(invited)
                    users.push(user)
                }

                res.status(200).json(users)
                return;
            }

            res.status(500).json({ message: "internal server error" })
            return
        } catch (err) {
            console.log(err);
            res.status(500).json({ message: "internal server error" })
        }
    }
}