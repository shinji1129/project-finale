import { ProjectService } from '../services/ProjectService';
import { Request, Response } from 'express';

export class ProjectController {
    constructor(private projectService: ProjectService) { }

    get = async (req: Request, res: Response) => {
        try {
            const { requestID } = req.params
            if (req.user) {
                const project = await this.projectService.getProjectStatus(parseInt(requestID))
                const applicants = await this.projectService.getApplicants(parseInt(requestID))
                const invitedList = await this.projectService.getInviteList(parseInt(requestID))
                const applicantsArr = [];
                const invitedListArr = [];
                for (let applicant of applicants) {
                    applicantsArr.push(applicant.freelancer_id);
                }

                for (let invited of invitedList) {
                    invitedListArr.push(invited.freelancer_id);
                }

                project['applicants'] = applicantsArr
                project['invitedList'] = invitedListArr

                res.json(project);
            } else {
                res.status(400).json({ message: 'please log in' })
            }
        } catch (err) {
            res.status(500).json({ message: 'internal server error' })
            console.error(err.message)
        }

    }

    getInviteList = async (req: Request, res: Response) => {
        const { requestID } = req.params
        const invitedList = await this.projectService.getInviteList(parseInt(requestID))
        return invitedList
    }

    postApplicantByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID, response, apply_budget, user_id } = req.params;
        try {
            if (req.user) {
                if (apply_budget) {
                    const applicant = await this.projectService.postApplicant(parseInt(user_id), parseInt(requestID), response === "accept", parseInt(apply_budget))
                    if (applicant) {
                        res.json(applicant);
                    } else {
                        res.json({ message: 'Bad requests' })
                    }
                } else {
                    res.json({ message: 'Bad requests' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postInvited = async (req: Request, res: Response) => {
        try {
            const { invite_budget, requestID, user_id } = req.params
            if (req.body) {
                if (req.user) {
                    const status = await this.projectService.postInvited(parseInt(requestID), parseInt(invite_budget), parseInt(user_id))
                    res.json({ status: status })
                } else {
                    res.status(500).json({ message: 'internal server error, please login' })
                }
            } else {
                res.status(400).json({ message: 'Bad request' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e)
        }
    }

    cancelInvitedByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID, user_id } = req.params;
        try {
            if (req.user) {
                const invited = await this.projectService.cancelInvited(parseInt(user_id), parseInt(requestID))
                if (invited) {
                    res.json(invited);
                } else {
                    res.json({ message: 'Bad requests' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postConfirmationByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                const confirmationStatus = await this.projectService.postConfirmationStatus(req.user.id, parseInt(requestID))
                if (confirmationStatus) {
                    res.json(confirmationStatus);
                } else {
                    res.json({ message: 'Bad requests' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getInvitedUsers = async (req: Request, res: Response) => {
        try {
            const { requestID } = req.params;
            const invitedList = await this.projectService.getInviteList(parseInt(requestID))
            const invitedListIDArr = [];

            for (let invited of invitedList) {
                invitedListIDArr.push(invited.freelancer_id);
            }

            if (invitedListIDArr.length < 1) {
                res.status(500).json({ message: "internal server error" })
                return
            }

            if (req.user) {
                const users: any = []
                for (let invitedID of invitedListIDArr) {
                    const user = await this.projectService.getInvitedUserById(invitedID)
                    users.push(user)
                }

                res.status(200).json(users)
                return;
            }
            res.status(500).json({ message: "internal server error" })
            return
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }
}