import { ProfileService } from '../services/ProfileService';
import { Request, Response } from 'express';
import { IUser, MulterFile } from '../services/models'

export class ProfileController {
    constructor(private profileService: ProfileService) { }

    getProfile = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const profileAll: IUser | undefined = await this.profileService.getProfile(req.user.id);
                if (!profileAll) {
                    res.status(401).json({ message: "Unauthorized2" })
                    return
                }
                const { password, ...others } = profileAll;
                const profileInfo = { ...others }
                res.status(200).json(profileInfo)
            } else {
                res.status(400).json({ message: 'please log in' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
            console.error(e.message)
        }
    }


    getTag = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const tags = await this.profileService.getTags(req.user.id);
                res.status(200).json(tags)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
            console.error(e.message)
        }
    }

    addTags = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                await this.profileService.addTags(req.user.id, req.body.tagArr)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getArtwork = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const artworks = await this.profileService.getArtworks(req.user.id);
                res.status(200).json(artworks)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getVideo = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const videos = await this.profileService.getVideos(req.user.id);
                res.status(200).json(videos)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    deleteArtwork = async (req: Request, res: Response) => {
        try {
            const { artworkName } = req.params
            if (req.user) {
                await this.profileService.deleteArtwork(req.user.id, artworkName);
                res.status(200).json({ message: 'The artwork is deleted.' })
            } else {
                res.status(400).json({ message: 'Authentication Error' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    deleteVideo = async (req: Request, res: Response) => {
        try {
            const { videoName } = req.params
            if (req.user) {
                await this.profileService.deleteVideo(req.user.id, videoName);
                res.status(200).json({ message: 'The video is deleted.' })
            } else {
                res.status(400).json({ message: 'Authentication Error' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getRequest = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const projects = await this.profileService.getRequest(req.user.id);
                res.status(200).json(projects)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }


    getRequestById = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const { userId } = req.params;
                const projects = await this.profileService.getPreviewRequest(parseInt(userId));
                res.status(200).json(projects)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getPostedJob = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const postedJob = await this.profileService.getPostedJob(req.user.id);
                res.status(200).json(postedJob)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getPostedJobById = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const { userId } = req.params;
                const postedJob = await this.profileService.getPostedJob(parseInt(userId));
                res.status(200).json(postedJob)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }


    updateProfile = async (req: Request & { file: MulterFile }, res: Response) => {
        try {
            if (req.user) {
                if (req.file) {
                    await this.profileService.postProfile(req.user.id, req.body.introduction, req.file.key);
                    if (req.body.tagArr != "") {
                        await this.profileService.addTags(req.user.id, req.body.tagArr)
                    }
                    res.status(200).json({ message: 'Profile updated!' })
                } else {
                    await this.profileService.postProfile(req.user.id, req.body.introduction);
                    if (req.body.tagArr != "") {
                        await this.profileService.addTags(req.user.id, req.body.tagArr)
                    }
                    res.status(200).json({ message: 'Profile updated!' })
                }
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    deleteProfilePic = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                await this.profileService.deleteProfilePic(req.user.id)
                res.status(200).json({ message: 'Profile Picture deleted!' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    updateArtworks = async (req: Request & { files: Express.Multer.File[] }, res: Response) => {
        try {
            if (req.files) {
                let filesArr = req.files
                filesArr.map(async (file) => {
                    if (req.user) {
                        //@ts-ignore
                        await this.profileService.postArtworks(req.user.id, file.key, file.originalname, file.mimetype)
                    }
                })
                return res.status(200).json({ message: "artwork updated" })
            }
            return res.status(400).json({ message: "File is missing." })
        } catch (e) {
            return res.status(400).json({ message: "Internal Server Error" })
        }
    }

    updateArtworkDes = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                await this.profileService.postArtworksDes(req.user.id, req.body.artworkName, req.body.description)
                res.status(200).json({ message: 'Profile Picture deleted!' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    updateVideo = async (req: Request & { file: MulterFile }, res: Response) => {
        try {
            if (req.file) {
                if (req.user) {
                    //@ts-ignore
                    await this.profileService.postVideo(req.user.id, req.file.key, req.file.originalname, req.file.mimetype, req.body.description)
                }
                return res.status(200).json({ message: "video updated" })
            }
            return res.status(200).json({ message: "updated" })
        } catch (e) {
            console.error(e)
            return res.status(500).json({ message: e.message })
        }
    }

    getProfileByID = async (req: Request, res: Response) => {
        try {
            const { userID } = req.params
            const id = parseInt(userID)
            if (req.user) {
                const profileAll: IUser | undefined = await this.profileService.getProfile(id);
                if (!profileAll) {
                    res.status(401).json({ message: "Unauthorized2" })
                    return
                }
                const { password, ...others } = profileAll;
                const profileInfo = { ...others }
                res.status(200).json(profileInfo)
            } else {
                res.status(400).json({ message: 'please log in' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getTagByID = async (req: Request, res: Response) => {
        try {
            const { userID } = req.params
            const id = parseInt(userID)
            if (req.user) {
                const tags = await this.profileService.getTags(id);
                res.status(200).json(tags)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getArtworkByID = async (req: Request, res: Response) => {
        try {
            const { userID } = req.params
            const id = parseInt(userID)
            if (req.user) {
                const artworks = await this.profileService.getArtworks(id);
                res.status(200).json(artworks)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getVideoByID = async (req: Request, res: Response) => {
        try {
            const { userID } = req.params
            const id = parseInt(userID)
            if (req.user) {
                const videos = await this.profileService.getVideos(id);
                res.status(200).json(videos)
                console.log(videos)
            }
        } catch (e) {
            res.status(400).json({ message: e.message })
        }
    }

    getProjectByID = async (req: Request, res: Response) => {
        try {
            const { userID } = req.params
            const id = parseInt(userID)
            if (req.user) {
                const projects = await this.profileService.getRequest(id);
                res.status(200).json(projects)
                console.log(projects)
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
        }
    }

    getOverallRatingAsFreelancer = async (req: Request, res: Response) => {
        try {
            interface AvgRating {
                AvgFreelancerRating?: number
                AvgRequesterRating?: number
            }
            let avgRating: AvgRating = {};
            const { userId } = req.params
            const id = parseInt(userId)
            if (req.user) {
                const averageFreelancerRatings = await this.profileService.getOverallRatingAsFreelancer(id);
                const averageRequesterRatings = await this.profileService.getOverallRatingAsRequester(id);
                if (!averageFreelancerRatings && !averageRequesterRatings) {
                    return res.status(200).json({ message: "User did not have any rating yet." })

                }
                if (averageFreelancerRatings) {
                    if (averageFreelancerRatings.AvgFreelancer) {
                        let AvgFreelancerRating = averageFreelancerRatings.AvgFreelancer
                        avgRating["AvgFreelancerRating"] = AvgFreelancerRating;
                    }
                }
                if (averageRequesterRatings) {
                    if (averageRequesterRatings.AvgRequester) {
                        let AvgRequesterRating = averageRequesterRatings.AvgRequester
                        avgRating["AvgRequesterRating"] = AvgRequesterRating;
                    }
                }
                res.status(200).json(avgRating)
                return
            } else {
                return res.status(401).json({ error: 'Not Authorized' })
            }
        } catch (e) {
            res.status(400).json({ message: "Internal Server Error" })
            return
        }
    }

}