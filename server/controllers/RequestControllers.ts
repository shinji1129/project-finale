import { RequestService } from '../services/RequestService';
import { Request, Response } from 'express';

export class RequestController {


    constructor(private requestService: RequestService) { }

    getTags = async (req: Request, res: Response) => {
        try {
            const tags = await this.requestService.getTags();
            res.json(tags);
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getTagByCategory = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const { categoryID } = req.params;
                const tags = await this.requestService.getTagsByCategory(req.user.id, parseInt(categoryID));
                res.json(tags);
            } else {
                res.status(500).json({ message: 'internal server error' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }


    getCategory = async (req: Request, res: Response) => {
        try {
            const categories: string[] = await this.requestService.getCategories();
            res.json(categories)
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getRequestByCategory = async (req: Request, res: Response) => {
        const { categoryID } = req.params;

        try {
            if (req.user) {
                const requestList = await this.requestService.getRequestsByCategory(req.user.id, parseInt(categoryID))
                res.json(requestList);
            } else {
                res.status(500).json({ message: 'internal server error' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getBookmarkList = async (req: Request, res: Response) => {

        try {
            if (req.user) {
                const requestList = await this.requestService.getBookmarkedRequests(req.user.id)
                res.json(requestList);
            } else {
                res.status(500).json({ message: 'internal server error' })
            }

        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }


    getRequestByTag = async (req: Request, res: Response) => {
        const { tagID } = req.params;

        try {
            if (req.user) {
                const requestList = await this.requestService.getRequestsByTag(req.user.id, parseInt(tagID))
                res.json(requestList);
            }

        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getRequestsByTagAndCategory = async (req: Request, res: Response) => {
        const { tagID, categoryID } = req.params;

        try {
            if (req.user) {
                const requestList = await this.requestService.getRequestsByTagAndCategory(req.user.id, parseInt(tagID), parseInt(categoryID))
                res.json(requestList);
            }

        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getComments = async (req: Request, res: Response) => {
        try {
            const { requestId } = req.params;
            const comments = await this.requestService.getComments(parseInt(requestId))
            res.json(comments);
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getTagsByRequestId = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            const requestTags = await this.requestService.getRequestTag(parseInt(requestID))
            res.json(requestTags);
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getBookmarkByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                const bookmark = await this.requestService.getBookmark(req.user.id, parseInt(requestID))
                res.json(bookmark);
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postBookmark = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                await this.requestService.postBookmark(req.user.id, parseInt(requestID))
            
                res.json({ message: 'success' });
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }


    getApplicantByRequestID = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                const applicants = await this.requestService.getApplication(req.user.id, parseInt(requestID))
                if (applicants) {
                    res.json(applicants);
                } else {
                    res.json({ message: 'not applicants yet' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getApplicantByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID, userID } = req.params;
        try {
            if (req.user) {
                const applicants = await this.requestService.getApplication(parseInt(userID), parseInt(requestID))
                if (applicants) {
                    res.json(applicants);
                } else {
                    res.json({ message: 'not applicants yet' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postApplication = async (req: Request, res: Response) => {
        try {
            const { expectPrice, requestID } = req.params
            if (req.body) {
                if (req.user) {
                    const status = await this.requestService.postApplication(req.user.id, parseInt(requestID), parseInt(expectPrice))
                    res.json({ status: status })
                } else {
                    res.status(500).json({ message: 'internal server error, please login' })
                }
            } else {
                res.status(400).json({ message: 'Bad request' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e)
        }
    }

    cancelApplicantByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                const applicants = await this.requestService.cancelApplication(req.user.id, parseInt(requestID))
                res.json(applicants);
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getInvitationByRequestID = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                const invitation = await this.requestService.getInvitation(req.user.id, parseInt(requestID))
                if (invitation) {
                    res.json(invitation);
                } else {
                    res.json({ message: 'no invitation' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getInvitationByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID, userID } = req.params;
        try {
            if (req.user && userID && requestID) {
                const invitation = await this.requestService.getInvitation(parseInt(userID), parseInt(requestID))
                if (invitation) {
                    res.json(invitation);
                } else {
                    res.json({ message: 'no invitation' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postInvitationByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID, response, invite_budget } = req.params;
        try {
            if (req.user) {
                if (invite_budget) {
                    const invitation = await this.requestService.postInvitation(req.user.id, parseInt(requestID), response === "accept", parseInt(invite_budget))
                    if (invitation) {
                        res.json(invitation);
                    } else {
                        res.json({ message: 'Bad requests' })
                    }
                } else {
                    res.json({ message: 'Bad requests' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getConfirmationStatus = async (req: Request, res: Response) => {
        const { requestID } = req.params;
        try {
            if (req.user) {
                const confirmationStatus = await this.requestService.getConfirmationStatus(req.user.id, parseInt(requestID))
                if (confirmationStatus) {
                    res.json(confirmationStatus);
                } else {
                    res.json({ message: 'not applicants yet 123' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postConfirmationByUserIDAndRequestID = async (req: Request, res: Response) => {
        const { requestID, response } = req.params;
        try {
            if (req.user) {
                const confirmationStatus = await this.requestService.postConfirmationStatus(req.user.id, parseInt(requestID), response === "accept")
                if (confirmationStatus) {
                    res.json(confirmationStatus);
                } else {
                    res.json({ message: 'Bad requests' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }


    getRequestImage = async (req: Request, res: Response) => {
        try {
            const { requestId } = req.params
            if (req.user) {
                const images = await this.requestService.getImages(parseInt(requestId))
                if (images) {
                    res.json(images);
                } else {
                    res.json({ message: 'Bad requests' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postRequest = async (req: Request & { files: Express.Multer.File[] }, res: Response) => {
        try {
            if (req.body) {
                const { title, tags, category, duration, expiryDate, details, images, venue, company_name, min_budget, max_budget, time_start } = JSON.parse(req.body.submit)
                console.log(images);

                if (req.user) {
                    const id: number = await this.requestService.postRequest(req.user.id, title, category, duration, expiryDate, details)

                    await this.requestService.addTags(id, tags)

                    if (req.files) {
                        let filesArr = req.files
                        filesArr.map(async (file) => {
                            if (req.user) {
                                //@ts-ignore
                                await this.requestService.addImages(id, file.key, file.originalname, file.mimetype)
                            }
                        })
                    }

                    if (venue) {
                        await this.requestService.addVenue(id, venue)
                    }

                    if (company_name) {
                        await this.requestService.addCompany_name(id, company_name)
                    }
                    if (min_budget && max_budget) {
                        if (min_budget < max_budget) {
                            await this.requestService.addBudget(id, min_budget, max_budget)
                        } else {
                            res.status(401).json({ message: "Invalid input: Budget" })
                        }
                    }
                    if (time_start) {
                        await this.requestService.addTimeStart(id, time_start)
                    }

                    res.status(200).json({ message: `success, ${id}` })
                } else {
                    res.status(500).json({ message: 'internal server error' })
                }

            } else {
                res.status(500).json({ message: 'internal server error' })
            }
        } catch (err) {
            res.status(500).json({ message: 'internal server error' })
            console.error(err)
        }
    }


    postCommentByRequestId = async (req: Request, res: Response) => {
        const { requestId } = req.params;
        try {
            if (req.user) {
                await this.requestService.addComments(req.user.id, parseInt(requestId), req.body.comment)
                res.status(200).json({ message: `comment is sent successfully` })
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    getRating = async (req: Request, res: Response) => {
        const { requestId } = req.params;
        interface Rating {
            requests_id: number,
            ratingOnFreelancer?: number,
            requesterId?: number,
            ratingOnRequester?: number,
            freelancerId?: number,
            requesterName?: string,
            freelancerName?: string
        }
        try {
            if (req.user) {
                const rating: Rating = await this.requestService.getRating(parseInt(requestId))
                if (rating) {
                    if (rating.requesterId) {
                        const requesterName = await this.requestService.getUsernameById(rating.requesterId)
                        rating['requesterName'] = requesterName['username'];
                    }
                    if (rating.freelancerId) {
                        const freelancerName = await this.requestService.getUsernameById(rating.freelancerId)
                        rating['freelancerName'] = freelancerName['username'];
                    }
                    res.json(rating);
                } else {
                    res.json({ message: 'There is no rating yet.' })
                }
            } else {
                res.status(500).json({ message: 'internal server error, please login' })
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }

    postRating = async (req: Request, res: Response) => {
        try {
            const { requestId } = req.params
            const userIds = await this.requestService.getUserIdByRequestId(parseInt(requestId));
            if (req.user) {
                if (req.user.id === userIds.requester_id) {
                    await this.requestService.postRatingOnFreelancer(parseInt(requestId), req.user.id, req.body.rating)
                    res.status(200).json({ message: `Rating on Freelancer is sent successfully` })
                } else if (req.user.id === userIds.freelancer_id) {
                    await this.requestService.postRatingOnRequester(parseInt(requestId), req.user.id, req.body.rating)
                    res.status(200).json({ message: `Rating On Requester is sent successfully` })
                }
            }
        } catch (e) {
            res.status(500).json({ message: 'internal server error' })
            console.error(e.message)
        }
    }
}
