import { ChatRoomService } from '../services/ChatRoomService';
import { Request, Response } from 'express';

export class ChatRoomController {
    constructor(private chatRoomService: ChatRoomService) { }

    getChatRoom = async (req:Request, res:Response) => {
        try{
            if (req.user) {
                const messages = await this.chatRoomService.getMessageById(req.user.id)
                let chatRoomsArr:number[] = [];
                await Promise.all(messages.map(async (message)=>{
                    chatRoomsArr.push(message.room_id)
                    if(message.user_one !== req.user?.id){
                        const contactUser = await this.chatRoomService.getUsernameById(message.user_one);
                        message["contactUserId"] = message.user_one
                        message["contactUser"] = contactUser["username"]
                        const profilePic = await this.chatRoomService.getProfilePicById(message.user_one)
                        message["profile_pic"] = profilePic["profile_pic"]

                    } else {
                        const contactUser = await this.chatRoomService.getUsernameById(message.user_two);
                        message["contactUserId"] = message.user_two
                        message["contactUser"] = contactUser["username"]
                        const profilePic = await this.chatRoomService.getProfilePicById(message.user_two)
                        message["profile_pic"] = profilePic["profile_pic"]
                    }
                }))
                let chatRoomIds = chatRoomsArr.filter((value, index, arr)=> arr.indexOf(value) === index)
                let chatRooms = [];
                for (let i = 0; i < chatRoomIds.length; i++){
                    let messagesByChatRoom = messages.filter((message)=>message.room_id === chatRoomIds[i])
                    chatRooms.push(messagesByChatRoom);
                }
                res.json(chatRooms)
            }
        }catch(err){
            res.status(500).json({ message: 'internal server error' })
            console.error(err.message)
        }
    }

    getMessage = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const messages = await this.chatRoomService.getMessageById(req.user.id)
                res.json(messages)
                // res.json(messages.filter(message=>message.room_id === parseInt(req.query.roomId + "")))
            }
        } catch (err) {
            res.status(500).json({ message: 'internal server error' })
            console.error(err.message)
        }
    }

    // postMessage = async (req:Request, res:Response)=>{
    //     try{
    //         if(req.user){
    //         const message = {
    //             user_one:req.body.userOne,
    //             user_two:req.body.userTwo,
    //             message: req.body.message,
    //             status: 0,
    //             room_id: parseInt(req.body.roomId)
    //         };
    
    //         // messages.push(message)
    
    //         io.to(`chatroom:${message.room_id}`).emit('new_messages', message);
    //         res.json({success: true});
    //         }
    //     }catch(err){
    //         console.log(err)
    //     }
    // }
}