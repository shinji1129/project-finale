import * as Knex from "knex";




export async function up(knex: Knex): Promise<void> {
    async function createTable(
        tableName: string,
        cb: (table: Knex.CreateTableBuilder) => void
    ) {
        if (await knex.schema.hasTable(tableName)) {
            return;
        }
        await knex.schema.createTable(tableName, cb);
    }


    await createTable("users", (table) => {
        table.increments();
        table.string("username").notNullable();;
        table.string("password").nullable();
        table.string("self_intro");
        table.string("profile_pic");
        table.string("email").notNullable();
        table.boolean('freelancer').nullable();
        table.boolean('requester').nullable();
        table.timestamps(false, true);
    });

    await createTable("artworks", (t) => {
        t.increments();
        t.integer('users_id').unsigned();
        t.string('filename');
        t.string('original_name');
        t.string('description');
        t.string('extension');
        t.timestamps(false, true);
    })

    await createTable("requests", (table) => {
        table.increments();
        table.integer("requester_id").unsigned();
        table.integer("freelancer_id").unsigned().nullable();
        table.integer("status").notNullable();
        table.jsonb("images").nullable();
        table.string("company_name").nullable();
        table.string("details").notNullable();
        table.integer("budget_min").nullable();
        table.integer("budget_max").nullable();
        table.integer("assigned_budget").nullable();
        table.timestamp('time_start').nullable();
        table.integer('duration').notNullable();
        table.timestamp('expiry_date').notNullable();
        table.timestamps(false, true);

        table.foreign('requester_id').references('users.id').onDelete('CASCADE');
        table.foreign('freelancer_id').references('users.id').onDelete('CASCADE');
    });

    await createTable("tags", (t) => {
        t.increments();
        t.string('tag');
        t.timestamps(false, true);
    });

    await createTable("request_tag", (t) => {
        t.increments();
        t.integer('requests_id').unsigned();
        t.integer('tags_id').unsigned();
        t.timestamps(false, true)

        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
        t.foreign('tags_id').references('tags.id').onDelete('CASCADE');
    });

    await createTable("user_tag", (t) => {
        t.increments();
        t.integer('users_id').unsigned();
        t.integer('tags_id').unsigned();
        t.timestamps(false, true)

        t.foreign('users_id').references('users.id').onDelete('CASCADE');
        t.foreign('tags_id').references('tags.id').onDelete('CASCADE');
    });

    await createTable('chat_box', (t) => {
        t.increments();
        t.integer('room_id');
        t.integer('requests_id').unsigned();
        t.integer('user_one').unsigned();
        t.integer('user_two').unsigned();
        t.string('message');
        t.integer('status');
        t.timestamps(false, true);

        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
        t.foreign('user_one').references('users.id').onDelete('CASCADE');
        t.foreign('user_two').references('users.id').onDelete('CASCADE');
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("chat_box");
    await knex.schema.dropTableIfExists("request_tag");
    await knex.schema.dropTableIfExists("user_tag");
    await knex.schema.dropTableIfExists("tags");
    await knex.schema.dropTableIfExists("requests");
    await knex.schema.dropTableIfExists("artworks");
    await knex.schema.dropTableIfExists("users");
}

