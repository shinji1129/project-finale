import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests' , table => {
        table.string('time_start').alter();
        table.string('expiry_date').alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests' , table => {
        table.timestamp('time_start').alter();
        table.timestamp('expiry_date').alter();
    })
}

