import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('chat_box' , table => {
        // table.dropForeign(['requests_id'])
        table.dropColumn('requests_id')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('chat_box' , table => {
        // table.foreign(['requests_id'])
        table.integer('requests_id').unsigned();
        table.foreign('requests_id').references('requests.id').onDelete('CASCADE');
    })
}

