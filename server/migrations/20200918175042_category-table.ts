import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    async function createTable(
        tableName: string,
        cb: (table: Knex.CreateTableBuilder) => void
    ) {
        if (await knex.schema.hasTable(tableName)) {
            return;
        }
        await knex.schema.createTable(tableName, cb);
    }

    await createTable("categories", (t) => {
        t.increments();
        t.string('category');
    });

    await knex.schema.alterTable('requests' , t => {
        t.integer('category_id').unsigned()
        t.foreign('category_id').references('categories.id').onDelete('CASCADE');
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests' , t => {
        t.dropColumn('category_id')
    })

    await knex.schema.dropTable('categories');
}

