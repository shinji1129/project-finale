import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('request_application' , table => {
        table.integer('status').nullable().alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('request_application' , table => {
        table.boolean('status').nullable().alter();
    })
}

