import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests' , table => {
        table.dropColumn('images')
    })

    async function createTable(
        tableName: string,
        cb: (table: Knex.CreateTableBuilder) => void
    ) {
        if (await knex.schema.hasTable(tableName)) {
            return;
        }
        await knex.schema.createTable(tableName, cb);
    }

    await createTable("request_image", (t) => {
        t.increments();
        t.integer('request_id').unsigned();
        t.string('filename');
        t.string('original_name');
        t.string('extension');
        t.timestamps(false, true);
    });

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("request_image");

    await knex.schema.alterTable('requests' , table => {
        table.jsonb('images')
    })
}

