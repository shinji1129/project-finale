import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    async function createTable(
        tableName: string,
        cb: (table: Knex.CreateTableBuilder) => void
    ) {
        if (await knex.schema.hasTable(tableName)) {
            return;
        }
        await knex.schema.createTable(tableName, cb);
    }


    await createTable("videos", (t) => {
        t.increments();
        t.integer('users_id').unsigned();
        t.string('filename');
        t.string('original_name');
        t.string('description');
        t.string('extension');
        t.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("videos");

}

