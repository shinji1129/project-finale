import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('request_invitation', table => {
        table.integer('invite_budget');
    })

    await knex.schema.alterTable('request_application', table => {
        table.renameColumn('confirmed_budget', 'apply_budget')
    })

    await knex.schema.alterTable('requests', table=> {
        table.renameColumn('assigned_budget', 'confirmed_budget')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests', table=> {
        table.renameColumn('confirmed_budget', 'assigned_budget')
    })

    await knex.schema.alterTable('request_application', table => {
        table.renameColumn('apply_budget', 'confirmed_budget')
    })

    await knex.schema.alterTable('request_invitation', table => {
        table.dropColumn('invite_budget')
    })
}

