import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    async function createTable(
        tableName: string,
        cb: (table: Knex.CreateTableBuilder) => void
    ) {
        if (await knex.schema.hasTable(tableName)) {
            return;
        }
        await knex.schema.createTable(tableName, cb);
    }

    await createTable("request_invitation", (t)=>{
        t.increments();
        t.integer('requests_id').unsigned();
        t.integer('freelancer_id').unsigned();
        t.integer('status');
        t.timestamps(false,true)

        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
        t.foreign('freelancer_id').references('users.id').onDelete('CASCADE');
    });

    await createTable("request_application", (t)=>{
        t.increments();
        t.integer('requests_id').unsigned();
        t.integer('freelancer_id').unsigned();
        t.integer('confirmed_budget');
        t.integer('status');
        t.timestamps(false,true)

        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
        t.foreign('freelancer_id').references('users.id').onDelete('CASCADE');
    });

    await createTable("rating_on_freelancer", (t)=> {
        t.increments();
        t.integer('requests_id').unsigned();
        t.integer('requester_id').unsigned();
        t.integer('rating');
        t.timestamps(false,true);

        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
        t.foreign('requester_id').references('users.id').onDelete('CASCADE');
    })

    await createTable("rating_on_requester", (t)=> {
        t.increments();
        t.integer('requests_id').unsigned()
        t.integer('freelancer_id').unsigned();
        t.integer('rating');
        t.timestamps(false,true);

        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
        t.foreign('freelancer_id').references('users.id').onDelete('CASCADE');
    })

    await createTable("bookmark", (t)=> {
        t.increments();
        t.integer('users_id').unsigned();
        t.integer('requests_id').unsigned();
        t.timestamps(false,true);

        t.foreign('users_id').references('users.id').onDelete('CASCADE');
        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
    })

    await createTable("comment", (t)=> {
        t.increments();
        t.integer('users_id').unsigned();
        t.integer('requests_id').unsigned();
        t.string('comment');
        t.timestamps(false,true);

        t.foreign('users_id').references('users.id').onDelete('CASCADE');
        t.foreign('requests_id').references('requests.id').onDelete('CASCADE');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("comment");
    await knex.schema.dropTableIfExists("bookmark");
    await knex.schema.dropTableIfExists("rating_on_requester");
    await knex.schema.dropTableIfExists("rating_on_freelancer");
    await knex.schema.dropTableIfExists("request_application");
    await knex.schema.dropTableIfExists("request_invitation");
}

