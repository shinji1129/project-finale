import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests' , table => {
        table.dropUnique(['venue'])
        table.dropUnique(['title'])
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('requests' , table => {
        table.unique(['venue', 'title'])
        // table.unique()
    })
}

