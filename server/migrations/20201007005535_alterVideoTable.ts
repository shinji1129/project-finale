import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('videos' , table => {
        table.string('description',1000).alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('videos' , table => {
        table.string("description");
    })
}

