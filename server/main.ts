import express from 'express';
import bodyParser from 'body-parser';
import Knex from 'knex';
import SocketIO from 'socket.io';
import http from 'http'
import multer from 'multer';
import AWS from 'aws-sdk';
import multerS3 from 'multer-s3';
import cors from 'cors';
const knexConfig = require('./knexfile')
const knex = Knex(knexConfig['development'])

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//       cb(null, `${__dirname}/uploads`);
//     },
//     filename: function (req, file, cb) {
//       cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
//     }
//   })
// export const upload = multer({storage})

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'ap-southeast-1'
});


export const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'jobscreative',
        metadata: (req, file, cb) => {
            cb(null, { fieldName: file.fieldname });
        },
        key: (req, file, cb) => {
            cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        }
    })
})

const app = express();
const server = http.createServer(app);
const io = SocketIO.listen(server)

// interface Message{
//     user_one:number;
//     user_two:number;
//     message:string;
//     status: number;
//     room_id:number;
// }


// app.put('/messages/:id', (req,res)=>{
//     const message = messages.find(message => message.id === parseInt(req.params.id));
//     if(message == null){
//         res.status(400).json({result: "message_not_found"})
//         return;
//     }

//     io.to(`chatroom:${message.room_id}`).emit('vote_message', message);

//     res.json({success: true});
// })




app.use(cors({
    origin: [
        'http://localhost:3000'
    ]
}))

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

io.on('connection', socket =>{
    console.log('a user connected'),
    
    socket.on('join_chatroom', (roomId:number)=>{
        socket.join('chatroom:'+ roomId),
        console.log('join chatroom:'+ roomId)
    })

    socket.on('leave_chatroom', (roomId:number)=>{
        socket.leave('chatroom:'+ roomId),
        console.log('leave chatroom:'+ roomId)

    })
})



// app.get('/messages', (req,res)=>{
//     res.json(messages);
// })

// app.get('messages/:id',(req,res)=>{
//     res.json(messages.filter(message=>message.room_id === parseInt(req.query.roomId + "")))
// })

app.post('/api/v1/messages',async (req,res)=>{
    try{
        // const message = {
        //     user_one:req.body.userOne,
        //     user_two:req.body.userTwo,
        //     message: req.body.message,
        //     status: 0,
        //     room_id: parseInt(req.body.roomId)
        // };

        io.to(`chatroom:${req.body.roomId}`).emit('new_messages', { room_id: req.body.roomId ,user_one: req.body.userOne, user_two: req.body.userTwo, message: req.body.message, status: 1, created_at:Date.now() });

        // messages.push(message)
        await knex('chat_box').insert({ room_id: req.body.roomId ,user_one: req.body.userOne, user_two: req.body.userTwo, message: req.body.message, status: 1 }).returning('status');
        res.json({success: true});
        
    }catch(err){
        console.log(err)
    }
})



import { UserService } from './services/UserService';
import { UserController } from './controllers/UserControllers';

import { RequestService } from './services/RequestService';
import { RequestController } from './controllers/RequestControllers';


import { ProfileService } from './services/ProfileService';
import { ProfileController } from './controllers/ProfileControllers';

import { ProjectService } from './services/ProjectService'
import { ProjectController } from './controllers/ProjectControllers'

import { ChatRoomController } from './controllers/ChatRoomControllers';
import { ChatRoomService } from './services/ChatRoomService';

export const userService = new UserService(knex)
export const userController = new UserController(userService);

export const requestService = new RequestService(knex);
export const requestController = new RequestController(requestService)

export const profileService = new ProfileService(knex);
export const profileController = new ProfileController(profileService)

export const projectService = new ProjectService(knex);
export const projectController = new ProjectController(projectService)

export const chatRoomService = new ChatRoomService(knex);
export const chatroomController = new ChatRoomController(chatRoomService)


const API_VERSION = '/api/v1';

import { routes } from './routes'

app.use(API_VERSION, routes)


const PORT = 8080;


server.listen(PORT, () => {
    console.log(`[INFO] Port listening to: ${PORT}`);
});