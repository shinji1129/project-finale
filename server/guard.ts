import { Bearer } from 'permit';
import express from 'express';
import jwtSimple from "jwt-simple";
import jwt from './jwt';
import { IUser } from './services/models';
import { userService } from './main'

const permit = new Bearer({
    query: "access_token"
})

// middleware (check token)
export async function isLoggedIn(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) {
    try {
        const token = permit.check(req);
        if (!token) {
            return res.status(401).json({ message: "Unauthorized1" })
        }
        const payload = jwtSimple.decode(token, jwt.jwtSecret)

        const user: IUser | undefined= await userService.getUserById(payload.id)
        if (!user){
            return res.status(401).json({ message: "Unauthorized2"})
        }
        const {password, ...other} = user;
        req.user = { ...other };
        return next ();

    } catch (e) {
        console.log(e.message)
        return res.status(401).json({ msg: "Unauthorized3" })
    }
}