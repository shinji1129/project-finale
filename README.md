## Jobs Creatives


##Instructions:

Jobs Creatives is a freelancer jobs platform that aims to shorten the distance between freelancers and job publishers. In this day and age, artworks are needed everywhere. However, finding a suitable freelancer is not an easy task. On the other hand, it is not easy for freelancers to find a trustful job publishers. Via Jobs Creatives, both can be achieved by just fews of simple clicks.


## Technologies
Project is created with:
* react native version: 0.63.2


## Features
* Clear Workflow
* Comment and Rating
* Quick Search
* Real-time Chatroom
* Data-visualizing User Interface



